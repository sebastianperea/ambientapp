<?php
use Illuminate\Support\Facades\Input;
use App\cliente;
use App\sedes;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', array('http', function () {

    if(Auth::guest())
    {
    return view('auth.login');
    }

    else{

    
        $identificacion = auth()->user()->identificacion;

        if (auth()->user()->type == 'cliente' && auth()->user()->lastlogin !== '0000-00-00 00:00:00') {
             auth()->user()->lastlogin = new DateTime();
            auth()->user()->save();

           return route('cliente', $identificacion);
        }

        elseif(auth()->user()->type == 'cliente'){

             auth()->user()->lastlogin = new DateTime();
        auth()->user()->save();

            Flash::warning('Por favor cambie la contraseña en la etiqueta usuarios contraseña. Muchas gracias por confiar en nosotros');    
            return route('cliente.edit', $identificacion);

        }

        if (auth()->user()->type == 'comercial') {
            return redirect('/comercial');
             auth()->user()->lastlogin = new DateTime();
            auth()->user()->save();
        }

        if (auth()->user()->type == 'gerenciaadmin') {
            return redirect ('/geradmin');
             auth()->user()->lastlogin = new DateTime();
            auth()->user()->save();
        }

        $iduser = auth()->user()->id;
        if (auth()->user()->type == 'ejdecuenta') {
            auth()->user()->lastlogin = new DateTime();
            auth()->user()->save();
           return route('ejdecuenta', $iduser);
        }

        $iduser = auth()->user()->id;
        if (auth()->user()->type == 'facturacion') {
            auth()->user()->lastlogin = new DateTime();
            auth()->user()->save();
            return redirect('/facturacion');    
        }

        $iduser = auth()->user()->id;
        if (auth()->user()->type == 'posconsumos') {
            auth()->user()->lastlogin = new DateTime();
            auth()->user()->save();
            return redirect('/pos');    
        }
   
    }

}));



Route::resource('pos','Posconsumos\PosController', ['middleware' => ['auth', 'posconsumos'],'except' => ['show']]);

Route::group(['prefix' => 'posconsumos','middleware' => ['auth', 'posconsumos']], function () {

    Route::get('/lumina/{id}/pdf', 
    [ 'uses' => 'Posconsumos\LuminaController@certpdf',
    'as' => 'lumina.pdf.generate']);

    Route::get('/lumina/almc/{id}/pdf', 
    [ 'uses' => 'Posconsumos\LuminaController@certalmcpdf',
    'as' => 'lumina.pdf.almc.generate']);

    Route::get('/lumina/almc/{id}/pdf/down', 
    [ 'uses' => 'Posconsumos\LuminaController@certalmcpdfdown',
    'as' => 'lumina.pdf.almc.down']);

    Route::get('/lumina/almc/{id}/pdf/download', 
    [ 'uses' => 'Posconsumos\LuminaController@certalmcpdfdownload',
    'as' => 'lumina.pdf.almc.generate.download']);

Route::post('import', 'Posconsumos\GestoresController@import')->name('gestores.import');


Route::post('import', 'Posconsumos\GestoresController@import')->name('gestores.import');



Route::get('export', 'Posconsumos\GestoresController@export')->name('gestores.export');

    Route::resource('lumina','Posconsumos\LuminaController');
    Route::post('lumina/update', 
    [ 'uses' => 'Posconsumos\LuminaController@update',   
    'as' => 'lumina.update']);
    

    Route::resource('generadores','Posconsumos\GeneradoresController');

    Route::get('exportgen', 'Posconsumos\GeneradoresController@export')->name('generadores.export');

    Route::post('importgen', 'Posconsumos\GeneradoresController@import')->name('generadores.import');

    Route::resource('gestores','Posconsumos\GestoresController');

    Route::get('/autocompletegen',array('as'=>'autocompletegen','uses'=>'Posconsumos\LuminaController@autocompletegen'));

    Route::get('/autocompleteges',array('as'=>'autocompleteges','uses'=>'Posconsumos\LuminaController@autocompleteges'));

    Route::get('/autocompletecanal',array('as'=>'autocompletecanal','uses'=>'Posconsumos\LuminaController@autocompletecanal'));



    });

Route::resource('facturacion','Facturacion\FacturacionController', ['middleware' => ['auth', 'facturacion'],'except' => ['show']]);
Route::group(['prefix' => 'facturacion','middleware' => ['auth', 'facturacion']], function () {

    Route::resource('remisiones','Facturacion\RemisionController', ['middleware' => ['auth', 'facturacion']]);

    Route::get('/autocompleteser',array('as'=>'facturacion.autocompleteser','uses'=>'Facturacion\RemisionController@autocompleteser'));
    Route::get('/autocompleter',array('as'=>'facturacion.autocompleter','uses'=>'Facturacion\RemisionController@autocompleter'));

    Route::get('{id}/pdf', 
    [ 'uses' => 'Facturacion\RemisionController@certpdf',
    'as' => 'cert.pdf']);

    Route::get('certpeligrosos/{id}/{identificacion}/pdf', 
    [ 'uses' => 'Facturacion\RemisionController@certpeligrosospdf',
    'as' => 'facturacion.certpeligrosospdf.pdf']);

    Route::put('certpeligrosos/{id}/upload', 
    [ 'uses' => 'Facturacion\FilescertController@store',
    'as' => 'facturacion.certpeligrosos.up']);

});


// Rutas ejdecuenta
Route::group(['prefix' => 'ejdecuenta','middleware' => ['auth', 'ejdecuenta']], function () {


    
    Route::resource('visitasejdecuenta','VisitasEjdecuentaController',['except' => ['show']]);

    Route::get('/visitas/{idvisita}', 
    [ 'uses' => 'VisitasEjdecuentaController@view',
    'as' => 'ejdecuenta.visitas.view']);

    Route::get('/{iduser}', 
    [ 'uses' => 'ejdecuenta@index',
    'as' => 'ejdecuenta']);

     Route::resource('ofertasejdecuenta','OfertasEjdecuentaController',['except' => ['show']]);

    Route::get('ofertasejdecuenta/create/{iduser}', 
    [ 'uses' => 'OfertasEjdecuentaController@create',   
    'as' => 'ofertasejdecuenta.create']); 

    Route::get('ofertasejdecuenta/index/{iduser}', 
    [ 'uses' => 'OfertasEjdecuentaController@index',   
    'as' => 'ofertasejdecuenta.index']);

    Route::get('{id}/{iduser}/pdf', 
    [ 'uses' => 'PdfControllerEjdecuenta@ofertapcpdf',
    'as' => 'ofertasejdecuenta.pdf']); 

    Route::get('{id}/{iduser}/pdf/download', 
    [ 'uses' => 'PdfControllerEjdecuenta@ofertapcpdfdownload',
    'as' => 'ofertasejdecuenta.pdf.download']);
    
    Route::get('/{id}/mail', 
    [ 'uses' => 'PdfController@mail',
    'as' => 'ofertasejdecuenta.mail']);

    Route::get('clientes/view', 
    [ 'uses' => 'ClientesEjdecuentaController@index',   
    'as' => 'clientes.ejdecuenta.index']);

    Route::get('clientes/edit/{identificacion}', 
    [ 'uses' => 'ClientesEjdecuentaController@edit',   
    'as' => 'ejdecuenta.clientes.edit']);

    Route::put('clientes/update/{identificacion}', 
    [ 'uses' => 'ClientesEjdecuentaController@update',   
    'as' => 'ejdecuenta.clientes.update']);

    Route::get('clientes/show/{identificacion}', 
    [ 'uses' => 'ClientesEjdecuentaController@show',   
    'as' => 'ejdecuenta.clientes.show']);

    Route::put('/{identificacion}/files', 
    [ 'uses' => 'CreacionController@storecomercial',
    'as' => 'ejdecuenta.clientes.files.store']);

    Route::get('clientes/sedes/{sede}/eliminar', 
    [ 'uses' => 'ClientesEjdecuentaController@borrarsede',
    'as' => 'ejdecuenta.clientes.sedes.borrar']);

    Route::post('clientes/sedes/guardar', 
    [ 'uses' => 'ClientesEjdecuentaController@save',    
    'as' => 'ejdecuenta.clientes.sedes.save']);

    Route::get('clientes/sedes/{id}/{identificacion}/editar', 
    [ 'uses' => 'ClientesEjdecuentaController@editsede',    
    'as' => 'ejdecuenta.clientes.sedes.edit']);

    

    Route::put('clientes/sedes/update/{id}/{identificacion}', 
    [ 'uses' => 'ClientesEjdecuentaController@updatesede',
    'as' => 'ejdecuenta.clientes.sedes.update']);




      Route::get('uploadedFile/{filename}',['uses' => 'CreacionController@getFile',
    'as' => 'ejdecuenta.viewfiles']);

   Route::get('uploadedFile/{filename}/delete',['uses' => 'CreacionController@deleteFile',
    'as' => 'ejdecuenta.deletefiles']);

   Route::get('uploadedFile/{filename}/residuos',['uses' => 'ResiduosController@getFile',
    'as' => 'ejdecuenta.viewfilesresiduos']);

   Route::get('uploadedFile/{filename}/residuos/delete',['uses' => 'ResiduosController@deleteFile',
    'as' => 'ejdecuenta.deletefilesresiduos']);

   Route::get('clientes/creacion/{identificacion}', 
    [ 'uses' => 'ClientesEjdecuentaController@creacion',
    'as' => 'ejdecuenta.clientes.creacion']);
   Route::put('clientes/creacion/{identificacion}/actualizar', 
    [ 'uses' => 'ClientesEjdecuentaController@up',
    'as' => 'ejdecuenta.clientes.creacion.up']);
        // Rutas edición productos

       // rutas peligrosos

         Route::resource('peligrosos','Peligrososcontroller',['except' => ['show']]);
            Route::put('{id}/peligrosos', 
            [ 'uses' => 'OfertasEjdecuentaController@peligrosos',
            'as' => 'ejdecuenta.peligrosos']);


            Route::get('peligrosos/{idoferta}/{idpeligroso}/eliminar', 
            [ 'uses' => 'OfertasEjdecuentaController@borrarp',
            'as' => 'ejdecuenta.peligrosos.borrar']);

    
            // rutas aprovechables

            Route::resource('aprovechables','Aprovechablescontroller',['except' => ['show']]);
            Route::put('{id}/aprovechables', 
            [ 'uses' => 'OfertasEjdecuentaController@aprovechables',
            'as' => 'ejdecuenta.aprovechables']);
            

            Route::get('/{idoferta}/{idaprovechables}/aprovechables/eliminar', 
            [ 'uses' => 'OfertasEjdecuentaController@borrara',
            'as' => 'ejdecuenta.aprovechables.borrara']);


            // rutas posconsumos


           Route::resource('posconsumos','Posconsumoscontroller',['except' => ['show']]);
            Route::put('{id}/posconsumos', 
            [ 'uses' => 'OfertasEjdecuentaController@posconsumos',
            'as' => 'ejdecuenta.posconsumos']);
            

            Route::get('/{idoferta}/{idposconsumo}/posconsumos/eliminar', 
            [ 'uses' => 'OfertasEjdecuentaController@borrarpo',
            'as' => 'ejdecuenta.posconsumos.borrarpo']);


            // rutas peligrosos aprovechables


            Route::resource('peligrososa','Peligrososacontroller',['except' => ['show']]);
            Route::put('/{id}/peligrososa', 
            [ 'uses' => 'OfertasEjdecuentaController@peligrososa',
            'as' => 'ejdecuenta.peligrososa']);
            

            Route::get('/{idoferta}/{idpeligrososa}/peligrososa/eliminar', 
            [ 'uses' => 'OfertasEjdecuentaController@borrarpa',
            'as' => 'ejdecuenta.peligrosoa.borrarpa']);

            // rutas especiales

            Route::resource('especiales','Especialescontroller',['except' => ['show']]);
            Route::put('/{id}/especiales', 
            [ 'uses' => 'OfertasEjdecuentaController@especiales',
            'as' => 'ejdecuenta.especiales']);
            

            Route::get('/{idoferta}/{idespeciales}/especiales/eliminar', 
            [ 'uses' => 'OfertasEjdecuentaController@borrare',
            'as' => 'ejdecuenta.especiales.borrare']);

            // rutas otros
           
            Route::get('/{id}/otros/eliminar', 
            [ 'uses' => 'OfertasEjdecuentaController@bo',
            'as' => 'ejdecuenta.otros.borraro']);


            Route::resource('transportes','Transportecontroller',['except' => ['show']]);
           Route::put('{id}/transportes', 
            [ 'uses' => 'OfertasEjdecuentaController@transporte',
            'as' => 'ejdecuenta.transporte']);

           Route::get('/{idoferta}/{idtransporte}/eliminar', 
            [ 'uses' => 'OfertasEjdecuentaController@borrart',
            'as' => 'ejdecuenta.transporte.borrart']);

           Route::resource('pys','PysController',['except' => ['show']]);
           Route::put('ofertas/{id}/pys', 
            [ 'uses' => 'OfertasEjdecuentaController@pys',
            'as' => 'ejdecuenta.pys']);

           Route::get('/{idoferta}/{idpys}/pys/eliminar', 
            [ 'uses' => 'OfertasEjdecuentaController@borrarpys',
            'as' => 'ejdecuenta.pys.borrarpys']);



           
    });



    
 Route::get('clientes/Upload/{identificacion}', 
    [ 'uses' => 'ClienteController@upload',
    'as' => 'cliente.upload']);

 Route::put('clientes/Up/{identificacion}', 
    [ 'uses' => 'ClienteController@up',
    'as' => 'cliente.up']);

 Route::get('clientes/Up/success/{random}', 
    [ 'uses' => 'ClienteController@success',
    'as' => 'cliente.up.success']);




Route::group(['prefix' => 'clientes','middleware' => ['auth', 'clientes']], function () {

    

    
 Route::get('/{identificacion}', 
    [ 'uses' => 'ClienteController@index',
    'as' => 'cliente']);

 Route::get('/{identificacion}/edit', 
    [ 'uses' => 'ClienteController@edit',
    'as' => 'cliente.edit']);

 Route::get('{id}/{identificacion}/pdf', 
    [ 'uses' => 'PdfControllerclientes@ofertapcpdf',
    'as' => 'ofertas.clientes.pdf']);

 Route::get('{id}/{identificacion}/pdf/download', 
    [ 'uses' => 'PdfControllerclientes@ofertapcpdfdownload',
    'as' => 'ofertas.clientes.pdf.download']);

 Route::get('{identificacion}/ofertas', 
    [ 'uses' => 'OfertasControllerclientes@index',
    'as' => 'ofertas.clientes.index']);

 Route::get('{identificacion}/ofertas/create', 
    [ 'uses' => 'OfertasControllerclientes@create',
    'as' => 'ofertas.clientes.create']);

 Route::post('{identificacion}/ofertas/store', 
    [ 'uses' => 'OfertasControllerclientes@store',
    'as' => 'ofertas.clientes.store']);

 Route::get('{id}/{identificacion}/ofertas/aprove', 
    [ 'uses' => 'OfertasControllerclientes@aprove',
    'as' => 'ofertas.clientes.aprove']);

  Route::get('{id}/{identificacion}/ofertas/refused', 
    [ 'uses' => 'OfertasControllerclientes@refused',
    'as' => 'ofertas.clientes.refused']);

  Route::get('/{identificacion}/perfil', 
    [ 'uses' => 'ClienteController@perfil',
    'as' => 'clientes.perfil']);

  Route::put('/{identificacion}/perfil/update', 
    [ 'uses' => 'ClienteController@update',
    'as' => 'clientes.update.perfil']);

  Route::put('sedes/{identificacion}/guardar', 
    [ 'uses' => 'ClienteController@save',
    'as' => 'cliente.sedes.save']);

   Route::put('{identificacion}/usuario/update', 
    [ 'uses' => 'ClienteController@usuario',
    'as' => 'cliente.usuario.update']);

   Route::get('{identificacion}/recoleccion/solicitar', 
    [ 'uses' => 'ClienteController@solicitudrecoleccion',
    'as' => 'cliente.recoleccion.solicitud']);

});



// Rutas creacion de clientes

 Route::resource('creacion_de_clientes','CreacionController');
 Route::get('creacion_de_clientes/success/{random}', 
    [ 'uses' => 'CreacionController@success',
    'as' => 'creacion']);

Route::resource('inforesiduos','ResiduosController',['except' => ['show']]);
Route::get('inforesiduos/success/{random}', 
    [ 'uses' => 'ResiduosController@success',
    'as' => 'informacion']);



Route::resource('geradmin','GeradminController', ['middleware' => ['auth', 'gerenciaadministrativa'],'except' => ['show']]);


Route::group(['prefix' => 'geradmin','middleware' => ['auth', 'gerenciaadministrativa']], function () {

   Route::resource('users','UsersController');
   Route::get('users/{id}/destroy', 
    [ 'uses' => 'UsersController@destroy',
    'as' => 'ger.users.destroy']);


    Route::get('reportes',
    [ 'uses' => 'GeradminReportesController@index',
    'as' => 'ger.reportes.index']);

    Route::get('reportes/procesos',
    [ 'uses' => 'GeradminReportesController@show',
    'as' => 'ger.reportes.procesos']);


 Route::resource('ofertasg','Ofertasgcontroller',['except' => ['show']]);
    Route::get('{id}/destroy', 
    [ 'uses' => 'Ofertasgcontroller@destroy',
    'as' => 'comercial.ofertasg.destroy']);
    // Route::resource('clientesg','ClientesgController');





    // rutas PYS
   
 Route::resource('pys','PysController',['except' => ['show']]);
   Route::put('ofertas/{id}/pys', 
    [ 'uses' => 'Ofertasgcontroller@pys',
    'as' => 'comercialg.pys']);

   Route::get('/{idoferta}/{idpys}/eliminar', 
    [ 'uses' => 'Ofertasgcontroller@borrarpys',
    'as' => 'comercialg.pys.borrarpys']);

      // rutas transporte
   
 Route::resource('transportes','Transportecontroller',['except' => ['show']]);
   Route::put('/{id}/transportes', 
    [ 'uses' => 'Ofertasgcontroller@transporte',
    'as' => 'comercialg.transporte']);

   Route::get('/{idoferta}/{idtransporte}/eliminar', 
    [ 'uses' => 'Ofertasgcontroller@borrart',
    'as' => 'comercialg.transporte.borrart']);
   
   // Route::put('/{identificacion}/files', 
   //  [ 'uses' => 'CreaciongController@storecomercial',
   //  'as' => 'comercialg.files.storecomercial']);



    



    // rutas peligrosos

 Route::resource('peligrosos','Peligrososcontroller',['except' => ['show']]);
    Route::put('{id}/peligrosos', 
    [ 'uses' => 'Ofertasgcontroller@peligrosos',
    'as' => 'comercialg.peligrosos']);


    Route::get('peligrosos/{idoferta}/{idpeligroso}/eliminar', 
    [ 'uses' => 'Ofertasgcontroller@borrarp',
    'as' => 'comercialg.peligrosos.borrar']);

    



    // rutas aprovechables

    Route::resource('aprovechables','Aprovechablescontroller',['except' => ['show']]);
    Route::put('{id}/aprovechables', 
    [ 'uses' => 'Ofertasgcontroller@aprovechables',
    'as' => 'comercialg.aprovechables']);
    

    Route::get('/{idoferta}/{idaprovechables}/aprovechables/eliminar', 
    [ 'uses' => 'Ofertasgcontroller@borrara',
    'as' => 'comercialg.aprovechables.borrara']);

    


    // rutas posconsumos


   Route::resource('posconsumos','Posconsumoscontroller',['except' => ['show']]);
    Route::put('{id}/posconsumos', 
    [ 'uses' => 'Ofertasgcontroller@posconsumos',
    'as' => 'comercialg.posconsumos']);
    

    Route::get('/{idoferta}/{idposconsumo}/posconsumos/eliminar', 
    [ 'uses' => 'Ofertascontroller@borrarpo',
    'as' => 'comercialg.posconsumos.borrarpo']);


    // rutas peligrosos aprovechables


    Route::resource('peligrososa','Peligrososacontroller',['except' => ['show']]);
    Route::put('comercial/{id}/peligrososa', 
    [ 'uses' => 'Ofertasgcontroller@peligrososa',
    'as' => 'comercialg.peligrososa']);
    

    Route::get('/{idoferta}/{idpeligrososa}/peligrososa/eliminar', 
    [ 'uses' => 'Ofertasgcontroller@borrarpa',
    'as' => 'comercialg.peligrosoa.borrarpa']);



    
    // rutas especiales

    
    Route::resource('especiales','Especialescontroller',['except' => ['show']]);
    Route::put('/{id}/especiales', 
    [ 'uses' => 'Ofertasgcontroller@especiales',
    'as' => 'comercialg.especiales']);
    

    Route::get('/{idoferta}/{idespeciales}/especiales/eliminar', 
    [ 'uses' => 'Ofertasgcontroller@borrare',
    'as' => 'comercialg.especiales.borrare']);



    // rutas otros
   
    Route::get('/{id}/otros/eliminar', 
    [ 'uses' => 'Ofertasgcontroller@bo',
    'as' => 'otrosg.borraro']);


    Route::resource('transportes','Transportecontroller',['except' => ['show']]);
   Route::put('{id}/transportes', 
    [ 'uses' => 'Ofertasgcontroller@transporte',
    'as' => 'comercialg.transporte']);

   Route::get('/{idoferta}/{idtransporte}/eliminar', 
    [ 'uses' => 'Ofertasgcontroller@borrart',
    'as' => 'comercialg.transporte.borrart']);

    // Mail

    Route::get('/{id}/mail', 
    [ 'uses' => 'PdfControllerg@mail',
    'as' => 'ofertasg.mail']);


   Route::get('{id}/pdf', 
    [ 'uses' => 'PdfControllerg@ofertapcpdf',
    'as' => 'ofertasg.pdf']);

   Route::get('{id}/pdf/download', 
    [ 'uses' => 'PdfControllerg@ofertapcpdfdownload',
    'as' => 'ofertasg.pdf.download']);


});
 

Route::resource('comercial','ComercialController', ['middleware' => ['auth','comercial'],'except' => ['show']]);

    
    
    Route::get('/crearyenv/{identificacion}', 
    [ 'uses' => 'UsersController@crearyenviar',
    'as' => 'comercial.users.crearyenviar']);

 Route::resource('solicitud','SolicitudController');


   Route::group(['prefix' => 'comercial','middleware' => ['auth','comercial']], function () {


    Route::get('/edit/user/{identificacion}', 
    [ 'uses' => 'UsersController@editcomercial',
    'as' => 'comercial.users.edit']);

    Route::put('/edit/user/update/{identificacion}', 
    [ 'uses' => 'UsersController@updatecomercial',
    'as' => 'comercial.users.update']);

   Route::post('/seguimiento', 
    [ 'uses' => 'ComercialController@storeseg',
    'as' => 'comercial.storeseg']);

   Route::post('/seguimiento/detalle/{id}', 
    [ 'uses' => 'ComercialController@detalleseg',
    'as' => 'comercial.detalleseg']);

   
   Route::resource('ofertas','Ofertascontroller',['except' => ['show']]);
   Route::get('comercial/{id}/destroy', 
   	[ 'uses' => 'Ofertascontroller@destroy',
   	'as' => 'Comercial.ofertas.destroy']);

    // Rutas Clientes comercial

   Route::resource('clientes','ClientesController');

   Route::get('clientes/creacion/{identificacion}', 
    [ 'uses' => 'ClientesController@creacion',
    'as' => 'comercial.clientes.creacion']);
   Route::put('clientes/creacion/{identificacion}/actualizar', 
    [ 'uses' => 'ClientesController@up',
    'as' => 'comercial.clientes.creacion.up']);

   // Rutas comercial residuos


    Route::get('/residuos', 
    [ 'uses' => 'ResiduosController@index',
    'as' => 'comercial.residuos']);

    // Rutas dispositores

    Route::resource('dispositores','DispositoresController', ['except' => ['show']]);
   
   Route::get('/dispositores/{dispositores}/eliminar', 
    [ 'uses' => 'DispositoresController@destroy',
    'as' => 'dispositores.borrar']);

// rutas PYS
   
 Route::resource('pys','PysController',['except' => ['show']]);
   Route::put('ofertas/{id}/pys', 
    [ 'uses' => 'Ofertascontroller@pys',
    'as' => 'comercial.pys']);

   Route::get('/{idoferta}/{idpys}/eliminar', 
    [ 'uses' => 'Ofertascontroller@borrarpys',
    'as' => 'comercial.pys.borrarpys']);

      // rutas transporte
   
 Route::resource('transportes','Transportecontroller',['except' => ['show']]);
   Route::put('ofertas/{id}/transportes', 
    [ 'uses' => 'Ofertascontroller@transporte',
    'as' => 'comercial.transporte']);

   Route::get('/{idoferta}/{idtransporte}/transporte/eliminar', 
    [ 'uses' => 'Ofertascontroller@borrart',
    'as' => 'comercial.transporte.borrart']);
   
   Route::put('/{identificacion}/files', 
    [ 'uses' => 'CreacionController@storecomercial',
    'as' => 'comercial.files.storecomercial']);

   Route::resource('proccomercial','ProcComercialController',['except' => ['show']]);



    



    // rutas peligrosos

 Route::resource('peligrosos','Peligrososcontroller',['except' => ['show']]);
    Route::put('comercial/{id}/peligrosos', 
    [ 'uses' => 'Ofertascontroller@peligrosos',
    'as' => 'comercial.peligrosos']);


    Route::get('peligrosos/{idoferta}/{idpeligroso}/eliminar', 
    [ 'uses' => 'Ofertascontroller@borrarp',
    'as' => 'comercial.peligrosos.borrar']);

    



    // rutas aprovechables

    Route::resource('aprovechables','Aprovechablescontroller',['except' => ['show']]);
    Route::put('comercial/{id}/aprovechables', 
    [ 'uses' => 'Ofertascontroller@aprovechables',
    'as' => 'comercial.aprovechables']);
    

    Route::get('/{idoferta}/{idaprovechables}/aprovechables/eliminar', 
    [ 'uses' => 'Ofertascontroller@borrara',
    'as' => 'comercial.aprovechables.borrara']);

    


    // rutas posconsumos


   Route::resource('posconsumos','Posconsumoscontroller',['except' => ['show']]);
    Route::put('comercial/{id}/posconsumos', 
    [ 'uses' => 'Ofertascontroller@posconsumos',
    'as' => 'comercial.posconsumos']);
    

    Route::get('/{idoferta}/{idposconsumo}/posconsumos/eliminar', 
    [ 'uses' => 'Ofertascontroller@borrarpo',
    'as' => 'comercial.posconsumos.borrarpo']);


    // rutas peligrosos aprovechables


    Route::resource('peligrososa','Peligrososacontroller',['except' => ['show']]);
    Route::put('comercial/{id}/peligrososa', 
    [ 'uses' => 'Ofertascontroller@peligrososa',
    'as' => 'comercial.peligrososa']);
    

    Route::get('/{idoferta}/{idpeligrososa}/peligrososa/eliminar', 
    [ 'uses' => 'Ofertascontroller@borrarpa',
    'as' => 'comercial.peligrosoa.borrarpa']);



    
    // rutas especiales

    
    Route::resource('especiales','Especialescontroller',['except' => ['show']]);
    Route::put('/{id}/especiales', 
    [ 'uses' => 'Ofertascontroller@especiales',
    'as' => 'comercial.especiales']);
    

    Route::get('/{idoferta}/{idespeciales}/especiales/eliminar', 
    [ 'uses' => 'Ofertascontroller@borrare',
    'as' => 'comercial.especiales.borrare']);



    // rutas otros
   
    Route::get('/{id}/otros/eliminar', 
    [ 'uses' => 'Ofertascontroller@bo',
    'as' => 'otros.borraro']);

    // Mail

    Route::get('/{id}/mail', 
    [ 'uses' => 'PdfController@mail',
    'as' => 'ofertas.mail']);

   
Route::get('{cbancaria}', 
    [ 'uses' => 'Ofertascontroller@destroy',
    'as' => 'Comercial.ofertas.destroy']);


Route::get('clientes/sedes/{sede}/eliminar', 
    [ 'uses' => 'ClientesController@borrarsede',
    'as' => 'comercial.sedes.borrar']);
  
    

Route::post('clientes/sedes/save', 
    [ 'uses' => 'ClientesController@save',
    'as' => 'clientes.sedes.save']);

Route::get('clientes/sedes/edit/{id}/{identificacion}', 
    [ 'uses' => 'ClientesController@editsede',
    'as' => 'clientes.sedes.edit']);

Route::put('clientes/sedes/update/{id}/{identificacion}', 
    [ 'uses' => 'ClientesController@updatesede',
    'as' => 'clientes.sedes.update']);

// app/routes.php





    
Route::post('notification/get','NotificationController@get');
Route::post('notification/read','NotificationController@read');


});

// Route::group(['prefix' => 'comercial' ,'middleware' => 'auth'], function () {
    
//    Route::resource('clientes','ClientesController');
//    Route::get('clientes/{identificacion}/destroy', 
//       [ 'uses' => 'ClientesController@destroy',
//       'as' => 'comercial.clientes.destroy']);
// });


Route::get('ofertas/{id}/pdf', 
    [ 'uses' => 'PdfController@ofertapcpdf',
    'as' => 'ofertas.pdf'])->middleware(['auth','comercial']);

   Route::get('ofertas/{id}/pdf/download', 
    [ 'uses' => 'PdfController@ofertapcpdfdownload',
    'as' => 'ofertas.pdf.download'])->middleware(['auth','comercial']);

   Route::get('uploadedFile/{filename}',['uses' => 'CreacionController@getFile',
    'as' => 'viewfiles'])->middleware(['auth','comercial']);

   Route::get('uploadedFile/{filename}/delete',['uses' => 'CreacionController@deleteFile',
    'as' => 'deletefiles'])->middleware(['auth','comercial']);

   Route::get('uploadedFile/{filename}/residuos',['uses' => 'ResiduosController@getFile',
    'as' => 'viewfilesresiduos'])->middleware(['auth','comercial']);

   Route::get('uploadedFile/{filename}/residuos/delete',['uses' => 'ResiduosController@deleteFile',
    'as' => 'deletefilesresiduos'])->middleware(['auth','comercial']);

   Route::get('dropdown', function(){

    $identificacion = Input::get('option');
    $sedes = cliente::find($identificacion)->sedes;
    return $sedes->pluck('nombre', 'id');
})->middleware(['auth','comercial']);

   Route::get('dropdownej', function(){

        $identificacion = Input::get('option');
        $sedes = cliente::find($identificacion)->sedes;
        return $sedes->pluck('nombre', 'id');
    })->middleware(['auth','ejdecuenta']);

Auth::routes();





Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/home', 'HomeController@index');
Route::get('/autocomplete',array('as'=>'autocomplete','uses'=>'Ofertascontroller@autocomplete'));
Route::get('/autocompletea',array('as'=>'autocompletea','uses'=>'Ofertascontroller@autocompletea'));
Route::get('/autocompletepo',array('as'=>'autocompletepo','uses'=>'Ofertascontroller@autocompletepo'));
Route::get('/autocompletepa',array('as'=>'autocompletepa','uses'=>'Ofertascontroller@autocompletepa'));
Route::get('/autocompletee',array('as'=>'autocompletee','uses'=>'Ofertascontroller@autocompletee'));
Route::get('/autocompletec',array('as'=>'autocompletec','uses'=>'Ofertascontroller@autocompletec'));

Route::get('/autocompleteejdecuenta',array('as'=>'autocompleteejdecuenta','uses'=>'OfertasEjdecuentaController@autocompleteejdecuenta'));
Route::get('/autocompletet',array('as'=>'autocompletet','uses'=>'Ofertascontroller@autocompletet'));
Route::get('/autocompletepys',array('as'=>'autocompletepys','uses'=>'Ofertascontroller@autocompletepys'));







