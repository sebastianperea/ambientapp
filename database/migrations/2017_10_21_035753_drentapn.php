<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Drentapn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
      Schema::create('drentapn', function (Blueprint $table){

            $table->increments('id')->unsigned();
            $table->string('nombre');
            $table->BigInteger('cliente_id')->unsigned();
            $table->timestamps();
            $table->foreign('cliente_id')->references('identificacion')->on('clientes');
    });  //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       // Schema::drop('drentapn'); //
    }
}
