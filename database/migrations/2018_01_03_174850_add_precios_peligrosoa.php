<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPreciosPeligrosoa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('peligrososa', function ($table) {
       $table->string('preciou')->after('precio');
       $table->string('preciog')->after('preciou');
        });


        Schema::table('oferta_peligrososa', function ($table) {
       $table->string('preciou')->after('precio');
       $table->string('preciog')->after('preciou');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
