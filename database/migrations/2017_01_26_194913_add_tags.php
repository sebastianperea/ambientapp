<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
        });


        Schema::create('articulo_tag', function (Blueprint $table){

            $table->increments('id');
            $table->integer('articuloid')->unsigned();
            $table->integer('tagid')->unsigned();
            $table->foreign('articuloid')->references('id')->on('articulos');
            $table->foreign('tagid')->references('id')->on('tags');

        });
    
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::drop('tags');
    }
}
