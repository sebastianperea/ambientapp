<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTableSedes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('sedes', function (Blueprint $table){

            $table->string('responsable')->after('ciudad');
            $table->string('cargo')->after('responsable');
            $table->string('email')->after('responsable');
            $table->string('emailop')->after('email');
            $table->string('telefono')->after('emailop');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
