<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ofertas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofertas', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('identificacion')->unsigned();
            $table->enum('Estado',['En espera','Aprobado','Rechazado'])->default('En espera');
            $table->enum('estadoenv',['En Proceso','Enviado'])->default('En proceso');
            $table->dateTime('fecha');
            $table->date('fechaven');
            $table->dateTime('fechaenv');
            $table->timestamps();
            $table->foreign('identificacion')->references('identificacion')->on('clientes');

});
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    // Schema::drop('ofertas'); 
       //
    }
}