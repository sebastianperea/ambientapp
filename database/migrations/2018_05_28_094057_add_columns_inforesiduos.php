<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInforesiduos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inforesiduos', function (Blueprint $table){

            $table->biginteger('identificacion')->unsigned()->after('inforesiduos');
            $table->biginteger('declaracionta')->after('identificacion');
            $table->enum('habilitado_recolecc',['Habilitado','No habilitado'])->default('No habilitado')->after('inforesiduos');

            $table->foreign('identificacion')->references('identificacion')->on('clientes');
     });
    }

    /**
     * 
     */
   
    public function down()
    {
        //
    }
}
