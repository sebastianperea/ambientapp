<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Transporte extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
       
       Schema::create('transporte', function (Blueprint $table){

            $table->increments('id');
            $table->string('tipo');
            $table->integer('preciokg');
            $table->integer('preciog');
            $table->timestamps();

        }); 


        Schema::create('oferta_transporte', function (Blueprint $table){

            $table->increments('id');
            $table->integer('oferta_id')->unsigned();
            $table->integer('transporte_id')->unsigned();
            $table->integer('preciokg');
            $table->integer('preciog');
            

            

            $table->foreign('oferta_id')->references('id')->on('ofertas');
            $table->foreign('transporte_id')->references('id')->on('transporte');    

        });   
    
        
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      
    // Schema::drop('transporte');

        //
    }
}
