<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {



       
       Schema::create('clientes', function (Blueprint $table) {
            
            
            $table->primary('identificacion')->unsigned();
            $table->bigInteger('identificacion')->unsigned();
            $table->enum('tipo_de_identificacion',['NIT','CC','PAS','CE'])->default('NIT');
            $table->integer('dv');
            $table->string('razon_social');
            $table->string('departamento');
            $table->string('ciudad');
            $table->string('direccion');
            $table->enum('tipo_de_ide',['CC','PAS','CE']);
            $table->integer('ide');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('telefonocelular');
            $table->string('email');
            $table->integer('telefono_contacto');
            $table->integer('ext');
            $table->string('cargo');
            $table->string('nombrecartera');
            $table->integer('telefonofijocartera');
            $table->integer('telefonomovilcartera');
            $table->string('emailcartera');
            $table->string('nombreempresauno');
            $table->integer('telefonoempresauno');
            $table->string('nombreempresados');
            $table->integer('telefonoempresados');
            $table->string('nombrebanco');
            $table->integer('numerodecuenta');
            $table->string('tipodecuenta');
            $table->string('terminos');
            $table->string('politicadeprivacidad');
            $table->string('procesos');
            $table->enum('tipo',['Posible','Cliente'])->default('Posible');
            $table->enum('estado',['Activo','Inactivo'])->default('Activo');
            $table->string('tipodepersona');
            $table->string('credito');
            
            $table->timestamps();

            }); //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
       // Schema::drop('clientes'); //
    }
}