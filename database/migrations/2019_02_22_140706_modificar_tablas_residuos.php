<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModificarTablasResiduos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('peligrosos', function (Blueprint $table) {
            
            $table->integer('dispositor')->unsigned();
            $table->foreign('dispositor')->references('id')->on('dispositores');
        });

        


         Schema::table('aprovechables', function (Blueprint $table) {
            
             $table->integer('dispositor')->unsigned();

             $table->foreign('dispositor')->references('id')->on('dispositores');
         });

         Schema::table('peligrososa', function (Blueprint $table) {
            
             $table->integer('dispositor')->unsigned();

             $table->foreign('dispositor')->references('id')->on('dispositores');
         });


         Schema::table('especiales', function (Blueprint $table) {
            
             $table->renameColumn('proveedor', 'dispositor')->unsigned()->change();
         });

         Schema::table('especiales', function (Blueprint $table) {
            
             $table->integer('dispositor')->unsigned()->change();

             $table->foreign('dispositor')->references('id')->on('dispositores');
         });

         
 
    }




    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
