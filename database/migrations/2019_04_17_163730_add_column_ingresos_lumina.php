<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIngresosLumina extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('ingresos_lumina', function (Blueprint $table){

            
               $table->enum('estado_ingreso',['APROVECHADO', 'POR APROVECHAR'])->default('POR APROVECHAR')->after('canal');

               $table->enum('estado_reporte',['REPORTADO', 'NO REPORTADO'])->default('NO REPORTADO')->after('canal');

               $table->enum('estado',['CERRADO', 'ABIERTO'])->default('ABIERTO')->after('canal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
