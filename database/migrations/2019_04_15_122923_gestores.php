<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Gestores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gestores', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('razonsocial')->unique();
            $table->string('direccion');
            $table->string('telefono');
            $table->string('identificacion');
            $table->string('dv');
            $table->string('contacto_gestor');
            $table->string('correo');
            $table->string('ciudad');
            $table->string('departamento');
            $table->string('centro_de_acopio');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
