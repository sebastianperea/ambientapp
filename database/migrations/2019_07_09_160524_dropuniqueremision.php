<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dropuniqueremision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('ingresos_lumina', function(Blueprint $table)
        {
            $table->dropcolumn('remision');

        });

         Schema::table('ingresos_lumina', function(Blueprint $table)
        {
            $table->string('remision')->after('prefijo2');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
