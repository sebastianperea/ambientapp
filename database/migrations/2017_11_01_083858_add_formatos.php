<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFormatos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('cc', function (Blueprint $table){

            $table->increments('id');
            $table->string('nombre');
            $table->BigInteger('identificacion')->unsigned();
            $table->timestamps();
            $table->foreign('identificacion')->references('identificacion')->on('clientes');
     });

         Schema::create('info_residuos', function (Blueprint $table){

            $table->increments('id');
            $table->string('nombre');
            $table->BigInteger('identificacion')->unsigned();
            $table->timestamps();
            $table->foreign('identificacion')->references('identificacion')->on('clientes');
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
