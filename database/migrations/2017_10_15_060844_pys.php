<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pys', function (Blueprint $table){

            $table->increments('id')->unsigned();
            $table->string('nombre');
            $table->integer('precio');
            $table->timestamps();//
    });

  Schema::create('oferta_pys', function (Blueprint $table){

            $table->increments('id');
            $table->integer('oferta_id')->unsigned();
            $table->integer('pys_id')->unsigned();
            $table->integer('precio');
            

            

            $table->foreign('oferta_id')->references('id')->on('ofertas');
            $table->foreign('pys_id')->references('id')->on('pys');    

        });

}


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}