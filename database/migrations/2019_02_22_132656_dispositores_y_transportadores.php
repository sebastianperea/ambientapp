<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DispositoresYTransportadores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('dispositores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->BigInteger('identificacion')->unique();
            $table->integer('dv')->unsigned();
            $table->text('categoria')->nullable();
            
            $table->timestamps();
        });

       Schema::create('transportadores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->BigInteger('identificacion');
            $table->integer('dv')->unsigned();
            
            $table->timestamps();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
