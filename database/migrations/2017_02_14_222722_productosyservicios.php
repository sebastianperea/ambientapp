<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Productosyservicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
       Schema::create('productosyservicios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Nombre');
            $table->integer('Precio');
            $table->integer('Numerodeclaracion');
            $table->string('Tratamiento');
            $table->string('Hoja_de_seguridad');
            $table->enum('Estado',['En espera','Aprobado','Rechazado'])->default('En espera');
            $table->timestamps();
}); 



         //
    }

 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        // Schema::drop('productosyservicios'); //
    }
}
