<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Aprovechables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aprovechables', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('nombre');
            $table->integer('precio');
            $table->timestamps();

        });//
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
       // Schema::drop('aprovechables'); //
    }
}
