<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remision', function (Blueprint $table) {
            $table->increments('id');
            $table->biginteger('numero')->unsigned()->unique();
            $table->biginteger('identificacion')->unsigned();
            $table->date('fechaserv');
            $table->timestamps();
            $table->foreign('identificacion')->references('identificacion')->on('clientes');
            });

         Schema::create('peligroso_remision', function (Blueprint $table){

            $table->increments('id');
            $table->integer('peligroso_id')->unsigned();
            $table->integer('remision_id')->unsigned();
            $table->biginteger('cantidad');
            $table->foreign('remision_id')->references('id')->on('remision');
            $table->foreign('peligroso_id')->references('id')->on('peligrosos'); 


        });

         Schema::create('aprovechables_remision', function (Blueprint $table){

            $table->increments('id');
            $table->integer('aprovechables_id')->unsigned();
            $table->integer('remision_id')->unsigned();
            $table->biginteger('cantidadu')->unsigned();
            $table->biginteger('cantidada')->unsigned();
            $table->foreign('remision_id')->references('id')->on('remision');
            $table->foreign('aprovechables_id')->references('id')->on('aprovechables'); 


        });

         Schema::create('posconsumos_remision', function (Blueprint $table){

            $table->increments('id');
            $table->integer('posconsumos_id')->unsigned();
            $table->integer('remision_id')->unsigned();
            $table->integer('cantidad')->unsigned();
            $table->foreign('remision_id')->references('id')->on('remision');
            $table->foreign('posconsumos_id')->references('id')->on('posconsumos'); 
        });

         Schema::create('peligrososa_remision', function (Blueprint $table){

            $table->increments('id');
            $table->integer('peligrososa_id')->unsigned();
            $table->integer('remision_id')->unsigned();
            $table->biginteger('cantidad')->unsigned();
            $table->biginteger('cantidadpag')->unsigned();
            $table->biginteger('cantidadpau')->unsigned();
            $table->foreign('remision_id')->references('id')->on('remision');
            $table->foreign('peligrososa_id')->references('id')->on('peligrososa'); 
        });


         Schema::create('especiales_remision', function (Blueprint $table){

            $table->increments('id');
            $table->integer('especiales_id')->unsigned();
            $table->integer('remision_id')->unsigned();
            $table->integer('cantidad')->unsigned();
            $table->foreign('remision_id')->references('id')->on('remision');
            $table->foreign('especiales_id')->references('id')->on('especiales'); 
        });

         Schema::create('remision_transporte', function (Blueprint $table){

            $table->increments('id');
            $table->integer('transporte_id')->unsigned();
            $table->integer('remision_id')->unsigned();
            $table->integer('cantidadt')->unsigned();
            $table->foreign('remision_id')->references('id')->on('remision');
            $table->foreign('transporte_id')->references('id')->on('transporte'); 
        });

         Schema::create('pys_remision', function (Blueprint $table){
            
            $table->increments('id');
            $table->integer('pys_id')->unsigned();
            $table->integer('remision_id')->unsigned();
            $table->integer('cantidad')->unsigned();
            $table->foreign('remision_id')->references('id')->on('remision');
            $table->foreign('pys_id')->references('id')->on('pys'); 
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      
    }
}
