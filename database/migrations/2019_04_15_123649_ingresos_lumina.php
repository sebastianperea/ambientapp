<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IngresosLumina extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingresos_lumina', function (Blueprint $table){

            $table->increments('id')->unsigned();

            $table->string('prefijo');
            $table->string('prefijo2');

             $table->string('remision')->unique();

              $table->string('fecha_recepcion');
               
               $table->integer('generador')->unsigned();
               
               $table->integer('gestor')->unsigned();

               
               $table->integer('canal')->unsigned();
               $table->string('fecha_aprovechamiento');
            $table->timestamps();
            $table->foreign('generador')->references('id')->on('generadores');
            
            $table->foreign('gestor')->references('id')->on('gestores');

            $table->foreign('canal')->references('id')->on('canales');
        });


        Schema::create('ingresos_lumina_tipos_tecnologia', function (Blueprint $table){

            $table->increments('id')->unsigned();

            $table->integer('ingresos_lumina_id')->unsigned();
            $table->integer('tipos_tecnologia_id')->unsigned();
            $table->string('pesorkg');
            $table->string('pesodfkg');

;
            
            $table->foreign('ingresos_lumina_id')->references('id')->on('ingresos_lumina');
            $table->foreign('tipos_tecnologia_id')->references('id')->on('tipos_de_tecnologia');

             $table->timestamps();
        });





    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
