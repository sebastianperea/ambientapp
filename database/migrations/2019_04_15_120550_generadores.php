<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Generadores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generadores', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('razon_social');
            $table->string('direccion');
            $table->string('telefono');
            $table->string('responsable_envio');
            $table->string('correo_electronico');

            $table->string('ciudad');
            $table->string('departamento');
            $table->BigInteger('identificacion');
            $table->integer('dv');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
