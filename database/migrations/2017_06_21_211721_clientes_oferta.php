<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientesOferta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
       Schema::create('oferta_peligroso', function (Blueprint $table){

            $table->increments('id');
            $table->integer('peligroso_id')->unsigned();
            $table->integer('oferta_id')->unsigned();
            $table->integer('cantidad')->unsigned();
            $table->integer('precio')->unsigned();
            

            

            $table->foreign('oferta_id')->references('id')->on('ofertas');
            $table->foreign('peligroso_id')->references('id')->on('peligrosos');    

        });   
    
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}