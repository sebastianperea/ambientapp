<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Inforesiduos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('residuos', function (Blueprint $table){

            $table->increments('id');
            $table->BigInteger('identificacion')->unsigned();
            $table->timestamps();
            $table->foreign('identificacion')->references('identificacion')->on('clientes');
     });

        Schema::create('sedes', function (Blueprint $table){

            $table->increments('id');
            $table->BigInteger('identificacion')->unsigned();
            $table->string('nombre');
            $table->string('direccion');
            $table->string('departamento');
            $table->string('ciudad');
            $table->timestamps();
            $table->foreign('identificacion')->references('identificacion')->on('clientes');
     });

     Schema::create('inforesiduos', function (Blueprint $table){
            $table->increments('id');
            $table->string('nombreresiduo');
            $table->string('tipo');
            $table->string('detalle');
            $table->string('estado');
            $table->string('proceso');
            $table->string('contaminacion');
            $table->string('empaque');
            $table->string('cantidad');
            $table->integer('inforesiduos')->unsigned();
            $table->timestamps();
            $table->foreign('inforesiduos')->references('id')->on('residuos');
     }); 

     Schema::create('filespeligrosos', function (Blueprint $table){
            $table->increments('id')->unsigned();
            $table->string('nombre');
            $table->timestamps();

     }); 


      Schema::create('filespeligrososa', function (Blueprint $table){
            $table->increments('id')->unsigned();
            $table->string('nombre');
            $table->timestamps();
     }); 

      Schema::create('filese', function (Blueprint $table){
            $table->increments('id')->unsigned();
            $table->string('nombre');
            $table->timestamps();
     }); 


      Schema::create('filespeligrosos_residuos', function (Blueprint $table){
            $table->increments('id')->unsigned();
            $table->integer('filespeligrosos_id')->unsigned();
            $table->integer('residuos_id')->unsigned();
            $table->timestamps();
            $table->foreign('residuos_id')->references('id')->on('residuos');
            $table->foreign('filespeligrosos_id')->references('id')->on('filespeligrosos');
     }); 



      Schema::create('filespeligrososa_residuos', function (Blueprint $table){
            $table->increments('id')->unsigned();
            $table->integer('filespeligrososa_id')->unsigned();
            $table->integer('residuos_id')->unsigned();
            $table->timestamps();
            $table->foreign('residuos_id')->references('id')->on('residuos');
            $table->foreign('filespeligrososa_id')->references('id')->on('filespeligrososa');
     }); 


      Schema::create('filese_residuos', function (Blueprint $table){
            $table->increments('id')->unsigned();
            $table->integer('filese_id')->unsigned();
            $table->integer('residuos_id')->unsigned();
            $table->timestamps();
            $table->foreign('residuos_id')->references('id')->on('residuos');
            $table->foreign('filese_id')->references('id')->on('filese');
     }); 


     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inforesiduos');
    }
}
