<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Createvisitas3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('visitas', function (Blueprint $table){

            $table->increments('id')->unsigned();
            $table->enum('tipo_de_cliente',['ACTUAL','NUEVO'])->nullable();
            $table->enum('tipo_de_visita',['Acercamiento comercial','Proceso de creación','Seguimiento','Requiere nuevo seguimiento', 'PQR']);
            $table->enum('canald',['PRESENCIAL', 'PAG WEB', 'EMAIL ENVIADO', 'EMAIL RECIBIDO', 'LLAMADA ENTRANTE', 'LLAMADA SALIENTE'])->nullable();
            $table->string('lugar');
            $table->bigInteger('identificacion')->unsigned();
            $table->string('observaciones');
            $table->date('fecha_nuevo_contacto')->nullable();
            $table->timestamps();
            $table->foreign('identificacion')->references('identificacion')->on('clientes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
