<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsGestoresGeneradores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gestores', function ($table) {
       $table->string('telefono2op')->after('telefono');

       $table->string('correo2op')->after('correo')->nullable();
        });

        Schema::table('generadores', function ($table) {
       $table->string('telefono2')->after('telefono')->nullable();

       $table->string('correo2')->after('correo_electronico')->nullable();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
