<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fotocopiacpn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('fotocopiacpn', function (Blueprint $table){

            $table->increments('id');
            $table->string('nombre');
            $table->bigInteger('cliente_id')->unsigned();
            $table->timestamps();
            $table->foreign('cliente_id')->references('identificacion')->on('clientes');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::drop('fotocopiacpn');//
    }
}
