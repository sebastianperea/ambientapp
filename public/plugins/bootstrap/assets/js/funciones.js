$(document).ready(function () {

  
  
 
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        if ( $("#nombre").val()=='' || $("#apellidos").val()=='' || $("#cargo").val()=='' || $("#telefonocelular").val()=='' || $("#telefonocelular").val()=='' || $("#email").val()=='' || $("#identificacion").val()=='' ||  $("#razonsocial").val()=='' || $("#nidentificacion").val()=='' || $("#nidentificacion2").val()=='' || $("#departamento").val()=='' || $("#direccion").val()=='' || $("#tipo").val()=='SELECCIONE')   {
            swal("¡Error!", 
        "Hace falta por llenar algún campo.", 
        "error");
        
          }

          else if($("#nidentificacion").val() != $("#nidentificacion2").val() ) { 

            swal("¡Error!", 
        "El numero de identificación no coincide", 
        "error");


          }

        else{

          var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
              nextTab($active);
           }
        
    }); 

    




    $(".prev-step").click(function (e) {

     

        var $active = $('.wizard .nav-tabs li.active');

        prevTab($active);

    });

});

 function nextTab(elem) {
  
  
    $(elem).next().find('a[data-toggle="tab"]').click();
  
}

function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}



//according menu

$(document).ready(function()
{
    //Add Inactive Class To All Accordion Headers
    $('.accordion-header').toggleClass('inactive-header');
	
	//Set The Accordion Content Width
	var contentwidth = $('.accordion-header').width();
	$('.accordion-content').css({});
	
	//Open The First Accordion Section When Page Loads
	$('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
	$('.accordion-content').first().slideDown().toggleClass('open-content');
	
	// The Accordion Effect
	$('.accordion-header').click(function () {
		if($(this).is('.inactive-header')) {
			$('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content');
		}
		
		else {
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content');
		}
	});
	
	return false;
});

 // select all desired input fields and attach tooltips to them
      $("#formtip :input").tooltip({
 
      // place tooltip on the right edge
      position: "center right",
 
      // a little tweaking of the position
      offset: [-2, 10],
 
      // use the built-in fadeIn/fadeOut effect
      effect: "fade",
 
      // custom opacity setting
      opacity: 0.7,

      


 
      });

      $(document).ready(function() {
        $(".cono").click(function(event){
        var valor = $(event.target).val();
        if(valor =="si"){

            $("#step3").show();
            $("#3").show();
            $("#boton").val('Siguiente').attr({type:'button'});
            $("#step2").addClass('tab-pane');
                var nombre = $("#itNombre").val();
            
            
        } else if (valor == "no") {
          $("#boton").removeClass('next-step');
          $("#step2").removeClass('tab-pane');
          $("#3").hide();
            $("#step3").hide();
            $("#boton").val('Enviar').attr({type:'submit'});
            $('.wizard .nav-tabs > li').css('width', '40%');
            
            $(".prev-step").click(function (e) {
            $('#step2').addClass('tab-pane', 'next-step');
    });


                   
        } else { 

            $("#boton").val('Siguiente').attr({type:'button'});
            $("#boton").attr('disabled',true);
            $("#procesos").css("border-color", "#FF0000");
        }
    });
});



$("#nombre").focusout(function(){
        if($(this).val()==''){

            $(this).css("border-color", "#FF0000");
              
               $("#error_nombre").text("* Nombres Incorrectos");
          }
          else
          {
            $(this).css("border-color", "#2eb82e");
            
            $("#error_nombre").text("");


          }
       });

$("#apellidos").focusout(function(){
        if($(this).val()==''){
            $(this).css("border-color", "#FF0000");
              
               $("#error_apellidos").text("* Apellidos Incorrectos");
          }

          else
          {
            $(this).css("border-color", "#2eb82e");
            
            $("#error_apellidos").text("");

          }
       });


$("#procesos").focusout(function(){
        if($(this).val()==''){
            $(this).css("border-color", "#FF0000");
              $('#boton').attr('disabled',true);
               $("#procesos").text("");
          }

          else
          {
            $(this).css("border-color", "#2eb82e");
            $('#boton').attr('disabled',false);
            $("#procesos").text("");
          }
       });


$("#telefonocelular").focusout(function(){
            $tel =$("#telefonocelular").val();
        if($(this).val()==''){
            $(this).css("border-color", "#FF0000");
              
              $("#error_telefonocelular").text("* You have to enter your Phone Number!");
          }
          else if ($tel.length!=10)
          {   
                    $(this).css("border-color", "#FF0000");
              
              $("#error_telefonocelular").text("* Lenght of Phone Number Should Be Ten");
          }
          
          else{
            $(this).css({"border-color":"#2eb82e"});
           
            $("#error_telefonocelular").text("");
          }

      });

$("#email").focusout(function(){

      if($(this).val()==''){
            $(this).css("border-color", "#FF0000");
            
              $("#error_email").text("* Email incorrecto");
          }
          
          else if ($("#email").val().indexOf('@', 0) == -1 || $("#email").val().indexOf('.', 0) == -1) {

            $(this).css("border-color", "#FF0000");
              
              $("#error_email").text("* Email incorrecto");
            return false;
          }
          
          else{
            $(this).css({"border-color":"#2eb82e"});
            
            $("#error_email").text("");

          }

      });

$("#identificacion").focusout(function(){
        if($(this).val()==''){
            $(this).css("border-color", "#FF0000");
              
               $("#error_identificacion").text("* Identificación incorrecta");
          }

          else
          { 
            $(this).css("border-color", "#2eb82e");
            
            $("#error_identificacion").text("");


          }
       });

$("#razonsocial").focusout(function(){
        if($(this).val()==''){
            $(this).css("border-color", "#FF0000");
              
               $("#error_razonsocial").text("* Razón Social Incorrecta");
          }

          else
          { 
            $(this).css("border-color", "#2eb82e");
            
            $("#error_razonsocial").text("");

          }
       });

$("#cargo").focusout(function(){
        if($(this).val()==''){
            $(this).css("border-color", "#FF0000");
              
               $("#error_cargo").text("* El campo esta vacío");
          }

          else
          { 
            $(this).css("border-color", "#2eb82e");
            
            $("#error_cargo").text("");

          }
       });



$("#nidentificacion").focusout(function(){
        if($(this).val()==''){
            $(this).css("border-color", "#FF0000");
              
               $("#error_nidentificacion").text("* Identificación incorrecta");
          }

          else
          { 
            $(this).css("border-color", "#2eb82e");
            
            $("#error_nidentificacion").text("");

          }
       });

$("#dv").focusout(function(){
        if($(this).val()==''){

            $(this).css("border-color", "#FF0000");
              
               $("#error_dv").text("* DV incorrecto");
          }
          else
          {
            $(this).css("border-color", "#2eb82e");
            
            $("#error_dv").text("");


          }
       });

$("#departamento").focusout(function(){
        if($(this).val()==''){

            $(this).css("border-color", "#FF0000");
              
               $("#error_departamento").text("* Departamento incorrecto");
          }
          else
          {
            $(this).css("border-color", "#2eb82e");
            
            $("#error_departamento").text("");


          }
       });

$("#ciudad").focusout(function(){
        if($(this).val()==''){

            $(this).css("border-color", "#FF0000");
              
               $("#error_ciudad").text("* Ciudad Incorrecta");
          }
          else
          {
            $(this).css("border-color", "#2eb82e");
           
            $("#error_ciudad").text("");


          }
       });

$("#direccion").focusout(function(){
        if($(this).val()==''){

            $(this).css("border-color", "#FF0000");
              
               $("#error_direccion").text("* Dirección Incorrecta");
          }
          else
          {
            $(this).css("border-color", "#2eb82e");
            
            $("#error_direccion").text("");


          }
       });

jQuery(function($){ 
  var clone = $("#tabla tbody tr:eq(0)").clone(true);
  $('body').on('click', '#agregar', function() {
    var ParentRow = $("#tabla tbody tr").last();
    clone.clone(true).insertAfter(ParentRow);
    $('tr.fila select').chosen({

     no_results_text: "Su busqueda no arroja resultados",
    
 placeholder_text:"Seleccione",
 width:"80%"
    });
  });
   $(document).on("click",".eliminar",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

});
});


jQuery(function($){ 
  var clone = $("#tablaap tbody tr:eq(0)").clone(true);
  $('body').on('click', '#agregarap', function() {
    var ParentRow = $("#tablaap tbody tr").last();
    clone.clone(true).insertAfter(ParentRow);
    $('tr.filaap select').chosen({

     no_results_text: "Su busqueda no arroja resultados",
    
 placeholder_text:"Seleccione",
 width:"80%"
    });
  });
   $(document).on("click",".eliminarap",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

});
});



jQuery(function($){ 
  var clone = $("#tablapc tbody tr:eq(0)").clone(true);
  $('body').on('click', '#agregarpc', function() {
    var ParentRow = $("#tablapc tbody tr").last();
    clone.clone(true).insertAfter(ParentRow);
    $('tr.filapc select').chosen({

     no_results_text: "Su busqueda no arroja resultados",
    
 placeholder_text:"Seleccione",
 width:"80%"
    });
  });
   $(document).on("click",".eliminarpc",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

});
});

jQuery(function($){ 
  var clone = $("#tablapa tbody tr:eq(0)").clone(true);
  $('body').on('click', '#agregarpa', function() {
    var ParentRow = $("#tablapa tbody tr").last();
    clone.clone(true).insertAfter(ParentRow);
    $('tr.filapa select').chosen({

     no_results_text: "Su busqueda no arroja resultados",
    
 placeholder_text:"Seleccione",
 width:"80%"
    });
  });
   $(document).on("click",".eliminarpa",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

});
});

jQuery(function($){ 
  var clone = $("#tablae tbody tr:eq(0)").clone(true);
  $('body').on('click', '#agregare', function() {
    var ParentRow = $("#tablae tbody tr").last();
    clone.clone(true).insertAfter(ParentRow);
    $('tr.filae select').chosen({

     no_results_text: "Su busqueda no arroja resultados",
    
 placeholder_text:"Seleccione",
 width:"80%"
    });
  });
   $(document).on("click",".eliminare",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

});
});



$(".email").keyup(function(){
    
  
  if($("#email").val() == $("#email2").val()){
    $("#error_email2").text("");
    $(this).css("border-color", "#2eb82e");



    
    
  }else{
    
    $(this).css("border-color", "#FF0000");
     $("#error_email2").text("* Los numeros de identificación no coinciden");
  }
});
    
        



/*$(function(){
var clone = $("#tabla tbody tr:eq(1)").clone(true);
  $("#oculto").chosen({


    no_results_text: "Su busqueda no arroja resultados",
    width : "90%",
    placeholder_text_multiple:"Seleccione",

  });
  

  // Clona la fila oculta que tiene los campos base, y la agrega al final de la tabla
  $("#agregar").on('click', function(){
    

   


    
    clone.clone(true).removeClass('fila-base').appendTo("#tabla tbody ");

$('tr.fila select').chosen();

  });
  
  // Evento que selecciona la fila y la elimina 
  $(document).on("click",".eliminar",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

 

  });

    
});
*/

$("#nidentificacion2").keyup(function(){

  var pass1 = $('[id=nidentificacion]');
  var pass2 = $('[id=nidentificacion2]');
        if(pass1.val() != pass2.val() && pass2.val() !== ''){
            $(this).css("border-color", "#FF0000");
              
               $("#error_nidentificacion2").text("* Los numeros de identificacion no corresponden");
          }

          else
          { 
            $(this).css("border-color", "#2eb82e");
            
            $("#error_nidentificacion2").text("");

          }
       });













 





      
  
  
  
    

      

