@extends('admin.template.ejdecuenta.main')


@section('content')

@section('titulo', 'Ofertas Comerciales')



<div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          Ofertas comerciales Clientes
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>ID</th>
  <th>Ide Cliente</th>
  <th>Nombre</th>
  <th>Estado</th>
  <th>Estado Envío</th>
  <th>Fecha y hora<br>creación</th>
  <th>Fecha y hora<br>envío</th>
  <th>Acción</th>

        </tr>
    </thead>
    <tbody>


      @foreach($ofertasc as $oferta)

     
	 <tr> 
 
	 <td>{{$oferta->id}}</td>
	 <td>{{$oferta->identificacion}}</td>
	 <td>{{$oferta->razon_social}}</td>

	 

	 <td> @if($oferta->Estado=="En espera")	
	 <span class="label label-warning">{{$oferta->Estado}}</span>
	 @elseif($oferta->Estado=="Aprobado")
  <span class="label label-success">{{$oferta->Estado}}</span>

  @else
  <span class="label label-danger">{{$oferta->Estado}}</span>
	 @endif</td>

   <td> @if($oferta->estadoenv=="En Proceso") 
   <span class="label label-warning">{{$oferta->estadoenv}}</span>
   @else
<span class="label label-primary">{{$oferta->estadoenv}}</span>
   @endif</td>
   
	 <td>{{$oferta->created_at}}</td>
   <td>{{$oferta->fechaenv}}</td>

	 
	 <td><!-- <a href="{{ route('Comercial.ofertas.destroy', $oferta->id ) }}" onclick="return confirm('¿Seguro desea eliminarlo?')" class="btn btn-danger btn-xs" ><span class="fa fa-trash"></a> --> <a href="{{ route('ofertasejdecuenta.edit', $oferta->id ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a> <a href="{{route('ofertasejdecuenta.pdf' ,['id' => $oferta->id, 'iduser' => Auth::user()->id] )}}" target="_blank" class="btn btn-success btn-xs" ><span class="fa fa-eye"> </a> <a href="{{route('ofertasejdecuenta.pdf.download' , ['id' => $oferta->id, 'iduser' => Auth::user()->id])}}" target="" class="btn btn-primary btn-xs" ><span class="fa fa-arrow-down"> </a><a href="{{route('ofertasejdecuenta.mail' , $oferta->id )}}" target="" class="btn btn-success btn-xs" ><span class="fa fa-paper-plane"> </a></td>



	 </tr>
	



	 @endforeach

	 



      
           
          

           
           

          

             

    

            
     
        
       
    </tbody>
</table>
        </div>
      </div>
    </div>
 
<!--MIERDA-->

  






@endsection