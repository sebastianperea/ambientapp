
@extends('admin.template.ejdecuenta.main')

@section('content')

@section('titulo', 'Proceso Comercial')

<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Proceso Comercial # {{ $idvisita }}
        </div>
        <div class="card-body">
          <div class="row">
  <div class="col-md-6">
    <label>Razón social:</label><br>
    <p style="font-size: 20pt; color: green;"><strong>{{ $visitas->razon_social }}</strong></p>
    <hr>
    <label>Observaciones:</label>
    <p>{{ $visitas->observaciones }}</p>
    <div>
    </div>
    
  </div>
  <div class="col-md-6">
    <label>Tipo de proceso:</label>
        <p>{{ $visitas->tipo_de_visita }}</p><br>
    <label>Fecha y hora de creación:</label>
        <p>{{ $visitas->created_at }}</p><br>
    <label>Estado:</label>
      @if($visitas->ESTADO == 'HECHO')
        <span class="label label-success">{{$visitas->ESTADO}}</span>
      @else
        <span class="label label-danger">{{$visitas->ESTADO}}</span>
    @endif
    <br>
      <label>Fecha de nuevo contacto:</label>
      <p>{{ $visitas->fecha_nuevo_contacto }}</p>

  </div>
</div>
        </div>
      </div>
    </div>
    
  </div>
  @endsection