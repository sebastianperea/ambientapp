@extends('admin.template.ejdecuenta.main')

@section('content')

@section('titulo', 'Proceso Comercial')



<div class="col-xs-12">
      <div class="card">

        <div class="card-header">

          <p>Procesos comerciales</p>
          <div class="row"><a href="{{ route('visitasejdecuenta.create')}}" class="btn btn-success btn-xs" ><span class="fa fa-plus"></a></div>

        </div>

        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Ide Cliente</th>
                    <th>Razón Social</th>
                    <th>Fecha y hora<br>proceso</th>
                    <th>Tipo de proceso</th>
                    <th>Acción</th>
                </tr>
            </thead>
          <tbody>
            @foreach($procesos as $proceso)
      	 <tr> 
       
      	 <td>{{$proceso->id}}</td>
      	 <td>{{$proceso->identificacion}}</td>
      	 <td>{{$proceso->razon_social}}</td>
         
      	 <td>{{$proceso->created_at}}</td>
         <td>{{$proceso->tipo_de_visita}}</td>
         

      	 
      	 <td> <a href="{{ route('ejdecuenta.visitas.view', $proceso->id ) }}" class="btn btn-success btn-xs" ><span class="fa fa-eye"></a></td>

      	 </tr>
      	
      	 @endforeach

           </tbody>
          </table>
        </div>
      </div>
    </div>
 
<!--MIERDA-->

  






@endsection