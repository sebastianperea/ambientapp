@extends('admin.template.posconsumos.main')

@section('content')

@section('titulo', 'Certificados Lumina')

<div class="row">
<div class="sticky-container">
    <ul class="sticky"  id="sticky" style="visibility: hidden;">
        <li>

            <div id="sumchecked" style="visibility: hidden; font-size: 17pt;"> <strong><span id="checked-prices-total-sum">0</span>KG </strong> </div></p>
        </li>
        
    </ul>
</div>
<div class="col-md-12">
      <div class="card">

        
        <div class="card-header">
          
        <a class="btn btn-primary btn-xs" href="{{ route('lumina.create') }}"> <i class="fa fa-plus"></i></a>

          
          
        </div>

        
          <div class="card-body">
            {!! Form::open(['route' => 'lumina.update', 'method' => 'POST']) !!}

<table id="inventario" class="datatable table table-striped">
    <thead>
                  <tr>
                      <th></th>
                      <th>REMISIÓN</th>
                      <th>PESO(KG)</th>
                      <th>FECHA DE RECEPCION</th>
                      <th>ESTADO INGRESO</th>
                      <th>ACCIÓN</th>
                  </tr>
              </thead>
    <tbody id="inventario-data">


      @foreach($certificados as $certificado)
                 <tr>

                  @if($certificado->estado_ingreso == 'POR APROVECHAR')

                  <td><input type="checkbox" name="id[]" id="" class="data-check" value="{{ $certificado->ingresos_lumina_id }}" /></td>

                  @else
                    <td><input type="checkbox" name="id[]" id="" class="data-check" value="{{ $certificado->ingresos_lumina_id }}" hidden="hidden" /></td>

                  @endif
                     <td>{{$certificado->remision}}</td>
                     <td> {{ $certificado->pesodfkg }}</td>
                     <td class="">{{$certificado->fecha_recepcion}}</td>

                     <td> @if($certificado->estado_ingreso=="POR APROVECHAR") 
                       <span class="label label-warning">{{$certificado->estado_ingreso}}</span>
                       @elseif($certificado->estado_ingreso=="APROVECHADO")
                      <span class="label label-success">{{$certificado->estado_ingreso}}</span>
                       
                    </td>
                    @endif

                    <td> @if($certificado->estado == 'ABIERTO')<a href="{{ route('lumina.edit', $certificado->ingresos_lumina_id ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a> 

                      @endif
                      @if($certificado->estado_ingreso == 'APROVECHADO') <a href="{{ route('lumina.pdf.generate', $certificado->ingresos_lumina_id ) }}" class="btn btn-success btn-xs" ><span class="fa fa-eye"> </a>

                        <a target="blank" href="{{ route('lumina.pdf.almc.generate', $certificado->ingresos_lumina_id ) }}" class="btn btn-primary btn-xs" ><span class="fa fa-eye"> </a>

                          <a  href="{{ route('lumina.pdf.almc.down', $certificado->ingresos_lumina_id ) }}" class="btn btn-success btn-xs" ><span class="fa fa-download"> </a>
                          @endif
                    </td>

                   
                 </tr>
               @endforeach

                  </tbody>
                  
                  
              </table>

              <div id="sumchecked" style="visibility: hidden; font-size: 25pt;"> <strong><span id="checked-prices-total-sum">0</span>KG </strong> </div><br>

              <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-xs-2">
                                             {!!Form::submit('Aprovechar y enviar', ['class' => 'btn btn-success'])!!}
                                             {!! Form::close() !!}

                                        </div>
                                    </div>
                                </div>
                             
                        </div>
                      </div>
                  </div>
              </div>


 
<!--MIERDA-->


@endsection

@section('js')

<script type="text/javascript">
  
    $(document).ready(function() {
    $("#check-all").click(function() {
        var isChecked = $(this).prop('checked');
        $(".data-check").prop('checked', isChecked);
        var $sumchecked = $('#sumchecked');
        var $totalSum = $('#checked-prices-total-sum');

        if (isChecked) {
            $totalSum.html($('#totalkg').html());
            $sumchecked.css('visibility', 'visible');
        } else {
            $totalSum.html(0);
            $sumchecked.css('visibility', 'hidden');
        }
    });

    $('#inventario-data').on('change', 'input[type="checkbox"]', function(){
        var $sumchecked = $('#sumchecked');
        var $view= $('.view');
        var $totalSum = $('#checked-prices-total-sum');
        var totalSumValue = parseFloat($totalSum.html());
        var price = parseFloat($(this).parent().next().next().html().replace(",", "."));

        if ($(this).is(':checked')) {
            totalSumValue += price;
        } else {
            totalSumValue -= price;
        }

        $totalSum.html(totalSumValue.toFixed(1));
        totalSumValue > 0 ? $sumchecked.css('visibility', 'visible') : $sumchecked.css('visibility', 'hidden');
        
        document.getElementById("sticky").style.visibility = "visible";

    });
});

// $('.chkclass').click(function() {
//         var sum = 0;
//         $('.chkclass:checked').each(function() {
//             sum += parseFloat($(this).closest('tr').find('.wagein').text());


//         });
//         $('#sum').html(sum).append('KG');

//     });



</script>
@endsection