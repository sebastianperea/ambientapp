<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" type="text/css" href="/app/public/plugins/bootstrap/assets/css/bootstrap.min.css">



  <style type="text/css">
@font-face { 
    font-family:Quicksand; src: url('{{ asset('/app/public/plugins/bootstrap/assets/fonts/Quicksand-Regular.otf') }}');

}
  header{
position: fixed;

  width: 100%;
 
}


body{
font-family: 'Quicksand-Regular', sans-serif;
margin-left: 70px;



}

.logo{

  float: left;
  width: 100mm;
  height: auto;
  position: fixed;
  margin-left: 20px;


}
.pagina{
  page-break-before:always;

}


.titulo1{
text-align: left;
font-size: 20pt;
color: #604137;


}

.imgcurva{



}

.container1{

width: 600px;
height: auto;
margin-top: 30px;
margin-left: 20px;


}

.container2{

width:300px;
height: auto;
margin-left: 600px;


}

.container3{

float:left;
width:300px;
height: auto;
margin-top: 20px;




}


.container4{

width:95%;
height: auto;
margin-top: 30px;
margin-left: 20px;
font-size: 12pt;




}


.noferta{
width:100%;
margin-top: 30px;
margin-left: 20px;



}

.parrafo{
  font-size: 12pt;
  text-align: justify;
    text-justify: inter-word;
}



.portafolio1{

float: right;
  width: 830px;
  height: auto;
  margin-top: 50px;
  margin-right: 40px;
  

}

.container5{
margin-left: 20px;
  
}

.oferta{
float:right;

}

th {
    
    background-color: #95D935;
    color: white;

}
table {
    border-collapse: collapse;
    width: 90%;
    height: auto;
margin: auto;
z-index: 5mm;
margin-top: 20px;

}

table, td, th {
   border: 1px solid #ddd;
    text-align: left;
    border-radius: 5px;
}

.container5 p {
  margin-top: 30px;
  font-size: 12pt;
}


th, td {
 text-align: left;
    padding: 8px;
    font-size: 12pt;
}

}
tr:nth-child(even){background-color: #f2f2f2}
tr { page-break-inside: avoid !important; }

</style>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Cetificado de almacenamiento numero: {{$certificados->prefijo}}{{$certificados->prefijo2}}{{$certificados->id}} </title>
</head>
<body>

		<div class="row">

              <div class="noferta">

              	<div><strong><p style="font-size: 12pt;"> Certificado Número: {{$certificados->prefijo}}{{$certificados->prefijo2}}{{$certificados->id}}
                    </p>

                    <div style="text-align: center; ">
                    	<p style="font-size: 14pt;">CERTIFICADO DE ALMACENAMIENTO</p>
                    	<p style="font-size: 14pt;">ECOINDUSTRIA S.A.S E.S.P<BR>
                    	NIT: 900.140.609-1</p>

                    </div>
                </strong><br><br>

                <p style="font-size: 12pt;">
                	Gestor Autorizado por la Corporación PosConsumo de Residuos de iluminación - Lúmina  para la recolección y/o recepción. almacenamiento, transporte, y aprovechamiento de los residuos de iuminación.
                </p><br>
                <strong><p style="text-align: center; font-size: 14pt;">CERTIFICA QUE:</p></strong>

                <strong><p style="text-align: center; font-size: 14pt;">
                	{{ $conexion->razon_social }}
                </p></strong>
        <div class="container5">
       <!-- Inicio Dibujo peligrosos -->
              <table>
                <thead>
                  <tr>
                    <th scope="col"></th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                      <tbody>
                        
                        <tr>
                           <td><strong><div style="font-size: 14pt;">Identificada con NIT</div></strong></td>

                           <td><div style="font-size: 14pt;">{{ $conexion->identificacion }}-{{ $conexion->dv }}</div></td>


                          
                        </tr>

                        <tr>
                        	<td><strong><div style="font-size: 12pt;">Fecha Recepción</div></strong></td>
                        	<td><div style="font-size: 12pt;">{{ $conexion->fecha_recepcion }}</div></td>
                        </tr>

                        <tr>
                        	<td><strong><div style="font-size: 12pt;">Numero de Remisión</div></strong></td>
                        	<td><div style="font-size: 12pt;">{{ $conexion->remision }}</div></td>
                        </tr>
                        
                      </tbody>
                </table>
               </div>

        <div class="container5">
        <!-- Inicio Dibujo peligrosos -->
              <table>
                <thead>
                  <tr>
                    <th> <div style="font-size: 12pt;">Tipo de residuos</div></th>
                    <th> <div style="font-size: 12pt;">Cantidad en KG</div></th>
                    <th> <div style="font-size: 12pt;">Clasf CRETIRB</div></th>
                    <th><div style="font-size: 12pt;"> Corriente</th>
                    <th> <div style="font-size: 12pt;">Estado</div></th>       
                  </tr>
                </thead>
                      <tbody>
                        <?php 
                              foreach ($tipostecnologia as $tipos) {   
                           ?>
                        <tr>
                        	<td><div style="font-size: 12pt;">{{($tipos->nombre)}}</div></td>
                           <td><div style="font-size: 12pt;">{{($tipos->pesodfkg)}}</div></td>
                           <td><div style="font-size: 12pt;">{{ $tipos->clasificacion_cretib }}</div></td>
                           <td><div style="font-size: 12pt;">{{ $tipos->clasificacion_dec1076}}</div></td>
                           <td><div style="font-size: 12pt;">{{ $tipos->estado}}</div></td>
                          
                        </tr>
                        <?php } ?>
                      </tbody>
                </table>
               </div><br><br>

               
               
               <div style="font-size: 12pt;">La empresa <strong>{{ $conexion->razon_social }}</strong> entrego residuos de iluminación  @if($gestores->razonsocial == 'ECO INDUSTRIA SAS ESP') a la empresa <br>ECO INDUSTRIA S.A.S E.S.P para realizar almacenamiento bajo la resolución 03874 del 30 de noviembre de 2018 emitida por la Corporación Autónoma Regional de Cundinamarca, en cumplimiento de la Resolución 1511 de 2010 emitida por el Ministerio de Ambiente y Desarrollo sostenible, residuos que fueron transportados para ser aprovechados por la empresa INNOVA AMBIENTAL S.A.S identificada con nit. 900 489 388 -8 ubicada en la carrera 39 #13-32 Acopí -Yumbo, bajo la resolución 0100 N° 0150-00342 de 2013 y la resolución 0100 N° 01500247 del 2015 emitida por la corporación autonoma regional del valle del cauca en cumplimiento de la resolución 1511 de 2010 emitida por el ministerio de ambiente y desarrollo sostenible.<br><br> Una vez se efectue dicha fase le sera entregado el certificado de aprovechamiento.<br><br> La presente certificación se expide a solicitud del interesado el día {{ $fecha}}. Cualquier información adicional relacionada con este certificado se encuentra disponible para su consulta en nuestras instalaciones.
                @else

                por intermedio de <strong>{{ $gestores->razonsocial }} </strong> a la empresa ECO INDUS - <br>TRIA S.A.S E.S.P para realizar almacenamiento bajo la resolución 03874 del 30 de noviembre de 2018 emitida por la Corporación Autónoma Regional de Cundinamar - ca, en cumplimiento de la Resolución 1511 de 2010 emitida por el Ministerio de Ambiente y Desarrollo sostenible, residuos que fueron transportados para ser aprovechados por la empresa INNOVA AMBIENTAL S.A.S identificada con nit. 900 489 388 -8 ubicada en la carrera 39 #13-32 Acopí - Yumbo, bajo la resolución 0100 N° 0150-00342 de 2013 y la resolución 0100 N° 01500247 del 2015 emitida por la corporación autonoma <br>regional del VALLE DEL CAUCA en cumplimiento de la resolución 1511 de 2010 emitida por el ministerio de ambiente y desarrollo sostenible.<br><br> Una vez se efectue dicha fase le sera entregado el certificado de aprovechamiento.<br><br> La presente certificación se expide a solicitud del interesado el día {{ $fecha}}. Cualquier información adicional relacionada con este certificado se encuentra disponible para su consulta en nuestras instalaciones.


                @endif



                
               </div><br><br>

               <div class="oferta">
                	{!!QrCode::size(220)->color(168,207,69)->generate("https://www.ambientapp.ecoindustriasas.com/$certificados->id") !!}
                </div></div>

               <p style="font-size: 12pt;">
                <img src="/app/public/plugins/images/firma1-01.png" style="width: 230px; height: auto;"><br>

                Cordialmente, <br> <strong>Maritza Peña Reyes</strong><br>Coordinadora Nacional de Posconsumos</p>

            </div>
      </div>

      
</body>
</html>