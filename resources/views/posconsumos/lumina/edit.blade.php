@extends('admin.template.posconsumos.main')

@section('content')

<div class="row">



    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
         Generador
        </div>
        <div class="card-body">
                    
                {!! Form::open(['route' => 'lumina.store', 'method' => 'POST']) !!}
                    {!!Form::number('idgen',null, ['class' => 'form-control idgen','id'=>'idgen', 'required', 'readonly', 'style'=> 'display:none;']) !!}
          <div class="col-sm-4">
                   {!! Form::label('razon social','Razon Social')!!}
                   {!!Form::text('razon_social',$datos->razon_social, ['class' => 'form-control razonsocial', 'id' => 'razonsocial' ,'placeholder' => 'Razón social' , 'required'])!!}
                   
          </div>
                   <div class="col-sm-4">
                     {!! Form::label('identificacion','Identificación')!!}
                    {!!Form::number('identificacion',$datos->identificacion, ['class' => 'form-control identificacion', 'id'=> 'identificacion', 'placeholder' => 'Identificación' , 'required']) !!}
                   </div>

                   <div class="col-sm-2">
                    {!! Form::label('Dv','Dv')!!}
                    {!!Form::number('dv',$datos->dv, ['class' => 'form-control dv','id'=>'dv','placeholder' => 'DV' , 'required']) !!}
                </div>

                <div class="col-sm-3">
                    {!! Form::label('departamento','Departamento')!!}
                    {!!Form::text('departamento', $datos->departamento, ['class' => 'form-control departamento','id'=>'departamento','placeholder' => 'Departamento' , 'required']) !!}
                </div>

                <div class="col-sm-3">
                     {!! Form::label('ciudad','Ciudad')!!}
                    {!!Form::text('ciudad',$datos->ciudad, ['class' => 'form-control ciudad', 'id'=> 'ciudad', 'placeholder' => 'Ciudad' , 'required']) !!}
                   </div>

                   

                   
                   <div class="col-sm-4">
                   {!! Form::label('direccion','Direccion')!!}
                   {!!Form::text('direccion',$datos->direccion, ['class' => 'form-control direccion', 'id' => 'direccion' ,'placeholder' => 'Dirección' , 'required'])!!}
                   </div>

                   <div class="col-sm-3">
                   {!! Form::label('Telefono','Télefono')!!}
                   {!!Form::number('telefono',$datos->telefono, [  'class' => 'form-control telefono', 'placeholder' => 'Télefono' ,'id'=>'telefono', 'min'=> '0'])!!}

                   </div>

                   <div class="col-sm-3">
                   {!! Form::label('Telefono_OP','Télefono_opcional')!!}
                   {!!Form::number('telefono2',$datos->telefono2, [  'class' => 'form-control telefono2', 'placeholder' => 'Télefono' ,'id'=>'telefono2', 'min'=> '0'])!!}

                   </div>

                   <div class="col-sm-4">
                   {!! Form::label('responsable_envio','Responsable envío')!!}
                   {!!Form::text('responsable_envio',$datos->responsable_envio, ['class' => 'form-control responsableenvio', 'id' => 'responsableenvio' ,'placeholder' => 'Responsable Envío' , 'required'])!!}

                   </div>

                   <div class="row">

                    <div class="col-sm-3">
                   {!! Form::label('correoelectronico','Correo Eléctronico')!!}
                   
                   {!!Form::email('correoelectronico',$datos->correo_electronico, ['class' => 'form-control correoelectronico', 'id' => 'correoelectronico' ,'placeholder' => 'Correo Electronico' , 'required'])!!}
                   </div>


                   <div class="col-sm-3">
                   {!! Form::label('correo2','Correo Eléctronico Opcional')!!}
                   
                   {!!Form::email('correo2',$datos->correo2, ['class' => 'form-control correo2', 'id' => 'correo2' ,'placeholder' => 'Correo Electronico 2' ])!!}
                   </div>

                   <div class="col-sm-4">
                   {!! Form::label('gestor','Gestor')!!}
                {!!Form::select('gestor',$gestores,null,['class' => 'form-control select2', 'id'=>'gestor','required'])!!}

                </div>
                </div>


                

                
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="card-title">Remisión</div>
        </div>
        <div class="card-body">
          <div class="row">


            <div class="col-sm-3">
                   {!! Form::label('Fecha de recepción','')!!}
                   

                   <input type="date" class="form-control" name="fecha_recepcion" value="<?php echo date("Y-m-d");?>">
                  </div>
            
            
            <div class="col-sm-3">
              {!! Form::label('remision','# Remisión')!!}
                   {!!Form::number('remision',$datos->remision, [  'class' => 'form-control remision', 'placeholder' => 'Remisión' ,'id'=>'remision', 'min'=> '0', 'required'])!!}
            </div>

            <div class="col-sm-4">
              {!! Form::label('canal','Canal')!!}
                {!!Form::select('canal',$canales,null,['class' => 'form-control select2', 'id'=>'canal','required'])!!}
            </div>

          <div class="row"></div>

                  <div class="col-sm-12">
    <table class="table table-striped table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>NOMBRE</th>
          <th>Clasificación CRETIIB</th>
          <th>Clasificación DEC1076</th>
          <th>ESTADO</th>
          <th>PESOR(KG)</th>
          <th>PESODF(KG)</th>
        </tr>
      </thead>
      <tbody>



        @foreach($pesos as $tech)

        <tr>
          <td>

            {!!Form::number('id[]',$tech->id, [  'class' => 'form-control','id'=>'id', 'readonly'])!!}</td>
          <td>{{ $tech->nombre }}</td>
          <td>{{ $tech->clasificacion_cretib }}</td>
          <td>{{ $tech->clasificacion_dec1076 }}</td>
          <td>{{ $tech->estado }}</td>
          <td>{!!Form::number('pesorkg[]',null, [  'class' => 'form-control','id'=>'pesorkg', 'min'=> '0', 'step' => 'any'])!!}</td>
          <td>{!!Form::number('pesodfkg[]',null, [  'class' => 'form-control','id'=>'pesodfkg', 'min'=> '0', 'step' => 'any'])!!}</td>
         
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>

  <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               {!!Form::submit('Guardar y crear nuevo', ['class' => 'btn btn-primary'])!!}
                               {!! Form::close() !!}

                          </div>
                      </div>
                  </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>

    
          
@endsection

@section('js')



 <!-- Jquery -->




<script type="text/javascript">
    

$('#razonsocial').autocomplete({

  source: ' {!!URL::route('autocompletegen')!!}',
  minlenght: 2,
  autoFocus: true,
  select: function(e,ui){
    $('#idgen').val(ui.item.idgen);
    $('#razonsocial').val(ui.item.value);
    $('#identificacion').val(ui.item.identificacion);
    $('#dv').val(ui.item.dv);
    $('#direccion').val(ui.item.direccion);
    $('#departamento').val(ui.item.departamento);
    $('#ciudad').val(ui.item.ciudad);
    $('#telefono').val(ui.item.telefono);
    $('#telefono2').val(ui.item.telefono2);
    $('#responsableenvio').val(ui.item.responsableenvio);
    $('#correoelectronico').val(ui.item.correoelectronico);
    $('#correo2').val(ui.item.correo2);
   


  }





});


$('#razonsocialges').autocomplete({

  source: ' {!!URL::route('autocompleteges')!!}',
  minlenght: 2,
  autoFocus: true,
  select: function(e,ui){

    $('#razonsocialges').val(ui.item.value);
    $('#identificacionges').val(ui.item.identificacion);
    $('#dvges').val(ui.item.dv);
    $('#direccionges').val(ui.item.direccion);
    $('#departamentoges').val(ui.item.departamento);
    $('#ciudadges').val(ui.item.ciudad);
    $('#telefonoges').val(ui.item.telefono);
    $('#contactoges').val(ui.item.contactogestor);
    $('#correoelectronicoges').val(ui.item.correoelectronico);

    $('#idges').val(ui.item.id);
   


  }







});

$('#nombrecanal').autocomplete({

  source: ' {!!URL::route('autocompletecanal')!!}',
  minlenght: 2,
  autoFocus: true,
  select: function(e,ui){

    $('#nombre').val(ui.item.value);
  }

  





});





</script>







<!-- Peligrosos -->
<script>

$(document).ready(function(){

$(document).on('keydown', '.searchname', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocomplete')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#id_' + index).val(ui.item.id);
    $('#peligrososprecio_' + index).val(ui.item.value2);
   


  }
    });

});

 
 // Add more
 $('#agregar2').click(function(){
  
 


  // Get last id 
  var lastname_id = $('.fila2 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila2'><td><input id='searchname_"+index+"' class=' form-control searchname' required name='nombre_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='id form-control' readonly id='id_"+index+"' name='id_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidad_[]' ></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='peligrososprecio_[]' class= 'form-control peligrososprecio' id= 'peligrososprecio_"+index+"'></div></td> <td class='eliminar2 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla2 tbody').append(html);
 
 });

$(document).on("click",".eliminar2",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

</script>


<!-- Aprovechables -->
  <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamea', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletea')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#ida_' + index).val(ui.item.id).clone();
    $('#aprovechablesprecio_' + index).val(ui.item.value2);
    $('#preciou_' + index).val(ui.item.value3);
   


  }
    });

});

 
 // Add more
 $('#agregar3').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila3 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila3'><td><input id='searchnamea_"+index+"' class=' form-control searchnamea' required name='nombrea_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='id form-control' readonly id='ida_"+index+"' name='ida_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidada_[]' ></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='aprovechablesprecio_[]' class= 'form-control aprovechablesprecio' id= 'aprovechablesprecio_"+index+"'></div></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='preciou_[]' class= 'form-control preciou' id= 'preciou_"+index+"'></div></td> <td class='eliminar3 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla3 tbody').append(html);
 
 });

$(document).on("click",".eliminar3",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>


    <!-- Posconsumos -->
 <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamepo', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletepo')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idpo_' + index).val(ui.item.id).clone(true);
    $('#posconsumosprecio_' + index).val(ui.item.value2);
   


  }
    });

});

 
 // Add more
 $('#agregar4').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila4 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila4'><td><input id='searchnamepo_"+index+"' class=' form-control searchnamepo' required name='nombrepo_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='idpo form-control' readonly id='idpo_"+index+"' name='idpo_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidadpo_[]' ></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='posconsumosprecio_[]' class= 'form-control posconsumosprecio' id= 'posconsumosprecio_"+index+"'></div></td> <td class='eliminar4 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla4 tbody').append(html);
 
 });

$(document).on("click",".eliminar4",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>


<!-- Peligrosos Aprovechables -->
 <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamepa', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletepa')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idpa_' + index).val(ui.item.id).clone(true);
    $('#peligrososaprecio_' + index).val(ui.item.value2);
    $('#peligrososapreciou_' + index).val(ui.item.value3);
    $('#peligrososapreciog_' + index).val(ui.item.value4);
   


  }
    });

});

 
 // Add more
 $('#agregar5').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila5 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila5'><td><input id='searchnamepa_"+index+"' class=' form-control searchnamepa' required name='nombrepa_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='idpa form-control' readonly id='idpa_"+index+"' name='idpa_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidadpa_[]' ></td> <td><div class='input-group'> <input type='number' name='peligrososaprecio_[]' class= 'form-control peligrososaprecio' id= 'peligrososaprecio_"+index+"'></div></td> <td><div class='input-group'> <input type='number' name='peligrososapreciou_[]' class= 'form-control peligrososapreciou' id= 'peligrososapreciou_"+index+"'></div></td><td><div class='input-group'> <input type='number' name='peligrososapreciog_[]' class= 'form-control peligrososapreciog' id= 'peligrososapreciog_"+index+"'></div></td> <td class='eliminar5 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla5 tbody').append(html);
 
 });

$(document).on("click",".eliminar5",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>
 


<script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamee', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletee')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#ide_' + index).val(ui.item.id).clone(true);
    $('#especialesprecio_' + index).val(ui.item.value2);
   


  }
    });

});

 
 // Add more
 $('#agregar6').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila6 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila6'><td><input id='searchnamee_"+index+"' class=' form-control searchnamee' required name='nombree_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='id form-control' readonly id='ide_"+index+"' name='ide_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidade_[]' ></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='especialesprecio_[]' class= 'form-control especialesprecio' id= 'especialesprecio_"+index+"'></div></td> <td class='eliminar6 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla6 tbody').append(html);
 
 });

$(document).on("click",".eliminar6",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>


  <!-- Transporte  -->

<script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamet', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletet')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idt_' + index).val(ui.item.id);
    $('#transportespreciokg_' + index).val(ui.item.value2);
    $('#transportespreciog_' + index).val(ui.item.value3);

  }
    });

});

 
 // Add more
 $('#agregar7').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila7 input[type=text]:nth-child(1)').last().attr('id');


  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila7'><td><input id='searchnamet_"+index+"' class=' form-control searchnamet' required name='tipo_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='idt form-control' readonly id='idt_"+index+"' name='idt_[]' ></td><td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='transportespreciokg_[]' class= 'form-control transportesprecio' id= 'transportespreciokg_"+index+"'></div></td><td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='transportespreciog_[]' class= 'form-control transportesprecio' id= 'transportespreciog_"+index+"'></div></td> <td class='eliminar6 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla7 tbody').append(html);
 
 });

$(document).on("click",".eliminar7",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

</script>

<!-- Inicio PYS -->


<script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamepys', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];


    $( '#' + id).autocomplete({  

     source: ' {!!URL::route('autocompletepys')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){


    $('#idpys_' + index).val(ui.item.id).clone(true);
    $('#pysprecio_' + index).val(ui.item.value2);
   
  }
    });

});

 
 // Add more
 $('#agregar8').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila8 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila6'><td><input id='searchnamee_"+index+"' class=' form-control searchnamepys' required name='nombrepys_[]'  type='text'  placeholder='Ingrese el nombre del PYS'></td><td><input type='number' class='id form-control' readonly id='idpys_"+index+"' name='idpys_[]' ></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='pysprecio_[]' class= 'form-control pysprecio' id= 'pysprecio_"+index+"'></div></td> <td class='eliminar8 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla8 tbody').append(html);
 
 });

$(document).on("click",".eliminar8",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>



@endsection