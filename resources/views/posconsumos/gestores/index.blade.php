@extends('admin.template.posconsumos.main')

@section('content')

@section('titulo', 'Gestores')

<div class="row">
<div class="col-md-12">
      <div class="card">
        <div class="card-header">
         Gestores
         
        </div>

          <div class="card-body">
            <a class="btn btn-warning" href="{{ route('gestores.export') }}">Exportar a XLS <i class="fa fa-download"></i></a>

            <a class="btn btn-success" href="{{ route('gestores.create') }}">CREAR NUEVO <i class="fa fa-plus"></i></a>
           
            <table class="datatable table table-striped primary" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      
                      <th>ID</th>
                      <th>RAZÓN SOCIAL</th>
                      <th>IDENTIFICACION</th>
                      <th>NOMBRE CONTACTO</th>
                      <th>CORREO</th>
                  </tr>
              </thead>

              
              <tbody>
                
                @foreach($gestores as $gestor)
                 <tr>


                     <td>{{$gestor->id}}</td>
                     <td>{{$gestor->razonsocial}}</td>
                     <td>{{ $gestor->identificacion }}-{{ $gestor->dv }}</td>

                     <td>{{ $gestor->contacto_gestor }}</td>

                     <td>{{ $gestor->correo }}</td>


                   
                 </tr>
               @endforeach

              </tbody>

            </table>

           
            
          </div>
        </div>
    </div>
</div>
 
<!--MIERDA-->


@endsection

@section('js')

<script type="text/javascript">
$('.chkclass').click(function() {
        var sum = 0;
        $('.chkclass:checked').each(function() {
            sum += parseFloat($(this).closest('tr').find('.wagein').text());


        });
        $('#sum').html(sum).append('KG');

    });

</script>
@endsection