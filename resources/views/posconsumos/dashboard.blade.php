@extends('admin.template.posconsumos.main')
@section('content')

<div class="row">
  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-green-light" href="{{ route('lumina.create', true)}}">
  <div class="card-body">
  	<i class=" icon fa fa-lightbulb-o fa-4x"></i>
    <div class="content">
      <div class="title"></div>
      <div class="value"><span class="sign">Ingresar Lumina</span></div>
    </div>
  </div>
</a>

  </div>
  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-blue-light" href="{{ route('gestores.index', true)}}">
  <div class="card-body">
    <i class="icon fa fa-rocket fa-4x"></i>
    <div class="content">
      <div class="title"></div>
      <div class="value"><span class="sign">Gestores</span></div>
    </div>
  </div>
</a>

  </div>
  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-yellow-light" href="{{ route('generadores.index', true)}}">
  <div class="card-body">
    <i class="icon fa fa-users fa-4x"></i>
    <div class="content">
      <div class="title"></div>
      <div class="value"><span class="sign">Generadores</span></div>
    </div>
  </div>
</a>
  </div>
</div>

<div class="row">
  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-blue-light" href="{{ route('lumina.create', true)}}">
  <div class="card-body">
  	<i class=" icon fa fa-asterisk fa-4x"></i>
    <div class="content">
      <div class="title"></div>
      <div class="value"><span class="sign">Canales</span></div>
    </div>
  </div>
</a>

  </div>
  
 
</div>

@endsection