@extends('admin.template.main')

@section('content')

@section('titulo', 'Residuos')



<div class="row">
    <div class="col-lg-12">
      <div class="card card-tab">
        <div class="card-header">
          <ul class="nav nav-tabs">
            <li role="tab1" class="active">
              <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Peligrosos</a>
            </li>
            <li role="tab3">
              <a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Aprovechables</a>
            </li>
            <li role="tab4">
              <a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">Posconsumos</a>
            </li>
          </ul>
        </div>
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">

              {!! Form::open(['route' => 'peligrosos.store', 'method' => 'POST']) !!}

              <div class="col-sm-12">
                <div class="section">
                  <div class="section-title">Creación</div>

                </div>
                <div class="col-md-12">
                  <input type="text" class="form-control" placeholder="Nombre"name="nombre">
                  <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">
                      <i class="fa fa-dollar" aria-hidden="true"></i></span>
                    <input type="text" class="form-control" placeholder="Precio" aria-describedby="basic-addon1" value="" name="precio"  >


                  </div>
                  
                  <input type="text" class="form-control" placeholder="Pre - tratamiento"name="pretratamiento">

                  
                   {!! Form::label('dispositor','Dispositor')!!}
                   
                    
                    {!!Form::select('dispositor', $dispositoressel,null,['class' => 'form-control select2', 'id'=>'dispositor','required'])!!}
                  
                  <input type="submit" class="btn btn-success btn-submit" value="Crear"><br>
                  {!! Form::close() !!}
                </div>
              </div>


              <div class="col-md-12">
                
                          
                           
                            <table class="table">
                              <thead>
                                <tr> 
                                  <th>#</th>
                                  <th>Nombre</th>
                                  <th>Precio</th>
                                  <th>Pretratamiento</th>
                                  <th>Acción</th>
                                </tr>
                                
                              </thead>
                              <tbody>
                                @foreach($peligrosos as $peligroso)
                                <tr>
                                  <th>{{ $peligroso->id }}</th>
                                  <td>{{ $peligroso->nombre }}</td>
                                  <td>{{ $peligroso->precio }}</td>
                                  <td>{{ $peligroso->pretratamiento }}</td>
                                  <td><a href="{{ route('peligrosos.edit', $peligroso->id ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a></td>
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                          
                
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="tab3">
            ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nullaip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nullaip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          </div>
          <div role="tabpanel" class="tab-pane" id="tab4">
            ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nullaip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nullaip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          Dispositores<br>
          <hr>
          <a  href="{{route('dispositores.index')}}" class="btn btn-primary btn-xs" >Ver todos </a>
        </div>
        <div class="card-body no-padding">
          <div class="table-responsive">
  <table class="table card-table">
    <thead>
      <tr>
        <th>#</th>
        <th>Nombre</th>
        <th>Categoria</th>
       </tr>
    </thead>
    <tbody>
      @foreach($dispositores as $dispositor)
                                <tr>
                                  <th>{{ $dispositor->id }}</th>
                                  <td>{{ $dispositor->nombre }}</td>
                                  <td>
                                 
                                    @foreach(json_decode($dispositor->categoria, true) as $d)
                                <div>{{ $d }}</div>
                                @endforeach</td>
                                  
                                </tr>
                                @endforeach
    </tbody>
  </table>
</div>
        </div>
      </div>
    </div>
    
    <div class="col-md-6">
      <div class="card card-mini">
        <div class="card-header">
          Crear dispositores
        </div>
        <div class="card-body">

            {!! Form::open(['route' => 'dispositores.store', 'method' => 'POST']) !!}

          
                  <input type="text" class="form-control" placeholder="Nombre"name="nombre">
                  
                    
                    <input type="number" min="0" name="identificacion" class="form-control" placeholder="Identificación" aria-describedby="basic-addon1" value="">

                    

                  

                  <input type="number" min="0" name="dv" class="form-control" placeholder="DV" aria-describedby="basic-addon1" value="">

                  
                  
                   {!! Form::label('categoria','Categoria')!!}
                    {!!Form::select('categoria[]', ['PELIGROSOS' =>'Peligrosos', 'APROVECHABLES' => 'Aprovechables','POSCONSUMOS' => 'Posconsumo', 'PELIGROSOS_APROVECHABLES'=> 'Peligrosos Aprovechables', 'ESPECIALES' => 'Especiales'],null,['class' => 'form-control select2', 'id'=>'categoria','required', 'MULTIPLE' => 'multiple'])!!}
                  
                  <input type="submit" class="btn btn-success btn-submit" value="Crear">
                  {!! Form::close() !!}
                
        </div>
      </div>
    </div>
  </div>

@endsection