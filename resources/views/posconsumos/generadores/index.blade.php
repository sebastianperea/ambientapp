@extends('admin.template.posconsumos.main')

@section('content')

@section('titulo', 'Generadores')

<div class="row">
<div class="col-md-12">
      <div class="card">
        <div class="card-header">
         Generadores
        </div>

          <div class="card-body">
            <a class="btn btn-warning" href="{{ route('generadores.export') }}">Exportar a XLS <i class="fa fa-download"></i></a>
            <a class="btn btn-success" href="{{ route('generadores.create') }}">Nuevo Generador <i class="fa fa-upload"></i></a>
           
            <table class="datatable table table-striped primary" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      
                      <th>ID</th>
                      <th>RAZÓN SOCIAL</th>
                      <th>IDENTIFICACION</th>
                      <th>NOMBRE CONTACTO</th>
                      <th>CORREO</th>
                  </tr>
              </thead>

              
              <tbody>
                
                @foreach($generadores as $generador)
                 <tr>


                     <td>{{$generador->id}}</td>
                     <td>{{$generador->razon_social}}</td>
                     <td>{{ $generador->identificacion }}-{{ $generador->dv }}</td>

                     <td>{{ $generador->responsable_envio }}</td>

                     <td>{{ $generador->correo_electronico }}</td>
                   
                 </tr>
               @endforeach

              </tbody>

            </table>

           
            
          </div>
        </div>
    </div>
</div>
 
<!--MIERDA-->


@endsection

@section('js')

<script type="text/javascript">
$('.chkclass').click(function() {
        var sum = 0;
        $('.chkclass:checked').each(function() {
            sum += parseFloat($(this).closest('tr').find('.wagein').text());


        });
        $('#sum').html(sum).append('KG');

    });

</script>
@endsection