@extends('admin.template.posconsumos.main')

@section('content')



<div class="row">

  <div class="col-md-12">
      <div class="card">
        <div class="card-header">
       

       <a class="btn btn-warning btn-sm" style="margin-left: 4px;" href="{{ route('generadores.index') }}"> Ver Generadores <i class="fa fa-eye"></i></a>

        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
         Creación de Generadores
        </div>
        
        <div class="card-body">
        	{!! Form::open(['route' => 'generadores.import', 'method' => 'POST', 'enctype'=> 'multipart/form-data']) !!}

        	
        	
        		{!! Form::label('','Importar archivo XLS')!!}
                <input type="file" name="file" class="form-control" required="required">
                <button class="btn btn-success btn-sm">Importar archivo</button>
                
            {!! Form::close() !!}<br>

       			<hr>

            
                    
                {!! Form::open(['route' => 'gestores.store', 'method' => 'POST']) !!}
                    
          <div class="col-sm-4">
                   {!! Form::label('razon social','Razon Social')!!}
                   {!!Form::text('razonsocial',null, ['class' => 'form-control razonsocial', 'id' => 'razonsocial' ,'placeholder' => 'Razón social' , 'required'])!!}
                   
          </div>
                   <div class="col-sm-4">
                     {!! Form::label('identificacion','Identificación')!!}
                    {!!Form::number('identificacion',null, ['class' => 'form-control identificacion', 'id'=> 'identificacion', 'placeholder' => 'Identificación' , 'required']) !!}
                   </div>

                   <div class="col-sm-2">
                    {!! Form::label('Dv','Dv')!!}
                    {!!Form::number('dv',null, ['class' => 'form-control dv','id'=>'dv','placeholder' => 'DV' , 'required']) !!}
                </div>

                <div class="col-sm-3">
                    {!! Form::label('departamento','Departamento')!!}
                    {!!Form::text('departamento',null, ['class' => 'form-control departamento','id'=>'departamento','placeholder' => 'Departamento' , 'required']) !!}
                </div>

                <div class="col-sm-3">
                     {!! Form::label('ciudad','Ciudad')!!}
                    {!!Form::text('ciudad',null, ['class' => 'form-control ciudad', 'id'=> 'ciudad', 'placeholder' => 'Ciudad' , 'required']) !!}
                   </div>

                   

                   
                   <div class="col-sm-4">
                   {!! Form::label('direccion','Direccion')!!}
                   {!!Form::text('direccion',null, ['class' => 'form-control direccion', 'id' => 'direccion' ,'placeholder' => 'Dirección' , 'required'])!!}
                   </div>

                   <div class="col-sm-4">
                   {!! Form::label('centro_de_acopio','Centro de acopio')!!}
                   {!!Form::text('centro_de_acopio',null, ['class' => 'form-control direccion', 'id' => 'direccion' ,'placeholder' => 'Centro de acopio' , 'required'])!!}
                   </div>

                   <div class="col-sm-3">
                   {!! Form::label('Telefono','Télefono')!!}
                   {!!Form::number('telefono',null, [  'class' => 'form-control telefono', 'placeholder' => 'Télefono' ,'id'=>'telefono', 'min'=> '0'])!!}

                   </div>

                   <div class="col-sm-3">
                   {!! Form::label('Telefono2op','Télefono_opcional')!!}
                   {!!Form::number('telefono2op',null, [  'class' => 'form-control telefono2', 'placeholder' => 'Télefono' ,'id'=>'telefono2', 'min'=> '0'])!!}

                   </div>

                   <div class="col-sm-4">
                   {!! Form::label('','Contacto Gestor')!!}
                   {!!Form::text('contacto_gestor',null, ['class' => 'form-control responsableenvio', 'id' => 'responsableenvio' ,'placeholder' => 'Contacto Gestor' , 'required'])!!}

                   </div>

                   <div class="row">

                    <div class="col-sm-3">
                   {!! Form::label('correo','Correo Eléctronico')!!}
                   
                   {!!Form::email('correo',null, ['class' => 'form-control correoelectronico', 'id' => 'correoelectronico' ,'placeholder' => 'Correo Electronico' , 'required'])!!}
                   </div>


                   <div class="col-sm-3">
                   {!! Form::label('correo2op','Correo Eléctronico Opcional')!!}
                   
                   {!!Form::email('correo2op',null, ['class' => 'form-control correo2', 'id' => 'correo2' ,'placeholder' => 'Correo Electronico 2' ])!!}
                   </div>

                   
                </div>


  <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               {!!Form::submit('Guardar y crear nuevo', ['class' => 'btn btn-primary btn-sm'])!!}
                               {!! Form::close() !!}

                          </div>
                      </div>
                  </div>
                

                
        </div>
      </div>
    </div>


    
  </div>

    
          
@endsection

