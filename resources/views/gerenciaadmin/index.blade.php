 
@extends('admin.template.gerenciaadmin.main')

@section('content')

@section('titulo')
Gerencia Administrativa

@endsection


@section('content')
      <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <a class="card card-banner card-green-light">
        <div class="card-body">
          <i class="icon fa fa-shopping-basket fa-4x"></i>
          <div class="content">
            <div class="title">Numero de ofertas</div>
            <div class="value"><span class="sign">{{ $ofertas }}</span></div>
          </div>
        </div>
      </a>

  </div>
  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-blue-light">
  <div class="card-body">
    <i class="icon fa fa-thumbs-o-up fa-4x"></i>
    <div class="content">
      <div class="title">clientes</div>
      <div class="value"><span class="sign"></span>{{ $clientes }}</div>
    </div>
  </div>
</a>


  </div>
  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-yellow-light">
  <div class="card-body">
    <i class="icon fa fa-user-plus fa-4x"></i>
    <div class="content">
      <div class="title">Ventas en pesos</div>
      <div class="value"><span class="sign"></span>$ 50000</div>
    </div>
  </div>
</a>

  </div>
</div>
<div class="row">
  <div class="col-sm-12 col-xs-12 col-md-12 ">
    <div class="card card-mini">
      <div class="card-header">
        <div class="card-title">Últimas ofertas</div>
        <ul class="card-action">
          <li>
            <a href="/">
              <i class="fa fa-refresh"></i>
            </a>
          </li>
        </ul>
      </div>
      <div class="card-body no-padding table-responsive">
        <table class="table card-table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Razón Social</th>
              <th>Estado</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>


             @foreach($ofertasc as $oferta)
              @if($oferta->Estado=="En espera")
            <tr>
              <td>{{$oferta->id}}</td>
              <td class="right">{{$oferta->razon_social}}</td>

              <td> @if($oferta->Estado=="En espera")  
                 <span class="badge badge-warning badge-icon"><i class="fa fa-clock-o" aria-hidden="true"></i>{{$oferta->Estado}}</span>
                 @elseif($oferta->Estado=="Aprobado")
              <span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i>{{$oferta->Estado}}</span>

              @elseif($oferta->Estado=="Rechazado")
              <span class="badge badge-danger badge-icon"><i class="fa fa-times" aria-hidden="true"></i>{{$oferta->Estado}}</span>

                 @endif</td>

                 <td> <a href="{{ route('ofertasg.edit', $oferta->id ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a> <a href="{{route('ofertasg.pdf' , $oferta->id )}}" target="_blank" class="btn btn-success btn-xs" ><span class="fa fa-eye"> </a> <a href="{{route('ofertasg.pdf.download' , $oferta->id )}}" target="" class="btn btn-primary btn-xs" ><span class="fa fa-arrow-down"> </a></td>
            </tr>


            
          </tbody>
           @endif 
          @endforeach
        </table>
      </div>
    </div>
  </div>
  
</div>






  

@endsection


