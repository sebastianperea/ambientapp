@extends('admin.template.gerenciaadmin.main')
@section('titulo', 'Reportes')
@section('content')



  <div class="row">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          Graficas y reportes
        </div>
        <div class="card-body">
          <div class="row">
			  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
			    <div class="section">
			      <div class="section-title">Procesos Comerciales</div>
			      <div class="section-body">
			        <canvas id="myChart" width="400" height="400"></canvas><br>

                   <strong> Total: {{ $seguimientost }}</strong> <a href="{{ route('ger.reportes.procesos') }}" type="button" class="btn btn-success">Ver todo</a>
			    </div>
			  </div>
				</div>
			</div>
		</div>
	</div>
</div>



@endsection

@section('js')
<script>
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ["Vencidos", "Hechos"],
        datasets: [{
            label: '# Seguimientos',
            data: [{{ $seguimientosvenc }}, {{ $seguimientoshc }}],
            backgroundColor: [
                '#ff9b00',
                '#29c75f' ],
            
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }


});
</script>

<script type="text/javascript">
	
	var primer_grafico = document.getElementById('grafico').getContext('2d');//seleccionamos el canvas
      window.pie = new Chart(primer_grafico,mi_primer_grafico);
</script>
@endsection