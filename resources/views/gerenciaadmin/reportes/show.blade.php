@extends('admin.template.gerenciaadmin.main')

@section('content')

@section('titulo', 'Proceso Comercial')

<div class="row">
  <div class="col-xs-12">
      <div class="card">

        <div class="card-header">

          <p>Procesos comerciales</p>
          

        </div>

        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Ide Cliente</th>
                    <th>Razón Social</th>
                    <th>Usuario</th>
                    <th>Fecha nuevo contacto</th>
                    <th>Estado</th>
                    <th>Acción</th>
                </tr>
            </thead>
          <tbody>
            @foreach($procesos as $proceso)
         <tr> 
       
         <td>{{$proceso->id}}</td>

         <td>{{$proceso->identificacion}}</td>
         <td>{{$proceso->razon_social}}</td>
         <td>{{$proceso->name}}</td>
         
         <td>{{$proceso->fecha_nuevo_contacto}}</td>
         <td>{{$proceso->ESTADO}}</td>
         

         
         <td>
          <div>
                
                <button
                     type="button"
                     class="btn btn-success btn-xs"
                      data-toggle="modal"
                      data-id="{{ $proceso->id }}"
                     data-title="{{ $proceso->razon_social }}"
                      data-fecha="{{ $proceso->created_at }}"
                    data-procidentificacion="{{ $proceso->identificacion}}"
                    
                    data-procemail="{{ $proceso->email}}"
                    data-procobservaciones="{{ $proceso->observaciones}}"
                      data-target="#favoritesModal">
                  <span class="fa fa-eye">
                </button>
              </div>


              


         </td>

         </tr>


        
         @endforeach

           </tbody>
          </table>

          <div class="modal fade" id="favoritesModal"
     tabindex="-1" role="dialog"
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close"
          data-dismiss="modal"
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><strong>Proceso Comercial #  <span id="idproc"></strong> </h4>
          <p>Fecha y hora: <span id="fecha"> </p>
          <p>Usuario: <span id="user"> </p>
      </div>
      <div class="modal-body">
          <p>Razón social:
          <b><span id="fav-title"></span></b><br>
          Identificación: <span id="proc-identificacion"></span><br>
          Procesos: <span id="proc-procesos"></span>
          </p>
          <hr>
          <h4><strong>Detalles proceso</strong></h4><br>
          <p>Observaciones:
            <span id="proc-observaciones"></span></p>

            <hr>
            <h4><strong>Historial de seguimientos</strong></h4><br>
              <table class="table table-sm">
                <thead>
                  <tr>
                    <th scope="col">Fecha</th>
                    <th scope="col">Observaciones</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Nuevo contacto</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($seguimientos as $seguimiento)
                  <tr>
                    <th scope="row">{{ \Carbon\Carbon::parse($seguimiento->created_at)->toFormattedDateString()}}</th>
                    <td>{{ $seguimiento->observaciones }}</td>
                    <td>{{ $seguimiento->ESTADO }}</td>
                    <td>{{ $seguimiento->fecha_nuevo_contacto }}</td>
                    
                  </tr>
                  @endforeach
                </tbody>
              </table>
    </div>
      <div class="modal-footer">
         
        <button type="button"
           class="btn btn-default"
           data-dismiss="modal">Cerrar</button>
        <span class="pull-right">
          <button type="button" class="btn btn-sm btn-success js-btn-step"  data-orientation="next">
            Hacer Seguimiento
          </button>
        </span>
      </div>
    </div>
  </div>
</div>
        </div>
      </div>
    </div>
  
    

</div>




 
<!--MIERDA-->

  






@endsection
@section('js')

@section('js')
<script>
$(function() {
    $('#favoritesModal').on("show.bs.modal", function (e) {
         $("#favoritesModalLabel").html($(e.relatedTarget).data('title'));
         $("#fav-title").html($(e.relatedTarget).data('title'));
         $("#idproc").html($(e.relatedTarget).data('id'));
         $("#fecha").html($(e.relatedTarget).data('fecha'));
         $("#proc-identificacion").html($(e.relatedTarget).data('procidentificacion'));
          $("#proc-procesos").html($(e.relatedTarget).data('procprocesos'));
          $("#proc-telcontacto").html($(e.relatedTarget).data('proctelcontacto'));
          $("#proc-email").html($(e.relatedTarget).data('procemail'));
          $("#proc-observaciones").html($(e.relatedTarget).data('procobservaciones'));

    });
});
</script>
@endsection