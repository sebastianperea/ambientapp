@extends('admin.template.gerenciaadmin.main')

@section('titulo')
Ofertas

@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body app-heading">
          
          <div class="app-title">
            <div class="title"><span class="highlight">{{$ofertas->id}}</span></div>
            <div class="description">Editar oferta</div>

          </div>
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="card card-tab card-mini">
        <div class="card-header">
          <ul class="nav nav-tabs">
            <li role="tab1" class="active">
              <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">General</a>
            </li>
            <li role="tab2">
              <a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Contácto</a>
            </li>
            <li role="tab3">
              <a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Servicio</a>
            </li>
          </ul>
        </div>
        <div class="card-body tab-content no-padding">
          <div role="tabpanel" class="tab-pane active" id="tab1">

            <div class="row">
            {!! Form::open(['route' => ['ofertasg.update', $ofertas], 'method' => 'PUT']) !!}
               

        @foreach($clientes as $cliente)


                  <div class="form-group">
                    <div class="col-xs-3">
                      {!! Form::label('identificacion','Identificación')!!}
                  
                      {!!Form::number('identificacion',$ofertas->identificacion, ['class' => 'form-control ', 'placeholder' => 'Identificación' , 'required','disabled']) !!}
                    </div>
                  </div>
                 <div class="col-xs-2">
                   {!! Form::label('DV','')!!}
                   {!!Form::number('dv',$cliente->dv, ['class' => 'form-control ', 'placeholder' => 'DV' , 'required']) !!}
                  </div>

                  <div class="col-xs-5">
                   {!! Form::label('Razon Social','')!!}
                   {!!Form::text('razon_social',$cliente->razon_social, ['class' => 'form-control', 'placeholder' => 'Razón social' , 'required'])!!}
                  </div>

                  <div class="col-xs-3">
                   {!! Form::label('departamento','')!!}
                   {!!Form::text('departamento',$cliente->departamento, ['class' => 'form-control', 'placeholder' => 'Departamento' , 'required'])!!}
                  </div>

                  <div class="col-xs-3">
                   {!! Form::label('Ciudad','')!!}
                   {!!Form::text('ciudad',$cliente->ciudad, ['class' => 'form-control', 'placeholder' => 'Ciudad' , 'required'])!!}
                  </div>

                  <div class="col-xs-6">
                   {!! Form::label('Direccion','')!!}
                   {!!Form::text('direccion',$cliente->direccion, ['class' => 'form-control', 'placeholder' => 'Dirección' , 'required'])!!}
                  </div>
                  


      
                  <div class="col-xs-3">
                  {!! Form::label('Estado')!!}
                  {!!Form::select('Estado',['Estado','En espera'=>'En espera','Aprobado'=> 'Aprobado', 'Rechazado' =>'Rechazado'], null,['class' => 'form-control ', 'required'])!!}
                  </div>

                  @endforeach


            </div>
          </div>


          <div role="tabpanel" class="tab-pane" id="tab2">
             <div class="row">
                  <div class="col-xs-3">
                   {!! Form::label('nombre ','Nombres')!!}
                   {!!Form::text('nombres',$cliente->nombres, ['class' => 'form-control', 'required'])!!}
                  </div>

                  <div class="col-xs-3">
                   {!! Form::label('apellidos','Apellidos')!!}
                   {!!Form::text('apellidos',$cliente->apellidos, ['class' => 'form-control', 'placeholder' => 'Apellidos' , 'required'])!!}
                  </div>
                  
                
                  <div class="col-xs-2">
                   {!! Form::label('telefonocelular','Móvil')!!}
                   {!!Form::number('telefonocelular',$cliente->telefonocelular, [  'class' => 'form-control ', 'placeholder' => 'Télefono' , 'required'])!!}
                  </div>


                  <div class="col-xs-3">
                   {!! Form::label('email','E-Mail')!!}
                   {!!Form::email('email',$cliente->email, ['class' => 'form-control ', 'placeholder' => 'E- Mail' , 'required'])!!}
                  </div>

           </div>
            <div class="row">
              <div class="col-xs-2">
                {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                {!! Form::close() !!}
              </div>
            </div>
          </div>

          <div role="tabpanel" class="tab-pane" id="tab3">
           <div class="row">


        <!-- Inicio Peligrosos -->

             {!! Form::open(['route' => ['comercialg.peligrosos', $ofertas], 'method' => 'PUT']) !!}
             <div class="col-xs-12"><h3 style="text-align: center;">Peligrosos</h3><hr> </div>
              <table class="table table-striped" id="tabla2">
                <thead>
 
                    <th>Nombre</th>
                    <th>Codigo Residuos peligrosos</th>
                    <th>Cantidad en kg</th>
                    <th>Precio por kg</th>

                </thead>

                    <tbody>
                       
                         
                         <tr class="fila2">

                              <td> {!!Form::text('nombre []',null,['id'=>'searchname_1','class' => 'form-control searchname ','placeholder' =>'Ingrese aquí el nombre']) !!}</td>

                              <td>{!!Form::number('id [] ', null, ['id'=>'id_1','class' => 'form-control id ', 'readonly']) !!}</td>

                           <td>

                            <div class="input-group">
                                {!!Form::number('cantidad [] ', null, ['class' => 'form-control ', 'id'=>'cantidad_1']) !!}
                             </div>
                          </td>


                          <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('peligrososprecio [] ',null, ['class' => 'form-control ', 'id'=>'peligrososprecio_1']) !!}
                             </div>
                          </td>

                          
                          <td class="eliminar2 btn btn-warning btn-xs">Eliminar Fila</td>
                          
                        </tr>






    
                       @foreach($peligrosos as $peligroso)
             
                          <tr class="fila2">

                              <td> {!!Form::text('nombre []',$peligroso->nombre,['id'=>'searchname_1','class' => 'form-control searchname ', 'required']) !!}</td>

                              <td>{!!Form::number('id [] ', $peligroso->peligroso_id, ['id'=>'id_1','class' => 'form-control id ', 'required', 'readonly']) !!}</td>

                           <td>

                            <div class="input-group">
                                {!!Form::number('cantidad [] ', $peligroso->cantidad, ['class' => 'form-control ', 'id'=>'cantidad_1','required']) !!}
                             </div>
                          </td>


                          <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('peligrososprecio [] ', $peligroso->precio, ['class' => 'form-control ', 'id'=>'peligrososprecio_1','required']) !!}
                             </div>
                          </td>

                          
                          <td><a href="{{route('comercialg.peligrosos.borrar' , array(  'idoferta'=>$peligroso->oferta_id,'idpeligroso'=>$peligroso->peligroso_id))}}" class="btn btn-danger btn-xs "> Eliminar </a></td>
                          
                        </tr>
      
                      @endforeach

                  </tbody>
                </table>

                      <td><input type="button" id="agregar2" value="Agregar fila" class="btn btn-success btn-xs"/></a></td><br>

              <div class="row">
                  <div class="col-lg-12">
                      <div class="col-xs-2">
                           {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                           {!! Form::close() !!}

                      </div>
                  </div>
              </div>

           </div>

  <!--    Fin peligrosos -->




   <!--  Inicio Aprovechables -->

                  {!! Form::open(['route' => ['comercialg.aprovechables', $ofertas], 'method' => 'PUT']) !!}
             <div class="col-xs-12"><h3 style="text-align: center;">Aprovechables</h3><hr> </div>
              <table class="table table-striped" id="tabla3">
                <thead>
 
                    <th>Nombre</th>
                    <th>Codigo Residuo</th>
                    <th>Cantidad en kg</th>
                    <th>Precio por kg</th>

                </thead>

                    <tbody>
                       
                         
                         <tr class="fila3">

                              <td> {!!Form::text('nombre []',null,['id'=>'searchnamea_1','class' => 'form-control searchnamea ','placeholder' =>'Ingrese aquí el nombre']) !!}</td>

                              <td>{!!Form::number('id [] ', null, ['id'=>'ida_1','class' => 'form-control ida ', 'readonly']) !!}</td>

                           <td>

                            <div class="input-group">
                                {!!Form::number('cantidad [] ', null, ['class' => 'form-control ', 'id'=>'cantidad_1']) !!}
                             </div>
                          </td>


                          <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('aprovechablesprecio [] ',null, ['class' => 'form-control ', 'id'=>'aprovechablesprecio_1']) !!}
                             </div>
                          </td>

                          
                          <td class="eliminar3 btn btn-warning btn-xs">Eliminar Fila</td>
                          
                        </tr>






    
                       @foreach($aprovechables as $aprovechable)
             
                          <tr class="fila3">

                              <td> {!!Form::text('nombre []',$aprovechable->nombre,['id'=>'searchnamea_1','class' => 'form-control searchnamea ', 'required']) !!}</td>

                              <td>{!!Form::number('id [] ', $aprovechable->aprovechables_id, ['id'=>'ida_1','class' => 'form-control id ', 'required', 'readonly']) !!}</td>

                           <td>

                            <div class="input-group">
                                {!!Form::number('cantidad [] ', $aprovechable->cantidad, ['class' => 'form-control ', 'id'=>'cantidad_1','required']) !!}
                             </div>
                          </td>


                          <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('aprovechablesprecio [] ', $aprovechable->precio, ['class' => 'form-control ', 'id'=>'aprovechablesprecio_1','required']) !!}
                             </div>
                          </td>

                          
                          <td><a href="{{route('comercialg.aprovechables.borrara' , array(  'idoferta'=>$aprovechable->oferta_id,'aprovechable'=>$aprovechable->aprovechables_id))}}" class="btn btn-danger btn-xs"> Eliminar </a></td>
                          
                        </tr>
      
                      @endforeach

                  </tbody>
                </table>

                      <td><input type="button" id="agregar3" value="Agregar fila" class="btn btn-success btn-xs"/></a></td><br>

              <div class="row">
                  <div class="col-lg-12">
                      <div class="col-xs-2">
                           {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                           {!! Form::close() !!}

                      </div>
                  </div>
              </div>

           

   <!--  Fin Aprovechables  -->




   <!-- Inicio posconsumos -->

        {!! Form::open(['route' => ['comercialg.posconsumos', $ofertas], 'method' => 'PUT']) !!}
             <div class="col-xs-12"><h3 style="text-align: center;"> Posconsumos </h3><hr> </div>
              <table class="table table-striped" id="tabla4">
                <thead>
 
                    <th>Nombre</th>
                    <th>Codigo Residuo</th>
                    <th>Cantidad en kg</th>
                    <th>Precio por kg</th>

                </thead>

                    <tbody>
                       
                         
                         <tr class="fila4">

                              <td> {!!Form::text('nombre []',null,['id'=>'searchnamepo_1','class' => 'form-control searchnamepo ','placeholder' =>'Ingrese aquí el nombre']) !!}</td>

                              <td>{!!Form::number('id [] ', null, ['id'=>'idpo_1','class' => 'form-control idpo ', 'readonly']) !!}</td>

                           <td>

                            <div class="input-group">
                                {!!Form::number('cantidad [] ', null, ['class' => 'form-control ', 'id'=>'cantidad_1']) !!}
                             </div>
                          </td>


                          <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('posconsumosprecio [] ',null, ['class' => 'form-control ', 'id'=>'posconsumosprecio_1']) !!}
                             </div>
                          </td>

                          
                          <td class="eliminar4 btn btn-warning btn-xs">Eliminar Fila</td>
                          
                        </tr>






    
                       @foreach($posconsumos as $posconsumo)
             
                          <tr class="fila4">

                              <td> {!!Form::text('nombre []',$posconsumo->nombre,['id'=>'searchnamea_1','class' => 'form-control searchnamepo ', 'required']) !!}</td>

                              <td>{!!Form::number('id [] ', $posconsumo->posconsumos_id, ['id'=>'idpo_1','class' => 'form-control idpo ', 'required', 'readonly']) !!}</td>

                           <td>

                            <div class="input-group">
                                {!!Form::number('cantidad [] ', $posconsumo->cantidad, ['class' => 'form-control ', 'id'=>'cantidad_1','required']) !!}
                             </div>
                          </td>


                          <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('posconsumosprecio [] ', $posconsumo->precio, ['class' => 'form-control ', 'id'=>'posconsumosprecio_1','required']) !!}
                             </div>
                          </td>

                          
                          <td><a href="{{route('comercialg.posconsumos.borrarpo' , array(  'idoferta'=>$posconsumo->oferta_id,'posconsumo'=>$posconsumo->posconsumos_id))}}" class="btn btn-danger btn-xs"> Eliminar </a></td>
                          
                        </tr>
      
                      @endforeach

                  </tbody>
                </table>

                      <td><input type="button" id="agregar4" value="Agregar fila" class="btn btn-success btn-xs"/></a></td><br>

              <div class="row">
                  <div class="col-lg-12">
                      <div class="col-xs-2">
                           {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                           {!! Form::close() !!}

                      </div>
                  </div>
              </div>

   <!-- Fin Posconsumos -->




   <!-- Inicio peligrosos aprovechables -->

        {!! Form::open(['route' => ['comercialg.peligrososa', $ofertas], 'method' => 'PUT']) !!}
             <div class="col-xs-12"><h3 style="text-align: center;"> Peligrosos Aprovechables </h3><hr> </div>
              <table class="table table-striped" id="tabla5">
                <thead>
 
                    <th>Nombre</th>
                    <th>Codigo Residuo</th>
                    <th>Cantidad en kg</th>
                    <th>Precio por kg</th>

                </thead>

                    <tbody>
                       
                         
                         <tr class="fila5">

                              <td> {!!Form::text('nombre []',null,['id'=>'searchnamepa_1','class' => 'form-control searchnamepa ','placeholder' =>'Ingrese aquí el nombre']) !!}</td>

                              <td>{!!Form::number('id [] ', null, ['id'=>'idpa_1','class' => 'form-control idpa ', 'readonly']) !!}</td>

                           <td>

                            <div class="input-group">
                                {!!Form::number('cantidad [] ', null, ['class' => 'form-control ', 'id'=>'cantidad_1']) !!}
                             </div>
                          </td>


                          <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('peligrososaprecio [] ',null, ['class' => 'form-control ', 'id'=>'peligrososaprecio_1']) !!}
                             </div>
                          </td>

                          
                          <td class="eliminar5 btn btn-warning btn-xs">Eliminar Fila</td>
                          
                        </tr>






    
                       @foreach($peligrososa as $peligrosoa)
             
                          <tr class="fila4">

                              <td> {!!Form::text('nombre []',$peligrosoa->nombre,['id'=>'searchnamepa_1','class' => 'form-control searchnamepa ', 'required']) !!}</td>

                              <td>{!!Form::number('id [] ', $peligrosoa->peligrososa_id, ['id'=>'idpa_1','class' => 'form-control idpo ', 'required', 'readonly']) !!}</td>

                           <td>

                            <div class="input-group">
                                {!!Form::number('cantidad [] ', $peligrosoa->cantidad, ['class' => 'form-control ', 'id'=>'cantidad_1','required']) !!}
                             </div>
                          </td>


                          <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('peligrosoaprecio [] ', $peligrosoa->precio, ['class' => 'form-control ', 'id'=>'peligrosoaprecio_1','required']) !!}
                             </div>
                          </td>

                          
                          <td><a href="{{route('comercialg.peligrosoa.borrarpa' , array(  'idoferta'=>$peligrosoa->oferta_id,'peligrosoa'=>$peligrosoa->peligrososa_id))}}" class="btn btn-danger btn-xs"> Eliminar </a></td>
                          
                        </tr>
      
                      @endforeach

                  </tbody>
                </table>

                      <td><input type="button" id="agregar5" value="Agregar fila" class="btn btn-success btn-xs"/></a></td><br>

              <div class="row">
                  <div class="col-lg-12">
                      <div class="col-xs-2">
                           {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                           {!! Form::close() !!}

                      </div>
                  </div>
              </div>

   <!-- Fin peligrosos aprovechables -->




   <!-- Inicio Especiales -->

        {!! Form::open(['route' => ['comercialg.especiales', $ofertas], 'method' => 'PUT']) !!}
             <div class="col-xs-12"><h3 style="text-align: center;"> Especiales </h3><hr> </div>
              <table class="table table-striped" id="tabla6">
                <thead>
 
                    <th>Nombre</th>
                    <th>Codigo Residuo</th>
                    <th>Cantidad en kg</th>
                    <th>Precio por kg</th>

                </thead>

                    <tbody>
                       
                         
                         <tr class="fila6">

                              <td> {!!Form::text('nombre []',null,['id'=>'searchnamee_1','class' => 'form-control searchnamee ','placeholder' =>'Ingrese aquí el nombre']) !!}</td>

                              <td>{!!Form::number('id [] ', null, ['id'=>'ide_1','class' => 'form-control ide ', 'readonly']) !!}</td>

                           <td>

                            <div class="input-group">
                                {!!Form::number('cantidad [] ', null, ['class' => 'form-control ', 'id'=>'cantidad_1']) !!}
                             </div>
                          </td>


                          <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('especialesprecio [] ',null, ['class' => 'form-control ', 'id'=>'especialesprecio_1']) !!}
                             </div>
                          </td>

                          
                          <td class="eliminar5 btn btn-warning btn-xs">Eliminar Fila</td>
                          
                        </tr>






    
                       @foreach($especiales as $especial)
             
                          <tr class="fila6">

                              <td> {!!Form::text('nombre []',$especial->nombre,['id'=>'searchnamepa_1','class' => 'form-control searchnamepa ', 'required']) !!}</td>

                              <td>{!!Form::number('id [] ', $especial->especiales_id, ['id'=>'ide_1','class' => 'form-control ide ', 'required', 'readonly']) !!}</td>

                           <td>

                            <div class="input-group">
                                {!!Form::number('cantidad [] ', $especial->cantidad, ['class' => 'form-control ', 'id'=>'cantidad_1','required']) !!}
                             </div>
                          </td>


                          <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('especialesprecio [] ', $especial->precio, ['class' => 'form-control ', 'id'=>'especialesprecio_1','required']) !!}
                             </div>
                          </td>

                          
                          <td><a href="{{route('comercialg.especiales.borrare' , array(  'idoferta'=>$especial->oferta_id,'especiales'=>$especial->especiales_id))}}" class="btn btn-danger btn-xs"> Eliminar </a></td>
                          
                        </tr>
      
                      @endforeach

                  </tbody>
                </table>

                      <td><input type="button" id="agregar6" value="Agregar fila" class="btn btn-success btn-xs"/></a></td><br>

              <div class="row">
                  <div class="col-lg-12">
                      <div class="col-xs-2">
                           {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                           {!! Form::close() !!}

                      </div>
                  </div>
              </div>

   <!-- Fin especiales -->



                
  <!--Inicio transporte-->

  {!! Form::open(['route' => ['comercialg.transporte', $ofertas], 'method' => 'PUT']) !!}
             <div class="col-xs-12"><h3 style="text-align: center;"> Transporte </h3><hr> </div>
              <table class="table table-striped" id="tabla7">
                <thead>
 
                    <th>Tipo</th>
                    <th>Codigo</th>
                    <th>Precio por kg</th>
                    <th>Precio Global</th>

                </thead>

                    <tbody>
                       
                         
                         <tr class="fila7">

                              <td> {!!Form::text('tipo []',null,['id'=>'searchnamet_1','class' => 'form-control searchnamet ','placeholder' =>'Ingrese aquí el nombre']) !!}</td>

                              <td>{!!Form::number('id [] ', null, ['id'=>'idt_1','class' => 'form-control ide ', 'readonly']) !!}</td>



                          <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('transportespreciokg [] ',null, ['class' => 'form-control ', 'id'=>'transportespreciokg_1']) !!}
                             </div>
                          </td>

                          <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('transportespreciog [] ',null, ['class' => 'form-control ', 'id'=>'transportespreciog_1']) !!}
                             </div>
                          </td>

                          
                          <td class="eliminar7 btn btn-warning btn-xs">Eliminar Fila</td>
                          
                        </tr>






    
                       @foreach($transportes as $transporte)
             
                          <tr class="fila7">

                              <td> {!!Form::text('tipo []',$transporte->tipo,['id'=>'searchnamet_1','class' => 'form-control searchnamet ', 'required']) !!}</td>

                              <td>{!!Form::number('id [] ', $transporte->transporte_id, ['id'=>'idt_1','class' => 'form-control idt ', 'required', 'readonly']) !!}</td>
                          <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('transportespreciokg [] ', $transporte->preciokg, ['class' => 'form-control ', 'id'=>'transportespreciokg_1']) !!}
                             </div>
                          </td>

                           <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('transportespreciog [] ', $transporte->preciog, ['class' => 'form-control ', 'id'=>'transportespreciog_1']) !!}
                             </div>
                          </td>

                          
                          <td><a href="{{route('comercialg.transporte.borrart' , array(  'idoferta'=>$transporte->oferta_id,'transporte'=>$transporte->transporte_id))}}" class="btn btn-danger btn-xs"> Eliminar </a></td>
                          
                        </tr>
      
                      @endforeach

                  </tbody>
                </table>

                      <td><input type="button" id="agregar7" value="Agregar fila" class="btn btn-success btn-xs"/></a></td><br>

              


     
                       

                         
              



                    </tbody>
             </table>


                <div class="row">
                    <div class="col-lg-12">
                      <div class="col-xs-2">
                        {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                        {!! Form::close() !!}

                    </div>
                </div>
              </div>

          

        <!--Final seccion transporte-->



        <!-- Inicio Pys -->

             {!! Form::open(['route' => ['comercialg.pys', $ofertas], 'method' => 'PUT']) !!}
             <div class="col-xs-12"><h3 style="text-align: center;">Productos y servicios</h3><hr> </div>
              <table class="table table-striped" id="tabla8">
                <thead>
 
                    <th>Nombre</th>
                    <th>Codigo</th>
                    <th>Precio</th>

                </thead>

                    <tbody>
                       
                         
                         <tr class="fila8">

                              <td> {!!Form::text('nombre []',null,['id'=>'searchnamepys_1','class' => 'form-control searchnamepys ','placeholder' =>'Ingrese aquí el nombre']) !!}</td>

                              <td>{!!Form::number('id [] ', null, ['id'=>'idpys_1','class' => 'form-control idpys ', 'readonly']) !!}</td>


                          <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('pysprecio [] ',null, ['class' => 'form-control ', 'id'=>'pysprecio_1']) !!}
                             </div>
                          </td>

                          
                          <td class="eliminar8 btn btn-warning btn-xs">Eliminar Fila</td>
                          
                        </tr>






                       @foreach($pys as $py)
             
                          <tr class="fila8">

                              <td> {!!Form::text('nombre []',$py->nombre,['id'=>'searchnamepys_1','class' => 'form-control searchnamepys ', 'required']) !!}</td>

                              <td>{!!Form::number('id [] ', $py->pys_id, ['id'=>'idpys_1','class' => 'form-control idpys ', 'required', 'readonly']) !!}</td>

                          <td>
                            <div class="input-group">
                              <span class="input-group-addon">$ </span>
                                {!!Form::number('pysprecio [] ', $py->precio, ['class' => 'form-control ', 'id'=>'pysprecio_1','required']) !!}
                             </div>
                          </td>

                          
                          <td><a href="{{route('comercialg.pys.borrarpys' , array(  'idoferta'=>$py->oferta_id,'idpys'=>$py->pys_id))}}" class="btn btn-danger btn-xs "> Eliminar </a></td>
                          
                        </tr>
      
                      @endforeach

                  </tbody>
                </table>

                      <td><input type="button" id="agregar8" value="Agregar fila" class="btn btn-success btn-xs"/></a></td><br>

              <div class="row">
                  <div class="col-lg-12">
                      <div class="col-xs-2">
                           {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                           {!! Form::close() !!}

                      </div>
                  </div>
              </div>

           

  <!-- Fin pys -->


            


                
  <!--Inicio otros-->

     <div class="col-xs-12"><h3 style="text-align: center;"> Otros </h3><hr> </div>
      <table class="table table-striped" id="tablao">
                    <thead>
                      <th>Nombre</th>
                      <th>Cantidad</th>

                    </thead>  

                  <tbody>

                    <tr>
                        
                 @foreach($otros as $otro)
                   <td>{{$otro->nombreotros}}</td>
                   <td>{{$otro->cantidadotros}}</td>
                   <td> <a href="{{route('otros.borraro', $otro->id)}} "  class=" btn btn-danger btn-xs"> Eliminar</a></td>

                   </tr>
              

                     @endforeach

                    </tbody>
             </table>

          </div>
        </div>

        <!-- Fin otros -->
           
          </div>
        </div>
      </div>
   
  
          
          
@endsection

@section('js')

<!-- Peligrosos -->
<script>

$(document).ready(function(){

$(document).on('keydown', '.searchname', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocomplete')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#id_' + index).val(ui.item.id);
    $('#peligrososprecio_' + index).val(ui.item.value2);
   


  }
    });

});

 
 // Add more
 $('#agregar2').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila2 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila2'><td><input id='searchname_"+index+"' class=' form-control searchname' required name='nombre_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='id form-control' readonly id='id_"+index+"' name='id_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidad_[]' ></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='peligrososprecio_[]' class= 'form-control peligrososprecio' id= 'peligrososprecio_"+index+"'></div></td> <td class='eliminar2 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla2 tbody').append(html);
 
 });

$(document).on("click",".eliminar2",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

 



//  jQuery(function($){ 


  

//   var clone = $("#tabla2 tbody tr:eq(0)").clone(true);
//   $('body').on('click', '#agregar2', function() {
//     var ParentRow = $("#tabla2 tbody tr").last();
//     clone.clone(true).insertAfter(ParentRow);

//     $('tr.fila2' '#' + id).autocomplete({  

                  

//      source: ' {!!URL::route('autocomplete')!!}',
//   minlenght: 1,
//   autoFocus: true,

//   select: function(e,ui){



//     $('#id').val(ui.item.id).clone(true);
//     // $('#nombre').val(ui.item.value);
   


//   }
//     });
    
// enable_autocomplete(ClonedField);


//   });
//    $(document).on("click",".eliminar2",function(){
//     var parent = $(this).parents().get(0);
//     $(parent).remove();

// });
// });


</script>


 <!-- Aprovechables -->
  <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamea', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletea')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#ida_' + index).val(ui.item.id);
    $('#aprovechablesprecio_' + index).val(ui.item.value2);
   


  }
    });

});

 
 // Add more
 $('#agregar3').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila3 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila3'><td><input id='searchnamea_"+index+"' class=' form-control searchnamea' required name='nombre_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='id form-control' readonly id='ida_"+index+"' name='id_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidad_[]' ></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='aprovechablesprecio_[]' class= 'form-control aprovechablesprecio' id= 'aprovechablesprecio_"+index+"'></div></td> <td class='eliminar3 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla3 tbody').append(html);
 
 });

$(document).on("click",".eliminar3",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>


  <!-- Posconsumos -->
  <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamepo', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletepo')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idpo_' + index).val(ui.item.id).clone(true);
    $('#posconsumosprecio_' + index).val(ui.item.value2);
   


  }
    });

});

 
 // Add more
 $('#agregar4').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila4 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila4'><td><input id='searchnamepo_"+index+"' class=' form-control searchnamepo' required name='nombre_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='idpo form-control' readonly id='idpo_"+index+"' name='id_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidapo_[]' ></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='posconsumosprecio_[]' class= 'form-control posconsumosprecio' id= 'posconsumosprecio_"+index+"'></div></td> <td class='eliminar4 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla4 tbody').append(html);
 
 });

$(document).on("click",".eliminar4",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>


  <!-- Peligrosos Aprovechables -->
  <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamepa', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletepa')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idpa_' + index).val(ui.item.id).clone(true);
    $('#peligrososaprecio_' + index).val(ui.item.value2);
   


  }
    });

});

 
 // Add more
 $('#agregar5').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila5 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila5'><td><input id='searchnamepa_"+index+"' class=' form-control searchnamepa' required name='nombre_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='idpa form-control' readonly id='idpa_"+index+"' name='id_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidapa_[]' ></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='peligrososaprecio_[]' class= 'form-control peligrososaprecio' id= 'peligrososaprecio_"+index+"'></div></td> <td class='eliminar5 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla5 tbody').append(html);
 
 });

$(document).on("click",".eliminar5",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>




  <!-- Especiales -->
  <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamee', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletee')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#ide_' + index).val(ui.item.id).clone(true);
    $('#especialesprecio_' + index).val(ui.item.value2);
   


  }
    });

});

 
 // Add more
 $('#agregar6').click(function(){
   
 

  // Get last id 
  var lastname_id = $('.fila6 input[type=text]:nth-child(1)').last().attr('id');

  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila6'><td><input id='searchnamee_"+index+"' class=' form-control searchnamee' required name='nombre_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='id form-control' readonly id='ide_"+index+"' name='id_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidad_[]' ></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='especialesprecio_[]' class= 'form-control especialesprecio' id= 'especialesprecio_"+index+"'></div></td> <td class='eliminar6 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla6 tbody').append(html);
 
 });

$(document).on("click",".eliminar6",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>

<!-- Transporte  -->

<script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamet', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletet')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idt_' + index).val(ui.item.id);
    $('#transportespreciokg_' + index).val(ui.item.value2);
    $('#transportespreciog_' + index).val(ui.item.value3);
   


  }
    });

});

 
 // Add more
 $('#agregar7').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila7 input[type=text]:nth-child(1)').last().attr('id');


  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila7'><td><input id='searchnamet_"+index+"' class=' form-control searchnamet' required name='tipo_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='idt form-control' readonly id='idt_"+index+"' name='id_[]' ></td><td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='transportespreciokg_[]' class= 'form-control transportespreciokg' id= 'transportespreciokg_"+index+"'></div></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='transportespreciog_[]' class= 'form-control transportespreciog' id= 'transportespreciog_"+index+"'></div></td> <td class='eliminar6 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla7 tbody').append(html);
 
 });

$(document).on("click",".eliminar7",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

</script>

<!-- PYS -->
<script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamepys', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletepys')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idpys_' + index).val(ui.item.id);
    $('#pysprecio_' + index).val(ui.item.value2);
   


  }
    });

});

 
 // Add more
 $('#agregar8').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila8 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila8'><td><input id='searchnamepys_"+index+"' class=' form-control searchnamepys' required name='nombre_[]'  type='text'  placeholder='Ingrese el nombre'></td><td><input type='number' class='idpys form-control' readonly id='idpys_"+index+"' name='id_[]' ></td><td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='pysprecio_[]' class= 'form-control pysprecio' id= 'pysprecio_"+index+"'></div></td> <td class='eliminar8 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla8 tbody').append(html);
 
 });

$(document).on("click",".eliminar8",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>













<!-- <script type="text/javascript">
  
$('#searchname').autocomplete({

  source: ' {!!URL::route('autocomplete')!!}',
  minlenght: 1,
  autoFocus: true,
  select: function(e,ui){

    $('#id').val(ui.item.id);
    $('#nombre').val(ui.item.value);

  }






});
</script>
 -->




@endsection


   

   



