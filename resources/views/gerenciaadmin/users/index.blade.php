@extends('admin.template.gerenciaadmin.main')

@section('content')

@section('titulo', 'Usuarios')



<div class="col-xs-12">
      <div class="card">
        <div class="card-header">
         Todos los usuarios
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Codigo usuario</th>
            <th>Nombre</th>
            <th>Email</th>
            <th>Tipo</th>
            <th>Fecha y hora<br>de creación</th>
            <th>Acciones</th>

        </tr>
    </thead>
    <tbody>


      @foreach($users as $user)

     
	 <tr> 
 
    	 <td>{{$user->id}}</td>
    	 <td>{{$user->name}}</td>
    	 <td>{{$user->email}}</td>
       <td>{{$user->type}}</td>
       <td>{{$user->created_at}}</td>

	 <td><a href="{{ route('ger.users.destroy', $user->id ) }}" onclick="return confirm('¿Seguro desea eliminarlo?')" class="btn btn-danger btn-xs" ><span class="fa fa-trash"></a> <a href="{{ route('users.edit', $user->id ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a></td>



	 </tr>
	



	 @endforeach

	 



      
           
          

           
           

          

             

    

            
     
        
       
    </tbody>
</table>
        </div>
      </div>
    </div>
 
<!--MIERDA-->

  






@endsection