@extends('admin.template.gerenciaadmin.main')

@section('titulo')
Usuarios

@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body app-heading">
          
          <div class="app-title">
            <div class="title"><span class="highlight">Editar Usuario</span></div>
            <div class="description">{{$users->name}}</div>
          
          </div>
        </div>
      </div>
    </div>
   
    <div class="col-md-12">
      @if ( count( $errors ) > 0 )
            <div class="alert alert-warning" role="alert">
               @foreach ($errors->all() as $error)
                  <div>{{ $error }}</div>
              @endforeach
            </div>
        @endif </br>

      <div class="card card-tab card-mini">
        <div class="card-header">
          <ul class="nav nav-tabs">
            <li role="tab1" class="active">

              <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">General</a>
              
            </li>
          </ul>
        </div>
        <div class="card-body tab-content no-padding">
          <div role="tabpanel" class="tab-pane active" id="tab1">

            <div class="row">
            {!! Form::open(['route' => ['users.update',$users], 'method' => 'PUT']) !!}
               
                  <div class="form-group">
                    <div class="col-xs-3">
                      {!! Form::label('name','Nombre y apellido')!!}
                  
                      {!!Form::text('name',$users->name, ['class' => 'form-control ', 'placeholder' => 'Nombre y Apellido' , 'required']) !!}
                    </div>
                  </div>


                    <div class="col-xs-3">
                   {!! Form::label('email','E-Mail')!!}
                   {!!Form::email('email',$users->email, ['class' => 'form-control ', 'placeholder' => 'E - mail' , 'required'])!!}
                  </div>

                
                            
                            

                            <div class="col-xs-3">
                              {!! Form::label('password','Contraseña')!!}
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                      

                  
                            

                            <div class="col-xs-3">
                              {!! Form::label('password-confirm','Confirmar Contraseña')!!}
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                       
                  <div class="col-xs-3">
                   {!! Form::label('type','Tipo')!!}
                    {!!Form::select('type',['comercial'=>'comercial','gerenciaadmin'=> 'gerenciaadmin'], null,['class' => 'form-control ', 'required'])!!}
                  </div>
            </div>
            <br>
            <div class="row">
              <div class="col-xs-2">
                {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                {!! Form::close() !!}
                <br><br><br><br>
              </div>
            </div>
          </div>

      </div>


    </div>
  </div>

</div>
   
    <br><br><br><br><br><br><br><br>
@endsection

@section('js')





@endsection


   

   



