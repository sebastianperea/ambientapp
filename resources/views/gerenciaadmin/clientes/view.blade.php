
@extends('admin.template.gerenciaadmin.main')

@section('content')

@section('titulo', 'Clientes')
<div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body app-heading">
          <!-- <div class="icon">
            <i class="fa fa-bell" aria-hidden:"true"></i>
          </div> -->
          <div class="app-title">
            <div class="title"><span class="highlight">{{$clientes->razon_social}}</span></div>
            <div class="description">{{$clientes->identificacion}}-{{$clientes->dv}} <br>{{$clientes->estado}}</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card card-tab">
        <div class="card-header">
          <ul class="nav nav-tabs">
            <li role="tab1" class="active">
              <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Perfil</a>
            </li>
            <li role="tab3">
              <a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Archivos</a>
            </li>
            <li role="tab4">
              <a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">Residuos</a>
            </li>

            <li role="tab5">
              <a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">Sedes</a>
            </li>
          </ul>
        </div>
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
              <div class="col-md-3 col-sm-12">
                <div class="section">

                  <div class="section-title"><i class="icon fa fa-user" aria-hidden="true"></i> Datos generales</div>
                  <div class="section-body __indent">Departamento: {{$clientes->departamento}} <br> Ciudad: {{$clientes->ciudad}}<br> Dirección: {{$clientes->direccion}} <br>{{$clientes->telefono_contacto}} Ext: {{$clientes->ext}}<br> Tipo: {{$clientes->tipo}}</div>
                </div>
                <div class="section">
                  <div class="section-title"><i class="icon fa fa-book" aria-hidden="true"></i> Contácto</div>
                  <div class="section-body __indent">{{$clientes->nombres}}<br>{{$clientes->apellidos}}<br>{{$clientes->cargo}}<br>{{$clientes->telefonocelular}} <br>{{$clientes->email}}</div>
                </div>
              </div>
              <div class="col-md-9 col-sm-12">
                <div class="section">
                  <div class="section-title">Ofertas</div>
                  <div class="section-body">
                    <div class="media social-post">
                      <div class="media-left">
                        <a href="#">
                          <img src="../assets/images/profile.png" />
                        </a>
                      </div>
                      
                    </div>
                    <div class="media social-post">
                      
                      <div class="media-body">
                        
                        <table class="datatable table table-striped primary" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                            
                        <th>Id oferta</th>
                        <th>Estado</th>
                        <th>Fecha y hora</th>
                        <th>Acción</th>

                              </tr>
                          </thead>
                          <tbody>
                           

                           @foreach($ofertas as $oferta)
                         <tr> 
                          <td> {{$oferta->id}}</td>
                         <td> @if($oferta->Estado=="En espera") 
                         <span class="label label-warning">{{$oferta->Estado}}</span>
                         @else
                      <span class="label label-primary">{{$oferta->Estado}}</span>
                         @endif</td>
                         <td>{{$oferta->created_at}}</td>

                         <td> <a href="{{ route('ofertasg.edit', $oferta->id ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a> <a href="{{route('ofertasg.pdf' , $oferta->id )}}" target="_blank" class="btn btn-success btn-xs" ><span class="fa fa-eye"> </a> <a href="{{route('ofertasg.pdf.download' , $oferta->id )}}" target="" class="btn btn-primary btn-xs" ><span class="fa fa-arrow-down"> </a></td>
                         </tr>
                        @endforeach
        
       
    </tbody>
</table>
                        
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="tab3">
            
      <div class="card">


       
        <div class="card-body">

        @if($clientes->tipodepersona == 'juridica') 
            <div class="section">
                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i> Persona {{$clientes->tipodepersona}}</div>
                  <div class="section-body __indent"></div>
                </div>
             
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img src="{{asset('plugins/images/addfile.png')}}" />
              </a>
            </div>
              <div class="media-body">
                  
        {!! Form::open(['route' => ['comercial.files.storecomercial', $clientes->identificacion],'files' => true, 'enctype'=>'multipart/form-data','method' => 'PUT' ]) !!}



        <h4 class="seccionarchivospj">ADJUNTAR ARCHIVOS </h4>

          <div>
                Tipo de Persona: <input type="radio" name="tipodepersona" class="tipodepersona" value="natural"> Natural
                <input type="radio" name="tipodepersona" class="tipodepersona" value="juridica"> Juridica

          </div><br>

            
              <div class="seccionarchivos" style="display: none;"><!-- inicio archivos-->
              <h4 class="seccionarchivos">ADJUNTAR ARCHIVOS (Persona Natural)</h4>
                  <p class="seccionarchivos">Luego de haber llenado todos los campos correctamente por favor adjunte los archivos requeridos respectivamente</p><br>

                  

                <p class="seccionarchivos">Fotocopia de la cedula:<br><br>
                {!! Form::file('fotocopiacpn', ['class' => 'imprimirboton'])!!}<br></p>

                <p class="seccionarchivos">Registro Unico Tributario (RUT)<br><br>
                {!! Form::file('rutpn', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>

                <p class="seccionarchivos">Certificado de matricula mercantil no mayor a 30 días (Si aplica)<br><br>
                {!! Form::file('certificadodeexistencia', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>

                <p class="seccionarchivos">Certificación Bancaria<br><br>
                {!! Form::file('certificacionbancaria', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>


                <hr style="height: 1.5px; border:none; background-color:#E6E20D ; width: 350px; margin-top: 40px;">
               <p> &nbsp; &nbsp; &nbsp;Requiere aprobación de credito: <input type="radio" name="creditopn" value="si" class="creditopn"> Si
                <input type="radio" name="creditopn" value="no" class="creditopn"> No </p><br>

                <div class="creditopn2" style="display: none;">
                <p class="seccionarchivos credito">Para solicitud de cupo de credito por favor adjunte la siguiente<br> información si es persona natural:</p><br>

                      <p class="seccionarchivos credito">Declaración de renta (Si declara) o extractos 3 ultimos meses<br><br>
                {!! Form::file('declaracionpn', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>
                </div>

                      </div><!-- Fin Archivos -->

                      <div class="seccionarchivospj" style="display: none;"><!-- inicio archivos-->
              <h4 class="seccionarchivospj">ADJUNTAR ARCHIVOS (Persona Jurídica)</h4>
                  <p class="seccionarchivospj">Luego de haber llenado todos los campos correctamente por favor adjunte los archivos requeridos respectivamente</p><br>

                  

                <p class="seccionarchivospj">Fotocopia de la cedula representante legal:<br><br>
                {!! Form::file('fotorepl', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>

                <p class="seccionarchivospj">Registro Unico Tributario (RUT)<br><br>
                {!! Form::file('rutpj', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>

                <p class="seccionarchivospj">Certificado de existencia y representación legal no mayor a 30 días <br><br>
                {!! Form::file('certificadoexistencia', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>

                <p class="seccionarchivospj">Certificación Bancaria<br><br>
                {!! Form::file('certificacionbancariapj', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>

                







                    <hr style="height: 1.5px; border:none; background-color:#E6E20D ; width: 350px; margin-top: 40px;">

                    <p> &nbsp; &nbsp; &nbsp;Requiere aprobación de credito: <input type="radio" name="creditopj" value="si" class="creditopj"> Si
                <input type="radio" name="creditopj" value="no" class="creditopj"> No </p><br>

                      <div class="creditopj2" style="display: none;">
                    <p class="seccionarchivospj">Para solicitud de cupo de credito por favor adjunte la siguiente<br> información si es persona jurídica:</p><br>

                      <p class="seccionarchivospj">Declaración de renta<br><br>
                {!! Form::file('declaracionpj', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>
                 <p class="seccionarchivospj">Estados financieros<br><br>
                {!! Form::file('estadospj', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>

                </div>
        </div><!-- Fin Archivos -->

                <p class="">F.Creación de clientes<br><br>
                {!! Form::file('creaciondeclientes', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>
               

                <p class="">F.información de residuos<br><br>
                {!! Form::file('inforesiduos', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>
                <input type="submit" value="Enviar" class="btn btn-success btn-xs">

                {!! Form::close() !!}

              </div>
        </div> 
           @if(count($cbancariapj)>0)
          <div class="section">
                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Certificados bancarios</div>
                  <div class="section-body __indent"></div>
                </div>
                @foreach($cbancariapj as $cbanpj)
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img src="{{asset('plugins/images/files4.jpg')}}" />
              </a>
            </div>
              <div class="media-body">
                <h4 class="media-heading">{{$cbanpj->nombre}}</h4>
                <p>{{$cbanpj->created_at}}</p>
                <div><a href=" {{route('viewfiles' , $cbanpj->nombre )}}" target="_blank" class="btn btn-success btn-xs" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $cbanpj->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
              </div>
        </div>
          @endforeach

          @endif

           @if(count($cerlpj)>0)  
          <div class="section">
                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Certificados de existencia</div>
                  <div class="section-body __indent"></div>
                </div>
                @foreach($cerlpj as $cerpj)
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img src="{{asset('plugins/images/files4.jpg')}}" />
              </a>
            </div>
              <div class="media-body">
                <h4 class="media-heading">{{$cerpj->nombre}}</h4>
                <p>{{$cerpj->created_at}}</p>
                <div><a href=" {{route('viewfiles' , $cerpj->nombre )}}" target="_blank" class="btn btn-success btn-xs" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $cerpj->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
              </div>
        </div>
          @endforeach

          @endif

          @if(count($fotocopiacpj)>0)  
          <div class="section">
                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Fotocopia cedula representante legal</div>
                  <div class="section-body __indent"></div>
                </div>
                @foreach($fotocopiacpj as $fotopj)
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img src="{{asset('plugins/images/files4.jpg')}}" />
              </a>
            </div>
              <div class="media-body">
                <h4 class="media-heading">{{$fotopj->nombre}}</h4>
                <p>{{$fotopj->created_at}}</p>
                <div><a href=" {{route('viewfiles' , $fotopj->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $fotopj->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
              </div>
        </div>
          @endforeach

          @endif

          @if(count($rutpj)>0)  
          <div class="section">
                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Rut</div>
                  <div class="section-body __indent"></div>
                </div>
                @foreach($rutpj as $rtpj)
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img src="{{asset('plugins/images/files4.jpg')}}" />
              </a>
            </div>
              <div class="media-body">
                <h4 class="media-heading">{{$rtpj->nombre}}</h4>
                <p>{{$rtpj->created_at}}</p>
                <div><a href=" {{route('viewfiles' , $rtpj->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $rtpj->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
              </div>
        </div>
          @endforeach

          @endif


          @if(count($drentapj)>0)  
          <div class="section">
                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Declaración de renta </div>
                  <div class="section-body __indent"></div>
                </div>
                @foreach($drentapj as $drpj)
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img src="{{asset('plugins/images/files4.jpg')}}" />
              </a>
            </div>
              <div class="media-body">
                <h4 class="media-heading">{{$drpj->nombre}}</h4>
                <p>{{$drpj->created_at}}</p>
                <div><a href=" {{route('viewfiles' , $drpj->nombre )}}"  class="btn btn-success btn-xs" target="_blank"role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $drpj->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
              </div>
        </div>
          @endforeach

          @endif


           @if(count($estadosfinancieros)>0)  
          <div class="section">
                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Estados Financieros</div>
                  <div class="section-body __indent"></div>
                </div>
                @foreach($estadosfinancieros as $estados)
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img src="{{asset('plugins/images/files4.jpg')}}" />
              </a>
            </div>
              <div class="media-body">
                <h4 class="media-heading">{{$estados->nombre}}</h4>
                <p>{{$estados->created_at}}</p>
                <div><a href=" {{route('viewfiles' , $estados->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $estados->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
              </div>
        </div>
          @endforeach

          @endif
    @endif     
          <br>  
             

          @if($clientes->tipodepersona == 'natural')   
             <div class="section">
                  <div class="section-title" style="center"> Persona Natural</div>
                  <div class="section-body __indent"></div>
                </div>


      @if(count($cbancariapn)>0)  
          <div class="section">
                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Certificación Bancaria</div>
                  <div class="section-body __indent"></div>
                </div>
                @foreach($cbancariapn as $cbancaria)
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img src="{{asset('plugins/images/files4.jpg')}}" />
              </a>
            </div>
              <div class="media-body">
                <h4 class="media-heading">{{$cbancaria->nombre}}</h4>
                <p>{{$cbancaria->created_at}}</p>
                <div><a href=" {{route('viewfiles' , $cbancaria->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $cbancaria->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
              </div>
        </div>
          @endforeach

          @endif

          

          @if(count($cmpn)>0)  
          <div class="section">
                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Certificado mercantil </div>
                  <div class="section-body __indent"></div>
                </div>
                @foreach($cmpn as $cm)
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img src="{{asset('plugins/images/files4.jpg')}}" />
              </a>
            </div>
              <div class="media-body">
                <h4 class="media-heading">{{$cm->nombre}}</h4>
                <p>{{$cm->created_at}}</p>
                <div><a href=" {{route('viewfiles' , $cm->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $cm->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
              </div>
        </div>
          @endforeach

          @endif

           @if(count($drentapn)>0)  
          <div class="section">
                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Declaración de renta </div>
                  <div class="section-body __indent"></div>
                </div>
                @foreach($drentapn as $drenta)
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img src="{{asset('plugins/images/files4.jpg')}}" />
              </a>
            </div>
              <div class="media-body">
                <h4 class="media-heading">{{$drenta->nombre}}</h4>
                <p>{{$drenta->created_at}}</p>
                <div><a href=" {{route('viewfiles' , $drenta->nombre )}}"  class="btn btn-success btn-xs" target="_blank"role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $drenta->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
              </div>
        </div>
          @endforeach

          @endif

          @if(count($fotocopiacpn)>0)  
          <div class="section">
                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Fotocopia cedula </div>
                  <div class="section-body __indent"></div>
                </div>
                @foreach($fotocopiacpn as $foto)
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img src="{{asset('plugins/images/files4.jpg')}}" />
              </a>
            </div>
              <div class="media-body">
                <h4 class="media-heading">{{$foto->nombre}}</h4>
                <p>{{$foto->created_at}}</p>
                <div><a href=" {{route('viewfiles' , $foto->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $foto->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
              </div>  
        </div>
          @endforeach

          @endif



          @if(count($rutpn)>0)  
          <div class="section">
                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i> Rut persona natural</div>
                  <div class="section-body __indent"></div>
                </div>
                @foreach($rutpn as $rt)
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img src="{{asset('plugins/images/files4.jpg')}}" />
              </a>
            </div>
              <div class="media-body">
                <h4 class="media-heading">{{$rt->nombre}}</h4>
                <p>{{$rt->created_at}}</p>
                <div><a href=" {{route('viewfiles' , $rt->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $rt->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
              </div>
        </div>
          @endforeach

          @endif
      @endif    


        </div>
      </div>
    
        </div>
          <div role="tabpanel" class="tab-pane" id="tab4">
            <div class="card">


       
        <div class="card-body">

        <table class="datatable table table-striped primary" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                            
                        <th>Id Residuo</th>
                        <th>Nombre residuo</th>
                        <th>Fecha y hora</th>
                        <th>Tipo</th>
                              </tr>
                          </thead>
                          <tbody>
                           
                      @if(count($peligroso)>0)
                             @foreach($peligroso as $peligro)

                         
                         <tr> 
                          <td> {{$peligro->id}}</td>
                          <td> {{$peligro->nombre}}</td>

                         <td>{{$peligro->created_at}}</td>
                         <td>Peligroso</td>

                         
                         </tr>
                        @endforeach
        
                      @endif


                      @if(count($peligrosoa)>0)
                             @foreach($peligrosoa as $peligroa)

                         
                         <tr> 
                          <td> {{$peligroa->id}}</td>
                          <td> {{$peligroa->nombre}}</td>

                         <td>{{$peligroa->created_at}}</td>
                         <td>Peligroso Aprovechable</td>

                         
                         </tr>
                        @endforeach
        
                      @endif


                       @if(count($especiales2)>0)
                             @foreach($especiales2 as $especiales)

                         
                         <tr> 
                          <td> {{$especiales->id}}</td>
                          <td> {{$especiales->nombre}}</td>

                         <td>{{$especiales->created_at}}</td>
                         <td>Especiales</td>

                         
                         </tr>
                        @endforeach
        
                      @endif

          </tbody>
          </table>
          @if(count($filespeligrosos)>0)
         <div class="section">
                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i> Peligrosos</div>
                  <div class="section-body __indent"></div>
                </div>
                @foreach($filespeligrosos as $peligrosos)
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img src="{{asset('plugins/images/files4.jpg')}}" />
              </a>
            </div>
              <div class="media-body">
                <h4 class="media-heading">{{$peligrosos->nombre}}</h4>
                <p>{{$peligrosos->created_at}}</p>
                <div><a href=" {{route('viewfilesresiduos' , $peligrosos->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefilesresiduos' , $peligrosos->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
              </div>
        </div>
        @endforeach

       @endif


       @if(count($filespeligrososa)>0)
         <div class="section">
                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i> Peligrosos Aprovechables</div>
                  <div class="section-body __indent"></div>
                </div>
                @foreach($filespeligrososa as $peligrososa)
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img src="{{asset('plugins/images/files4.jpg')}}" />
              </a>
            </div>
              <div class="media-body">
                <h4 class="media-heading">{{$peligrososa->nombre}}</h4>
                <p>{{$peligrososa->created_at}}</p>
                <div><a href=" {{route('viewfilesresiduos' , $peligrososa->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefilesresiduos' , $peligrososa->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
              </div>
        </div>
        @endforeach

       @endif


       @if(count($especiales3)>0)
         <div class="section">
                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i> Especiales</div>
                  <div class="section-body __indent"></div>
                </div>
                @foreach($especiales3 as $especial)
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img src="{{asset('plugins/images/files4.jpg')}}" />
              </a>
            </div>
              <div class="media-body">
                <h4 class="media-heading">{{$especial->nombre}}</h4>
                <p>{{$especial->created_at}}</p>
                <div><a href=" {{route('viewfilesresiduos' , $especial->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefilesresiduos' , $especial->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
              </div>
        </div>
        @endforeach

       @endif
        </div>
      </div>

          </div>

          <div role="tabpanel" class="tab-pane" id="tab5">
            <div class="card">


       
        <div class="card-body">
            
            <!-- Inicio Peligrosos -->

             {!! Form::open(['route' => ['clientes.sedes.save', $clientes->identificacion], 'method' => 'PUT']) !!}
             <div class="col-xs-12"><h3 style="text-align: center;">Sedes</h3><hr> </div>
              <table class="table table-striped" id="tabla">
                <thead>
                    <th>Id Cliente</th>
                    <th>Nombre</th>
                    <th>Dirección</th>
                    <th>Departamento</th>
                    <th>Ciudad</th>
                </thead>
                    <tbody>
             @foreach($sedes as $sede)
                          <tr class="fila">
                              <td> {!!Form::number('identificacion []',$sede->identificacion,['id'=>'searchname_1','class' => 'form-control searchname ', 'required', 'disabled']) !!}</td>
                              <td>{!!Form::text('nombre [] ', $sede->nombre, ['id'=>'id_1','class' => 'form-control id ', 'required', 'disabled']) !!}</td>
                           <td>
                                {!!Form::text('direccion [] ', $sede->direccion, ['class' => 'form-control ', 'id'=>'direccion_1','required', 'disabled']) !!}
                          </td>
                          <td>
                                {!!Form::text('departamento [] ', $sede->departamento, ['class' => 'form-control ', 'id'=>'departamento_1','required', 'disabled']) !!}
                          </td>
                          <td>
                                {!!Form::text('ciudad [] ', $sede->ciudad, ['class' => 'form-control ', 'id'=>'cantidad_1','required', 'disabled']) !!}
                          </td>

                          <td><a href="{{route('comercial.sedes.borrar' , $sede->id)}}" class="btn btn-danger btn-xs "> Eliminar </a></td>     

                        </tr>


                        
              @endforeach

              <tr class="fila">
                              <td> {!!Form::number('identificacion []',$clientes->identificacion,['id'=>'searchname_1','class' => 'form-control searchname ','readonly']) !!}</td>
                              <td>{!!Form::text('nombre [] ', null, ['id'=>'nombre','class' => 'form-control id ', 'required']) !!}</td>
                           <td>
                                {!!Form::text('direccion [] ', null, ['class' => 'form-control ', 'id'=>'direccion_1','required']) !!}
                          </td>
                          <td>
                                {!!Form::text('departamento [] ', null, ['class' => 'form-control ', 'id'=>'departamento','required']) !!}
                          </td>
                          <td>
                                {!!Form::text('ciudad [] ', null, ['class' => 'form-control ', 'id'=>'cantidad_1','required']) !!}
                          </td>

                            <td class="eliminar btn btn-warning btn-xs">Eliminar Fila</td>   

                        </tr>


                  </tbody>
                </table>

                      <td><input type="button" id="agregar" value="Agregar fila" class="btn btn-success btn-xs"/></a></td><br>





                    

              <div class="row">
                  <div class="col-lg-12">
                      <div class="col-xs-2">
                           {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                           {!! Form::close() !!}

                      </div>
                  </div>
              </div>

           </div>

  <!--    Fin peligrosos -->
        
          
        </div>
      </div>

          </div>
        </div>
      </div>
    </div>
  </div>


@endsection
@section('js')
<script>
      $(document).ready(function(){

        $(".tipodepersona").click(function(evento){
              
            var valor = $(this).val();

          
            if(valor == 'natural'){

                $(".seccionarchivos").show();
                $(".seccionarchivospj").hide();
            }else{

                $(".seccionarchivospj").show();
                $(".seccionarchivos").hide();
            }
              });
          });
    </script>

    <script>
      $(document).ready(function(){

        $(".creditopn").click(function(evento){
              
            var valor = $(this).val();

            if(valor == 'si'){
              $(".creditopn2").show();
                
            }
            else{

                $(".creditopn2").hide();
            }
              });
          });
    </script>
  

    <script>
      $(document).ready(function(){

        $(".creditopj").click(function(evento){
              
            var valor = $(this).val();

            if(valor == 'si'){
              $(".creditopj2").show();
                
            }
            else{

                $(".creditopj2").hide();
            }
              });
          });
    </script>
   

   <script type="text/javascript">
    

  $(document).ready(function(){



 
 // Add more
 $('#agregar').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila'><td><input id='identificacion' class=' form-control' required name='identificacion_[]' type='text' value= '{{$clientes->identificacion}}' readonly></td><td><input type='text' class='form-control' id='nombre' name='nombre_[]'></td><td><input type='text' class='form-control' id='direccion' name='direccion_[]'></td> <td><input type='text' class='form-control' id='departamento' name='departamento_[]'></td> <td><input type='text' class='form-control' id='ciudad' name='ciudad_[]'></td><td class='eliminar btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla tbody').append(html);
 
 });

$(document).on("click",".eliminar",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>

@endsection

