@extends('admin.template.main')

@section('content')

@section('titulo', 'Clientes')
<table class="table table-striped">
<thead>
 
  <th>Identificación</th>
  <th>Razón Social</th>
  <th>Fecha de creación</th>
  <th>Estado</th>
  <th>Fecha y hora</th>
  <th>Acción</th>



</thead>

<tbody>
	


	

     @foreach($clientes as $cliente)

	 <tr> 
 
	 <td>{{$cliente->identificacion}}</td>
	 
	 <td>{{$cliente->razon_social}}</td>
	 
	 <td>{{$cliente->created_at}}</td>

	 <td> <a href="{{ route('comercial.clientes.edit', $cliente->identificacion ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a></td>
	 
	 



	 </tr>



	 @endforeach

	 




</tbody>
</table>
{!! $clientes->render() !!}

@endsection