@extends('admin.template.main')

@section('content')

@section('titulo', 'Editar Oferta ' .  $ofertas->id_cliente)

  <div class="row">
    <div class="col-lg-12">
      <div class="card card-tab">
        
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
              
                <div class="section">
                <div class="section-title"><i class="icon fa fa-user" aria-hidden="true"></i> Crear Ofertas Comerciales</div>
{!! Form::open(['route' => 'comercial.ofertas.update', 'method' => 'PUT']) !!}
               
               
               <div class="row">

    <div class="col-xs-3">
                   {!! Form::label('','')!!}
              {!!Form::number('identificacion',null, ['class' => 'form-control', 'placeholder' => 'Identificación' , 'required']) !!}
                  </div>

                  <div class="col-xs-3">

              {!! Form::label('tipo_de_identificacion','Tipo de identificación')!!}
              {!!Form::select('tipo_de_identificacion', ['Seleccione','NIT' => 'NIT','CC'=> 'CC', 'PAS' =>'PAS', 'CE' => 'CE'],$tipo,['class' => 'form-control', 'required'])!!}

                   </div>


                  <div class="col-xs-1">
                   {!! Form::label('','')!!}
              {!!Form::number('dv',null, ['class' => 'form-control', 'placeholder' => 'DV' , 'required']) !!}
                  </div>

                  <div class="col-xs-5">
                   {!! Form::label('','')!!}
                   {!!Form::text('razon_social',null, ['class' => 'form-control', 'placeholder' => 'Razón social' , 'required'])!!}
                  </div>
                  <div class="col-xs-3">
                   {!! Form::label('','')!!}
                   {!!Form::text('departamento',null, ['class' => 'form-control', 'placeholder' => 'Departamento' , 'required'])!!}
                  </div>

                  <div class="col-xs-3">
                   {!! Form::label('','')!!}
                   {!!Form::text('ciudad',null, ['class' => 'form-control', 'placeholder' => 'Ciudad' , 'required'])!!}
                  </div>

                  <div class="col-xs-6">
                   {!! Form::label('','')!!}
                   {!!Form::text('direccion',null, ['class' => 'form-control', 'placeholder' => 'Dirección' , 'required'])!!}
                  </div>

                  

                  <div class="col-xs-3">
                   {!! Form::label('','')!!}
                   {!!Form::text('Nombre_contacto',null, ['class' => 'form-control', 'placeholder' => 'Nombre Contácto' , 'required'])!!}
                  </div>

                  <div class="col-xs-3">
                   {!! Form::label('','')!!}
                   {!!Form::text('Apellidos_contacto',null, ['class' => 'form-control', 'placeholder' => 'Apellidos' , 'required'])!!}
                  </div>
                  
                
                  <div class="col-xs-2">
                   {!! Form::label('','')!!}
                   {!!Form::number('Telefono_contacto',null, ['class' => 'form-control', 'placeholder' => 'Télefono' , 'required'])!!}
                  </div>

                  <div class="col-xs-1">
                   {!! Form::label('','')!!}
                   {!!Form::number('ext',null, ['class' => 'form-control', 'placeholder' => 'EXT' , 'required'])!!}
                  </div>

                  <div class="col-xs-3">
                   {!! Form::label('','')!!}
                   {!!Form::email('email',null, ['class' => 'form-control', 'placeholder' => 'E- Mail' , 'required'])!!}
                  </div>

                  <div class="col-xs-2">
                   {!!Form::submit('Crear', ['class' => 'btn btn-primary'])!!}
                  </div>


                  
</div>




              

                

                
              
                
</div>
  
   </div>
   </div>
   </div>
   </div>
   </div>
   </div>

                  

          
          
@endsection