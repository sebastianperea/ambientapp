@extends('admin.template.main')

@section('content')

@section('titulo', 'Ofertas Comerciales')
<table class="table table-striped">
<thead>
 
  <th>Codigo oferta</th>
  <th>Identificación Cliente</th>
  <th>Razon Social</th>
  <th>Estado</th>
  <th>Fecha y hora</th>
  <th>Acción</th>



</thead>

<tbody>
	


	 @foreach($ofertas as $oferta)

     @foreach($clientes as $cliente)
@if($oferta->identificacion==$cliente->identificacion)
	 <tr> 
 
	 <td>{{$oferta->id}}</td>
	 <td>
	 {{$cliente->identificacion}}
	 </td>
	 <td>{{$cliente->razon_social}}</td>

	 

	 <td> @if($oferta->Estado=="En espera")	
	 <span class="label label-warning">{{$oferta->Estado}}</span>
	 @else
<span class="label label-primary">{{$oferta->Estado}}</span>
	 @endif</td>
	 <td>{{$oferta->created_at}}</td>
	 
	 <td><a href="{{ route('Comercial.ofertas.destroy', $oferta->id ) }}" onclick="return confirm('¿Seguro desea eliminarlo?')" class="btn btn-danger btn-xs" ><span class="fa fa-trash"></a> <a href="{{ route('comercial.ofertas.edit', $oferta->id ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a> <a href="ofertapdf/1" target="_blank" class="btn btn-success btn-xs" ><span class="fa fa-eye"> </a> <a href="ofertapdf/2" target="_blank" class="btn btn-primary btn-xs" ><span class="fa fa-arrow-down"> </a></td>



	 </tr>
	
	 @endif

@endforeach
	 @endforeach

	 




</tbody>
</table>
{!! $ofertas->render() !!}
{!! $clientes->render() !!}






@endsection