<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">


  <title>AMBIENTAAP V 1.0</title>

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="userId" content="{{ Auth::check() ? Auth::user()->id : '' }}">


  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <link href="{{ asset('css/app.css')}}">
  <link rel="shortcut icon" href="{{{ asset('plugins/images/fav.ico') }}}">





  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap/assets/css/vendor.css')}}">
 
   
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap/assets/css/flat-admin.css')}}">
  <link href="{{ asset('jquery-ui.css')}}" type="text/css" rel="stylesheet"/>

  <!-- Theme -->
  <link rel="stylesheet" type="text/css" href="{{asset('plugins/bootstrap/assets/css/theme/blue-sky.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('plugins/bootstrap/assets/css/theme/blue.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('plugins/bootstrap/assets/css/theme/red.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('plugins/bootstrap/assets/css/theme/yellow.css')}}">
 




  <style>

    .sticky-container{
    padding:0px;
    margin:0px;
    position:fixed;
    right:-130px;
    top:230px;
    width:300px;
    z-index: 1100;
    
}
.sticky li{
    list-style-type:none;
    background-color:#fff;
    color:black;
    height:40px;
    padding:0px;
    margin:0px 0px 1px 0px;
   
}


.sticky li p{
    padding-top:5px;
    margin:0px;
    line-height:16px;
    font-size:11px;
}


body {
  overflow: hidden;
}


/* Preloader */

#preloader {
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(255, 255, 255, 0.8);
  /* change if the mask should have another color then white */
  z-index: 99;
  /* makes sure it stays on top */
}

#status {
  width: 300px;
  height: 300px;
  position: absolute;
  left: 50%;
  /* centers the loading animation horizontally one the screen */
  top: 50%;
  
  /* centers the loading animation vertically one the screen */
  background-image: url("{{ asset('plugins/images/load.gif')}}");
  /* path to your loading animation */
  background-repeat: no-repeat;
  background-position: center;
  margin: -100px 0 0 -100px;
  /* is width and height divided by two */
}
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>

   <!-- CSRF Token -->
    

  <div class="app app-default">
{{ csrf_field() }}

<aside class="app-sidebar" id="sidebar">
  <div id="preloader">
  <div id="status">&nbsp;</div>
</div>

  <div class="sidebar-header">
    <img src="{{asset('plugins/images/bat.png')}}" class="logo" style="margin-top: 20px; margin-left: 30px;">
    <button type="button" class="sidebar-toggle">
      <i class="fa fa-times"></i>
    </button>
  </div>
  <div class="sidebar-menu">
    <ul class="sidebar-nav">
      <li class="active">
        <a href="{{ route('pos.index', true)}}">
          <div class="icon">
            <i class="fa fa-tasks" aria-hidden="true"></i>
          </div>
          <div class="title"> Escritorio</div>
        </a>
      </li>
      <li class="dropdown ">
        <a href="{{ route('posconsumos.index', true)}}" class="dropdown-toggle" data-toggle="dropdown">
          <div class="icon">
            <i class="fa fa fa-lightbulb-o" aria-hidden="true"></i>
          </div>
          <div class="title">Lumina</div>
        </a>

        <div class="dropdown-menu">
          <ul>
            <li class="section"><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Información Lumina</li>
            <li><a href="{{ route('lumina.create', true)}}">Ingresar información</a></li>
            <li><a href="{{ route('lumina.index', true)}}">Ver Todo</a></li>
          </ul>

          <ul>
            <li class="section"><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Gestores</li>
            <li><a href="{{ route('gestores.create', true)}}">Crear nuevo</a></li>
            <li><a href="{{ route('gestores.index', true)}}">Ver todos los gestores</a></li>
          </ul>
        </div>
      </li>







      <li class="dropdown ">
        <a href="{{ route('ofertas.index', true)}}" class="dropdown-toggle" data-toggle="dropdown">
          <div class="icon">
            <i class="fa fa-recycle" aria-hidden="true"></i>
          </div>
          <div class="title">R.A.E.E</div>
        </a>
        <div class="dropdown-menu">
          <ul>
            <li class="section"><i class="fa fa-warning" aria-hidden="true"></i> Peligrosos</li>
            <li><a href="{{ route('peligrosos.create', true)}}">Crear</a></li>
            <li><a href="{{ route('peligrosos.index', true)}}">Ver peligrosos</a></li>
            
            
            <li class="line"></li>

            <li class="section"><i class="fa fa-recycle" aria-hidden="true"></i> Aprovechables</li>
            <li><a href="{{ route('aprovechables.create', true)}}">Crear</a></li>
            <li><a href="{{ route('aprovechables.index', true)}}">Ver</a></li>
             <li class="line"></li>


            <li class="section"><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Posconsumos</li>
            <li><a href="{{ route('posconsumos.create', true)}}">Crear</a></li>
            <li><a href="{{ route('posconsumos.index', true)}}">Ver</a></li>
            <li class="line"></li>

            <li class="line"></li>


            <li class="section"><i class="fa fa-recycle" aria-hidden="true"></i> Peligrosos Aprovechables</li>
            <li><a href="{{ route('peligrososa.create', true)}}">Crear</a></li>
            <li><a href="{{ route('peligrososa.index', true)}}">Ver</a></li>
            <li class="line"></li>

            <li class="line"></li>


            <li class="section"><i class="fa fa-recycle" aria-hidden="true"></i> Especiales</li>
            <li><a href="{{ route('especiales.create', true)}}">Crear</a></li>
            <li><a href="{{ route('especiales.index', true)}}">Ver</a></li>
            <li class="line"></li>

            <li class="section"><i class="fa fa-truck" aria-hidden="true"></i> Transportes</li>
            <li><a href="{{ route('transportes.create', true)}}">Crear</a></li>
            <li><a href="{{ route('transportes.index', true)}}">Ver</a></li>
            <li class="line"></li>

            
          </ul>
        </div>
      </li>

       <br><br><br><br>
    </ul>




  
  </div>




  <div class="sidebar-footer">
    <ul class="menu">
      <li>
        <a href="/" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-cogs" aria-hidden="true"></i>
        </a>
      </li>
      <li><a href="#"><span class="flag-icon flag-icon-th flag-icon-squared"></span></a></li>
    </ul>
  </div>
</aside>

<script type="text/ng-template" id="sidebar-dropdown.tpl.html">
  <div class="dropdown-background">
    <div class="bg"></div>
  </div>
  <div class="dropdown-container">
    
  </div>
</script>
<div class="app-container">

  <nav class="navbar navbar-default" id="navbar">
  <div class="container-fluid">
    <div class="navbar-collapse collapse in">
      <ul class="nav navbar-nav navbar-mobile">
        <li>
          <button type="button" class="sidebar-toggle">
            <i class="fa fa-bars"></i>
          </button>
        </li>
        <li class="logo">
          <a class="navbar-brand" href="#"><span class="highlight">Ambientapp</span></a>
        </li>
      
      </ul>
      <ul class="nav navbar-nav navbar-left">
        <li class="navbar-title"> @yield('titulo')</li>
        <!-- <li class="navbar-search hidden-sm">
          <input id="search" type="text" placeholder="Search..">
          <button class="btn-search"><i class="fa fa-search"></i></button>
        </li> -->
      </ul>
      <ul class="nav navbar-nav navbar-right">
        
        <li class="dropdown notification warning">
          
          <div class="dropdown-menu">
            <ul>
              <li class="dropdown-header">Message</li>
              <li>
                <a href="#">
                  <span class="badge badge-warning pull-right">10</span>
                  <div class="message">
                    <img class="profile" src="https://placehold.it/100x100">
                    <div class="content">
                      <div class="title">"Payment Confirmation.."</div>
                      <div class="description">Alan Anderson</div>
                    </div>
                  </div>
                </a>
              </li>
             
              <li>
                <a href="#">
                  <span class="badge badge-warning pull-right">2</span>
                  <div class="message">
                    <img class="profile" src="https://placehold.it/100x100">
                    <div class="content">
                      <div class="title">"Order Confirmation.."</div>
                      <div class="description">Brenda Lawson</div>
                    </div>
                  </div>
                </a>
              </li>
              <li class="dropdown-footer">
                <a href="#">View All <i class="fa fa-angle-right" aria-hidden="true"></i></a>
              </li>
            </ul>
          </div>
        </li>
      
        <li class="dropdown profile">
          <a href="/html/pages/profile.html" class="dropdown-toggle"  data-toggle="dropdown">
            <div class="icon">
            <i class="fa fa-users" aria-hidden="true"></i>
            </div>
          </a>
          <div class="dropdown-menu">
            <div class="profile-info">
              <h4 class="username">{{ Auth::user()->name }} </h4>

            </div>
            <ul class="action">
              
                
             
              <li> 

                <a href="{{ url('/logout') }}">
                  Cerrar sesión
                </a>
              </li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>

  <div class="btn-floating" id="help-actions">
  <div class="btn-bg"></div>
  <button type="button" class="btn btn-default btn-toggle" data-toggle="toggle" data-target="#help-actions">
    <i class="icon fa fa-plus"></i>
    <span class="help-text">Shortcut</span>
  </button>
  <div class="toggle-content">
    <ul class="actions">
      <li><a href="{{ route('ofertas.create', true)}}">Oferta Comercial</a></li>
      
    </ul>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card card-banner card-chart card-green no-br">
      
    </div>
  </div>
</div>
     @include('flash::message')

        @yield('content')
        
  <footer class="app-footer"> 
  <div class="row">
    <div class="col-xs-12">
      <div class="footer-copyright">

        

        Copyright © 2019 Eco Industria S.A.S E.S.P. AMBIENTAPP V 0.1 - COMERCIAL
      </div>
    </div>
  </div>
</footer>
</div>

  </div>
  
<script type="text/javascript" src="{{asset('plugins/bootstrap/assets/js/vendor.js')}}"></script>
  <script type="text/javascript" src="{{asset('plugins/bootstrap/assets/js/app.js')}}"></script>

<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js', true)}}"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



</body>

   



<script>
 // preloader
 $(window).on('load', function() { // makes sure the whole site is loaded 
  $('#status').fadeOut(); // will first fade out the loading animation 
  $('#preloader').delay(300).fadeOut('slow'); // will fade out the white DIV that covers the website. 
  $('body').delay(350).css({'overflow':'visible'});
})


</script>

<!-- 
<script src="{{ asset('js/app.js')}}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>



@yield('js')

  <!-- Jquery -->

<!--fin Jquery-->



</html>
