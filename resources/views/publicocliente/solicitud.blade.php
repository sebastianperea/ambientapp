<!DOCTYPE html>
<html>
<head>


  <title>Solicitud Oferta Comercial</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  

  <link rel="stylesheet" type="text/css" href="{{asset('plugins/bootstrap/assets/css/estilo.css')}}">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  

  

  </head>


<body>
<!-- <body oncontextmenu="return false"> -->


<div>@include('flash::message')</div>
@if ( count( $errors ) > 0 )
            <div class="alert alert-warning" role="alert">
               @foreach ($errors->all() as $error)
                  <div>{{ $error }}</div>
              @endforeach
            </div>
        @endif </br>
<div class="container login-container">
            
            <div class="row">
                <div class="col-md-6 login-form-1">
                    <center><img src="{{asset('plugins/images/logoeco.png')}}" width="45%" height="auto" class="d-inline-block align-top" alt=""></center><br>
                    
                        <div class="col-xs-6">
                      <p class="lead">Solicite su oferta comercial<span class="text-success"> AHORA</span></p>
                      <ul class="list-unstyled" style="line-height: 2">
                          <li><span class="fa fa-check text-success"></span> Operadores del programa pos consumo LUMINA</li>
                          
                          <li><span class="fa fa-check text-success"></span> Gestión integral de residuos con procesos innovadores</li>
                          <li><span class="fa fa-check text-success"></span> Soluciones digitales para facilitar los procesos</li>
                          <li><span class="fa fa-check text-success"></span> Más de 10 años de experiencia</li>
                          
                      </ul>
                      <p><a href="https://www.ecoindustriasas.com/gestion-de-residuos-2/" class="btn btn-success btn-sm">Conoce nuestros servicios</a></p>
                  </div>    
                        
                </div>

                <div class="col-md-6 login-form-2">
                   
                    

                        {!! Form::open(['route' => 'solicitud.store', 'method' => 'POST' ,'id'=>'formtip']) !!}



                           <div class="row">

        <!--Grid column-->
        <div class="col-md-12 col-xl-12">
             

                <div class="form-group">
                            <div class="input-group">
                                <div class="col-xs-3" style="color:white;">
                                   Tipo de Persona: <input type="radio" name="tipodepersona" class="tipodepersona" value="natural" required="required"> Natural
                                    <input type="radio" name="tipodepersona" class="tipodepersona" value="juridica" required="required"> Juridica
                                </div>
                            </div>
                          </div>
                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-9">
                        <div class="md-form">
                            {!!Form::number('identificacion',null, ['class' => 'form-control col-xs-6', 'placeholder' => 'Numero de identificacion' , 'required', 'id'=>'nidentificacion']) !!}
                            
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-3">
                        <div class="md-form">
                            {!!Form::number('dv',null, ['class' => 'form-control col-xs-2', 'placeholder' => 'DV', 'id'=>'dv']) !!}
                            
                        </div>
                    </div>
                    <!--Grid column-->



                </div><br>


                          
                        <div class="form-group">
                            {!!Form::text('razon_social',null, ['class' => 'form-control', 'placeholder' => 'Razón Social' , 'required', 'id'=>'razonsocial']) !!}
                        </div>

                        <!--Grid row-->
                        <div class="row">

                            <!--Grid column-->
                            <div class="col-md-6">
                                <div class="md-form">
                                    {!!Form::text('departamento',null, ['class' => 'form-control', 'placeholder' => 'Departamento' , 'required', 'id'=>'departamento']) !!}
                                    
                                </div>
                            </div>
                            <!--Grid column-->

                            <!--Grid column-->
                            <div class="col-md-6">
                                <div class="md-form">
                                    {!!Form::text('ciudad',null, ['class' => 'form-control', 'placeholder' => 'Ciudad' , 'required', 'id'=>'ciudad']) !!}
                                    
                                </div>
                            </div>
                            <!--Grid column-->



                        </div><br>

                        <div class="row">

                            <!--Grid column-->
                            <div class="col-md-6">
                                <div class="md-form">
                                    {!!Form::text('nombres',null, ['class' => 'form-control', 'placeholder' => 'Nombres' , 'required', 'id'=>'nombre']) !!}
                                    
                                </div>
                            </div>
                            <!--Grid column-->

                            <!--Grid column-->
                            <div class="col-md-6">
                                <div class="md-form">
                                     {!!Form::text('apellidos',null, ['class' => 'form-control', 'placeholder' => 'Apellidos' , 'required', 'id'=>'apellidos']) !!}
                                    
                                </div>
                            </div>
                            <!--Grid column-->



                        </div><br>

                        <div class="row">

                            <!--Grid column-->
                            <div class="col-md-8">
                                <div class="md-form">
                                     {!!Form::number('telefono_contacto',null, ['class' => 'form-control', 'placeholder' => 'Teléfono Contacto' , 'required', 'id'=>'telefonocelular']) !!}
                                    
                                </div>
                            </div>
                            <!--Grid column-->

                            <!--Grid column-->
                            <div class="col-md-4">
                                <div class="md-form">
                                     {!!Form::number('ext',null, ['class' => 'form-control', 'placeholder' => 'Extensión']) !!}
                                    
                                </div>
                            </div>
                            <!--Grid column-->



                        </div><br>

                        <div class="form-group">
                            {!!Form::email('email',null, ['class' => 'form-control', 'placeholder' => 'Email*' , 'required']) !!}
                        </div>  


                        <div class="form-group">
                             {!! Form::label('procesos','Procesos', ['style' => 'color:white;'])!!}
                   {!!Form::textarea('procesos',null, ['class' => 'form-control procesos', 'id'=>'procesos', 'placeholder' => 'Procesos' , 'required'])!!}
                        </div>
                          
                        <input type="number" name="user_id" value="{{$userid}}" style="display:none;">
                        
                        
                        <!-- <div class="form-group">
                            <input type="submit" class="btnSubmit" value="Enviar" />
                        
                               
                           
                        </div> -->

                        <ul class="list-inline pull-right">
                           
                           
                            <li><button type="submit" class="btn btn-primary btn-info-full" >Enviar</button></li>
                        </ul>

                        {!! Form::close() !!}
                    
                </div>
            </div>
        </div>
      
</body>










</html>