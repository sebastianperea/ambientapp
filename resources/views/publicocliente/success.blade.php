<!DOCTYPE html>
<html>
<head>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<title></title>

	<style type="text/css">
	.logo{
		width: 250px;
		height: auto;
		margin-top: 30px;
		margin-left: 25px;
		}

		.titulo{	
			text-align: center;
			margin-top: 40px;
			font-family: 'Quicksand', sans-serif;
				}

			*{
				font-family: 'Quicksand', sans-serif;
			}

		
</style>
</head>
<body onload="nobackbutton();"> 
	<div class="col-xs-9">
	<div class="col-xs-4"><img src="{{asset('/plugins/images/logoeco.png')}}" class="logo"><br><br></div>

	<h1 class="titulo">Creación de clientes</h1></div>
	
		<div class="panel panel-success">
	
  <div class="col-xs-12"><strong>@include('flash::message')</strong>
  	<h3><strong>Gracias!</strong></h3><br>
  		<p style="font-size: 12pt;">Para continuar con el proceso por favor suba el archivo anterior firmado y con huella en el paso 3: "Subir archivos". Si necesita más información contáctenos al PBX: 8966690 o al correo: comercial@ecoindustriasas.com<br><br>

  		<strong>Por favor revise su buzón de correo y diligencie el "Formato de información de residuos".</strong></p>	</div>
</div>


</body>

<script type="text/javascript">
	

function nobackbutton(){	
   window.location.hash="no-back-button";	
   window.location.hash="Again-No-back-button" //chrome	
   window.onhashchange=function(){window.location.hash="no-back-button";}
}
</script>
</html>

