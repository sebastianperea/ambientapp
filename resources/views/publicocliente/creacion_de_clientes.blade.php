<!DOCTYPE html>
<html lang="es">
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=dashboard">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Creación De clientes - Ambientapp v1.0</title>
<link href="{{asset('plugins/bootstrap/css/form1.css')}}" rel="stylesheet" media="screen">
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/bootstrap/css/imprimir.css')}}" media="print" />


<script language="javascript">


</script>


 
  </head>

  <body>

 
  <div class="general"> <!--Inicio div general -->
  	 <div class="cabecera"><!-- ini div header -->
     <IMG SRC=" {{asset('/plugins/images/logoeco.png')}}" class="logo">
     <h1 class="titulo">Creación de clientes</h1>
     <div class="derecha"> <!--Inicio div derecha -->
     <p>Codigo:    GC-FO- 001 </p>
     <p>Fecha de Actualización: 25/11/2016 </p>
     <p>Versión: 02</p>
     <div style=" margin-left: 950px;"> Fecha: {{$fecha}}</div > 
     </div><!-- Fin div derecha -->
     </div><!-- Fin div header -->
@foreach($clientes as $cliente)
    <div class="caja1"> <!--Inicio div caja1 -->

      @include('flash::message')
      @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
          
    <li>{{ $error }}</li></div>
            @endforeach
        </ul>
    @endif
   {!! Form::open(['route' => ['creacion_de_clientes.update', $cliente->identificacion],'files' => true, 'enctype'=>'multipart/form-data','method' => 'PUT' ]) !!}
   

<h4 class="titulo2">INFORMACIÓN DEL GENERADOR</h4><br>
<div>
    <p>Razón Social: {!!Form::text('Razón Social',$cliente->razon_social , ['class' => 'form-control ', 'class'=>'cuadro1','placeholder' => 'Razón Social' , 'required','readonly']) !!}
   Numero de identificación: 
    {!!Form::number('identificacion',$cliente->identificacion , ['class' => ' ', 'placeholder' => 'Identificación' , 'required','readonly']) !!} - {!!Form::number('dv',$cliente->dv, ['class' => ' indv ', 'placeholder' => 'Digito de verificación' ,'min'=>'0', 'required','readonly']) !!} {!!Form::select('tipo',['Cliente'=>'Cliente'],'selected',['class' => ' selecttipo ', 'required' ])!!} &nbsp; &nbsp; &nbsp; &nbsp; Tipo de Persona: <input type="radio" name="tipodepersona" class="tipodepersona" value="natural" required="required"> Natural
  <input type="radio" name="tipodepersona" class="tipodepersona" value="juridica" required="required"> Juridica</div></p>   


<div class="seccion2"><!-- inicio seccion2-->
    <h4 class="seccion2">CONTACTO TÉCNICO DEL GENERADOR</h4>
        <p class="seccion2">Nombres: {!!Form::text('nombres',$cliente->nombres , ['class' => 'form-control ', 'class'=>'cuadro1','placeholder' => 'Razón Social' , 'required']) !!}</p>

        <p class="seccion2">Apellidos: {!!Form::text('apellidos',$cliente->apellidos , ['class' => 'form-control ', 'class'=>'cuadro1','placeholder' => 'Razón Social' , 'required']) !!}

        <p class="seccion2">E-mail: {!!Form::email('email',$cliente->email , ['class' => 'form-control ', 'class'=>'cuadro1','placeholder' => 'E-mail' , 'required']) !!}</p> 

        <p class="seccion2">Cargo: {!!Form::text('cargo',$cliente->cargo , ['class' => 'form-control ', 'class'=>'cuadro1','placeholder' => 'Cargo' , 'required']) !!}</p>

        <p class="seccion2">Télefono Fijo: {!!Form::number('telefono_contacto',$cliente->telefono_contacto , ['class' => 'form-control ','min'=>'0', 'class'=>'cuadro1','placeholder' => 'Cargo' , 'required']) !!} Ext: {!!Form::number('telefono_contacto',$cliente->ext, ['class' => 'form-control ', 'class'=>'cuadro1','placeholder' => 'Cargo', 'class'=>'exttecnico' ,'min'=>'0', 'required']) !!}  </p>

        <p class="seccion2">Télefono Movil: {!!Form::number('telefonocelular',$cliente->telefonocelular, ['class' => 'form-control ', 'min'=>'0','class'=>'cuadro1','placeholder' => 'Cargo', 'class'=>'intelefonomoviltecnico' , 'required']) !!}</p> 
        </div><!-- Fin sección 2 -->

        <div class="seccion3"><!-- inicio seccion3-->
    <h4 class="seccion3">CONTACTO CARTERA DEL GENERADOR</h4>
        <p class="seccion3">Nombre y Apellido: <input type="text" name="nombrecartera" class="inseccion3" style="width:350px;" required></p>
        <p class="seccion3">Télefono Fijo: <input type="number" min="0" name="telefonofijocartera" class="" style="width:315px;" required> Ext:<input type="number" min="0" class="" name="extcartera" style="width:50px;"></p>
        <p class="seccion3">Télefono Movil: <input type="number" min="0" name="telefonomovilcartera" class="" style="width:385px;" required></p>  
        <p class="seccion3">Dirección de radicación<br>de facturas: <input type="text" name="direccionradicacion"  class="inseccion3" style="width:400px;" required></p> 
        <p class="seccion3">E-Mail: <input type="email" name="emailcartera" style="width:445px;"class="" required></p>

  </p>
  @endforeach
        
        </div><!-- Fin sección 3 -->

        

     <div class="seccion4"><!-- inicio seccion4-->
    <h4 class="seccion4">REFERENCIAS COMERCIALES</h4>
        <p class="seccion4">Empresa 1: <input type="text" name="nombreempresauno" class="inseccion4nombre" required></p>   
        <p class="seccion4">Télefono: <input type="number"  min="0" name="telefonoempresauno" class="inseccion4telefono" required></p>
       
        <p class="seccion4">Empresa 2:<input type="text" name="nombreempresados"  class="inseccion4" required></p> 
        <p class="seccion4">Télefono: <input type="number" min="0" name="telefonoempresados" class="inseccion4telefono2" required></p>
  </p>
            
        </div><!-- Fin sección 4 --><br>

<div class="seccion5"><!-- inicio seccion5-->
    <h4 class="seccion5">AUTORIZACIÓN TRANSFERENCIA ELECTRONICA PARA PAGO DE MATERIAL RECICLABLE</h4>
        <p class="seccion5">Nombre del Banco: <input type="text" name="nombrebanco" class="inseccion5"></p>
        
        <p class="seccion5">Numero de Cuenta: <input type="number"  min="0" name="numerodecuenta" class="inseccion5" required>  Tipo de cuenta: <input type="radio" name="tipodecuenta" value="ahorros"> Ahorros
  <input type="radio" name="tipodecuenta" value="corriente"> Corriente 
        
        <p class="seccion5">Titular de la Cuenta: <input type="text" name="nombrebanco" class="inseccion6" required></p>     
  </p>
            
        </div><!-- Fin sección 5 -->




        <div class="contenedorleyenda"><!--seccion leyenda-->
  
<p class="leyenda">Lea cuidadosamente la siguiente cláusula y pregunte lo que no comprenda. Declaro
que la información que he suministrado es verídica y doy mi consentimiento expreso e irrevocable
a (LA ENTIDAD), o a quien sea en el futuro el acreedor del crédito solicitado, para:
a) Consultar, en cualquier tiempo, en DataCrédito o en cualquier otra central de información de riesgo,
toda la información relevante para conocer mi desempeño como deudor, mi capacidad de pago
o para valorar el riesgo futuro de concederme un crédito. b) Reportar a DataCrédito o a cualquier central
de información de riesgo, datos, tratados o sin tratar, tanto sobre el cumplimiento oportuno como sobre
el incumplimiento, si lo hubiere, de mis obligaciones crediticias, o de mis deberes legales de contenido
patrimonial, de tal forma que éstas presenten una información veraz, pertinente, completa actualizada y
exacta de mi desempeño como deudor después de haber cruzado y procesado diversos datos útiles para
obtener una información significativa. c) Conservar, tanto en (LA ENTIDAD), en DataCrédito o en cualquier
otra central de información de riesgo, con la debidas actualizaciones y durante el período necesario
señalado en sus reglamentos la información indicada en los literales b) y d) de esta cláusula. d) Suministrar
a DataCrédito o a cualquier otra central de información de riesgo datos relativos a mis solicitudes de cré-
dito así como otros atinentes a mis relaciones comerciales, financieras y en general socioeconómicas que
yo haya entregado o que consten en registros públicos, bases de datos públicas o documentos públicos.</p>
  
</div><!--Fin Contenedor leyenda-->

<div class="seccionarchivos" style="display: none;"><!-- inicio archivos-->
<h4 class="seccionarchivos">ADJUNTAR ARCHIVOS (Persona Natural)</h4>
    <p class="seccionarchivos">Luego de haber llenado todos los campos correctamente por favor adjunte los archivos requeridos respectivamente</p><br>

    

  <p class="seccionarchivos">Fotocopia de la cedula:<br><br>
  {!! Form::file('fotocopiacpn', ['class' => 'imprimirboton'])!!}<br></p>

  <p class="seccionarchivos">Registro Unico Tributario (RUT)<br><br>
  {!! Form::file('rutpn', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>

  <p class="seccionarchivos">Certificado de matricula mercantil no mayor a 30 días (Si aplica)<br><br>
  {!! Form::file('certificadodeexistencia', ['class' => 'imprimirboton'])!!}<br></p>

  <p class="seccionarchivos">Certificación Bancaria<br><br>
  {!! Form::file('certificacionbancaria', ['class' => 'imprimirboton'])!!}<br></p>

  <hr style="height: 1.5px; border:none; background-color:#E6E20D ; width: 350px; margin-top: 40px;">
 <p> &nbsp; &nbsp; &nbsp;Requiere aprobación de credito: <input type="radio" name="creditopn" value="si" class="creditopn"> Si
  <input type="radio" name="creditopn" value="no" class="creditopn"> No </p><br>

  <div class="creditopn2" style="display: none;">
  <p class="seccionarchivos credito">Para solicitud de cupo de credito por favor adjunte la siguiente<br> información si es persona natural:</p><br>

        <p class="seccionarchivos credito">Declaración de renta (Si declara) o extractos 3 ultimos meses<br><br>
  {!! Form::file('declaracionpn', ['class' => 'imprimirboton'])!!}<br></p>
  </div>

        </div><!-- Fin Archivos -->

        <div class="seccionarchivospj" style="display: none;"><!-- inicio archivos-->
<h4 class="seccionarchivospj">ADJUNTAR ARCHIVOS (Persona Jurídica)</h4>
    <p class="seccionarchivospj">Luego de haber llenado todos los campos correctamente por favor adjunte los archivos requeridos respectivamente</p><br>

    

  <p class="seccionarchivospj">Fotocopia de la cedula representante legal:<br><br>
  {!! Form::file('fotorepl', ['class' => 'imprimirboton'])!!}<br></p>

  <p class="seccionarchivospj">Registro Unico Tributario (RUT)<br><br>
  {!! Form::file('rutpj', ['class' => 'imprimirboton'])!!}<br></p>

  <p class="seccionarchivospj">Certificado de existencia y representación legal no mayor a 30 días <br><br>
  {!! Form::file('certificadoexistencia', ['class' => 'imprimirboton'])!!}<br></p>

  <p class="seccionarchivospj">Certificación Bancaria<br><br>
  {!! Form::file('certificacionbancariapj', ['class' => 'imprimirboton'])!!}<br></p>

      <hr style="height: 1.5px; border:none; background-color:#E6E20D ; width: 350px; margin-top: 40px;">

      <p> &nbsp; &nbsp; &nbsp;Requiere aprobación de credito: <input type="radio" name="creditopj" value="si" class="creditopj"> Si
  <input type="radio" name="creditopj" value="no" class="creditopj"> No </p><br>

        <div class="creditopj2" style="display: none;">
      <p class="seccionarchivospj">Para solicitud de cupo de credito por favor adjunte la siguiente<br> información si es persona jurídica:</p><br>

        <p class="seccionarchivospj">Declaración de renta<br><br>
  {!! Form::file('declaracionpj', ['class' => 'imprimirboton'])!!}<br></p>
   <p class="seccionarchivospj">Estados financieros<br><br>
  {!! Form::file('estadospj', ['class' => 'imprimirboton'])!!}<br></p>

  </div>
        </div><!-- Fin Archivos -->


    <div class="leyinformativa"><!--Contenedor leyenda informativa-->

    <p>Antes de dar click en enviar siga los siguientes pasos:<br>1. Imprima este formulario escanee, incluyendo firma y huella del representante legal, adjuntelo en el link paso 3 del correo enviado a ustedes.<br>2. Lea detenidamente los terminos y condiciones y la politica de tratamiento de datos, a su satisfación proceda a dar click en enviar, para que sus datos queden guardados en nuestra base de datos.</p> 
    </div>  

    <div class="terminosycondiciones"><!--Inicio Contenedor terminos y condiciones--> 

<input type="checkbox" name="terminos" value="terminos" required="required">Acepto los <a href="http://ambientapp.ecoindustriasas.com/terminos_y_condiciones.pdf" target="_blank">Terminos y condiciones </a><br><br>
  <input type="checkbox" name="politicadeprivacidad" value="politicadeprivacidad" required="required"> Acepto la <a href="http://ambientapp.ecoindustriasas.com/aviso_de_privacidad.pdf" target="_blank">Politica de privacidad</a><br>

    </div><!--Fin Contenedor Terminos-->

<div class="firma"><!--Inicio Contenedor firma-->
<hr style="margin-top: 110px;" align="center" size="1" width="360" color="black" />
<p style="text-align: center;">FIRMA Y SELLO DEL REPRESENTANTE LEGAL<br></p>
<P style="margin-left: 60px;">CEDULA:</P>
    <div class="huella"><!-- Contenedor Huella-->

    </div><!--Fin Contenedor Huella-->

    <p class="huella">Huella</p>
 </div><!--Fin Contenedor Firma-->
 
    <div style="margin-top: -200px;">
    <input type="button" name="imprimir" id="sub" value="Imprimir" onclick="window.print();" class="imprimirboton">
    <input type="submit" id="env" value="Enviar" class="imprimirboton" disabled="disabled">
    </div>

    
  </div> <!-- Fin div general -->


		
		{!! Form::close() !!}

    
	  

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="{{asset('plugins/bootstrap/assets/js/bootstrap.min.js')}}"> </script>
    <script>
      $(document).ready(function(){

        $(".tipodepersona").click(function(evento){
              
            var valor = $(this).val();

          
            if(valor == 'natural'){

                $(".seccionarchivos").show();
                $(".seccionarchivospj").hide();
            }else{

                $(".seccionarchivospj").show();
                $(".seccionarchivos").hide();
            }
              });
          });
    </script>

    <script>
      $(document).ready(function(){

        $(".creditopn").click(function(evento){
              
            var valor = $(this).val();

            if(valor == 'si'){
              $(".creditopn2").show();
                
            }
            else{

                $(".creditopn2").hide();
            }
              });
          });
    </script>
  

    <script>
      $(document).ready(function(){

        $(".creditopj").click(function(evento){
              
            var valor = $(this).val();

            if(valor == 'si'){
              $(".creditopj2").show();
                
            }
            else{

                $(".creditopj2").hide();
            }
              });
          });
    </script>

<!--     Disabled print button -->

    <script type="text/javascript">
      
      $(document).ready(function(){
        $( "#sub" ).click(function() {
            
            $('#env').attr('disabled',false);

           });
      });
    </script>
  </body>
</html>