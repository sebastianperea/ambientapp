<!DOCTYPE html>
<html lang="es">
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=dashboard">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Subir Archivos - Ambientapp v1.0</title>
<link href="{{asset('plugins/bootstrap/css/form1.css')}}" rel="stylesheet" media="screen">
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/bootstrap/css/imprimir.css')}}" media="print" />


<script language="javascript">


</script>


 
  </head>

  <body>

 
  <div class="general"> <!--Inicio div general -->
  	 <div class="cabecera"><!-- ini div header -->
     <IMG SRC=" {{asset('/plugins/images/logoeco.png')}}" class="logo">
     <h1 class="titulo">Subir archivos</h1>
     <div class="derecha"> <!--Inicio div derecha -->
     <p>Codigo:    GC-FO- 001 </p>
     <p>Fecha de Actualización: 25/11/2016 </p>
     <p>Versión: 02</p>
     
     </div><!-- Fin div derecha -->
     </div><!-- Fin div header -->
@foreach($clientes as $cliente)
    <div class="caja1"> <!--Inicio div caja1 -->

      @include('flash::message')
      @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
          
    <li>{{ $error }}</li></div>
            @endforeach
        </ul>
    @endif
   {!! Form::open(['route' => ['cliente.up', $cliente->identificacion],'files' => true, 'enctype'=>'multipart/form-data','method' => 'PUT' ]) !!}
   

<h4 class="titulo2">INFORMACIÓN DEL GENERADOR</h4><br>
<div>
    <p>Razón Social: {!!Form::text('Razón Social',$cliente->razon_social , ['class' => 'form-control ', 'class'=>'cuadro1','placeholder' => 'Razón Social' , 'required','readonly']) !!}
   Numero de identificación: 
    {!!Form::number('identificacion',$cliente->identificacion , ['class' => ' ', 'placeholder' => 'Identificación' , 'required','readonly']) !!} - {!!Form::number('dv',$cliente->dv, ['style'=> 'width:30px;','placeholder' => 'Digito de verificación' ,'min'=>'0', 'required','readonly']) !!} {!!Form::select('tipo',['Aprobado'=>'Aprobado'],'selected',['class' => ' selecttipo ', 'required' ])!!} 



       @endforeach
        



</div><!--Fin Contenedor leyenda-->

<div class="seccionarchivos"><!-- inicio archivos-->
<h4 class="seccionarchivos">ADJUNTAR ARCHIVOS</h4>
    <p class="seccionarchivos">Luego de haber llenado todos los campos correctamente por favor adjunte los archivos requeridos respectivamente</p><br>

    

  <p class="seccionarchivos">Formato de creación de clientes firmado:<br><br>
  {!! Form::file('creaciondeclientes', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>

  <p class="seccionarchivos">Formato de información de residuos<br><br>
  {!! Form::file('inforesiduos', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>

        </div><!-- Fin Archivos -->

       


 
    <div style="margin-top: 30px;">
    <input type="submit" id="env" value="Enviar" class="imprimirboton">
    </div><br><br>

    
  </div> <!-- Fin div general -->


		
		{!! Form::close() !!}

    
	  

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="{{asset('plugins/bootstrap/assets/js/bootstrap.min.js')}}"> </script>

  </body>
</html>