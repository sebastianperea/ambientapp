<html>
<head>

<title>Oferta {{$ofertas->id}}</title>
  <style>
@import url('https://fonts.googleapis.com/css?family=Quicksand|Roboto');


 </style>


<style type="text/css">


  
  header{
position: fixed;

  font-family: 'Quicksand', sans-serif;

  width: 100%;

 
}

footer{

     position: fixed;

  font-family: 'Quicksand', sans-serif;
  bottom: 50px;
  float: center;

}

body{
font-family: 'Quicksand', sans-serif;

}

.logo{

  float: left;
  width: 230px;
  height: auto;
  position: fixed;
}
.iconosheader{

  float: right;
  width: 460px;
  height: auto;
  position: fixed;

}

.titulo1{
text-align: left;
font-size: 13pt;
color: #604137;


}

.imgcurva{



}

.container1{

float: left;
width:300px;
height: auto;
margin-top: 90px;

}

.container2{

width:300px;
height: auto;
float: right;
margin-left: 500px;


}

.container3{

float: left;
width:300px;
height: auto;


margin-top: 40px;



}


.container4{

width:700px;
height: auto;
margin-top: 140px;



}


.noferta{

float: right;

margin-top: 70px;






}

.parrafo{
  font-size: 10pt;
  text-align: justify;
    text-justify: inter-word;
}



.portafolio1{

float: right;
  width: 430px;
  height: auto;
  margin-right: 50px;
  

}


th {
    
    background-color: #95D935;
    color: white;

}
table {
    border-collapse: collapse;
    width: 100%;
margin: auto;
z-index: 5px;

}

table, td, th {
   border: 1px solid #ddd;
    text-align: left;
    border-radius: 5px;
}

th, td {
 text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}


</style>

  

<body>
  <header>
    
        <img class="logo" src="plugins/images/logoeco.png">
        <img class="iconosheader" src="plugins/images/iconosheader.png">
        </div>


  </header>


  <body>

  <div>
 
 <div class="container1">


 <h2 class="titulo1" >Nosotros</h2>
 <p class="parrafo">Nuestra empresa <strong>Eco industria SAS ESP</strong>, nace en el año 2007, lo cual nos acredita con nueve años de experiencia en la gestión de residuos tanto peligrosos como aprovechables. <br ><br> Basamos nuestro modelo de negocio en la prestación integral de servicios de recolección, tratamiento, aprovechamiento y/o disposición final de residuos.<br><br>
 Gracias a un arduo trabajo, constancia disciplina y dedicación hemos logrado participar en importantes proyectos ambientales en Colombia como lo es la operación logística de dos de los Programas de Pos consumo de residuos peligrosos más importantes del país por 4 años consecutivos. “PILAS CON EL AMBIENTE” ,“LUMINA”Y AHORA “RECOPILA”. Operando las principales zonas y contribuyendo con un gran porcentaje de recolección de los programasPos consumo. <br><br>Garantizamos a nuestros clientes que los residuos gestionados por nuestra empresa se trataran teniendo en cuenta el menor impacto ambiental, el cumplimiento de la normatividad legal vigente y calidad en toda la cadena de servicio desde el transporte hasta la disposición final, aprovechamiento y/o valorización de los residuos. Nos encontramos en proceso de certificación en el sistema integrado</p>

 </div>

 <div class="container2">
  
   <img class="portafolio1" src="plugins/images/portafolio.png">
 

 </div>



</div>

      <div style="page-break-after:always;"></div>
        <div>
          
              
             
              <div class="noferta">
              <p style="font-size: 14px;"> Oferta Comercial Número: {{$clientes->id}}</p>
              
             
            </div>

              <div class="container3">
              
              <strong style="color: #604137; font-size: 12pt; margin-bottom:20px;">Señores:</strong>
              <p style="font-size: 11pt;">{{$clientes->razon_social}}</p>
              <p style="font-size: 11pt; color: #42464C;">{{$clientes->nombres}} {{$clientes->apellidos}}</p>
             
            </div><br>

           

             <div class="container4">

              <p  style="font-size: 11pt;">Esperamos poder contar con usted como uno de nuestros clientes y con su ayuda día a día ayudar a salvar el planeta. A continuación su oferta comercial detallada:</p><br>
            </div>  
               

           
  @if(count($peligrosos)>0) <!-- Inicio If general 1-->
        <strong><p>Peligrosos</p></strong><!-- Inicio Dibujo peligrosos -->
              <table>
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Pretratamiento</th>
                    <th scope="col">Precio por kg</th>        
                  </tr>
                </thead>
                      <tbody>
                        <?php 
                              foreach ($peligrosos as $peligroso) {   
                           ?>
                        <tr>
                           <td>{{($peligroso->peligroso_id)}}</td>
                          <td>{{($peligroso->nombre)}}</td>
                          <td>{{($peligroso->pretratamiento)}}</td>
                          <td>${{($peligroso->precio)}}</td>
                        </tr>
                        <?php } ?>
                      </tbody>
                </table><br><!-- Fin dibujo peligrosos -->





      @if(count($aprovechables)>0) <!-- Inicio If aprovechables-->
        <strong><p>Aprovechables</p></strong><!-- Inicio dibujo aprovechables -->
          <table>
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Nombre</th>
                  <th scope="col">Precio por kg</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                     foreach ($aprovechables as $aprovechable) {
                  ?>
                <tr>
                  <td >{{($aprovechable->aprovechables_id)}}</td>
                  <td >{{($aprovechable->nombre)}}</td>
                  <td>${{($aprovechable->precio)}}</td>
                </tr>
               <?php } ?>
              </tbody>
          </table><!-- Fin dibujo aprovechables -->

            @if(count($posconsumos)>0) <!-- Inicio if posconsumos aprovechables -->
                <strong><p>Posconsumos</p></strong><!-- Inicio dibujo posconsumos -->
                    <table>
                      <thead>
                        <tr>
                           <th scope="col">ID</th>
                           <th scope="col">Nombre</th>
                           <th scope="col">Pretratamiento</th>
                           <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                        <tbody>
                          <?php 
                                foreach ($posconsumos as $posconsumo) {
                             ?>
                          <tr>
                            <td >{{($posconsumo->posconsumos_id)}}</td>
                            <td >{{($posconsumo->nombre)}}</td>
                            <td >{{($posconsumo->pretratamiento)}}</td>
                            <td>${{($posconsumo->precio)}}</td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table><!-- Fin dibujo posconsumos -->
              @if(count($peligrososa)>0)<!-- Inicio peligrososa aprovechables -->
                  <strong><p>Peligrosos Aprovechables</p></strong><!-- Inicio dibujo peligrososa -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($peligrososa as $peligrosoa) {
                          ?>
                        <tr>
                          <td >{{($peligrosoa->peligrososa_id)}}</td>
                          <td >{{($peligrosoa->nombre)}}</td>
                          <td >{{($peligrosoa->pretratamiento)}}</td>
                          <td>${{($peligrosoa->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                  </table><!-- Fin dibujo peligrososa -->
              @if(count($especiales)>0)<!-- Inicio especiales aprovechables -->
                  <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
              @if(count($transportes)>0)<!-- Inicio transportes aprovechables -->
                   <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Tipo</th>
                          <th scope="col">Precio por kg</th>  
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($transportes as $transporte) {
                          ?>
                        <tr>
                          <td >{{($transporte->transporte_id)}}</td>
                          <td >{{($transporte->tipo)}}</td>
                          <td>${{($transporte->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>

                  @if(count($pys)>0)<!-- Inicio Pys -->
                   <strong><p>Peligrosos</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                  @else <!-- Else PYS -->
                  @endif <!-- Fin PYS --> 
              @else<!-- Else transportes aprovechables -->
                <p>FIN</p>
              @endif <!-- Fin transportes aprovechables -->       
              @else<!-- Else especiales -->
              @endif<!-- Fin especiales aprovechables -->      

              @else<!-- Else peligrososa aprovechables -->
              @endif <!-- Fin peligrososa aprovechables -->



        @else <!-- Else posconsumos aprovechables-->
          @if(count($peligrososa)>0)<!-- peligrosos aprovechables else posconsumos aprovechables -->
              <strong><p>Peligrosos Aprovechables</p></strong><!-- Inicio dibujo peligrososa -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($peligrososa as $peligrosoa) {
                          ?>
                        <tr>
                          <td >{{($peligrosoa->peligrososa_id)}}</td>
                          <td >{{($peligrosoa->nombre)}}</td>
                          <td >{{($peligrosoa->pretratamiento)}}</td>
                          <td>${{($peligrosoa->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                  </table><!-- Fin dibujo peligrososa -->
            @if(count($especiales)>0)<!-- Especiales aprovechables else posconsumos aprovechables -->
                <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
              @if(count($transportes)>0)<!-- Inicio Transportes especiales aprovechables else posconsumos aprovechables -->
                  <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Tipo</th>
                          <th scope="col">Precio por kg</th>  
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($transportes as $transporte) {
                          ?>
                        <tr>
                          <td >{{($transporte->transporte_id)}}</td>
                          <td >{{($transporte->tipo)}}</td>
                          <td>${{($transporte->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>

              @else <!-- Else especiales aprovechables else posconsumos aprovechables -->
                  <p>Fin especiales aprovechables else posconsumos aprovechables</p>
              @endif<!-- Final especiales aprovechables else posconsumos aprovechables -->

                  

          @else<!-- else especiales aprovechables else posconsumos aprovechables error-->
              @if(count($transportes)>0)<!-- Inicio transportes else especiales aprovechables else posconsumos aprovechables error-->
                  <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Tipo</th>
                          <th scope="col">Precio por kg</th>  
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($transportes as $transporte) {
                          ?>
                        <tr>
                          <td >{{($transporte->transporte_id)}}</td>
                          <td >{{($transporte->tipo)}}</td>
                          <td>${{($transporte->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
                 @else<!-- Inicio transportes else especiales aprovechables else posconsumos aprovechables error-->
                    <p>Inicio transportes else especiales aprovechables else posconsumos aprovechables error</p>
                 @endif <!-- Inicio transportes else especiales aprovechables else posconsumos aprovechables error-->  


          @endif<!-- Fin else especiales aprovechables else posconsumos aprovechables error-->
          @else<!-- Else peligrosos aprovechables else posconsumos aprovechables -->
              <!-- El no de PA -->

          @endif<!-- peligrosos aprovechables else posconsumos aprovechables -->

        @endif<!-- Endif posconsumos aprovechables -->
  @else <!-- Else Aprovechables -->


            @if(count($posconsumos)>0) <!-- Inicio posconsumos else aprovechables-->
                <strong><p>Posconsumos</p></strong><!-- Dibujo posconsumos else -->
                  <table>
                    <thead>
                      <tr>
                         <th scope="col">ID</th>
                         <th scope="col">Nombre</th>
                         <th scope="col">Pretratamiento</th>
                         <th scope="col">Precio por kg</th>
                      </tr>
                    </thead>
                      <tbody>
                         <?php 
                             foreach ($posconsumos as $posconsumo) {
                          ?>
                        <tr>
                           <td >{{($posconsumo->posconsumos_id)}}</td>
                           <td >{{($posconsumo->nombre)}}</td>
                           <td >{{($posconsumo->pretratamiento)}}</td>
                           <td>${{($posconsumo->precio)}}</td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
              @if(count($peligrososa)>0)<!-- Inicio peligrososa else aprovechables -->
                  <strong><p>Peligrosos Aprovechables</p></strong><!-- Inicio dibujo peligrososa -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($peligrososa as $peligrosoa) {
                          ?>
                        <tr>
                          <td >{{($peligrosoa->peligrososa_id)}}</td>
                          <td >{{($peligrosoa->nombre)}}</td>
                          <td >{{($peligrosoa->pretratamiento)}}</td>
                          <td>${{($peligrosoa->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                  </table><!-- Fin dibujo peligrososa else-->  


                 @if(count($especiales)>0)<!-- Inicio especiales else aprovechables -->
                     <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table> 
                   @if(count($transportes)>0)<!-- Inicio transportes else aprovechables -->
                      <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->precio)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>  

                   @else<!-- Else especiales transportes else aprovechables -->
                      <p>Fin transportes else aprovechables</p>
                   @endif <!-- Fin transportes else aprovechables -->
                 @else <!-- Else especiales else aprovechables -->  
                     @if(count($transportes)>0)<!-- Inicio transportes else aprovechables else especiales -->
                      <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->precio)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>
                     @else<!-- Else transportes else aprovechables else especiales -->
                        <p>Fin transportes else aprovechables else especiales</p>
                     @endif<!-- Fin transportes else aprovechables else especiales -->          
                 @endif <!-- Final especiales else aprovechables --> 
                @else<!-- Else peligrososa else aprovechables -->
                  @if(count($especiales)>0)<!-- Else especiales else aprovechables else peligrososa -->
                      <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table> 
                    @if(count($transportes)>0)<!-- Inicio transportes else peligrososa -->
                        <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->precio)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>
                    @else<!-- Else transportes else peligrosos aprovechables especiales if -->
                      <p>Fin</p>
                    @endif<!-- Fin transportes else peligrosos aprovechables especiales if -->

                  @else<!-- Else especiales else aprovechables else peligrososa -->
                      @if(count($transportes)>0)<!-- Inicio transportes else especiales else peligrososa -->
                          <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->precio)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>
                      @else<!-- Else transportes else especiales else peligrososa -->
                         <p>Fin Else transportes else especiales else peligrososa</p> 
                      @endif<!-- Fin transportes else especiales else peligrososa -->
                  @endif<!-- Fin especiales else aprovechables else peligrososa-->
                @endif<!-- Fin peligrososa else aprovechables -->
              @else<!-- Else posconsumos else aprovechables -->

                @if(count($peligrososa)>0)<!-- Inicio peligrososa else posconsumos else aprovechables -->
                    <strong><p>Peligrosos Aprovechables</p></strong><!-- dibujo peligrososa -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($peligrososa as $peligrosoa) {
                          ?>
                        <tr>
                          <td >{{($peligrosoa->peligrososa_id)}}</td>
                          <td >{{($peligrosoa->nombre)}}</td>
                          <td >{{($peligrosoa->pretratamiento)}}</td>
                          <td>${{($peligrosoa->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                  </table>
                  @if(count($especiales)>0)<!-- Inicio especiales peligrososa else posconsumos else aprovechables -->
                            <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                              <table>
                                <thead>
                                  <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Pretratamiento</th>
                                    <th scope="col">Precio por kg</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php 
                                       foreach ($especiales as $especial) {
                                    ?>
                                  <tr>
                                    <td >{{($especial->especiales_id)}}</td>
                                    <td >{{($especial->nombre)}}</td>
                                    <td >{{($especial->pretratamiento)}}</td>
                                    <td>${{($especial->precio)}}</td>
                                  </tr>
                                 <?php } ?>
                                </tbody>
                              </table>
                    @if(count($transportes)>0)<!-- Inicio transportes especiales peligrososa else posconsumos else aprovechables -->
                         <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->precio)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                    @else<!-- Else transportes especiales peligrososa else posconsumos else aprovechables -->
                        <p>Fin Else transportes especiales peligrososa else posconsumos else aprovechables</p>
                    @endif <!-- Final transportes especiales peligrososa else posconsumos else aprovechables -->

                  @else<!-- Else especiales peligrososa else posconsumos else aprovechables -->
                     @if(count($transportes)>0)<!-- Inicio transportes else especiales peligrososa else posconsumos else aprovechables -->
                         <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->precio)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>
                      @else<!-- Else transportes else especiales peligrososa else posconsumos else aprovechables -->
                        <p>Fin Else transportes else especiales peligrososa else posconsumos else aprovechables</p>
                      @endif  <!-- Final transportes else especiales peligrososa else posconsumos else aprovechables --> 

                  @endif<!-- Final especiales peligrososa else posconsumos else aprovechables -->


                @else<!-- Else peligrososa else posconsumos else aprovechables -->

                 @if(count($especiales)>0)<!-- Inicio especiales else posconsumos else aprovechables -->
                      <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
                  @if(count($transportes)>0)<!-- Inicio transportes else especiales else posconsumos else aprovechables -->
                      <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->precio)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                  @else<!-- Else transportes else especiales else posconsumos else aprovechables -->
                      <p>Fin Else transportes else especiales else posconsumos else aprovechables</p>
                  @endif <!-- Fin transportes else especiales else posconsumos else aprovechables --> 

                 @else <!-- Else especiales else posconsumos else aprovechables -->
                    @if(count($transportes)>0)<!-- Inicio transportes Else especiales else posconsumos else aprovechables -->
                        <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->precio)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                    @else<!-- Else transportes Else especiales else posconsumos else aprovechables -->
                      <p>Fin Else transportes Else especiales else posconsumos else aprovechables</p>
                    @endif<!-- Else transportes Else especiales else posconsumos else aprovechables -->
                 @endif <!-- Fin especiales else posconsumos else aprovechables --> 

                 
                 @endif<!-- Else peligrososa else posconsumos else aprovechables -->
              @endif<!-- Fin posconsumos else aprovechables -->
    @endif<!-- Final aprovechables -->



  @else <!-- Else peligrosos -->

  @endif<!-- Final peligrosos -->
                              


    
       </div>
    
  </body>
  <footer>

   
     <p style="font-size: 10px; text-align: center;">Calle 2 No 18-93 torre 1 bodega 8 “PARQUE INDUSTRIAL SAN JORGE”.<br>Mosquera Cundinamarca ,Colombia.<br>PBX: 8966690 Email: contacto@ecoindustriasas.com</p>
    
  </footer>
  
</body>
</html>