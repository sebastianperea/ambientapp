<!DOCTYPE html>
<html>
<head>

  <title>Oferta {{$ofertas->id}}</title>
  <link rel="stylesheet" type="text/css" href="/app/public/plugins/bootstrap/assets/css/bootstrap.min.css">

  <!-- <link rel="stylesheet" type="text/css" href="C:\xampp\htdocs\ambientapp\public\plugins\bootstrap\assets\css\bootstrap.min.css"> -->
  <style>

@font-face { 
    font-family:Quicksand; src: url('{{ asset('/app/public/plugins/bootstrap/assets/fonts/Quicksand-Regular.otf') }}');

}

/*@font-face { 
    font-family:Quicksand; src: url('{{ asset('C:\xampp\htdocs\ambientapp\public\plugins\bootstrap\assets\fonts\Quicksand-Regular.otf') }}');*/


 </style>


<style type="text/css">


  
  header{
position: fixed;


  width: 100%;

 
}


body{
font-family: 'Montserrat', sans-serif;
margin-left: 70px;



}

.logo{

  float: left;
  width: 100mm;
  height: auto;
  position: fixed;
  margin-left: 20px;


}
.pagina{
  page-break-before:always;

}


.titulo1{
text-align: left;
font-size: 30pt;
color: #604137;


}

.imgcurva{



}

.container1{

width: 600px;
height: auto;
margin-top: 30px;
margin-left: 20px;


}

.container2{

width:300px;
height: auto;
margin-left: 600px;


}

.container3{

float:left;
width:300px;
height: auto;
margin-top: 20px;




}


.container4{

width:95%;
height: auto;
margin-top: 30px;
margin-left: 20px;
font-size: 12pt;




}


.noferta{
width:95%;
margin-top: 30px;
margin-left: 20px;



}

.parrafo{
  font-size: 17pt;
  text-align: justify;
    text-justify: inter-word;
}



.portafolio1{

float: right;
  width: 830px;
  height: auto;
  margin-top: 50px;
  margin-right: 40px;
  

}

.container5{
margin-left: 20px;
  
}

.oferta{
float:right;

}

th {
    
    background-color: #95D935;
    color: white;

}
table {
    border-collapse: collapse;
    width: 90%;
    height: auto;
margin: auto;
z-index: 5mm;
margin-top: 20px;

}

table, td, th {
   border: 1px solid #ddd;
    text-align: left;
    border-radius: 5px;
}

.container5 p {
  margin-top: 30px;
  font-size: 18pt;
}


th, td {
 text-align: left;
    padding: 8px;
    font-size: 12pt;
}

}
tr:nth-child(even){background-color: #f2f2f2}
tr { page-break-inside: avoid !important; }


</style>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

  </head>

<body>
  



  <div class="row">
 
 <img style=" width: 100%;" src="/app/public/plugins/images/body.png ">

  <!-- <img style=" width: 100%;" src=" C:\xampp\htdocs\ambientapp\public\plugins\images\body.png"> -->




</div>
   
        <div style="page-break-after: always;"></div>
          
            
          <div class="row">
              <div class="noferta">
                <div class="oferta">
                    <p style="font-size: 12pt;"> Oferta Comercial Número: {{$clientes->id}}<br>Mosquera, Cundinamarca {{ \Carbon\Carbon::parse($ofertas->created_at)->format('d/m/Y')}}<br>
                    Vencimiento: {{$ofertas->fechaven}} </p><br>
                </div>

           <div class="container3">
              <strong style="color: #604137; font-size: 14pt; margin-bottom:20px;">Señores:</strong>
              <p style="font-size: 12pt;">{{$clientes->razon_social}}</p>
              <p style="font-size: 12pt; color: #42464C;">{{$clientes->responsable}}</p>
              <p style="font-size: 12pt; color: #42464C;">{{$clientes->cargo}} </p>
              <p style="font-size: 12pt; color: #42464C;">{{$clientes->direccion}} </p>
             
            </div><br>
            </div>
      </div>
              
          <div class="container4">
              <p>Esperamos poder contar con usted como uno de nuestros clientes y con su ayuda día a día ayudar a preservar el ambiente. A continuación su oferta comercial detallada:</p>
            </div>  
               

<div class="container5">
  @if(count($peligrosos)>0) <!-- Inicio If general 1-->
        <strong><p>Peligrosos</p></strong><!-- Inicio Dibujo peligrosos -->
              <table>
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Pretratamiento</th>
                    <th scope="col">Precio por kg</th>        
                  </tr>
                </thead>
                      <tbody>
                        <?php 
                              foreach ($peligrosos as $peligroso) {   
                           ?>
                        <tr>
                           <td>{{($peligroso->peligroso_id)}}</td>
                          <td>{{($peligroso->nombre)}}</td>
                          <td>{{($peligroso->pretratamiento)}}</td>
                          <td>${{($peligroso->precio)}}</td>
                        </tr>
                        <?php } ?>
                      </tbody>
                </table><br><!-- Fin dibujo peligrosos -->





      @if(count($aprovechables)>0) <!-- Inicio If aprovechables-->
        <strong><p>Aprovechables - Compra de Aprovechables</p></strong>
        <!-- Inicio dibujo aprovechables -->
          <table>
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Nombre</th>
                  <th scope="col">Precio por kg</th>
                  <th scope="col">Precio por Unidad</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                     foreach ($aprovechables as $aprovechable) {
                  ?>
                <tr>
                  <td >{{($aprovechable->aprovechables_id)}}</td>
                  <td >{{($aprovechable->nombre)}}</td>
                  <td>${{($aprovechable->precio)}}</td>
                  <td>${{($aprovechable->preciou)}}</td>
                </tr>
               <?php } ?>
              </tbody>
          </table><!-- Fin dibujo aprovechables -->

            @if(count($posconsumos)>0) <!-- Inicio if posconsumos aprovechables -->
                <strong><p>Posconsumos</p></strong><!-- Inicio dibujo posconsumos -->
                    <table>
                      <thead>
                        <tr>
                           <th scope="col">ID</th>
                           <th scope="col">Nombre</th>
                           <th scope="col">Pretratamiento</th>
                           <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                        <tbody>
                          <?php 
                                foreach ($posconsumos as $posconsumo) {
                             ?>
                          <tr>
                            <td >{{($posconsumo->posconsumos_id)}}</td>
                            <td >{{($posconsumo->nombre)}}</td>
                            <td >{{($posconsumo->pretratamiento)}}</td>
                            <td>${{($posconsumo->precio)}}</td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table><!-- Fin dibujo posconsumos -->
              @if(count($peligrososa)>0)<!-- Inicio peligrososa aprovechables -->
                  <strong><p>Peligrosos Aprovechables</p></strong><!-- Inicio dibujo peligrososa -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio por Unidad</th>
                          <th scope="col">Precio por Galones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($peligrososa as $peligrosoa) {
                          ?>
                        <tr>
                          <td >{{($peligrosoa->peligrososa_id)}}</td>
                          <td >{{($peligrosoa->nombre)}}</td>
                          <td >{{($peligrosoa->pretratamiento)}}</td>
                          @if($peligrosoa->precio >0)
                          <td>${{($peligrosoa->precio)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciou >0)
                          <td>${{($peligrosoa->preciou)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciog >0)
                          <td>${{($peligrosoa->preciog)}}</td>

                          @else
                          <td> - </td>
                          @endif
                        </tr>
                       <?php } ?>
                      </tbody>
                  </table><!-- Fin dibujo peligrososa -->
              @if(count($especiales)>0)<!-- Inicio especiales aprovechables -->
                  <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
              @if(count($transportes)>0)<!-- Inicio transportes aprovechables -->

                   <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Tipo</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio Global</th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($transportes as $transporte) {
                          ?>


                        <tr>
                          <td >{{($transporte->transporte_id)}}</td>
                          <td >{{($transporte->tipo)}}</td>
                          <td>${{($transporte->preciokg)}}</td>
                          <td>${{($transporte->preciog)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>

                  @if(count($pys)>0)<!-- Inicio Pys -->
                   <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                  @else <!-- Else PYS -->
                  @endif <!-- Fin PYS --> 
              @else<!-- Else transportes aprovechables -->
                @if(count($pys)>0)<!-- Inicio Pys Else transportes aprovechables-->
                   <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                  @else <!-- Else PYS Else transportes aprovechables-->
                  @endif <!-- Fin PYS Else transportes aprovechables--> 
              @endif <!-- Fin transportes aprovechables --> 

              @else<!-- Else especiales -->
                @if(count($transportes)>0)<!-- Inicio transportes Else especiales-->
                   <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Tipo</th>
                          <th scope="col">Precio por kg</th> 
                          <th scope="col">Precio Global</th>  
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($transportes as $transporte) {
                          ?>


                        <tr>
                          <td >{{($transporte->transporte_id)}}</td>
                          <td >{{($transporte->tipo)}}</td>
                          <td>${{($transporte->preciokg)}}</td>
                          <td>${{($transporte->preciog)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>

                  @if(count($pys)>0)<!-- Inicio Pys -->
                   <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                  @else <!-- Else PYS -->
                  @endif <!-- Fin PYS -->

                @else<!-- Else transportes Else especiales -->
                @if(count($pys)>0)<!-- Inicio Pys Else transportes aprovechables-->
                   <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                  @else <!-- Else PYS Else transportes aprovechables-->
                  @endif <!-- Fin PYS Else transportes aprovechables--> 
                  @endif<!-- Fin transportes Else especiales -->   

              @endif<!-- Fin especiales aprovechables -->      

              @else<!-- Else peligrososa aprovechables -->
                 @if(count($especiales)>0) <!-- Inicio especiales Else peligrososa aprovechables -->
                    <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>

                  @if(count($transportes)>0) <!-- Inicio transportes Else especiales Else peligrososa aprovechables -->
                      <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Tipo</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio Global</th>  
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($transportes as $transporte) {
                          ?>
                        <tr>
                          <td >{{($transporte->transporte_id)}}</td>
                          <td >{{($transporte->tipo)}}</td>
                          <td>${{($transporte->preciokg)}}</td>
                          <td>${{($transporte->preciog)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
                  @if(count($pys)>0) <!-- Inicio pys transportes Else especiales Else peligrososa aprovechables -->
                      <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>

                  @else <!-- Else pys transportes Else especiales Else peligrososa aprovechables -->
                    <!-- Fin -->
                  @endif  <!-- Final pys transportes Else especiales Else peligrososa aprovechables -->

                  @else <!-- Else transportes Else especiales Else peligrososa aprovechables -->
                      @if(count($pys)>0) <!-- Inicio pys transportes Else especiales Else peligrososa aprovechables -->
                      <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>

                  @else <!-- Else pys transportes Else especiales Else peligrososa aprovechables -->
                    <!-- Fin -->
                  @endif  <!-- Final pys Else transportes Else especiales Else peligrososa aprovechables -->
                  @endif  <!-- Fin transportes Else especiales Else peligrososa aprovechables -->  
                   

                 @else <!-- Else especiales Else peligrososa aprovechables -->
                    @if(count($transportes)>0) <!-- Inicio transportes Else especiales Else peligrososa aprovechables -->
                      <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Tipo</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio Global</th>  
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($transportes as $transporte) {
                          ?>
                        <tr>
                          <td >{{($transporte->transporte_id)}}</td>
                          <td >{{($transporte->tipo)}}</td>
                          <td>${{($transporte->preciokg)}}</td>
                          <td>${{($transporte->preciog)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
                    @if(count($pys)>0) <!-- Inicio pys transportes Else especiales Else peligrososa aprovechables -->
                      <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>

                  @else <!-- Else pys transportes Else especiales Else peligrososa aprovechables -->
                    <!-- Fin -->
                  @endif  <!-- Final pys transportes Else especiales Else peligrososa aprovechables -->

                   @else<!-- Else transportes Else especiales Else peligrososa aprovechables -->
                      @if(count($pys)>0) <!-- Inicio pys transportes Else especiales Else peligrososa aprovechables -->
                      <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>

                  @else <!-- Else pys transportes Else especiales Else peligrososa aprovechables -->
                    <!-- Fin -->
                  @endif  <!-- Final pys transportes Else especiales Else peligrososa aprovechables -->

                   @endif <!-- Fin transportes Else especiales Else peligrososa aprovechables -->

                 @endif <!-- Fin especiales Else peligrososa aprovechables -->
              @endif <!-- Fin peligrososa aprovechables -->



        @else <!-- Else posconsumos aprovechables-->
          @if(count($peligrososa)>0)<!-- peligrosos aprovechables else posconsumos aprovechables -->
              <strong><p>Peligrosos Aprovechables</p></strong><!-- Inicio dibujo peligrososa -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio por Unidad</th>
                          <th scope="col">Precio por Galones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($peligrososa as $peligrosoa) {
                          ?>
                        <tr>
                          <td >{{($peligrosoa->peligrososa_id)}}</td>
                          <td >{{($peligrosoa->nombre)}}</td>
                          <td >{{($peligrosoa->pretratamiento)}}</td>
                         @if($peligrosoa->precio >0)
                          <td>${{($peligrosoa->precio)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciou >0)
                          <td>${{($peligrosoa->preciou)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciog >0)
                          <td>${{($peligrosoa->preciog)}}</td>

                          @else
                          <td> - </td>
                          @endif
                        </tr>
                       <?php } ?>
                      </tbody>
                  </table><!-- Fin dibujo peligrososa --><br>
            @if(count($especiales)>0)<!-- Especiales aprovechables else posconsumos aprovechables -->
                <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
              @if(count($transportes)>0)<!-- Inicio Transportes especiales aprovechables else posconsumos aprovechables -->
                  <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Tipo</th>
                          <th scope="col">Precio por kg</th> 
                          <th scope="col">Precio Global</th>  
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($transportes as $transporte) {
                          ?>
                        <tr>
                          <td >{{($transporte->transporte_id)}}</td>
                          <td >{{($transporte->tipo)}}</td>
                          <td>${{($transporte->preciokg)}}</td>
                          <td>${{($transporte->preciog)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
                @if(count($pys)>0)<!-- Inicio PYS Transportes especiales aprovechables else posconsumos aprovechables -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS Transportes especiales aprovechables else posconsumos aprovechables -->
                 <!-- FIN -->
                @endif <!-- Fin PYS Transportes especiales aprovechables else posconsumos aprovechables -->   

              @else <!-- Else especiales aprovechables else posconsumos aprovechables -->

                  @if(count($pys)>0)<!-- Inicio PYS Transportes especiales aprovechables else posconsumos aprovechables -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS Transportes especiales aprovechables else posconsumos aprovechables -->
                 <!-- FIN -->
                @endif <!-- Fin PYS Transportes especiales aprovechables else posconsumos aprovechables -->  

              @endif<!-- Final especiales aprovechables else posconsumos aprovechables -->

                  

          @else<!-- else especiales aprovechables else posconsumos aprovechables error-->
              @if(count($transportes)>0)<!-- Inicio transportes else especiales aprovechables else posconsumos aprovechables error-->
                  <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Tipo</th>
                          <th scope="col">Precio por kg</th> 
                          <th scope="col">Precio Global</th>  
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($transportes as $transporte) {
                          ?>
                        <tr>
                          <td >{{($transporte->transporte_id)}}</td>
                          <td >{{($transporte->tipo)}}</td>
                          <td>${{($transporte->preciokg)}}</td>
                          <td>${{($transporte->preciog)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
                      @if(count($pys)>0)<!-- Inicio PYS transportes else especiales aprovechables else posconsumos aprovechables error -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS transportes else especiales aprovechables else posconsumos aprovechables error -->
                 <!-- FIN -->
                @endif <!-- Fin PYS transportes else especiales aprovechables else posconsumos aprovechables error -->  
                 @else<!-- Inicio transportes else especiales aprovechables else posconsumos aprovechables error-->
                       @if(count($pys)>0)<!-- Inicio PYS transportes else especiales aprovechables else posconsumos aprovechables error -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS transportes else especiales aprovechables else posconsumos aprovechables error -->
                 <!-- FIN -->
                @endif <!-- Fin PYS transportes else especiales aprovechables else posconsumos aprovechables error -->  

                    <!-- <p>Inicio transportes else especiales aprovechables else posconsumos aprovechables error</p> -->
                 @endif <!-- Inicio transportes else especiales aprovechables else posconsumos aprovechables error-->  


          @endif<!-- Fin else especiales aprovechables else posconsumos aprovechables error-->
          @else<!-- Else peligrosos aprovechables else posconsumos aprovechables -->
              <!-- El no de PA -->

               @if(count($especiales)>0)<!-- Inicio especiales el no de PA -->
                     <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>

                   @if(count($transportes)>0) <!-- Inicio transportes el no de pa -->
                   <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                        @if(count($pys)>0)<!-- Inicio PYS transportes else aprovechables -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                    @else<!-- Else PYS el no de pa -->
                     <!-- FIN -->
                    @endif <!-- Fin PYS el no de pa-->

                  @else<!-- Else transportes el no de pa -->
                    @if(count($pys)>0)<!-- Inicio PYS else transportes el no de pa -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                    @else<!-- Else PYS else else transportes el no de pa -->
                     <!-- FIN -->
                    @endif <!-- Fin PYS else else transportes el no de pa-->

                  @endif  <!-- Fin transportes el no de pa -->

                @else <!-- Else especiales el no de PA -->

                    @if(count($transportes)>0) <!-- Inicio transportes el no de pa -->
                   <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th> 
                              <th scope="col">Precio Global</th>       
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                        @if(count($pys)>0)<!-- Inicio PYS transportes else aprovechables -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                    @else<!-- Else PYS especiales el no de pa -->
                     <!-- FIN -->
                    @endif <!-- Fin PYS especiales el no de pa-->

                  @else<!-- Else transportes el no de pa -->
                      @if(count($pys)>0)<!-- Inicio PYS else transportes el no de pa -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                    @else<!-- Else PYS else transportes el no de pa -->
                     <!-- FIN -->
                    @endif <!-- Fin PYS else transportes el no de pa-->

                  @endif  <!-- Fin transportes el no de pa -->
                @endif <!-- Fin especiales el no de PA -->   

          @endif<!-- peligrosos aprovechables else posconsumos aprovechables -->

        @endif<!-- Endif posconsumos aprovechables -->
  @else <!-- Else Aprovechables -->


            @if(count($posconsumos)>0) <!-- Inicio posconsumos else aprovechables-->
                <strong><p>Posconsumos</p></strong><!-- Dibujo posconsumos else -->
                  <table>
                    <thead>
                      <tr>
                         <th scope="col">ID</th>
                         <th scope="col">Nombre</th>
                         <th scope="col">Pretratamiento</th>
                         <th scope="col">Precio por kg</th>
                      </tr>
                    </thead>
                      <tbody>
                         <?php 
                             foreach ($posconsumos as $posconsumo) {
                          ?>
                        <tr>
                           <td >{{($posconsumo->posconsumos_id)}}</td>
                           <td >{{($posconsumo->nombre)}}</td>
                           <td >{{($posconsumo->pretratamiento)}}</td>
                           <td>${{($posconsumo->precio)}}</td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
              @if(count($peligrososa)>0)<!-- Inicio peligrososa else aprovechables -->
                  <strong><p>Peligrosos Aprovechables</p></strong><!-- Inicio dibujo peligrososa -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio por Unidad</th>
                          <th scope="col">Precio por Galones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($peligrososa as $peligrosoa) {
                          ?>
                        <tr>
                          <td >{{($peligrosoa->peligrososa_id)}}</td>
                          <td >{{($peligrosoa->nombre)}}</td>
                          <td >{{($peligrosoa->pretratamiento)}}</td>
                         @if($peligrosoa->precio >0)
                          <td>${{($peligrosoa->precio)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciou >0)
                          <td>${{($peligrosoa->preciou)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciog >0)
                          <td>${{($peligrosoa->preciog)}}</td>

                          @else
                          <td> - </td>
                          @endif
                        </tr>
                       <?php } ?>
                      </tbody>
                  </table><!-- Fin dibujo peligrososa else-->  


                 @if(count($especiales)>0)<!-- Inicio especiales else aprovechables -->
                     <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table> 
                   @if(count($transportes)>0)<!-- Inicio transportes else aprovechables -->
                      <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th> 
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table> 

                        @if(count($pys)>0)<!-- Inicio PYS transportes else aprovechables -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS transportes else aprovechables -->
                 <!-- FIN -->
                @endif <!-- Fin PYS transportes else aprovechables -->   

                   @else<!-- Else especiales transportes else aprovechables -->

                      @if(count($pys)>0)<!-- Inicio PYS Else especiales transportes else aprovechables -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS Else especiales transportes else aprovechables -->
                 <!-- FIN -->
                @endif <!-- Fin PYS Else especiales transportes else aprovechables -->   
                      
                   @endif <!-- Fin transportes else aprovechables -->
                 @else <!-- Else especiales else aprovechables -->  
                     @if(count($transportes)>0)<!-- Inicio transportes else aprovechables else especiales -->
                      <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>
                       
                       @if(count($pys)>0)<!-- Inicio PYS transportes else aprovechables else especiales -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS transportes else aprovechables else especiales -->
                 <!-- FIN -->
                @endif <!-- Fin PYS transportes else aprovechables else especiales -->     

                     @else<!-- Else transportes else aprovechables else especiales -->

                     @if(count($pys)>0)<!-- Inicio PYS Else transportes else aprovechables else especiales -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS Else transportes else aprovechables else especiales -->
                 <!-- FIN -->
                @endif <!-- Fin PYS Else transportes else aprovechables else especiales -->  
                        
                     @endif<!-- Fin transportes else aprovechables else especiales -->          
                 @endif <!-- Final especiales else aprovechables --> 
                @else<!-- Else peligrososa else aprovechables -->
                  @if(count($especiales)>0)<!-- Else especiales else aprovechables else peligrososa -->
                      <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table> 
                    @if(count($transportes)>0)<!-- Inicio transportes else peligrososa -->
                        <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th> 
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                        @if(count($pys)>0)<!-- Inicio PYS transportes else peligrososa -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                      @else<!-- Else PYS transportes else peligrososa -->
                       <!-- FIN -->
                      @endif <!-- Fin PYS transportes else peligrososa --> 
                    @else<!-- Else transportes else peligrosos aprovechables especiales if -->
                      @if(count($pys)>0)<!-- Inicio PYS transportes else peligrosos aprovechables especiales if -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                      @else<!-- Else PYS transportes else peligrosos aprovechables especiales if -->
                       <!-- FIN -->
                      @endif <!-- Fin PYS transportes else peligrosos aprovechables especiales if --> 
                    @endif<!-- Fin transportes else peligrosos aprovechables especiales if -->

                  @else<!-- Else especiales else aprovechables else peligrososa -->
                      @if(count($transportes)>0)<!-- Inicio transportes else especiales else peligrososa -->
                          <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                     @if(count($pys)>0)<!-- Inicio PYS transportes else especiales else peligrososa -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS transportes else especiales else peligrososa -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS transportes else especiales else peligrososa --> 
                      @else<!-- Else transportes else especiales else peligrososa -->
                         @if(count($pys)>0)<!-- Inicio PYS transportes else especiales else peligrososa -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS transportes else especiales else peligrososa -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS transportes else especiales else peligrososa --> 

                      @endif<!-- Fin transportes else especiales else peligrososa -->
                  @endif<!-- Fin especiales else aprovechables else peligrososa-->
                @endif<!-- Fin peligrososa else aprovechables -->
              @else<!-- Else posconsumos else aprovechables -->

                @if(count($peligrososa)>0)<!-- Inicio peligrososa else posconsumos else aprovechables -->
                    <strong><p>Peligrosos Aprovechables</p></strong><!-- dibujo peligrososa -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio por Unidad</th>
                          <th scope="col">Precio por Galones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($peligrososa as $peligrosoa) {
                          ?>
                        <tr>
                          <td >{{($peligrosoa->peligrososa_id)}}</td>
                          <td >{{($peligrosoa->nombre)}}</td>
                          <td >{{($peligrosoa->pretratamiento)}}</td>
                          @if($peligrosoa->precio >0)
                          <td>${{($peligrosoa->precio)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciou >0)
                          <td>${{($peligrosoa->preciou)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciog >0)
                          <td>${{($peligrosoa->preciog)}}</td>

                          @else
                          <td> - </td>
                          @endif
                        </tr>
                       <?php } ?>
                      </tbody>
                  </table>
                  @if(count($especiales)>0)<!-- Inicio especiales peligrososa else posconsumos else aprovechables -->
                            <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                              <table>
                                <thead>
                                  <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Pretratamiento</th>
                                    <th scope="col">Precio por kg</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php 
                                       foreach ($especiales as $especial) {
                                    ?>
                                  <tr>
                                    <td >{{($especial->especiales_id)}}</td>
                                    <td >{{($especial->nombre)}}</td>
                                    <td >{{($especial->pretratamiento)}}</td>
                                    <td>${{($especial->precio)}}</td>
                                  </tr>
                                 <?php } ?>
                                </tbody>
                              </table>
                    @if(count($transportes)>0)<!-- Inicio transportes especiales peligrososa else posconsumos else aprovechables -->
                         <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                        @if(count($pys)>0)<!-- Inicio PYStransportes especiales peligrososa else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS transportes especiales peligrososa else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS transportes especiales peligrososa else posconsumos else aprovechables --> 

                    @else<!-- Else transportes especiales peligrososa else posconsumos else aprovechables -->
                       @if(count($pys)>0)<!-- Inicio PYS Else transportes especiales peligrososa else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS Else transportes especiales peligrososa else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS Else transportes especiales peligrososa else posconsumos else aprovechables --> 
                    @endif <!-- Final transportes especiales peligrososa else posconsumos else aprovechables -->

                  @else<!-- Else especiales peligrososa else posconsumos else aprovechables -->
                     @if(count($transportes)>0)<!-- Inicio transportes else especiales peligrososa else posconsumos else aprovechables -->
                         <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                        @if(count($pys)>0)<!-- Inicio PYS transportes else especiales peligrososa else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS transportes else especiales peligrososa else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS transportes else especiales peligrososa else posconsumos else aprovechables --> 


                      @else<!-- Else transportes else especiales peligrososa else posconsumos else aprovechables -->
                        
                        @if(count($pys)>0)<!-- Inicio PYS Else transportes else especiales peligrososa else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS Else transportes else especiales peligrososa else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS Else transportes else especiales peligrososa else posconsumos else aprovechables --> 

                      @endif  <!-- Final transportes else especiales peligrososa else posconsumos else aprovechables --> 

                  @endif<!-- Final especiales peligrososa else posconsumos else aprovechables -->


                @else<!-- Else peligrososa else posconsumos else aprovechables -->

                 @if(count($especiales)>0)<!-- Inicio especiales else posconsumos else aprovechables -->
                      <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
                  @if(count($transportes)>0)<!-- Inicio transportes else especiales else posconsumos else aprovechables -->
                      <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                          @if(count($pys)>0)<!-- Inicio PYS  transportes else especiales else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS  transportes else especiales else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS  transportes else especiales else posconsumos else aprovechables --> 


                  @else<!-- Else transportes else especiales else posconsumos else aprovechables -->
                      @if(count($pys)>0)<!-- Inicio PYS  Else transportes else especiales else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS  Else transportes else especiales else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS  Else transportes else especiales else posconsumos else aprovechables --> 

                  @endif <!-- Fin transportes else especiales else posconsumos else aprovechables --> 

                 @else <!-- Else especiales else posconsumos else aprovechables -->

                    @if(count($transportes)>0)<!-- Inicio transportes Else especiales else posconsumos else aprovechables -->
                        <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                        @if(count($pys)>0)<!-- Inicio PYS Else especiales else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS  Else Else especiales else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS  Else especiales else posconsumos else aprovechables --> 

                    @else<!-- Else transportes Else especiales else posconsumos else aprovechables -->
                      @if(count($pys)>0)<!-- Inicio PYS Else transportes Else especiales else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS  Else transportes Else especiales else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS  Else transportes Else especiales else posconsumos else aprovechables --> 

                    @endif<!-- Else transportes Else especiales else posconsumos else aprovechables -->
                 @endif <!-- Fin especiales else posconsumos else aprovechables --> 

                 
                 @endif<!-- Else peligrososa else posconsumos else aprovechables -->
              @endif<!-- Fin posconsumos else aprovechables -->
    @endif<!-- Final aprovechables -->



  @else <!-- Else peligrosos -->
      @if(count($aprovechables)>0) <!-- Inicio If aprovechables-->
        <strong><p>Aprovechables - Compra de Aprovechables</p></strong>
        <!-- Inicio dibujo aprovechables -->
          <table>
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Nombre</th>
                  <th scope="col">Precio por kg</th>
                  <th scope="col">Precio por Unidad</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                     foreach ($aprovechables as $aprovechable) {
                  ?>
                <tr>
                  <td >{{($aprovechable->aprovechables_id)}}</td>
                  <td >{{($aprovechable->nombre)}}</td>
                  <td>${{($aprovechable->precio)}}</td>
                  <td>${{($aprovechable->preciou)}}</td>
                </tr>
               <?php } ?>
              </tbody>
          </table><!-- Fin dibujo aprovechables -->

            @if(count($posconsumos)>0) <!-- Inicio if posconsumos aprovechables -->
                <strong><p>Posconsumos</p></strong><!-- Inicio dibujo posconsumos -->
                    <table>
                      <thead>
                        <tr>
                           <th scope="col">ID</th>
                           <th scope="col">Nombre</th>
                           <th scope="col">Pretratamiento</th>
                           <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                        <tbody>
                          <?php 
                                foreach ($posconsumos as $posconsumo) {
                             ?>
                          <tr>
                            <td >{{($posconsumo->posconsumos_id)}}</td>
                            <td >{{($posconsumo->nombre)}}</td>
                            <td >{{($posconsumo->pretratamiento)}}</td>
                            <td>${{($posconsumo->precio)}}</td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table><!-- Fin dibujo posconsumos -->
              @if(count($peligrososa)>0)<!-- Inicio peligrososa aprovechables -->
                  <strong><p>Peligrosos Aprovechables</p></strong><!-- Inicio dibujo peligrososa -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio por Unidad</th>
                          <th scope="col">Precio por Galones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($peligrososa as $peligrosoa) {
                          ?>
                        <tr>
                          <td >{{($peligrosoa->peligrososa_id)}}</td>
                          <td >{{($peligrosoa->nombre)}}</td>
                          <td >{{($peligrosoa->pretratamiento)}}</td>
                         @if($peligrosoa->precio >0)
                          <td>${{($peligrosoa->precio)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciou >0)
                          <td>${{($peligrosoa->preciou)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciog >0)
                          <td>${{($peligrosoa->preciog)}}</td>

                          @else
                          <td> - </td>
                          @endif
                        </tr>
                       <?php } ?>
                      </tbody>
                  </table><!-- Fin dibujo peligrososa -->
              @if(count($especiales)>0)<!-- Inicio especiales aprovechables -->
                  <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
              @if(count($transportes)>0)<!-- Inicio transportes aprovechables -->
                   <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Tipo</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio Global</th>  
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($transportes as $transporte) {
                          ?>
                        <tr>
                          <td >{{($transporte->transporte_id)}}</td>
                          <td >{{($transporte->tipo)}}</td>
                          <td>${{($transporte->preciokg)}}</td>
                          <td>${{($transporte->preciog)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>

                  @if(count($pys)>0)<!-- Inicio Pys -->
                   <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                  @else <!-- Else PYS -->
                  @endif <!-- Fin PYS --> 
              @else<!-- Else transportes aprovechables -->
                @if(count($pys)>0)<!-- Inicio Pys Else transportes aprovechables-->
                   <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                  @else <!-- Else PYS Else transportes aprovechables-->
                  @endif <!-- Fin PYS Else transportes aprovechables--> 
              @endif <!-- Fin transportes aprovechables --> 

              @else<!-- Else especiales -->
                @if(count($transportes)>0)<!-- Inicio transportes Else especiales-->
                   <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Tipo</th>
                          <th scope="col">Precio por kg</th> 
                          <th scope="col">Precio Global</th>  
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($transportes as $transporte) {
                          ?>
                        <tr>
                          <td >{{($transporte->transporte_id)}}</td>
                          <td >{{($transporte->tipo)}}</td>
                          <td>${{($transporte->preciokg)}}</td>
                          <td>${{($transporte->preciog)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>

                  @if(count($pys)>0)<!-- Inicio Pys -->
                   <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                  @else <!-- Else PYS -->
                  @endif <!-- Fin PYS -->

                @else<!-- Else transportes Else especiales -->
                @if(count($pys)>0)<!-- Inicio Pys Else transportes aprovechables-->
                   <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                  @else <!-- Else PYS Else transportes aprovechables-->
                  @endif <!-- Fin PYS Else transportes aprovechables--> 
                  @endif<!-- Fin transportes Else especiales -->   

              @endif<!-- Fin especiales aprovechables -->      

              @else<!-- Else peligrososa aprovechables -->
                 @if(count($especiales)>0) <!-- Inicio especiales Else peligrososa aprovechables -->
                    <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>

                  @if(count($transportes)>0) <!-- Inicio transportes Else especiales Else peligrososa aprovechables -->
                      <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Tipo</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio Global</th>  
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($transportes as $transporte) {
                          ?>
                        <tr>
                          <td >{{($transporte->transporte_id)}}</td>
                          <td >{{($transporte->tipo)}}</td>
                          <td>${{($transporte->preciokg)}}</td>
                          <td>${{($transporte->preciog)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
                  @if(count($pys)>0) <!-- Inicio pys transportes Else especiales Else peligrososa aprovechables -->
                      <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>

                  @else <!-- Else pys transportes Else especiales Else peligrososa aprovechables -->
                    <!-- Fin -->
                  @endif  <!-- Final pys transportes Else especiales Else peligrososa aprovechables -->

                  @else <!-- Else transportes Else especiales Else peligrososa aprovechables -->
                      @if(count($pys)>0) <!-- Inicio pys transportes Else especiales Else peligrososa aprovechables -->
                      <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>

                  @else <!-- Else pys transportes Else especiales Else peligrososa aprovechables -->
                    <!-- Fin -->
                  @endif  <!-- Final pys Else transportes Else especiales Else peligrososa aprovechables -->
                  @endif  <!-- Fin transportes Else especiales Else peligrososa aprovechables -->  
                   

                 @else <!-- Else especiales Else peligrososa aprovechables -->
                    @if(count($transportes)>0) <!-- Inicio transportes Else especiales Else peligrososa aprovechables -->
                      <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Tipo</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio Global</th>  
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($transportes as $transporte) {
                          ?>
                        <tr>
                          <td >{{($transporte->transporte_id)}}</td>
                          <td >{{($transporte->tipo)}}</td>
                          <td>${{($transporte->preciokg)}}</td>
                          <td>${{($transporte->preciog)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
                    @if(count($pys)>0) <!-- Inicio pys transportes Else especiales Else peligrososa aprovechables -->
                      <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>

                  @else <!-- Else pys transportes Else especiales Else peligrososa aprovechables -->
                    <!-- Fin -->
                  @endif  <!-- Final pys transportes Else especiales Else peligrososa aprovechables -->

                   @else<!-- Else transportes Else especiales Else peligrososa aprovechables -->
                      @if(count($pys)>0) <!-- Inicio pys transportes Else especiales Else peligrososa aprovechables -->
                      <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {
                                   
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>

                  @else <!-- Else pys transportes Else especiales Else peligrososa aprovechables -->
                    <!-- Fin -->
                  @endif  <!-- Final pys transportes Else especiales Else peligrososa aprovechables -->

                   @endif <!-- Fin transportes Else especiales Else peligrososa aprovechables -->

                 @endif <!-- Fin especiales Else peligrososa aprovechables -->
              @endif <!-- Fin peligrososa aprovechables -->



        @else <!-- Else posconsumos aprovechables-->
          @if(count($peligrososa)>0)<!-- peligrosos aprovechables else posconsumos aprovechables -->
              <strong><p>Peligrosos Aprovechables</p></strong><!-- Inicio dibujo peligrososa -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio por Unidad</th>
                          <th scope="col">Precio por Galones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($peligrososa as $peligrosoa) {
                          ?>
                        <tr>
                          <td >{{($peligrosoa->peligrososa_id)}}</td>
                          <td >{{($peligrosoa->nombre)}}</td>
                          <td >{{($peligrosoa->pretratamiento)}}</td>
                          @if($peligrosoa->precio >0)
                          <td>${{($peligrosoa->precio)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciou >0)
                          <td>${{($peligrosoa->preciou)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciog >0)
                          <td>${{($peligrosoa->preciog)}}</td>

                          @else
                          <td> - </td>
                          @endif
                        </tr>
                       <?php } ?>
                      </tbody>
                  </table><!-- Fin dibujo peligrososa --><br>
            @if(count($especiales)>0)<!-- Especiales aprovechables else posconsumos aprovechables -->
                <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
              @if(count($transportes)>0)<!-- Inicio Transportes especiales aprovechables else posconsumos aprovechables -->
                  <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Tipo</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio Global</th>  
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($transportes as $transporte) {
                          ?>
                        <tr>
                          <td >{{($transporte->transporte_id)}}</td>
                          <td >{{($transporte->tipo)}}</td>
                          <td>${{($transporte->preciokg)}}</td>
                          <td>${{($transporte->preciog)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
                @if(count($pys)>0)<!-- Inicio PYS Transportes especiales aprovechables else posconsumos aprovechables -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS Transportes especiales aprovechables else posconsumos aprovechables -->
                 <!-- FIN -->
                @endif <!-- Fin PYS Transportes especiales aprovechables else posconsumos aprovechables -->   

              @else <!-- Else especiales aprovechables else posconsumos aprovechables -->

                  @if(count($pys)>0)<!-- Inicio PYS Transportes especiales aprovechables else posconsumos aprovechables -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS Transportes especiales aprovechables else posconsumos aprovechables -->
                 <!-- FIN -->
                @endif <!-- Fin PYS Transportes especiales aprovechables else posconsumos aprovechables -->  

              @endif<!-- Final especiales aprovechables else posconsumos aprovechables -->

                  

          @else<!-- else especiales aprovechables else posconsumos aprovechables error-->
              @if(count($transportes)>0)<!-- Inicio transportes else especiales aprovechables else posconsumos aprovechables error-->
                  <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Tipo</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio Global</th>  
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($transportes as $transporte) {
                          ?>
                        <tr>
                          <td >{{($transporte->transporte_id)}}</td>
                          <td >{{($transporte->tipo)}}</td>
                          <td>${{($transporte->preciokg)}}</td>
                          <td>${{($transporte->preciog)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
                      @if(count($pys)>0)<!-- Inicio PYS transportes else especiales aprovechables else posconsumos aprovechables error -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS transportes else especiales aprovechables else posconsumos aprovechables error -->
                 <!-- FIN -->
                @endif <!-- Fin PYS transportes else especiales aprovechables else posconsumos aprovechables error -->  
                 @else<!-- Inicio transportes else especiales aprovechables else posconsumos aprovechables error-->
                       @if(count($pys)>0)<!-- Inicio PYS transportes else especiales aprovechables else posconsumos aprovechables error -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS transportes else especiales aprovechables else posconsumos aprovechables error -->
                 <!-- FIN -->
                @endif <!-- Fin PYS transportes else especiales aprovechables else posconsumos aprovechables error -->  

                    <!-- <p>Inicio transportes else especiales aprovechables else posconsumos aprovechables error</p> -->
                 @endif <!-- Inicio transportes else especiales aprovechables else posconsumos aprovechables error-->  


          @endif<!-- Fin else especiales aprovechables else posconsumos aprovechables error-->
          @else<!-- Else peligrosos aprovechables else posconsumos aprovechables -->
              <!-- El no de PA -->

               @if(count($especiales)>0)<!-- Inicio especiales el no de PA -->
                     <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>

                   @if(count($transportes)>0) <!-- Inicio transportes el no de pa -->
                   <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                        @if(count($pys)>0)<!-- Inicio PYS transportes else aprovechables -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                    @else<!-- Else PYS el no de pa -->
                     <!-- FIN -->
                    @endif <!-- Fin PYS el no de pa-->

                  @else<!-- Else transportes el no de pa -->
                    @if(count($pys)>0)<!-- Inicio PYS else transportes el no de pa -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                    @else<!-- Else PYS else else transportes el no de pa -->
                     <!-- FIN -->
                    @endif <!-- Fin PYS else else transportes el no de pa-->

                  @endif  <!-- Fin transportes el no de pa -->

                @else <!-- Else especiales el no de PA -->

                    @if(count($transportes)>0) <!-- Inicio transportes el no de pa -->
                   <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                        @if(count($pys)>0)<!-- Inicio PYS transportes else aprovechables -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                    @else<!-- Else PYS especiales el no de pa -->
                     <!-- FIN -->
                    @endif <!-- Fin PYS especiales el no de pa-->

                  @else<!-- Else transportes el no de pa -->
                      @if(count($pys)>0)<!-- Inicio PYS else transportes el no de pa -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                    @else<!-- Else PYS else transportes el no de pa -->
                     <!-- FIN -->
                    @endif <!-- Fin PYS else transportes el no de pa-->

                  @endif  <!-- Fin transportes el no de pa -->
                @endif <!-- Fin especiales el no de PA -->   

          @endif<!-- peligrosos aprovechables else posconsumos aprovechables -->

        @endif<!-- Endif posconsumos aprovechables -->
  @else <!-- Else Aprovechables -->


            @if(count($posconsumos)>0) <!-- Inicio posconsumos else aprovechables-->
                <strong><p>Posconsumos</p></strong><!-- Dibujo posconsumos else -->
                  <table>
                    <thead>
                      <tr>
                         <th scope="col">ID</th>
                         <th scope="col">Nombre</th>
                         <th scope="col">Pretratamiento</th>
                         <th scope="col">Precio por kg</th>
                      </tr>
                    </thead>
                      <tbody>
                         <?php 
                             foreach ($posconsumos as $posconsumo) {
                          ?>
                        <tr>
                           <td >{{($posconsumo->posconsumos_id)}}</td>
                           <td >{{($posconsumo->nombre)}}</td>
                           <td >{{($posconsumo->pretratamiento)}}</td>
                           <td>${{($posconsumo->precio)}}</td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
              @if(count($peligrososa)>0)<!-- Inicio peligrososa else aprovechables -->
                  <strong><p>Peligrosos Aprovechables</p></strong><!-- Inicio dibujo peligrososa -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio por Unidad</th>
                          <th scope="col">Precio por Galones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($peligrososa as $peligrosoa) {
                          ?>
                        <tr>
                          <td >{{($peligrosoa->peligrososa_id)}}</td>
                          <td >{{($peligrosoa->nombre)}}</td>
                          <td >{{($peligrosoa->pretratamiento)}}</td>
                         @if($peligrosoa->precio >0)
                          <td>${{($peligrosoa->precio)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciou >0)
                          <td>${{($peligrosoa->preciou)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciog >0)
                          <td>${{($peligrosoa->preciog)}}</td>

                          @else
                          <td> - </td>
                          @endif
                        </tr>
                       <?php } ?>
                      </tbody>
                  </table><!-- Fin dibujo peligrososa else-->  


                 @if(count($especiales)>0)<!-- Inicio especiales else aprovechables -->
                     <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table> 
                   @if(count($transportes)>0)<!-- Inicio transportes else aprovechables -->
                      <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table> 

                        @if(count($pys)>0)<!-- Inicio PYS transportes else aprovechables -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS transportes else aprovechables -->
                 <!-- FIN -->
                @endif <!-- Fin PYS transportes else aprovechables -->   

                   @else<!-- Else especiales transportes else aprovechables -->

                      @if(count($pys)>0)<!-- Inicio PYS Else especiales transportes else aprovechables -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS Else especiales transportes else aprovechables -->
                 <!-- FIN -->
                @endif <!-- Fin PYS Else especiales transportes else aprovechables -->   
                      
                   @endif <!-- Fin transportes else aprovechables -->
                 @else <!-- Else especiales else aprovechables -->  
                     @if(count($transportes)>0)<!-- Inicio transportes else aprovechables else especiales -->
                      <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>
                       
                       @if(count($pys)>0)<!-- Inicio PYS transportes else aprovechables else especiales -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS transportes else aprovechables else especiales -->
                 <!-- FIN -->
                @endif <!-- Fin PYS transportes else aprovechables else especiales -->     

                     @else<!-- Else transportes else aprovechables else especiales -->

                     @if(count($pys)>0)<!-- Inicio PYS Else transportes else aprovechables else especiales -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                @else<!-- Else PYS Else transportes else aprovechables else especiales -->
                 <!-- FIN -->
                @endif <!-- Fin PYS Else transportes else aprovechables else especiales -->  
                        
                     @endif<!-- Fin transportes else aprovechables else especiales -->          
                 @endif <!-- Final especiales else aprovechables --> 
                @else<!-- Else peligrososa else aprovechables -->
                  @if(count($especiales)>0)<!-- Else especiales else aprovechables else peligrososa -->
                      <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table> 
                    @if(count($transportes)>0)<!-- Inicio transportes else peligrososa -->
                        <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                        @if(count($pys)>0)<!-- Inicio PYS transportes else peligrososa -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                      @else<!-- Else PYS transportes else peligrososa -->
                       <!-- FIN -->
                      @endif <!-- Fin PYS transportes else peligrososa --> 
                    @else<!-- Else transportes else peligrosos aprovechables especiales if -->
                      @if(count($pys)>0)<!-- Inicio PYS transportes else peligrosos aprovechables especiales if -->
                    <strong><p>Productos y servicios</p></strong>
                      <table>
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                               foreach ($pys as $py) {       
                            ?>
                          <tr>
                            <td >{{($py->pys_id)}}</td>
                            <td >{{($py->nombre)}}</td>
                            <td>${{($py->precio)}}</td>
                          </tr>
                         <?php } ?>
                        </tbody>
                    </table><br>
                      @else<!-- Else PYS transportes else peligrosos aprovechables especiales if -->
                       <!-- FIN -->
                      @endif <!-- Fin PYS transportes else peligrosos aprovechables especiales if --> 
                    @endif<!-- Fin transportes else peligrosos aprovechables especiales if -->

                  @else<!-- Else especiales else aprovechables else peligrososa -->
                      @if(count($transportes)>0)<!-- Inicio transportes else especiales else peligrososa -->
                          <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                     @if(count($pys)>0)<!-- Inicio PYS transportes else especiales else peligrososa -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS transportes else especiales else peligrososa -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS transportes else especiales else peligrososa --> 
                      @else<!-- Else transportes else especiales else peligrososa -->
                         @if(count($pys)>0)<!-- Inicio PYS transportes else especiales else peligrososa -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS transportes else especiales else peligrososa -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS transportes else especiales else peligrososa --> 

                      @endif<!-- Fin transportes else especiales else peligrososa -->
                  @endif<!-- Fin especiales else aprovechables else peligrososa-->
                @endif<!-- Fin peligrososa else aprovechables -->
              @else<!-- Else posconsumos else aprovechables -->

                @if(count($peligrososa)>0)<!-- Inicio peligrososa else posconsumos else aprovechables -->
                    <strong><p>Peligrosos Aprovechables</p></strong><!-- dibujo peligrososa -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                          <th scope="col">Precio por Unidad</th>
                          <th scope="col">Precio por Galones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($peligrososa as $peligrosoa) {
                          ?>
                        <tr>
                          <td >{{($peligrosoa->peligrososa_id)}}</td>
                          <td >{{($peligrosoa->nombre)}}</td>
                          <td >{{($peligrosoa->pretratamiento)}}</td>
                          @if($peligrosoa->precio >0)
                          <td>${{($peligrosoa->precio)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciou >0)
                          <td>${{($peligrosoa->preciou)}}</td>
                          @else
                          <td> - </td>
                          @endif

                          @if($peligrosoa->preciog >0)
                          <td>${{($peligrosoa->preciog)}}</td>

                          @else
                          <td> - </td>
                          @endif
                        </tr>
                       <?php } ?>
                      </tbody>
                  </table>
                  @if(count($especiales)>0)<!-- Inicio especiales peligrososa else posconsumos else aprovechables -->
                            <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                              <table>
                                <thead>
                                  <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Pretratamiento</th>
                                    <th scope="col">Precio por kg</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php 
                                       foreach ($especiales as $especial) {
                                    ?>
                                  <tr>
                                    <td >{{($especial->especiales_id)}}</td>
                                    <td >{{($especial->nombre)}}</td>
                                    <td >{{($especial->pretratamiento)}}</td>
                                    <td>${{($especial->precio)}}</td>
                                  </tr>
                                 <?php } ?>
                                </tbody>
                              </table>
                    @if(count($transportes)>0)<!-- Inicio transportes especiales peligrososa else posconsumos else aprovechables -->
                         <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                        @if(count($pys)>0)<!-- Inicio PYStransportes especiales peligrososa else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS transportes especiales peligrososa else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS transportes especiales peligrososa else posconsumos else aprovechables --> 

                    @else<!-- Else transportes especiales peligrososa else posconsumos else aprovechables -->
                       @if(count($pys)>0)<!-- Inicio PYS Else transportes especiales peligrososa else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS Else transportes especiales peligrososa else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS Else transportes especiales peligrososa else posconsumos else aprovechables --> 
                    @endif <!-- Final transportes especiales peligrososa else posconsumos else aprovechables -->

                  @else<!-- Else especiales peligrososa else posconsumos else aprovechables -->
                     @if(count($transportes)>0)<!-- Inicio transportes else especiales peligrososa else posconsumos else aprovechables -->
                         <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                        @if(count($pys)>0)<!-- Inicio PYS transportes else especiales peligrososa else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS transportes else especiales peligrososa else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS transportes else especiales peligrososa else posconsumos else aprovechables --> 


                      @else<!-- Else transportes else especiales peligrososa else posconsumos else aprovechables -->
                        
                        @if(count($pys)>0)<!-- Inicio PYS Else transportes else especiales peligrososa else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS Else transportes else especiales peligrososa else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS Else transportes else especiales peligrososa else posconsumos else aprovechables --> 

                      @endif  <!-- Final transportes else especiales peligrososa else posconsumos else aprovechables --> 

                  @endif<!-- Final especiales peligrososa else posconsumos else aprovechables -->


                @else<!-- Else peligrososa else posconsumos else aprovechables -->

                 @if(count($especiales)>0)<!-- Inicio especiales else posconsumos else aprovechables -->
                      <strong><p>Especiales</p></strong><!-- Dibujo especiales -->
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Pretratamiento</th>
                          <th scope="col">Precio por kg</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                             foreach ($especiales as $especial) {
                          ?>
                        <tr>
                          <td >{{($especial->especiales_id)}}</td>
                          <td >{{($especial->nombre)}}</td>
                          <td >{{($especial->pretratamiento)}}</td>
                          <td>${{($especial->precio)}}</td>
                        </tr>
                       <?php } ?>
                      </tbody>
                    </table>
                  @if(count($transportes)>0)<!-- Inicio transportes else especiales else posconsumos else aprovechables -->
                      <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                          @if(count($pys)>0)<!-- Inicio PYS  transportes else especiales else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS  transportes else especiales else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS  transportes else especiales else posconsumos else aprovechables --> 


                  @else<!-- Else transportes else especiales else posconsumos else aprovechables -->
                      @if(count($pys)>0)<!-- Inicio PYS  Else transportes else especiales else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS  Else transportes else especiales else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS  Else transportes else especiales else posconsumos else aprovechables --> 

                  @endif <!-- Fin transportes else especiales else posconsumos else aprovechables --> 

                 @else <!-- Else especiales else posconsumos else aprovechables -->
                    @if(count($transportes)>0)<!-- Inicio transportes Else especiales else posconsumos else aprovechables -->
                        <strong><p>Transportes</p></strong><!-- Dibujo transportes -->
                        <table>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Precio por kg</th>
                              <th scope="col">Precio Global</th>        
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                                 foreach ($transportes as $transporte) {
                              ?>
                              
                            <tr>
                              <td >{{($transporte->transporte_id)}}</td>
                              <td >{{($transporte->tipo)}}</td>
                              <td>${{($transporte->preciokg)}}</td>
                              <td>${{($transporte->preciog)}}</td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>

                        @if(count($pys)>0)<!-- Inicio PYS Else especiales else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS  Else Else especiales else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS  Else especiales else posconsumos else aprovechables --> 

                    @else<!-- Else transportes Else especiales else posconsumos else aprovechables -->
                      @if(count($pys)>0)<!-- Inicio PYS Else transportes Else especiales else posconsumos else aprovechables -->
                        <strong><p>Productos y servicios</p></strong>
                          <table>
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                   foreach ($pys as $py) {       
                                ?>
                              <tr>
                                <td >{{($py->pys_id)}}</td>
                                <td >{{($py->nombre)}}</td>
                                <td>${{($py->precio)}}</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                        </table><br>
                          @else<!-- Else PYS  Else transportes Else especiales else posconsumos else aprovechables -->
                           <!-- FIN -->
                          @endif <!-- Fin PYS  Else transportes Else especiales else posconsumos else aprovechables --> 

                    @endif<!-- Else transportes Else especiales else posconsumos else aprovechables -->
                 @endif <!-- Fin especiales else posconsumos else aprovechables --> 

                 
                 @endif<!-- Else peligrososa else posconsumos else aprovechables -->
              @endif<!-- Fin posconsumos else aprovechables -->
    @endif<!-- Final aprovechables -->

  @endif<!-- Final peligrosos -->

  <br><H3 style="font-size: 12pt;"><strong>Observaciones</strong></H3>

     @if(count($aprovechables)>0)

  <p>La recolección de materiales reciclables no tendrá costo mientras las cantidades sean superiores a 250 kilogramos. De no ser así se cobrará un flete adicional de $100.000. </p>
    

@endif




  @if(!empty($ofertas->observacion)>0)

  <p>{{$ofertas->observacion}}</p>

  

</div>
@endif


<p>El costo de transporte está excluido del impuesto a las ventas. </p>
    <p> Nuestros servicios de disposición final de residuos peligrosos se encuentran excluidos del impuesto sobre las ventas – IVA, de acuerdo con el decreto 624 de 1989, en el artículo 476, modificado por el artículo 48 de la Ley 488 de 1998, en el numeral 4º. </p> 
                              
</div><br><br>
    
    

<p>Cordialmente, <br> <strong>{{$user}}</strong><br>Email: {{$useremail}}<br>Equipo comercial</p>


  </body>
  <footer>
  </footer>
  
</html>