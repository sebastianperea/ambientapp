



<!DOCTYPE html>
<html>
<head>
  <title>AMBIENTAPP V1.0 - LOGIN</title>
  
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap/assets/css/vendor.css', true)}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap/assets/css/flat-admin.css', true)}}">
  <link href="{{ asset('jquery-ui.css', true)}}" type="text/css" rel="stylesheet"/>

   <!-- Theme -->
  <link rel="stylesheet" type="text/css" href="{{asset('plugins/bootstrap/assets/css/theme/blue-sky.css', true)}}">
  <link rel="stylesheet" type="text/css" href="{{asset('plugins/bootstrap/assets/css/theme/blue.css', true)}}">
  <link rel="stylesheet" type="text/css" href="{{asset('plugins/bootstrap/assets/css/theme/red.css',true)}}">
  <link rel="stylesheet" type="text/css" href="{{asset('plugins/bootstrap/assets/css/theme/yellow.css', true)}}">

</head>
<body>
  <div class="app app-default">

<div class="app-container app-login">

  <div class="flex-center">
    <div class="app-header"></div>
    <div class="app-body">

      <div class="loader-container text-center">
          <div class="icon">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
              </div>
            </div>
          <div class="title">Logging in...</div>
      </div>
      <div class="app-block">
      <div class="app-form">
        <div class="form-header">
          <div class="app-brand"><img src="{{asset('plugins/images/logo2.png', true)}}"   class="logo" ></div>
        </div>
        <form action="{{ url('/login') }}" role="form" method="POST" class="formulario">
          @if ( count( $errors ) > 0 )
            <div class="alert alert-warning" role="alert">
               @foreach ($errors->all() as $error)
                  <div>{{ $error }}</div>
              @endforeach
            </div>
        @endif </br>
        {{ csrf_field() }}
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">
                <i class="fa fa-envelope-o" aria-hidden="true"></i></span>
              <input type="email" class="form-control" placeholder="E-mail" aria-describedby="basic-addon1" name="email" id="email" value="{{ old('email') }}">
            </div>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon2">
                <i class="fa fa-key" aria-hidden="true"></i></span>
              <input type="password" class="form-control" placeholder="Contraseña" aria-describedby="basic-addon2" name="password">
            </div>
            <div class="text-center">
                <input type="submit" class="btn btn-success btn-submit" value="Ingresar">
            </div>
        </form>

        <div class="form-line">
          
      </div>
      </div>
    </div>
    <div class="app-footer">
    </div>
  </div>
</div>

  </div>
  
  

</body>
</html>


