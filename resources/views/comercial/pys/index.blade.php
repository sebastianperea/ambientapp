@extends('admin.template.main')

@section('content')

@section('titulo', 'Productos y servicios')



<div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          Productos y Servicios
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
            <thead>
                <tr>
                  <th>Id PYS</th>
                  <th>Nombre</th>
                  <th>Precio</th>
                  <th>Fecha y hora</th>
                  <th>Acción</th>

                </tr>
            </thead>
              <tbody>
      
                 <tr> 
               @foreach($pys as $PY)
                  <td>{{$PY->id}}</td>
                  <td>{{$PY->nombre}}</td>
                  <td>{{$PY->precio}}</td>
                  <td>{{$PY->created_at}}</td>
                  <td> <a href="{{ route('pys.edit', $PY->id ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a></td>

               


                 </tr>
              @endforeach
       
    </tbody>
</table>
        </div>
      </div>
    </div>


  







@endsection