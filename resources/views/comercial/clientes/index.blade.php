@extends('admin.template.main')

@section('content')

@section('titulo', 'Ofertas Comerciales')



<div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          Clientes
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
        <tr>
      
  <th>Identificación Cliente</th>
  <th>Razon Social</th>
  <th>Tipo</th>
  <th>Fecha y hora</th>
  <th>Acción</th>

        </tr>
    </thead>
    <tbody>



     @foreach($clientes as $cliente)
   <tr> 
 
   <td>
   {{$cliente->identificacion}}
   </td>
   <td>{{$cliente->razon_social}}</td>
   <td>{{$cliente->tipo}}</td>
   <td>{{$cliente->created_at}}</td>

      <td> <a href="{{ route('clientes.edit', $cliente->identificacion ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a> <a href="{{route('clientes.show' , $cliente->identificacion )}}" target="" class="btn btn-success btn-xs" ><span class="fa fa-eye"> </a></td>

   </tr>


@endforeach
 

   



      
           
          

           
           

          

             

    

            
     
        
       
    </tbody>
</table>


        </div>
      </div>
    </div>
    <br><br><br><br>


  












@endsection
