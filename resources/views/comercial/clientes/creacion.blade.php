
@extends('admin.template.main')

@section('content')

@section('titulo', 'Creación')
<div class="row">

   @if ( count( $errors ) > 0 )
            <div class="alert alert-warning" role="alert">
               @foreach ($errors->all() as $error)
                  <div>{{ $error }}</div>
              @endforeach
            </div>
        @endif </br>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body app-heading">
          <!-- <div class="icon">
            <i class="fa fa-bell" aria-hidden:"true"></i>
          </div> -->
          <div class="app-title">
            <div class="title"><span class="highlight">{{$clientes->razon_social}}</span></div>
            <div class="description">{{$clientes->identificacion}}-{{$clientes->dv}} <br>{{$clientes->estado}}</div><br>
            <a href="{{route('clientes.show' , $clientes->identificacion )}}" class="btn btn-success btn-xs" ><span class="fa fa-arrow-circle-left">  Volver</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card card-tab">
        <div class="card-header">
          <ul class="nav nav-tabs">
            <li role="tab1" class="active">
              <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Creación</a>
            </li>
          </ul>
        </div>
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
              <div class=" col-sm-12">
                <div class="section">
                  <div class="section-title"><i class="icon fa fa-user" aria-hidden="true"></i> Datos generales</div>
                    <div class="section-body __indent">
                       {!! Form::open(['route' => ['comercial.clientes.creacion.up', $clientes],'files' => true, 'enctype'=>'multipart/form-data','method' => 'PUT' ]) !!}

                       <div class="row">
                        <div class="col-xs-3"> 
                          Tipo de Persona:

                        @if ($clientes->tipodepersona == 'natural') 
                              Natural <input type="radio" name="tipodepersona" class="tipodepersona" value="natural" checked="checked">
                              juridica
                            <input type="radio" name="tipodepersona" class="tipodepersona" value="juridica"> <br>
                          @else
                               Natural <input type="radio" name="tipodepersona" class="tipodepersona" value="natural">
                              Juridica
                            <input type="radio" name="tipodepersona" class="tipodepersona" value="juridica" checked="checked"> <br>
                          @endif   

                        </div>
                       </div><br>
                      <div class="row">
                        <div class="col-xs-3">
                            {!! Form::label('Departamento','')  !!}
                            {!!Form::text('departamento',$clientes->departamento, ['class' => 'form-control', 'placeholder' => 'Departamento' , 'required'])!!}
                        </div>
                        <div class="col-xs-3">
                            {!! Form::label('Ciudad','')  !!}
                            {!!Form::text('ciudad',$clientes->ciudad, ['class' => 'form-control', 'placeholder' => 'Departamento' , 'required'])!!}
                        </div>
                        <div class="col-xs-3">
                            {!! Form::label('Dirección','')  !!}
                            {!!Form::text('direccion',$clientes->direccion, ['class' => 'form-control', 'placeholder' => 'Direccion' , 'required'])!!}
                        </div>
                      </div>
                    </div>
                </div>
                <div class="section">
                  <div class="section-title"><i class="icon fa fa-book" aria-hidden="true"></i> Contácto</div>
                    <div class="section-body __indent">
                      <div class="row">
                        <div class="col-xs-3">
                            {!! Form::label('Identificación','')  !!}
                            {!!Form::text('ide',$clientes->ide, ['class' => 'form-control', 'placeholder' => 'Identificación' , 'required'])!!}
                        </div>
                        <div class="col-xs-3">
                            {!! Form::label('Nombres','')  !!}
                            {!!Form::text('nombres',$clientes->nombres, ['class' => 'form-control', 'placeholder' => 'Nombres' , 'required'])!!}
                        </div>
                        <div class="col-xs-3">
                            {!! Form::label('Apellidos','')  !!}
                            {!!Form::text('apellidos',$clientes->apellidos, ['class' => 'form-control', 'placeholder' => 'Apellidos' , 'required'])!!}
                        </div>
                        <div class="col-xs-3">
                            {!! Form::label('Teléfono Celular','')  !!}
                            {!!Form::number('telefonocelular',$clientes->telefonocelular, ['class' => 'form-control', 'placeholder' => 'Télefono Celular' , 'required'])!!} 
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-3">
                            {!! Form::label('E-Mail','')  !!}
                            {!!Form::email('email',$clientes->email, ['class' => 'form-control', 'placeholder' => 'E-Mail' , 'required'])!!}
                        </div>
                        <div class="col-xs-3">
                            {!! Form::label('Cargo','')  !!}
                            {!!Form::text('cargo',$clientes->cargo, ['class' => 'form-control', 'placeholder' => 'Cargo' , 'required'])!!}
                        </div>
                      </div>
                  </div>
                </div>
                <div class="section">
                  <div class="section-title"><i class="icon fa fa-money" aria-hidden="true"></i> Cartera</div>
                    <div class="section-body __indent">
                      <div class="row">
                          <div class="col-sm-3">
                              {!! Form::label('Nombres Y Apellidos Encargado','')  !!}
                              {!!Form::text('nombrecartera',$clientes->nombrecartera, ['class' => 'form-control', 'placeholder' => 'Nombre Encargado' , 'required'])!!}
                          </div>
                          <div class="col-sm-3">
                              {!! Form::label('Teléfono fijo','')  !!}
                              {!!Form::text('telefonofijocartera',$clientes->telefonofijocartera, ['class' => 'form-control', 'placeholder' => 'Teléfono Fijo' , 'required'])!!}
                          </div>
                          <div class="col-sm-1">
                              {!! Form::label('Ext','')  !!}
                              {!!Form::text('ext',$clientes->extcartera, ['class' => 'form-control', 'placeholder' => 'Extensión' , 'required'])!!}
                          </div>
                          <div class="col-sm-3">
                              {!! Form::label('Teléfono móvil','')  !!}
                              {!!Form::text('telefonomovilcartera',$clientes->telefonomovilcartera, ['class' => 'form-control', 'placeholder' => 'Teléfono móvil cartera' , 'required'])!!}
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-3">
                            {!! Form::label('Email','')  !!}
                            {!!Form::email('emailcartera',$clientes->emailcartera, ['class' => 'form-control', 'placeholder' => 'Email Cartera' , 'required'])!!}
                        </div>
                        <div class="col-sm-4">
                            {!! Form::label('Dirección Radicación','')  !!}
                            {!!Form::text('direccionradicacion',$clientes->direccionradicacion, ['class' => 'form-control', 'placeholder' => 'Dirección Radicación' , 'required'])!!}
                        </div>
                      </div>
                    </div>
                </div>
                <div class="section">
                  <div class="section-title"><i class="icon fa fa-money" aria-hidden="true"></i> Información Bancaria</div>
                    <div class="section-body __indent">
                      <div class="row">
                        <div class="col-sm-3">
                            {!! Form::label('Nombre Banco','')  !!}
                            {!!Form::text('nombrebanco',$clientes->nombrebanco, ['class' => 'form-control', 'placeholder' => 'Nombre Banco' , 'required'])!!}
                        </div>

                        <div class="col-sm-3">
                            {!! Form::label('Numero de cuenta','')  !!}
                            {!!Form::number('numerodecuenta',$clientes->numerodecuenta, ['min'=>'0','class' => 'form-control', 'placeholder' => 'Numero de cuenta' , 'required'])!!}
                        </div>

                        <div class="col-sm-3">
                            {!! Form::label('Tipo de cuenta','')  !!}<br>
                            {!!Form::text('tipodecuenta',$clientes->tipodecuenta, ['class' => 'form-control', 'placeholder' => 'Tipo de cuenta' , 'required'])!!}
                        </div>
                      </div>
                    </div>
                </div>
                <div class="section">
                  <div class="section-title"><i class="icon fa fa-money" aria-hidden="true"></i>Referencias </div>
                    <div class="section-body __indent">
                      <div class="row">
                        <div class="col-sm-3">
                            {!! Form::label('Nombre Empresa 1','')  !!}
                            {!!Form::text('nombreempresauno',$clientes->nombreempresauno, ['class' => 'form-control', 'placeholder' => 'Nombre empresa uno' , 'required'])!!}
                        </div>

                        <div class="col-sm-3">
                            {!! Form::label('Telefono empresa 1','')  !!}
                            {!!Form::number('telefonoempresauno',$clientes->numerodecuenta, ['min'=>'0','class' => 'form-control', 'placeholder' => 'Telefono empresa 1' , 'required'])!!}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-3">
                            {!! Form::label('Nombre Empresa 2','')  !!}
                            {!!Form::text('nombreempresados',$clientes->nombreempresados, ['class' => 'form-control', 'placeholder' => 'Nombre empresa 2' , 'required'])!!}
                        </div>

                        <div class="col-sm-3">
                            {!! Form::label('Telefono empresa 2','')  !!}
                            {!!Form::number('telefonoempresados',$clientes->telefonoempresados, ['min'=>'0','class' => 'form-control', 'placeholder' => 'Telefono empresa 2' , 'required'])!!}
                        </div>

                      </div>
                    </div>
                </div>



                   <hr>
                    <div class="seccionarchivos" style="display: none;"><!-- inicio archivos-->
                    <h4 class="seccionarchivos">ADJUNTAR ARCHIVOS (Persona Natural)</h4>
                    <p class="seccionarchivos">Luego de haber llenado todos los campos correctamente por favor adjunte los archivos requeridos respectivamente</p><br>

                    @if($fotocopiacpn == null)
                    <p class="seccionarchivos">Fotocopia de la cedula:<br><br>
                    {!! Form::file('fotocopiacpn', ['class' => 'imprimirboton'])!!}<br></p>
                    @endif

                    @if($rutpn == null)
                    <p class="seccionarchivos">Registro Unico Tributario (RUT)<br><br>
                    {!! Form::file('rutpn', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>
                    @endif

                    @if($certificadodeexistencia == null)
                    <p class="seccionarchivos">Certificado de matricula mercantil no mayor a 30 días (Si aplica)<br><br>
                    {!! Form::file('certificadodeexistencia', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>

                    @endif

                    @if($certificacionbancaria == null)
                    <p class="seccionarchivos">Certificación Bancaria<br><br>
                    {!! Form::file('certificacionbancaria', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>

                    @endif

                    <hr style="height: 1.5px; border:none; background-color:#E6E20D ; width: 350px; margin-top: 40px;">
                    <p> &nbsp; &nbsp; &nbsp;Requiere aprobación de credito:
                    @if($clientes->credito == 'si') 
                    <input type="radio" name="creditopn" value="si" class="creditopn" checked="checked"> Si
                    <input type="radio" name="creditopn" value="no" class="creditopn"> No </p><br>
                    

                    @else
                     <input type="radio" name="creditopn" value="si" class="creditopn" > Si
                    <input type="radio" name="creditopn" value="no" class="creditopn" checked="checked"> No </p><br>

                    @endif

                    
                    <div class="creditopn2" style="display: none;">
                      <p class="seccionarchivos credito">Para solicitud de cupo de credito por favor adjunte la siguiente<br> información si es persona natural:</p><br>
                      <p class="seccionarchivos credito">Declaración de renta (Si declara) o extractos 3 ultimos meses<br><br>
                        @if($declaracionpn == null)
                        
                    {!! Form::file('declaracionpn', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>

                    @endif
                    </div>

                    
                  </div><!-- Fin Archivos Persona natural-->



                  <div class="seccionarchivospj" style="display: none;"><!-- inicio archivos-->
                    <h4 class="seccionarchivospj">ADJUNTAR ARCHIVOS (Persona Jurídica)</h4>
                    <p class="seccionarchivospj">Luego de haber llenado todos los campos correctamente por favor adjunte los archivos requeridos respectivamente</p><br>
                    @if($fotocopiacpj == null)
                    <p class="seccionarchivospj">Fotocopia de la cedula representante legal:<br><br>
                    {!! Form::file('fotorepl', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>
                    @endif
                    @if($rutpj == null)
                    <p class="seccionarchivospj">Registro Unico Tributario (RUT)<br><br>
                    {!! Form::file('rutpj', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>
                    @endif
                    @if($cerlpj == null)
                    <p class="seccionarchivospj">Certificado de existencia y representación legal no mayor a 30 días <br><br>
                    {!! Form::file('certificadoexistencia', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>
                    @endif
                    @if($cbancariapj == null)
                    <p class="seccionarchivospj">Certificación Bancaria<br><br>
                    {!! Form::file('certificacionbancariapj', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>
                    @endif


                    <hr style="height: 1.5px; border:none; background-color:#E6E20D ; width: 350px; margin-top: 40px;">
                    <p> &nbsp; &nbsp; &nbsp;Requiere aprobación de credito: 

                    @if($clientes->credito== 'si')<input type="radio" name="creditopj" value="si" class="creditopj" checked="checked"> Si
                    <input type="radio" name="creditopj" value="no" class="creditopj"> No </p><br>
                    @endif

                    @if($clientes->credito == 'no')<input type="radio" name="creditopj" value="si" class="creditopj" > Si
                    <input type="radio" name="creditopj" value="no" class="creditopj" checked="checked"> No </p><br>

                    @else
                    <input type="radio" name="creditopj" value="si" class="creditopj"> Si
                    <input type="radio" name="creditopj" value="no" class="creditopj"> No </p><br>

                    @endif



                    <div class="creditopj2" style="display: none;">
                      <p class="seccionarchivospj">Para solicitud de cupo de credito por favor adjunte la siguiente<br> información si es persona jurídica:</p><br>
                      @if($drentapj == null)
                      <p class="seccionarchivospj">
                      Declaración de renta<br><br>
                      {!! Form::file('declaracionpj', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>
                      @endif
                      @if($estadosfinancieros == null)
                     <p class="seccionarchivospj">
                      Estados financieros<br><br>
                    {!! Form::file('estadospj', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf'])!!}<br></p>
                    @endif
                  </div>
                </div><!-- Fin Archivos -->

              </div>



                @if($cc == null)
                  <p class="">F.Creación de clientes<br><br>
                  {!! Form::file('creaciondeclientes', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf', 'multiple'])!!}<br></p>
                  @endif

                  @if($info_residuos == null)
                  <p class="">F.información de residuos<br><br>
                  {!! Form::file('inforesiduos', ['class' => 'imprimirboton', 'accept'=>'image/png, .jpeg, .jpg, image/gif, .pdf', 'multiple' ])!!}<br></p>
                  @endif
                  
                      
                    
                
                


              </div>
              <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                               {!! Form::close() !!}
                          </div>
                      </div>
                  </div>   
          </div>
      </div>



              </div>   
          </div>
        </div>
      </div>
    </div>
  </div>



@section('js')
    <script type="text/javascript">
    

  $(document).ready(function(){



 
 // Add more
 $('#agregar').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila'><td><input id='identificacion' class=' form-control' required name='identificacion_[]' type='text' value= '{{$clientes->identificacion}}' readonly></td><td><input type='text' class='form-control' id='nombre' name='nombre_[]'></td><td><input type='text' class='form-control' id='direccion' name='direccion_[]'></td> <td><input type='text' class='form-control' id='departamento' name='departamento_[]'></td> <td><input type='text' class='form-control' id='ciudad' name='ciudad_[]'></td><td class='eliminar btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla tbody').append(html);
 
 });

$(document).on("click",".eliminar",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>


  <script>
      $(document).ready(function(){

        var check = $('input[name="tipodepersona"]:checked').val();
        if(check == 'natural'){

                $(".seccionarchivos").show();
                $(".seccionarchivospj").hide();
            }else{

                $(".seccionarchivospj").show();
                $(".seccionarchivos").hide();
            }


        $(".tipodepersona").click(function(evento){
              
            var valor = $(this).val();

          
            if(valor == 'natural'){

                $(".seccionarchivos").show();
                $(".seccionarchivospj").hide();
            }else{

                $(".seccionarchivospj").show();
                $(".seccionarchivos").hide();
            }
              });
          });
    </script>

    <script>
      $(document).ready(function(){

        $(".creditopn").click(function(evento){
              
            var valor = $(this).val();

            if(valor == 'si'){
              $(".creditopn2").show();
                
            }
            else{

                $(".creditopn2").hide();
            }
              });
          });
    </script>
  

    <script>
      $(document).ready(function(){

         var check = $('input[name="creditopj"]:checked').val();

        if(check == 'si'){
              $(".creditopj2").show();
                
            }
            else{

                $(".creditopj2").hide();
            }

        $(".creditopj").click(function(evento){
              
            var valor = $(this).val();

            if(valor == 'si'){
              $(".creditopj2").show();
                
            }
            else{

                $(".creditopj2").hide();
            }
              });
          });


    </script>

    
  
@endsection

@endsection


