
@extends('admin.template.main')

@section('content')

@section('titulo', 'Clientes')
<div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body app-heading">
          <!-- <div class="icon">
            <i class="fa fa-bell" aria-hidden:"true"></i>
          </div> -->
          <div class="app-title">
            <div class="title"><span class="highlight">{{$clientes->razon_social}}</span></div>
            <div class="description">{{$clientes->identificacion}}-{{$clientes->dv}} <br>{{$clientes->estado}}</div><br>
            
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card card-tab">
        <div class="card-header">
          <ul class="nav nav-tabs">
            <li role="tab1" class="active">
              <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Perfil</a>
            </li>
            <li role="tab3">
              <a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Archivos</a>
            </li>
            <li role="tab4">
              <a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">Residuos</a>
            </li>

            <li role="tab5">
              <a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">Sedes</a>
            </li>

            <li role="tab6">
              <a href="#tab6" aria-controls="tab6" role="tab" data-toggle="tab">Seguimiento</a>
            </li>

          </ul>
        </div>
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
              <div class="col-md-3 col-sm-12">
                <div class="section">
                  <div class="section-title"><i class="icon fa fa-user" aria-hidden="true"></i> Datos generales</div>
                  <div class="section-body __indent">Departamento: {{$clientes->departamento}} <br> Ciudad: {{$clientes->ciudad}}<br> Dirección: {{$clientes->direccion}} <br> Télefono: {{$clientes->telefono_contacto}} Ext: {{$clientes->ext}}<br> Tipo: {{$clientes->tipo}}</div>
                </div>
                <div class="section">
                  <div class="section-title"><i class="icon fa fa-book" aria-hidden="true"></i> Contácto</div>
                  <div class="section-body __indent">{{$clientes->nombres}}<br>{{$clientes->apellidos}}<br>{{$clientes->cargo}}<br>{{$clientes->telefonocelular}} <br>{{$clientes->email}}</div>
                </div>

                <div class="section">
                  <div class="section-title"><i class="icon fa fa-book" aria-hidden="true"></i>Información</div>
                  <div class="section-body __indent">
                    @if($userifexist)
                    <p>Ultimo acceso: {{ Carbon\Carbon::parse($user->lastlogin)->diffForHumans() }}</p>
                    @endif
                    <a href="{{route('comercial.clientes.creacion', $clientes->identificacion)}}" class="btn btn-success btn-xs">Completar Info. <span class="fa fa-pencil"></a>

                    @if($userifexist)
                    
                    <a href="{{route('comercial.users.edit', $clientes->identificacion)}}" class="btn btn-primary btn-xs">Editar Usuario <span class="fa fa-pencil"></a>

                      @else
                      
                    <a href="{{route('comercial.users.crearyenviar', $clientes->identificacion)}}" class="btn btn-warning btn-xs">Crear usuario y enviar por email <span class="fa fa-envelope"></a>
                      @endif
                      
                    </div>
                </div>
              </div>
              <div class="col-md-9 col-sm-12">
                <div class="section">
                  <div class="section-title">Ofertas</div>
                  <div class="section-body">
                    <div class="media social-post">
                      <div class="media-left">
                        <a href="#">
                          <img src="../assets/images/profile.png" />
                        </a>
                      </div>
                      
                    </div>
                    <div class="media social-post">
                      
                      <div class="media-body">
                        
                        <table class="datatable table table-striped primary" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                            
                        <th>Id oferta</th>
                        <th>Estado</th>
                        <th>Fecha y hora</th>
                        <th>Acción</th>

                              </tr>
                          </thead>
                          <tbody>
                           

                           @foreach($ofertas as $oferta)
                         <tr> 
                          <td> {{$oferta->id}}</td>
                         <td> @if($oferta->Estado=="En espera") 
                         <span class="label label-warning">{{$oferta->Estado}}</span>
                         @else
                      <span class="label label-primary">{{$oferta->Estado}}</span>
                         @endif</td>
                         <td>{{$oferta->created_at}}</td>

                         <td> <a href="{{ route('ofertas.edit', $oferta->id ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a> <a href="{{route('ofertas.pdf' , $oferta->id )}}" target="_blank" class="btn btn-success btn-xs" ><span class="fa fa-eye"> </a> <a href="{{route('ofertas.pdf.download' , $oferta->id )}}" target="" class="btn btn-primary btn-xs" ><span class="fa fa-arrow-down"> </a></td>
                         </tr>
                        @endforeach

                    </tbody>
                  </table> 
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="tab3">
            <div class="card">
              <div class="card-body">
                @if($clientes->tipodepersona == 'juridica')
                <div class="section">
                    <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i> Persona {{$clientes->tipodepersona}}</div>
                    <div class="section-body __indent"></div>
                </div>

                <div class="media">
                  <div class="media-left">
                  <a href="#">
                  <img src="{{asset('plugins/images/addfile.png')}}" />
                  </a>
                </div>
                <div class="media-body">
                  @if(count($cbancariapj)>0)
                    <div class="section">
                     <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>Certificados bancarios</div>
                      <div class="section-body __indent"></div>
                    </div>
                    @foreach($cbancariapj as $cbanpj)
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img src="{{asset('plugins/images/files4.jpg')}}" />
                        </a>
                      </div>
                        <div class="media-body">
                          <h4 class="media-heading">{{$cbanpj->nombre}}</h4>
                          <p>{{$cbanpj->created_at}}</p>
                          <div><a href=" {{route('viewfiles' , $cbanpj->nombre )}}" target="_blank" class="btn btn-success btn-xs" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $cbanpj->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                        </div>
                   </div>
                   @endforeach
                  @endif
                  @if(count($cerlpj)>0)  
                    <div class="section">
                            <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Certificados de existencia</div>
                            <div class="section-body __indent"></div>
                          </div>
                          @foreach($cerlpj as $cerpj)
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img src="{{asset('plugins/images/files4.jpg')}}" />
                        </a>
                      </div>
                        <div class="media-body">
                          <h4 class="media-heading">{{$cerpj->nombre}}</h4>
                          <p>{{$cerpj->created_at}}</p>
                          <div><a href=" {{route('viewfiles' , $cerpj->nombre )}}" target="_blank" class="btn btn-success btn-xs" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $cerpj->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                        </div>
                  </div>
                  @endforeach
                  @endif

                    @if(count($fotocopiacpj)>0)  
                  <div class="section">
                          <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Fotocopia cedula representante legal</div>
                          <div class="section-body __indent"></div>
                        </div>
                        @foreach($fotocopiacpj as $fotopj)
                  <div class="media">
                    <div class="media-left">
                      <a href="#">
                        <img src="{{asset('plugins/images/files4.jpg')}}" />
                      </a>
                    </div>
                      <div class="media-body">
                        <h4 class="media-heading">{{$fotopj->nombre}}</h4>
                        <p>{{$fotopj->created_at}}</p>
                        <div><a href=" {{route('viewfiles' , $fotopj->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $fotopj->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                      </div>
                </div>
                  @endforeach

                  @endif

                   @if(count($rutpj)>0)  
                      <div class="section">
                              <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>Rut</div>
                              <div class="section-body __indent"></div>
                         </div>
                            @foreach($rutpj as $rtpj)
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img src="{{asset('plugins/images/files4.jpg')}}" />
                          </a>
                        </div>
                          <div class="media-body">
                            <h4 class="media-heading">{{$rtpj->nombre}}</h4>
                            <p>{{$rtpj->created_at}}</p>
                            <div><a href=" {{route('viewfiles' , $rtpj->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $rtpj->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                          </div>
                    </div>
                      @endforeach

                      @endif


                      @if(count($drentapj)>0)  
                      <div class="section">
                              <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Declaración de renta </div>
                              <div class="section-body __indent"></div>
                            </div>
                            @foreach($drentapj as $drpj)
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img src="{{asset('plugins/images/files4.jpg')}}" />
                          </a>
                        </div>
                          <div class="media-body">
                            <h4 class="media-heading">{{$drpj->nombre}}</h4>
                            <p>{{$drpj->created_at}}</p>
                            <div><a href=" {{route('viewfiles' , $drpj->nombre )}}"  class="btn btn-success btn-xs" target="_blank"role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $drpj->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                          </div>
                    </div>
                      @endforeach

                      @endif


                       @if(count($estadosfinancieros)>0)  
                      <div class="section">
                              <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Estados Financieros</div>
                              <div class="section-body __indent"></div>
                            </div>
                            @foreach($estadosfinancieros as $estados)
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img src="{{asset('plugins/images/files4.jpg')}}" />
                          </a>
                        </div>
                          <div class="media-body">
                            <h4 class="media-heading">{{$estados->nombre}}</h4>
                            <p>{{$estados->created_at}}</p>
                            <div><a href=" {{route('viewfiles' , $estados->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $estados->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                          </div>
                    </div>

                    
                      @endforeach

                      @endif


                </div>

                </div>
                @endif
                <br>  

                  @if($clientes->tipodepersona == 'natural')
                    <div class="section">
                      <div class="section-title" style="center"> Persona Natural</div>
                      <div class="section-body __indent"></div>
                        <div class="media-body">
                          @if(count($cbancariapn)>0)
                          <div class="section">
                            <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Certificación Bancaria</div>
                            <div class="section-body __indent"></div>
                          </div>
                             @foreach($cbancariapn as $cbancaria)
                               <div class="media">
                                <div class="media-left">
                                  <a href="#">
                                    <img src="{{asset('plugins/images/files4.jpg')}}" />
                                  </a>
                                </div>
                                <div class="media-body">
                                  <h4 class="media-heading">{{$cbancaria->nombre}}</h4>
                                  <p>{{$cbancaria->created_at}}</p>
                                  <div><a href=" {{route('viewfiles' , $cbancaria->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $cbancaria->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                                </div>
                               </div>
                             @endforeach
                          @endif
                             @if(count($cmpn)>0)
                              <div class="section">
                                <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Certificado mercantil </div>
                                <div class="section-body __indent"></div>
                              </div>
                              @foreach($cmpn as $cm)
                                <div class="media">
                                  <div class="media-left">
                                    <a href="#">
                                      <img src="{{asset('plugins/images/files4.jpg')}}" />
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="media-heading">{{$cm->nombre}}</h4>
                                    <p>{{$cm->created_at}}</p>
                                    <div><a href=" {{route('viewfiles' , $cm->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $cm->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                                  </div>
                                </div>
                              @endforeach
                             @endif
                              @if(count($drentapn)>0)
                                <div class="section">
                                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Declaración de renta </div>
                                  <div class="section-body __indent"></div>
                                </div>
                                @foreach($drentapn as $drenta)
                                   <div class="media">
                                    <div class="media-left">
                                      <a href="#">
                                        <img src="{{asset('plugins/images/files4.jpg')}}" />
                                      </a>
                                    </div>
                                    <div class="media-body">
                                      <h4 class="media-heading">{{$drenta->nombre}}</h4>
                                      <p>{{$drenta->created_at}}</p>
                                      <div><a href=" {{route('viewfiles' , $drenta->nombre )}}"  class="btn btn-success btn-xs" target="_blank"role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $drenta->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                                    </div>
                                   </div>
                                @endforeach
                              @endif
                                @if(count($fotocopiacpn)>0)
                                  <div class="section">
                                    <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>   Fotocopia cedula </div>
                                    <div class="section-body __indent"></div>
                                  </div>
                                  @foreach($fotocopiacpn as $foto)
                                    <div class="media">
                                      <div class="media-left">
                                        <a href="#">
                                          <img src="{{asset('plugins/images/files4.jpg')}}" />
                                        </a>
                                      </div>
                                        <div class="media-body">
                                          <h4 class="media-heading">{{$foto->nombre}}</h4>
                                          <p>{{$foto->created_at}}</p>
                                          <div><a href=" {{route('viewfiles' , $foto->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $foto->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                                        </div>  
                                  </div>
                                  @endforeach
                                @endif
                                 @if(count($rutpn)>0)
                                 <div class="section">
                                  <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i> Rut persona natural</div>
                                  <div class="section-body __indent"></div>
                                </div>
                                @foreach($rutpn as $rt)
                                <div class="media">
                                  <div class="media-left">
                                    <a href="#">
                                      <img src="{{asset('plugins/images/files4.jpg')}}" />
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="media-heading">{{$rt->nombre}}</h4>
                                    <p>{{$rt->created_at}}</p>
                                    <div><a href=" {{route('viewfiles' , $rt->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $rt->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                                  </div>
                                </div>
                                @endforeach
                                 @endif
                        </div> 
                </div>
                  @endif
                    @if(count($cc)>0)
                     <div class="section">
                      <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i> Formato de creación de clientes</div>
                      <div class="section-body __indent"></div>
                    </div>
                    @foreach($cc as $c)
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img src="{{asset('plugins/images/files4.jpg')}}" />
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading">{{$c->nombre}}</h4>
                        <p>{{$c->created_at}}</p>
                        <div><a href=" {{route('viewfiles' , $c->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $c->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                      </div>
                    </div>
                    @endforeach
                    @endif

                     @if(count($inforesiduos)>0)  
                      <div class="section">
                              <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i> Formato de información de residuos</div>
                              <div class="section-body __indent"></div>
                            </div>
                            @foreach($inforesiduos as $info)
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img src="{{asset('plugins/images/files4.jpg')}}" />
                          </a>
                        </div>
                          <div class="media-body">
                            <h4 class="media-heading">{{$info->nombre}}</h4>
                            <p>{{$info->created_at}}</p>
                            <div><a href=" {{route('viewfiles' , $info->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefiles' , $info->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                          </div>
                    </div>
                    @endforeach
                    @endif
              </div>
          </div>
        </div>

          <div role="tabpanel" class="tab-pane" id="tab4">
            <div class="card">
              <div class="card-body">
                
                @if(count($filespeligrosos)>0)
               <div class="section">
                        <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i> Peligrosos</div>
                        <div class="section-body __indent"></div>
                      </div>
                      @foreach($filespeligrosos as $peligrosos)
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img src="{{asset('plugins/images/files4.jpg')}}" />
                    </a>
                  </div>
                    <div class="media-body">
                      <h4 class="media-heading">{{$peligrosos->nombre}}</h4>
                      <p>{{$peligrosos->created_at}}</p>
                      <div><a href=" {{route('viewfilesresiduos' , $peligrosos->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefilesresiduos' , $peligrosos->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                    </div>
              </div>
              @endforeach

             @endif


             @if(count($filespeligrososa)>0)
               <div class="section">
                        <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i> Peligrosos Aprovechables</div>
                        <div class="section-body __indent"></div>
                      </div>
                      @foreach($filespeligrososa as $peligrososa)
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img src="{{asset('plugins/images/files4.jpg')}}" />
                    </a>
                  </div>
                    <div class="media-body">
                      <h4 class="media-heading">{{$peligrososa->nombre}}</h4>
                      <p>{{$peligrososa->created_at}}</p>
                      <div><a href=" {{route('viewfilesresiduos' , $peligrososa->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefilesresiduos' , $peligrososa->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                    </div>
              </div>
              @endforeach

             @endif


             @if(count($especiales3)>0)
               <div class="section">
                        <div class="section-title"><i class="icon fa fa-file-pdf-o" aria-hidden="true"></i> Especiales</div>
                        <div class="section-body __indent"></div>
                      </div>
                      @foreach($especiales3 as $especial)
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img src="{{asset('plugins/images/files4.jpg')}}" />
                    </a>
                  </div>
                    <div class="media-body">
                      <h4 class="media-heading">{{$especial->nombre}}</h4>
                      <p>{{$especial->created_at}}</p>
                      <div><a href=" {{route('viewfilesresiduos' , $especial->nombre )}}"  class="btn btn-success btn-xs" target="_blank" role="button">Ver y descargar</a>  <a href=" {{route('deletefilesresiduos' , $especial->nombre)}}"  class="btn btn-danger btn-xs" role="button"> Borrar </a></div>
                    </div>
              </div>
              @endforeach

             @endif
              </div>
            </div>
          </div>



<div role="tabpanel" class="tab-pane" id="tab5">

                <button
                     type="button"
                     class="btn btn-success btn-xs"
                      data-toggle="modal"
                      data-sedeidentificacion="{{ $clientes->identificacion }}"
                      data-target="#sedesmodal"> Crear nueva 
                  <span class="fa fa-plus">
                </button>

  
        <br><br>
    <table class="datatable table table-striped primary" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Nombre</th>
                      <th>Dirección</th>
                      <th>Departamento</th>
                      <th>Ciudad</th>
                      <th>Responsable</th>
                      <th>Acción</th>
                  </tr>
              </thead>
              
              <tbody>
                @foreach($sedes as $sede )
                <tr>
                  <td>{{ $sede->id }}</td>
                  <td>{{ $sede->nombre }}</td>
                  <td>{{ $sede->direccion }}</td>
                  <td>{{ $sede->departamento }}</td>
                  <td>{{ $sede->ciudad }}</td>
                  <td>{{ $sede->responsable }}</td>
                  <td><a href="{{ route('clientes.sedes.edit', [$sede->id, $sede->identificacion] ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a>
                   </a>
                  </td>
                </tr>
                @endforeach
              </tbody>

            </table>

     </div>
            
            
      <div role="tabpanel" class="tab-pane" id="tab6">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="list-group">
                      <a href="#" class="list-group-item list-group-item-success">
                        <span class="badge badge-success">{{ $seguimientoscountp }}</span> Presencial
                      </a>
                      <a href="#" class="list-group-item list-group-item-success">
                        <span class="badge badge-success">{{ $seguimientoscountpw }}</span> Pagina Web
                      </a>
                      <a href="#" class="list-group-item list-group-item-success">
                        <span class="badge badge-info">{{ $seguimientocountee }}</span> Email Enviado
                      </a>
                      <a href="#" class="list-group-item list-group-item-success">
                        <span class="badge badge-info">{{ $seguimientocounter }}</span> Email Recibido
                      </a>
                      <a href="#" class="list-group-item list-group-item-success">
                        <span class="badge badge-warning">{{ $seguimientocountle }}</span> Llamada Entrante
                      </a>

                      <a href="#" class="list-group-item list-group-item-success">
                        <span class="badge badge-warning">{{ $seguimientocountls }}</span> Llamada Saliente
                      </a>

                      <a href="#" class="list-group-item list-group-item-success">
                        <span class="badge ">{{ $seguimientocountsecop }}</span> Secop
                      </a>
                      <a href="#" class="list-group-item list-group-item-danger">
                        <span class="badge badge-danger">{{ $seguimientocountvenc }}</span>
                        Vencidos
                      </a>
                    </div>
                  </div>
                  <H4>Últimos Seguimientos</H4>
                  @foreach($seguimientos as $seguimiento)
                  <div class="col-sm-6">

                    <div class="list-group __timeline">

                      <a data-toggle="modal" href="#favoritesModal" class="list-group-item"  data-id="{{ $seguimiento->id }}"
                     data-title="{{ $seguimiento->razon_social }}"
                      data-fecha="{{ $seguimiento->created_at }}"
                    data-procidentificacion="{{ $seguimiento->identificacion}}"
                    data-procprocesos="{{ $seguimiento->procesos}}"
                    data-proctelcontacto="{{ $seguimiento->telefono_contacto}}"
                    data-procemail="{{ $seguimiento->email}}"
                    data-procobservaciones="{{ $seguimiento->observaciones}}"
                    data-usuario="{{ $seguimiento->name}}"
                    data-estado="{{ $seguimiento->ESTADO }}"> 
                        {{ $seguimiento->observaciones }}
                      </a>
                    </div>

                  </div>




                  @endforeach

                </div>
              </div>
            </div>
          </div>


          
        
      </div>
      <div class="modal fade" id="favoritesModal"
       tabindex="-1" role="dialog"
       aria-labelledby="favoritesModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close"
            data-dismiss="modal"
            aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><strong>Proceso Comercial #  <span id="idproc"></strong> </h4>
            <p>Fecha y hora: <span id="fecha" class=""> </p><br>
              <p>Estado:  <span id="proc-estado" class="badge badge-warning"></p>
        </div>
        <div class="modal-body">
         
          <p><strong>Usuario:</strong> <span id="usuario"></span> </p>
                       <h4><strong>Detalles proceso</strong></h4><br>
            <p>Observaciones:
              <span id="proc-observaciones"></span></p>
        
        
      </div>
        <div class="modal-footer">
           
          <button type="button"
             class="btn btn-default"
             data-dismiss="modal">Cerrar</button>
          <span class="pull-right">
            
          </span>
        </div>
      </div>
    </div>
  </div>
 
<!--  Fin modal seguimientos -->

 <div class="modal fade" id="sedesmodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close"
          data-dismiss="modal"
          aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
          <h4 class="modal-title"><strong>Crear nueva sede  <span id="idproc"></strong> 
          </h4>
      </div>
      <div class="modal-body"> 
          {!! Form::open(['route' => 'clientes.sedes.save', 'method' => 'POST']) !!}

            <div class="col-sm-8">
                     {!! Form::label('identificacion','Identificación')!!}
                      {!!Form::number('identificacion',null, ['class' => 'form-control sedeidentificacion', 'id'=> 'sedeidentificacion', 'placeholder' => 'Identificación', 'required', 'readonly']) !!}
                  </div>
              <div class="col-sm-6">
                      {!! Form::label('nombre','Nombre')!!}
                      {!!Form::text('nombre',null, ['class' => 'form-control nombre', 'id'=>'nombre', 'placeholder' => 'Nombre Sede'])!!}
                </div>
                <div class="col-sm-6">
                      {!! Form::label('direccion','Dirección')!!}
                      {!!Form::text('direccion',null, ['class' => 'form-control direccion', 'id'=>'Dirección', 'placeholder' => 'Dirección'])!!}
                  </div>

                  <div class="col-sm-6">
                      {!! Form::label('departamento','Departamento')!!}
                      {!!Form::text('departamento',null, ['class' => 'form-control departamento', 'id'=>'Departamento', 'placeholder' => 'Departamento'])!!}
                  </div>
                  <div class="col-sm-6">
                      {!! Form::label('ciudad','Ciudad')!!}
                      {!!Form::text('ciudad',null, ['class' => 'form-control ciudad', 'id'=>'ciudad', 'placeholder' => 'Ciudad'])!!}
                  </div>
                
                      <div id="target" style="display: none;">
                      </div>
          <br>

                  <div class="col-sm-12">
                      {!! Form::label('responsable','Nombres y apellidos responsable')!!}
                      {!!Form::text('responsable',null, ['class' => 'form-control responsable', 'id'=>'responsable', 'placeholder' => 'responsable'])!!}
                  </div>

                  <div class="col-sm-6">
                      {!! Form::label('email','E-mail')!!}
                      {!!Form::email('email',null, ['class' => 'form-control email', 'id'=>'email', 'placeholder' => 'email'])!!}
                  </div>

                  <div class="col-sm-6">
                     {!! Form::label('telefono','Télefono')!!}
                      {!!Form::number('telefono',null, ['class' => 'form-control email', 'id'=>'email', 'placeholder' => 'Télefono', 'min' => 0])!!}
                  </div>
                  
                <div class="col-sm-4">
                  <input type="text" name="tipo_de_cliente" value="ACTUAL" readonly="readonly" style="display: none;">
                </div>
               {!!Form::number('idv',null, ['style'=> 'display:none;','class' => 'form-control id', 'id'=> 'id', 'required', 'readonly']) !!}
                 <hr>
                      <div class="row">
                        <div class="col-lg-12">
                            <div class="col-xs-2">
                                 {!!Form::submit('Guardar', ['class' => 'btn btn-primary guardar', 'id'=>'guardar'])!!}
                                 {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
      </div>
      <div class="modal-footer">
        <button type="button"
           class="btn btn-default"
           data-dismiss="modal">Cerrar
         </button>
      </div>

    </div>
  </div>
</div>

  <!-- Fin modal create sede -->


    </div>
  </div>



@endsection
@section('js')



<script>
      $(document).ready(function(){

        $(".tipodepersona").click(function(evento){
              
            var valor = $(this).val();

          
            if(valor == 'natural'){

                $(".seccionarchivos").show();
                $(".seccionarchivospj").hide();
            }else{

                $(".seccionarchivospj").show();
                $(".seccionarchivos").hide();
            }
              });
          });
    </script>

    <script>
      $(document).ready(function(){

        $(".creditopn").click(function(evento){
              
            var valor = $(this).val();

            if(valor == 'si'){
              $(".creditopn2").show();
                
            }
            else{

                $(".creditopn2").hide();
            }
              });
          });
    </script>
  

    <script>
      $(document).ready(function(){

        $(".creditopj").click(function(evento){
              
            var valor = $(this).val();

            if(valor == 'si'){
              $(".creditopj2").show();
                
            }
            else{

                $(".creditopj2").hide();
            }
              });
          });
    </script>
   

   <script type="text/javascript">
    

  $(document).ready(function(){



 
 // Add more
 $('#agregar').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila'><td><input id='identificacion' class=' form-control' required name='identificacion_[]' type='text' value= '{{$clientes->identificacion}}' readonly></td><td><input type='text' class='form-control' id='nombre' name='nombre_[]'></td><td><input type='text' class='form-control' id='direccion' name='direccion_[]'></td> <td><input type='text' class='form-control' id='departamento' name='departamento_[]'></td> <td><input type='text' class='form-control' id='ciudad' name='ciudad_[]'></td><td class='eliminar btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla tbody').append(html);
 
 });

$(document).on("click",".eliminar",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>

  <script>
$(function() {
    $('#sedesmodal').on("show.bs.modal", function (event) {
      
      var button = $(event.relatedTarget); 
      var sedesidentificacion = button.data("sedeidentificacion");
       
      document.getElementById('sedeidentificacion').value= sedesidentificacion;
         
    });
});
</script>


<script type="text/javascript">
 /* global jQuery */

(function($){
    'use strict';

    $.fn.modalSteps = function(options){
        var $modal = this;

        var settings = $.extend({
            btnCancelHtml: 'Cancel',
            btnPreviousHtml: 'Previous',
            btnNextHtml: 'Editar',
            btnLastStepHtml: '',
            disableNextButton: false,
            completeCallback: function(){},
            callbacks: {}
        }, options);


        var validCallbacks = function(){
            var everyStepCallback = settings.callbacks['*'];

            if (everyStepCallback !== undefined && typeof(everyStepCallback) !== 'function'){
                throw 'everyStepCallback is not a function! I need a function';
            }

            if (typeof(settings.completeCallback) !== 'function') {
                throw 'completeCallback is not a function! I need a function';
            }

            for(var step in settings.callbacks){
                if (settings.callbacks.hasOwnProperty(step)){
                    var callback = settings.callbacks[step];

                    if (step !== '*' && callback !== undefined && typeof(callback) !== 'function'){
                        throw 'Step ' + step + ' callback must be a function';
                    }
                }
            }
        };

        var executeCallback = function(callback){
            if (callback !== undefined && typeof(callback) === 'function'){
                callback();
                return true;
            }
            return false;
        };

        $modal
            .on('show.bs.modal', function(){
                var $modalFooter = $modal.find('.modal-footer'),
                    $btnCancel = $modalFooter.find('.js-btn-step[data-orientation=cancel]'),
                    $btnPrevious = $modalFooter.find('.js-btn-step[data-orientation=previous]'),
                    $btnNext = $modalFooter.find('.js-btn-step[data-orientation=next]'),
                    everyStepCallback = settings.callbacks['*'],
                    stepCallback = settings.callbacks['1'],
                    actualStep,
                    $actualStep,
                    titleStep,
                    $titleStepSpan,
                    nextStep;

                if (settings.disableNextButton){
                    $btnNext.attr('disabled', 'disabled');
                }
                $btnPrevious.attr('disabled', 'disabled');

                validCallbacks();
                executeCallback(everyStepCallback);
                executeCallback(stepCallback);

                // Setting buttons
                $btnCancel.html(settings.btnCancelHtml);
                $btnPrevious.html(settings.btnPreviousHtml);
                $btnNext.html(settings.btnNextHtml);

                $actualStep = $('<input>').attr({
                    'type': 'hidden',
                    'id': 'actual-step',
                    'value': '1',
                });

                $modal.find('#actual-step').remove();
                $modal.append($actualStep);

                actualStep = 1;
                nextStep = actualStep + 1;

                $modal.find('[data-step=' + actualStep + ']').removeClass('hide');
                $btnNext.attr('data-step', nextStep);

                titleStep = $modal.find('[data-step=' + actualStep + ']').data('title');
                $titleStepSpan = $('<span>')
                                    .addClass('label label-success')
                                    .html(actualStep);

                $modal
                    .find('.js-title-step')
                    .append($titleStepSpan)
                    .append(' ' + titleStep);
            })
            .on('hidden.bs.modal', function(){
                var $actualStep = $modal.find('#actual-step'),
                    $btnNext = $modal.find('.js-btn-step[data-orientation=next]');

                $modal
                    .find('[data-step]')
                    .not($modal.find('.js-btn-step'))
                    .addClass('hide');

                $actualStep
                    .not($modal.find('.js-btn-step'))
                    .remove();

                $btnNext
                    .attr('data-step', 1)
                    .html(settings.btnNextHtml);

                $modal.find('.js-title-step').html('');
            });

        $modal.find('.js-btn-step').on('click', function(){
            var $btn = $(this),
                $actualStep = $modal.find('#actual-step'),
                $btnPrevious = $modal.find('.js-btn-step[data-orientation=previous]'),
                $btnNext = $modal.find('.js-btn-step[data-orientation=next]'),
                $title = $modal.find('.js-title-step'),
                orientation = $btn.data('orientation'),
                actualStep = parseInt($actualStep.val()),
                everyStepCallback = settings.callbacks['*'],
                steps,
                nextStep,
                $nextStep,
                newTitle;

            steps = $modal.find('div[data-step]').length;

            // Callback on Complete
            if ($btn.attr('data-step') === 'complete'){
                settings.completeCallback();
                
                return;
            }

            // Check the orientation to make logical operations with actualStep/nextStep
            if (orientation === 'next'){
                nextStep = actualStep + 1;

                $btnPrevious.attr('data-step', actualStep);
                $actualStep.val(nextStep);

            } else if (orientation === 'previous'){
                nextStep = actualStep - 1;

                $btnNext.attr('data-step', actualStep);
                $btnPrevious.attr('data-step', nextStep - 1);

                $actualStep.val(actualStep - 1);

            } else {
                $modal.modal('hide');
                return;
            }

            if (parseInt($actualStep.val()) === steps){
                $btnNext
                    .attr('data-step', 'complete')
                    .html(settings.btnLastStepHtml)
                    
                    ;
            } else {
                $btnNext
                    .attr('data-step', nextStep)
                    .html(settings.btnNextHtml);
            }

            if (settings.disableNextButton){
                $btnNext.attr('disabled', 'disabled');
            }

            // Hide and Show steps
            $modal
                .find('[data-step=' + actualStep + ']')
                .not($modal.find('.js-btn-step'))
                .addClass('hide');

            $modal
                .find('[data-step=' + nextStep + ']')
                .not($modal.find('.js-btn-step'))
                .removeClass('hide');

            // Just a check for the class of previous button
            if (parseInt($btnPrevious.attr('data-step')) > 0 ){
                $btnPrevious.removeAttr('disabled');
            } else {
                $btnPrevious.attr('disabled', 'disabled');
            }

            if (orientation === 'previous'){
                $btnNext.removeAttr('disabled');
            }

            // Get the next step
            $nextStep = $modal.find('[data-step=' + nextStep + ']');

            // Verify if we need to unlock continue btn of the next step
            if ($nextStep.attr('data-unlock-continue')){
                $btnNext.removeAttr('disabled');
            }

            // Set the title of step
            newTitle = $nextStep.attr('data-title');
            var $titleStepSpan = $('<span>')
                                .addClass('label label-success')
                                .html(nextStep);

            $title
                .html($titleStepSpan)
                .append(' ' + newTitle);

            var stepCallback = settings.callbacks[$actualStep.val()];
            executeCallback(everyStepCallback);
            executeCallback(stepCallback);
        });

        return this;
    };
}(jQuery));

</script>
@endsection

