@extends('admin.template.main')

@section('titulo')
Editar sede

@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
      <div class="card">
          <div class="app-title">
            <div class="title"><span class="highlight">{{$sede->id}}</span></div>
            <div class="description">Editar Sede</div>
            Fecha de creación: {{ $sede->created_at }}<br>
            
            <a href="{{ URL::previous() }}">Volver </a> 

          </div>
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="card card-tab card-mini">
        <div class="card-header">
          <ul class="nav nav-tabs">
            <li role="tab1" class="active">
              <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">General</a>
            </li>  
          </ul>
        </div>
        <div class="card-body tab-content no-padding">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
            {!! Form::open(['route' => ['clientes.sedes.update', $sede->id, $sede->identificacion], 'method' => 'PUT']) !!}
          <div class="col-sm-12">
      
                 <div class="col-sm-4">

                    {!! Form::Label('Nombre', 'Nombre') !!}
                    {!!Form::text('Nombre',$sede->nombre, ['class' => 'form-control', 'placeholder' => 'Nombre' , 'required'])!!}
                  </div> 

                  <div class="col-sm-4">
                    {!! Form::Label('Direccion', 'Dirección') !!}
                    {!!Form::text('direccion',$sede->direccion, ['class' => 'form-control', 'placeholder' => 'Dirección' , 'required'])!!}
                  </div> 

                  <div class="col-sm-2">
                    {!! Form::Label('Departamento', 'Departamento') !!}
                    {!!Form::text('Departamento',$sede->departamento, ['class' => 'form-control', 'placeholder' => 'Departamento' , 'required'])!!}
                  </div> 

                  <div class="col-sm-2">
                    {!! Form::Label('Ciudad', 'Ciudad') !!}
                    {!!Form::text('Ciudad',$sede->ciudad, ['class' => 'form-control', 'placeholder' => 'Ciudad' , 'required'])!!}
                  </div> 

                  <div class="col-sm-4">
                    {!! Form::Label('responsable', 'Responsable') !!}
                    {!!Form::text('responsable',$sede->responsable, ['class' => 'form-control', 'placeholder' => 'Responsable' , 'required'])!!}
                  </div>

                  <div class="col-sm-4">
                    {!! Form::Label('Email', 'Email') !!}
                    {!!Form::text('Email',$sede->email, ['class' => 'form-control', 'placeholder' => 'Email' , 'required'])!!}
                  </div>  

                  <div class="col-sm-4">
                    {!! Form::Label('Emailop', 'Emailop') !!}
                    {!!Form::text('Emailop',$sede->emailop, ['class' => 'form-control', 'placeholder' => 'Emailop' ])!!}
                  </div>    

                  <div class="col-sm-3">
                    {!! Form::Label('telefono', 'Teléfono') !!}
                    {!!Form::text('telefono',$sede->telefono, ['class' => 'form-control', 'placeholder' => 'telefono' , 'required'])!!}
                  </div>

                  <div class="col-sm-5">
                    {!! Form::Label('cargo', 'Cargo') !!}
                    {!!Form::text('cargo',$sede->cargo, ['class' => 'form-control', 'placeholder' => 'cargo' , 'required'])!!}
                  </div>      
          </div>
                 

            </div>
            <div class="row">
              <div class="col-xs-2">
                {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                {!! Form::close() !!}
              </div>
            </div>
          </div>


         

          
      </div>
    </div><br><br><br>
  </div>
   
  
          
          
@endsection

@section('js')





@endsection


   

   



