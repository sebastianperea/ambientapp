 
@extends('admin.template.main')

@section('content')

@section('titulo')
Ofertas Comerciales

@endsection



@section('content')

<div class="row">
  <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          <div class="card-title"><h4 style="color: #29c75f;">Hola, {{ Auth::user()->name }} Feliz día! </h4></div>
        </div>
      </div>
    </div>
   </div> 

      <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <a class="card card-banner card-green-light">
        <div class="card-body">
          <i class="icon fa fa-shopping-basket fa-4x"></i>
          <div class="content">
            <div class="title">Numero de ofertas</div>
            <div class="value"><span class="sign">{{ $ofertas }}</span></div>
          </div>
        </div>
      </a>

  </div>
  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-blue-light">
  <div class="card-body">
    <i class="icon fa fa-thumbs-o-up fa-4x"></i>
    <div class="content">
      <div class="title">clientes</div>
      <div class="value"><span class="sign"></span>{{ $clientes }}</div>
    </div>
  </div>
</a>


  </div>
  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-yellow-light">
  <div class="card-body">
    <i class="icon fa fa-user-plus fa-4x"></i>
    <div class="content">
      <div class="title">...</div>
      <div class="value"><span class="sign"></span>50</div>
    </div>
  </div>
</a>

  </div>
</div>



<div class="row">
  <div class="col-sm-12 ">
    <div class="card card-mini">
      <div class="card-header">
        <div class="card-title">Últimas ofertas</div>
        <ul class="card-action">
          <li>
            <a href="/">
              <i class="fa fa-refresh"></i>
            </a>
          </li>
        </ul>
      </div>
      <div class="card-body no-padding table-responsive">
        <table class="table card-table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Razón Social</th>
              <th>Usuario</th>
              <th>Estado <br> Envío</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>


             @foreach($ofertasc as $oferta)
              @if($oferta->Estado=="En espera")
            <tr>

              <td>  {{$oferta->id}}</td>
              <td class="right">{{$oferta->razon_social}}</td>
              <td class="right">{{$oferta->name}}</td>


                 <td> @if($oferta->estadoenv=="En Proceso")  
                 <span class="badge badge-warning badge-icon"><i class="fa fa-clock-o" aria-hidden="true"></i>{{$oferta->estadoenv}}</span>
                 @elseif($oferta->estadoenv=="Enviado")
              <span class="badge badge-primary badge-icon"><i class="fa fa-check" aria-hidden="true"></i>{{$oferta->estadoenv}}</span>

              @elseif($oferta->Estado=="Rechazado")
              <span class="badge badge-danger badge-icon"><i class="fa fa-times" aria-hidden="true"></i>{{$oferta->Estado}}</span>

                 @endif</td>

                 <td> <a href="{{ route('ofertas.edit', $oferta->id ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a> <a href="{{route('ofertas.pdf' , $oferta->id )}}" target="_blank" class="btn btn-success btn-xs" ><span class="fa fa-eye"> </a> <a href="{{route('ofertas.pdf.download' , $oferta->id )}}" target="" class="btn btn-primary btn-xs" ><span class="fa fa-arrow-down"> </a></td>
            </tr>


            
          </tbody>
           @endif 
          @endforeach
        </table>
      </div>
    </div>
  </div>

  
  <!-- 
    Card Mini -->
   <div class="col-md-6">
      <div class="card card-mini">
        <div class="card-header">
          Seguimientos para hoy: {{ $hoy }}
        </div>
        <div class="card-body">
          <table class="table card-table">
              <thead>
              <tr>
                <th>Id</th>
                <th>Razón Social</th>
                <th>Acción</th>
              </tr>
            </thead>
          @foreach($seguimientos as $seguimiento)
            <tbody>


              <td>{{ $seguimiento->id }}</td>
              <td>{{ $seguimiento->razon_social }} </td>
              <td> 
               <div>
                
                <button
                     type="button"
                     class="btn btn-success btn-xs"
                      data-toggle="modal"
                      data-id="{{ $seguimiento->id }}"
                     data-title="{{ $seguimiento->razon_social }}"
                      data-fecha="{{ $seguimiento->created_at }}"
                    data-procidentificacion="{{ $seguimiento->identificacion}}"
                    data-procprocesos="{{ $seguimiento->procesos}}"
                    data-proctelcontacto="{{ $seguimiento->telefono_contacto}}"
                    data-procemail="{{ $seguimiento->email}}"
                    data-procobservaciones="{{ $seguimiento->observaciones}}"
                      data-target="#favoritesModal">
                  <span class="fa fa-eye">
                </button>
              </div>


              <div class="modal fade" id="favoritesModal"
     tabindex="-1" role="dialog"
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close"
          data-dismiss="modal"
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><strong>Proceso Comercial #  <span id="idproc"></strong> </h4>
          <p>Fecha y hora: <span id="fecha"> </p>
      </div>
      <div class="modal-body">
        <div class="row hide" data-step="1" data-title="This is the first step!">
          <p>Razón social:
          <b><span id="fav-title"></span></b><br>
          Identificación: <span id="proc-identificacion"></span><br>
          Procesos: <span id="proc-procesos"></span>

          </p>
          <hr><br>
          <h4><strong>Contácto</strong></h4><br>
          <p>
            <i class="fa fa-phone" aria-hidden="true" style="color:green;" ></i> <span id="proc-telcontacto"></span><br>
            <i class="fa fa-envelope" aria-hidden="true" style="color: green;"></i> <span id="proc-email"></span>

          </p><br>
          <hr>
          <h4><strong>Detalles proceso</strong></h4><br>
          <p>Observaciones:
            <span id="proc-observaciones"></span></p>
      </div>
      <div class="row hide" data-step="2" data-title="This is the first step!">

          {!! Form::open(['route' => 'comercial.storeseg', 'method' => 'POST']) !!}

            <input type="number" name="id_user" value="{{ Auth::id()}}" style="display: none; readonly">

            
            <div class="col-sm-6">
                    {!! Form::label('tipo_de_visita','Tipo de Proceso')!!}
                    {!!Form::select('tipo_de_visita', ['Seleccione','Acercamiento comercial' => 'Acercamiento comercial','Proceso de creación'=> 'Proceso de creación', 'Seguimiento' =>'Seguimiento', 'Requiere nuevo seguimiento' => 'Requiere nuevo seguimiento', 'PQR' => 'PQR', 'Licitación'=>'Licitación'],null,['class' => 'form-control tipovisita', 'id'=>'tipovisita','required'])!!}
              </div>
              <div class="col-sm-4">
                    {!! Form::label('canald','Canal')!!}
                    {!!Form::select('canald', ['Seleccione',' PRESENCIAL' => 'Presencial','EMAIL ENVIADO' => 'Email enviado','EMAIL RECIBIDO' => 'Email Recibido', 'LLAMADA ENTRANTE'=> 'Llamada entrante', 'LLAMADA SALIENTE' => 'Llamada saliente','PAG WEB' =>'Pag web', 'Secop' => 'Secop'],null,['class' => 'form-control canal', 'id'=>'canald','required'])!!}
                </div>

              <div class="col-sm-8">
                   {!! Form::label('identificacion','Identificación')!!}
                    {!!Form::number('identificacion',null, ['class' => 'form-control identificacion', 'id'=> 'procidentificacion2', 'placeholder' => 'Identificación', 'required', 'readonly']) !!}
                </div>
                

          
            
                 

                    <div id="target" style="display: none;">
                    </div>
   
        <br>



                <div class="col-sm-12">
                   {!! Form::label('observaciones','Observaciones')!!}
                   {!!Form::textarea('observaciones',null, ['class' => 'form-control observacion','style'=>'height:60px;', 'id'=>'observacion','rows'=>'10','placeholder' => 'Observaciones', 'required'])!!}
                </div>

                <div class="col-sm-6">
                   {!! Form::label('fecha_nuevo_contacto','Fecha Nuevo Contacto')!!}
                   {!!Form::date('fecha_nuevo_contacto',null, ['class' => 'form-control fecha_nuevocontacto','id'=>'fecha_nuevo_contacto'])!!}
                </div>
                
              <div class="col-sm-4">
                <input type="text" name="tipo_de_cliente" value="ACTUAL" readonly="readonly" style="display: none;">
              </div>
             {!!Form::number('idv',null, ['style'=> 'display:none;','class' => 'form-control id', 'id'=> 'id', 'required', 'readonly']) !!}
               <hr>
                    <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               {!!Form::submit('Guardar', ['class' => 'btn btn-primary guardar', 'id'=>'guardar'])!!}
                               {!! Form::close() !!}

                          </div>
                      </div>
                  </div>

      </div>
    </div>
      <div class="modal-footer">
         
        <button type="button"
           class="btn btn-default"
           data-dismiss="modal">Cerrar</button>
        <span class="pull-right">
          <button type="button" class="btn btn-sm btn-success js-btn-step"  data-orientation="next">
            Hacer Seguimiento
          </button>
        </span>
      </div>
    </div>
  </div>
</div>




            
              </td>
            </tbody>
          @endforeach
          </table>
        </div>


<!-- Vencidos -->
        <div class="card-header">
          Vencidos
        </div>

        <div class="card-body">
          <table class="table card-table">
              <thead>
              <tr>
                <th>Id</th>
                <th>Razón Social</th>
                <th>Acción</th>
              </tr>
            </thead>

            @foreach($seguimientosvenc as $seguimientovenc)
            <tbody>


              <td>{{ $seguimientovenc->id }}</td>
              <td>{{ $seguimientovenc->razon_social }} </td>
              <td>
               <div>
                
                <button
                     type="button"
                     class="btn btn-success btn-xs"
                      data-toggle="modal"
                      data-id="{{ $seguimientovenc->id }}"
                     data-title="{{ $seguimientovenc->razon_social }}"
                      data-fecha="{{ $seguimientovenc->created_at }}"
                    data-procidentificacion="{{ $seguimientovenc->identificacion}}"
                    data-procprocesos="{{ $seguimientovenc->procesos}}"
                    data-proctelcontacto="{{ $seguimientovenc->telefono_contacto}}"
                    data-procemail="{{ $seguimientovenc->email}}"
                    data-procobservaciones="{{ $seguimientovenc->observaciones}}"
                      data-target="#favoritesModal">
                  <span class="fa fa-eye">
                </button>
              </div>


              <div class="modal fade" id="favoritesModal"
     tabindex="-1" role="dialog"
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close"
          data-dismiss="modal"
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><strong>Proceso Comercial #  <span id="idproc"></strong> </h4>
          <p>Fecha y hora: <span id="fecha"> </p>
      </div>
      <div class="modal-body">
        <div class="row hide" data-step="1" data-title="This is the first step!">
          <p>Razón social:
          <b><span id="fav-title"></span></b><br>
          Identificación: <span id="proc-identificacion"></span><br>
          Procesos: <span id="proc-procesos"></span>

          </p>
          <hr><br>
          <h4><strong>Contácto</strong></h4><br>
          <p>
            <i class="fa fa-phone" aria-hidden="true" style="color:green;" ></i> <span id="proc-telcontacto"></span><br>
            <i class="fa fa-envelope" aria-hidden="true" style="color: green;"></i> <span id="proc-email"></span>

          </p><br>
          <hr>
          <h4><strong>Detalles proceso</strong></h4><br>
          <p>Observaciones:
            <span id="proc-observaciones"></span></p>
      </div>
      <div class="row hide" data-step="2" data-title="This is the first step!">

          {!! Form::open(['route' => 'comercial.storeseg', 'method' => 'POST']) !!}

            <input type="number" name="id_user" value="{{ Auth::id()}}" style="display: none; readonly">

            
            <div class="col-sm-6">
                    {!! Form::label('tipo_de_visita','Tipo de Proceso')!!}
                    {!!Form::select('tipo_de_visita', ['Seleccione','Acercamiento comercial' => 'Acercamiento comercial','Proceso de creación'=> 'Proceso de creación', 'Seguimiento' =>'Seguimiento', 'Requiere nuevo seguimiento' => 'Requiere nuevo seguimiento', 'PQR' => 'PQR', 'Licitación'=>'Licitación'],null,['class' => 'form-control tipovisita', 'id'=>'tipovisita','required'])!!}
              </div>
              <div class="col-sm-4">
                    {!! Form::label('canald','Canal')!!}
                    {!!Form::select('canald', ['Seleccione',' PRESENCIAL' => 'Presencial','EMAIL ENVIADO' => 'Email enviado','EMAIL RECIBIDO' => 'Email Recibido', 'LLAMADA ENTRANTE'=> 'Llamada entrante', 'LLAMADA SALIENTE' => 'Llamada saliente','PAG WEB' =>'Pag web', 'Secop' => 'Secop'],null,['class' => 'form-control canal', 'id'=>'canald','required'])!!}
                </div>

              <div class="col-sm-8">
                   {!! Form::label('identificacion','Identificación')!!}
                    {!!Form::number('identificacion',null, ['class' => 'form-control identificacion', 'id'=> 'procidentificacion2', 'placeholder' => 'Identificación', 'required', 'readonly']) !!}
                </div>
                

          
            
                 

                    <div id="target" style="display: none;">
                    </div>
   
        <br>



                <div class="col-sm-12">
                   {!! Form::label('observaciones','Observaciones')!!}
                   {!!Form::textarea('observaciones',null, ['class' => 'form-control observacion','style'=>'height:60px;', 'id'=>'observacion','rows'=>'10','placeholder' => 'Observaciones', 'required'])!!}
                </div>

                <div class="col-sm-6">
                   {!! Form::label('fecha_nuevo_contacto','Fecha Nuevo Contacto')!!}
                   {!!Form::date('fecha_nuevo_contacto',null, ['class' => 'form-control fecha_nuevocontacto','id'=>'fecha_nuevo_contacto'])!!}
                </div>
                
              <div class="col-sm-4">
                <input type="text" name="tipo_de_cliente" value="ACTUAL" readonly="readonly" style="display: none;">
              </div>
             {!!Form::number('idv',null, ['style'=> 'display:none;','class' => 'form-control id', 'id'=> 'id', 'required', 'readonly']) !!}
               <hr>
                    <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               {!!Form::submit('Guardar', ['class' => 'btn btn-primary guardar', 'id'=>'guardar'])!!}
                               {!! Form::close() !!}

                          </div>
                      </div>
                  </div>

      </div>
    </div>
      <div class="modal-footer">
         
        <button type="button"
           class="btn btn-default"
           data-dismiss="modal">Cerrar</button>
        <span class="pull-right">
          <button type="button" class="btn btn-sm btn-success js-btn-step"  data-orientation="next">
            Hacer Seguimiento
          </button>
        </span>
      </div>
    </div>
  </div>
</div>

            
              </td>
            </tbody>
          @endforeach
          </table>
        </div>
      </div>
    </div>
  
</div>

  







  

@endsection

@section('js')
<script>
$(function() {
    $('#favoritesModal').on("show.bs.modal", function (e) {
         $("#favoritesModalLabel").html($(e.relatedTarget).data('title'));
         $("#fav-title").html($(e.relatedTarget).data('title'));
         $("#idproc").html($(e.relatedTarget).data('id'));
         $("#fecha").html($(e.relatedTarget).data('fecha'));
         $("#proc-identificacion").html($(e.relatedTarget).data('procidentificacion'));
          $("#proc-procesos").html($(e.relatedTarget).data('procprocesos'));
          $("#proc-telcontacto").html($(e.relatedTarget).data('proctelcontacto'));
          $("#proc-email").html($(e.relatedTarget).data('procemail'));
          $("#proc-observaciones").html($(e.relatedTarget).data('procobservaciones'));

    });
});
</script>

<script>
$('#favoritesModal').modalSteps();
</script>

<script type="text/javascript">
 /* global jQuery */

(function($){
    'use strict';

    $.fn.modalSteps = function(options){
        var $modal = this;

        var settings = $.extend({
            btnCancelHtml: 'Cancel',
            btnPreviousHtml: 'Previous',
            btnNextHtml: 'Hacer Seguimiento',
            btnLastStepHtml: '',
            disableNextButton: false,
            completeCallback: function(){},
            callbacks: {}
        }, options);


        var validCallbacks = function(){
            var everyStepCallback = settings.callbacks['*'];

            if (everyStepCallback !== undefined && typeof(everyStepCallback) !== 'function'){
                throw 'everyStepCallback is not a function! I need a function';
            }

            if (typeof(settings.completeCallback) !== 'function') {
                throw 'completeCallback is not a function! I need a function';
            }

            for(var step in settings.callbacks){
                if (settings.callbacks.hasOwnProperty(step)){
                    var callback = settings.callbacks[step];

                    if (step !== '*' && callback !== undefined && typeof(callback) !== 'function'){
                        throw 'Step ' + step + ' callback must be a function';
                    }
                }
            }
        };

        var executeCallback = function(callback){
            if (callback !== undefined && typeof(callback) === 'function'){
                callback();
                return true;
            }
            return false;
        };

        $modal
            .on('show.bs.modal', function(){
                var $modalFooter = $modal.find('.modal-footer'),
                    $btnCancel = $modalFooter.find('.js-btn-step[data-orientation=cancel]'),
                    $btnPrevious = $modalFooter.find('.js-btn-step[data-orientation=previous]'),
                    $btnNext = $modalFooter.find('.js-btn-step[data-orientation=next]'),
                    everyStepCallback = settings.callbacks['*'],
                    stepCallback = settings.callbacks['1'],
                    actualStep,
                    $actualStep,
                    titleStep,
                    $titleStepSpan,
                    nextStep;

                if (settings.disableNextButton){
                    $btnNext.attr('disabled', 'disabled');
                }
                $btnPrevious.attr('disabled', 'disabled');

                validCallbacks();
                executeCallback(everyStepCallback);
                executeCallback(stepCallback);

                // Setting buttons
                $btnCancel.html(settings.btnCancelHtml);
                $btnPrevious.html(settings.btnPreviousHtml);
                $btnNext.html(settings.btnNextHtml);

                $actualStep = $('<input>').attr({
                    'type': 'hidden',
                    'id': 'actual-step',
                    'value': '1',
                });

                $modal.find('#actual-step').remove();
                $modal.append($actualStep);

                actualStep = 1;
                nextStep = actualStep + 1;

                $modal.find('[data-step=' + actualStep + ']').removeClass('hide');
                $btnNext.attr('data-step', nextStep);

                titleStep = $modal.find('[data-step=' + actualStep + ']').data('title');
                $titleStepSpan = $('<span>')
                                    .addClass('label label-success')
                                    .html(actualStep);

                $modal
                    .find('.js-title-step')
                    .append($titleStepSpan)
                    .append(' ' + titleStep);
            })
            .on('hidden.bs.modal', function(){
                var $actualStep = $modal.find('#actual-step'),
                    $btnNext = $modal.find('.js-btn-step[data-orientation=next]');

                $modal
                    .find('[data-step]')
                    .not($modal.find('.js-btn-step'))
                    .addClass('hide');

                $actualStep
                    .not($modal.find('.js-btn-step'))
                    .remove();

                $btnNext
                    .attr('data-step', 1)
                    .html(settings.btnNextHtml);

                $modal.find('.js-title-step').html('');
            });

        $modal.find('.js-btn-step').on('click', function(){
            var $btn = $(this),
                $actualStep = $modal.find('#actual-step'),
                $btnPrevious = $modal.find('.js-btn-step[data-orientation=previous]'),
                $btnNext = $modal.find('.js-btn-step[data-orientation=next]'),
                $title = $modal.find('.js-title-step'),
                orientation = $btn.data('orientation'),
                actualStep = parseInt($actualStep.val()),
                everyStepCallback = settings.callbacks['*'],
                steps,
                nextStep,
                $nextStep,
                newTitle;

            steps = $modal.find('div[data-step]').length;

            // Callback on Complete
            if ($btn.attr('data-step') === 'complete'){
                settings.completeCallback();
                
                return;
            }

            // Check the orientation to make logical operations with actualStep/nextStep
            if (orientation === 'next'){
                nextStep = actualStep + 1;

                $btnPrevious.attr('data-step', actualStep);
                $actualStep.val(nextStep);

            } else if (orientation === 'previous'){
                nextStep = actualStep - 1;

                $btnNext.attr('data-step', actualStep);
                $btnPrevious.attr('data-step', nextStep - 1);

                $actualStep.val(actualStep - 1);

            } else {
                $modal.modal('hide');
                return;
            }

            if (parseInt($actualStep.val()) === steps){
                $btnNext
                    .attr('data-step', 'complete')
                    .html(settings.btnLastStepHtml)
                    
                    ;
            } else {
                $btnNext
                    .attr('data-step', nextStep)
                    .html(settings.btnNextHtml);
            }

            if (settings.disableNextButton){
                $btnNext.attr('disabled', 'disabled');
            }

            // Hide and Show steps
            $modal
                .find('[data-step=' + actualStep + ']')
                .not($modal.find('.js-btn-step'))
                .addClass('hide');

            $modal
                .find('[data-step=' + nextStep + ']')
                .not($modal.find('.js-btn-step'))
                .removeClass('hide');

            // Just a check for the class of previous button
            if (parseInt($btnPrevious.attr('data-step')) > 0 ){
                $btnPrevious.removeAttr('disabled');
            } else {
                $btnPrevious.attr('disabled', 'disabled');
            }

            if (orientation === 'previous'){
                $btnNext.removeAttr('disabled');
            }

            // Get the next step
            $nextStep = $modal.find('[data-step=' + nextStep + ']');

            // Verify if we need to unlock continue btn of the next step
            if ($nextStep.attr('data-unlock-continue')){
                $btnNext.removeAttr('disabled');
            }

            // Set the title of step
            newTitle = $nextStep.attr('data-title');
            var $titleStepSpan = $('<span>')
                                .addClass('label label-success')
                                .html(nextStep);

            $title
                .html($titleStepSpan)
                .append(' ' + newTitle);

            var stepCallback = settings.callbacks[$actualStep.val()];
            executeCallback(everyStepCallback);
            executeCallback(stepCallback);
        });

        return this;
    };
}(jQuery));

</script>

<script type="text/javascript">

    
    
    var target = document.getElementById('target');
var watchId;


function appendLocation(location) { 
  var newLocation = document.getElementById('target');
  newLocation.innerHTML ='<input type="text" name="lugar" value="+'+location.coords.latitude + ', ' + location.coords.longitude+ '">';
  target.append(newLocation);
}

if ('geolocation' in navigator) {
  window.addEventListener('load', function () {
    navigator.geolocation.getCurrentPosition(function (location) {
      appendLocation(location);
    });
    
  });
} else {
  target.innerText = 'Geolocation API not supported.';
}
  
  </script>

<script type="text/javascript">


  

   $('#favoritesModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) 
  var identificacion = button.data("procidentificacion");
  var id = button.data("id");

  document.getElementById('procidentificacion2').value= identificacion;

  document.getElementById('id').value= id;


  
});

</script>
@endsection


