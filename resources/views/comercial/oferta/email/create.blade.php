<style type="text/css">
	@font-face { 
    font-family:Quicksand; src: url('{{ asset('/home/ecoindustriasas/public_html/ambientapp/plugins/bootstrap/assets/fonts/Quicksand-Regular.otf') }}');
</style>

<style type="text/css">
	
	body{
font-family: 'Quicksand', sans-serif;

}

img{
	width: 100%;
}
</style>
<body>
<strong>Cordial saludo,</strong>
<p style="font-size: 11pt;">{{$clientes->responsable}}</p><br>

<p>Adjunto a este correo encontrara la oferta comercial por parte de <STRONG>Eco Industria S.A.S E.S.P</STRONG> Si tiene alguna duda no dude en contactarse con nosotros al télefono: (57)(8966690) o a los correos comercial@ecoindustriasas.com, y contacto@ecoindustriasas.com. Para nosotros es un placer atenderle</p><br>


<!-- 
{{$parameter2}} -->

@if($clientes->tipo ===	 "Posible")


<p>Recuerda seguir los pasos que encontraras en el manual de usuario.</p>
<a href="https://ecoindustriasas.com/ambientapp/storage/Manual%20de%20usuario%20formatos.pdf" target="" class="btn btn-primary btn-xs"> Manual de usuario Creación de Clientes - Ambientapp</a><br>

<p>Para aceptar está oferta comercial debe seguir los siguientes pasos:</p>

1. Diligenciar el formato de creación de clientes:<br>

<a href="{{route('creacion_de_clientes.edit' , $parameter2 )}}" target="" class="btn btn-primary btn-xs"> Creación de clientes   {{$clientes->razon_social}}</a><br>

2. Diligenciar el formato de información de residuos:<br>

<a href="{{route('inforesiduos.edit' , $parameter2 )}}" target="" class="btn btn-primary btn-xs"> Formato información de residuos. </a><br>

3.Subir archivos
<a href="{{route('cliente.upload' , $parameter2)}}" target="" class="btn btn-primary btn-xs"> Subir archivos </a><br>







@else

@endif


<p>Cordialmente,</p>

<p>Departamento comercial</p><br>
<p><strong>Eco Industria S.A.S E.S.P</strong></p><br><br>
<p>En cumplimiento de la Ley Orgánica 15/1999 de 13 de Diciembre, de Protección de Datos de Carácter Personal, xxxx SL pone en su conocimiento que esta información ha sido remitida por personal al servicio de la citada empresa con la finalidad de cumplimiento de las funciones de su competencia. Ponemos en su conocimiento la posibilidad de ejercer sus derechos de acceso, rectificación, cancelación y especificación en los términos establecidos en la legislación vigente, que podrá hacer efectivos dirigiéndose por escrito a Comercial Ecoindustria, con dirección en Calle 2 No 18-93 torre 1 bodega 8 ,PARQUE INDUSTRIAL SAN JORGE. Mosquera Cundinamarca ,Colombia. . Si desea dejar de recibir correos de esta lista, reenvíe este e-mail a comercial@ecoindustriasas.com.
</p><br>


</body>