@extends('admin.template.main')

@section('content')



  <div class="row">
    <div class="col-lg-12">
      <div class="card card-tab">
        
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
             {!! Form::label('Digite aquí numero identificacion empresa','')!!}
              
{!! Form::open(['route' => 'ofertas.create', 'method' => 'GET']) !!}



                  <div class="input-group busqueda-clientes">
                    
                     {!!Form::number('identificacion',null, ['class' => 'form-control busqueda-clientes ', 'placeholder' => 'Buscar Empresa por numero de identificacion' , 'required']) !!}
                  
                  </div>

                  {!!Form::submit('Buscar', ['class' => 'btn btn-primary'])!!}
                  {!!Form::close() !!} 
                <div class="section" >
                <div class="section-title"><i class="icon fa fa-user" aria-hidden="true"></i> Crear Ofertas Comerciales</div>



{!! Form::open(['route' => 'ofertas.store', 'method' => 'POST']) !!}
               
               
               <div class="row">

    <div class="col-xs-3">


                  @foreach($clientes as $cliente)

                 

                   {!! Form::label('Identificación','')!!}
                 
              {!!Form::number('identificacion',$cliente->identificacion, ['class' => 'form-control ', 'placeholder' => 'Identificación' , 'required', 'readonly']) !!}

                                       

         
                  </div>

                  


                  <div class="col-xs-1">
                   {!! Form::label('DV','')!!}
              {!!Form::number('dv',$cliente->dv, ['class' => 'form-control ', 'placeholder' => 'DV' , 'required','readonly']) !!}
                  </div>

                  <div class="col-xs-5">
                   {!! Form::label('Razon Social','')!!}
                   {!!Form::text('razon_social',$cliente->razon_social, ['class' => 'form-control', 'placeholder' => 'Razón social' , 'required', 'readonly'])!!}
                  </div>
                  <div class="col-xs-3">
                   {!! Form::label('Departamento','')!!}
                   {!!Form::text('departamento', $cliente->departamento, ['class' => 'form-control', 'placeholder' => 'Departamento' , 'required', 'readonly'])!!}
                  </div>

                  <div class="col-xs-3">
                   {!! Form::label('Ciudad','')!!}
                   {!!Form::text('ciudad',$cliente->ciudad, ['class' => 'form-control', 'placeholder' => 'Ciudad' , 'required', 'readonly'])!!}
                  </div>

                  <div class="col-xs-6">
                   {!! Form::label('Dirección','')!!}
                   {!!Form::text('direccion',$cliente->direccion, ['class' => 'form-control', 'placeholder' => 'Dirección' , 'required', 'readonly'])!!}
                  </div>

                  

                  <div class="col-xs-3">
                   {!! Form::label('Nombre Contacto','')!!}
                   {!!Form::text('Nombre_contacto',$cliente->Nombre_contacto, ['class' => 'form-control', 'placeholder' => 'Nombre Contácto' , 'required', 'readonly'])!!}
                  </div>

                  <div class="col-xs-3">
                   {!! Form::label('Apellidos','')!!}
                   {!!Form::text('Apellidos_contacto',$cliente->Apellidos_contacto, ['class' => 'form-control', 'placeholder' => 'Apellidos' , 'required', 'readonly'])!!}
                  </div>
                  
                
                  <div class="col-xs-2">
                   {!! Form::label('Télefono','')!!}
                   {!!Form::number('Telefono_contacto',$cliente->Telefono_contacto, [  'class' => 'form-control ', 'placeholder' => 'Télefono' , 'required', 'readonly'])!!}
                  </div>

                  <div class="col-xs-1">
                   {!! Form::label('Extensión','')!!}
                   {!!Form::number('ext',$cliente->ext, ['class' => 'form-control', 'placeholder' => 'EXT' , 'required', 'readonly'])!!}
                  </div>

                  <div class="col-xs-6">
                   {!! Form::label('Email','')!!}
                   {!!Form::email('email',$cliente->email, ['class' => 'form-control ', 'placeholder' => 'E- Mail' , 'required','readonly'])!!}
                  </div>

                  
@endforeach


               <hr>

                      

                        <div><h4 class="titulos">Residuos Peligrosos</h4></div>
                        
                <table class="table table-striped" id="tabla">
                    <thead>
                     
                      <th>Nombre de residuo</th>
                      



                    </thead>

                    <tbody>
                        


                    <tr>
                        

             

                     
                         
                       
                         
                         
                         

                    </tr>


                    <tr  class="fila" >
                        
                     <td> {!!Form::select('nombre2 []',$peligrosos,null,[ 'id'=>'oculto', 'class' => 'form-control select-residuos .col-md-4'])!!}</td>
                      

                         
                       
                         
                         
                         <td class="eliminar btn btn-warning btn-xs">Eliminar</td>

                    </tr>

                       

                         
              



                    </tbody>
                </table>

                <td><input type="button" id="agregar" value="Agregar fila" class="btn btn-success btn-xs"/></a></td>
                    





                  <div class="col-xs-2">
                   {!!Form::submit('Crear', ['class' => 'btn btn-primary'])!!}
                  </div>





                  
</div>




              

                

                
              
                
</div>
  
   </div>
   </div>
   </div>
   </div>
   </div>
   </div>

                  

          
          
@endsection

@section('js')



 <!-- Jquery -->
<script type="text/javascript" src="{{asset('//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js')}}"></script>
<script src="{{asset('plugins/chosen/chosen.jquery.js')}}"></script>
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>



<script type="text/javascript" src="{{asset('plugins/bootstrap/assets/js/funciones.js')}}"></script> 
<script type="text/javascript" src="{{asset('plugins/bootstrap/assets/js/sweetalert.min.js')}}"></script>
<script src="{{asset('plugins/chosen/chosen.jquery.js')}}"></script>

<script>
  

   
jQuery(function($){
  
  $('.select-id').chosen({


    no_results_text: "Su busqueda no arroja resultados",
    
    width: '90%',

  });
});

jQuery(function($){
  
  $('.select-residuos').chosen({


    no_results_text: "Su busqueda no arroja resultados",
    
    width: '80%',

  });
});
</script>

@endsection