@extends('admin.template.main')

@section('content')




  <div class="row">
    <div class="col-lg-12">
      <div class="card card-tab">
        
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
              

                <div class="section">
                <div class="section-title"><i class="icon fa fa-user" aria-hidden="true"></i> Crear Ofertas Comerciales</div>
{!! Form::open(['route' => 'ofertasposiblesclientes.store', 'method' => 'POST']) !!}
               
               
               <div class="row">

    <div class="col-xs-3">


                   {!! Form::label('','')!!}
              {!!Form::number('identificacion',null, ['class' => 'form-control', 'placeholder' => 'Identificación' , 'required']) !!}
                  </div>

                  <div class="col-xs-3">

              {!! Form::label('tipo_de_identificacion','Tipo de identificación')!!}
              {!!Form::select('tipo_de_identificacion', ['Seleccione','NIT' => 'NIT','CC'=> 'CC', 'PAS' =>'PAS', 'CE' => 'CE'],null,['class' => 'form-control ', 'required'])!!}

                   </div>


                  <div class="col-xs-1">
                   {!! Form::label('','')!!}
              {!!Form::number('dv',null, ['class' => 'form-control ', 'placeholder' => 'DV' , 'required']) !!}
                  </div>

                  <div class="col-xs-5">
                   {!! Form::label('','')!!}
                   {!!Form::text('razon_social',null, ['class' => 'form-control', 'placeholder' => 'Razón social' , 'required'])!!}
                  </div>
                  <div class="col-xs-3">
                   {!! Form::label('','')!!}
                   {!!Form::text('departamento',null, ['class' => 'form-control', 'placeholder' => 'Departamento' , 'required'])!!}
                  </div>

                  <div class="col-xs-3">
                   {!! Form::label('','')!!}
                   {!!Form::text('ciudad',null, ['class' => 'form-control', 'placeholder' => 'Ciudad' , 'required'])!!}
                  </div>

                  <div class="col-xs-6">
                   {!! Form::label('','')!!}
                   {!!Form::text('direccion',null, ['class' => 'form-control', 'placeholder' => 'Dirección' , 'required'])!!}
                  </div>

                  

                  <div class="col-xs-3">
                   {!! Form::label('','')!!}
                   {!!Form::text('Nombre_contacto',null, ['class' => 'form-control', 'placeholder' => 'Nombre Contácto' , 'required'])!!}
                  </div>

                  <div class="col-xs-3">
                   {!! Form::label('','')!!}
                   {!!Form::text('Apellidos_contacto',null, ['class' => 'form-control', 'placeholder' => 'Apellidos' , 'required'])!!}
                  </div>
                  
                
                  <div class="col-xs-2">
                   {!! Form::label('','')!!}
                   {!!Form::number('Telefono_contacto',null, [  'class' => 'form-control ', 'placeholder' => 'Télefono' , 'required'])!!}
                  </div>

                  <div class="col-xs-1">
                   {!! Form::label('','')!!}
                   {!!Form::number('ext',null, ['class' => 'form-control', 'placeholder' => 'EXT' , 'required'])!!}
                  </div>

                  <div class="col-xs-3">
                   {!! Form::label('','')!!}
                   {!!Form::email('email',null, ['class' => 'form-control ', 'placeholder' => 'E- Mail' , 'required'])!!}
                  </div>

                  



               <hr>

                      

                        <h4 class="titulos">Residuos Peligrosos</h4>
                        
                <table class="table table-striped" id="tabla">
                    <thead>
                     
                      <th>Nombre de residuo</th>
                      



                    </thead>

                    <tbody>
                        


                    <tr>
                        

             

                     
                         
                       
                         
                         
                         

                    </tr>


                    <tr  class="fila" >
                        
                     <td> {!!Form::select('nombre2 []',$peligrosos,null,[ 'id'=>'oculto', 'class' => 'form-control select-residuos .col-md-4'])!!}</td>
                      

                         
                       
                         
                         
                         <td class="eliminar btn btn-warning btn-xs">Eliminar</td>

                    </tr>

                       

                         
              



                    </tbody>
                </table>

                <td><input type="button" id="agregar" value="Agregar fila" class="btn btn-success btn-xs"/></a></td>
                    





                  <div class="col-xs-2">
                   {!!Form::submit('Crear', ['class' => 'btn btn-primary'])!!}
                  </div>




                  
</div>




              

                

                
              
                
</div>
  
   </div>
   </div>
   </div>
   </div>
   </div>
   </div>

                  

          
          
@endsection

@section('js')



 <!-- Jquery -->
<script type="text/javascript" src="{{asset('//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js')}}"></script>
<script src="{{asset('plugins/chosen/chosen.jquery.js')}}"></script>
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>



<script type="text/javascript" src="{{asset('plugins/bootstrap/assets/js/funciones.js')}}"></script> 
<script type="text/javascript" src="{{asset('plugins/bootstrap/assets/js/sweetalert.min.js')}}"></script>
<script src="{{asset('plugins/chosen/chosen.jquery.js')}}"></script>

<script>
  

   
jQuery(function($){
  
  $('.select-transporte').chosen({


    no_results_text: "Su busqueda no arroja resultados",
    
    width: '90%',

  });
});

jQuery(function($){
  
  $('.select-residuos').chosen({


    no_results_text: "Su busqueda no arroja resultados",
    
    width: '80%',

  });
});
</script>

@endsection