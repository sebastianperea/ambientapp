 

@section('content')

@section('titulo')
Ofertas Comerciales

@endsection


@extends('admin.template.main')

@section('content')

@section('titulo')
<div class="title">Hola, {{ Auth::user()->name }}</div>
@endsection
<div class="row">
  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-green-light">
  <div class="card-body">
    <i class="icon fa fa-shopping-basket fa-4x"></i>
    <div class="content">
      <div class="title">Numero de ofertas</div>
      <div class="value"><span class="sign">{{ $ofertas }}</span></div>
    </div>
  </div>
</a>

  </div>
  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-blue-light">
  <div class="card-body">
    <i class="icon fa fa-thumbs-o-up fa-4x"></i>
    <div class="content">
      <div class="title">clientes</div>
      <div class="value"><span class="sign"></span>{{ $clientes }}</div>
    </div>
  </div>
</a>

  </div>
  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-yellow-light">
  <div class="card-body">
    <i class="icon fa fa-user-plus fa-4x"></i>
    <div class="content">
      <div class="title">New Registration</div>
      <div class="value"><span class="sign"></span>50</div>
    </div>
  </div>
</a>

  </div>
</div>



<div class="row">
  <div class=" col-md-12">
    <div class="card card-mini">
      <div class="card-header">
        <div class="card-title">Ofertas Recientes</div>
        <ul class="card-action">
          <li>
            <a href="/">
              <i class="fa fa-refresh"></i>
            </a>
          </li>
        </ul>
      </div>
      <div class="card-body no-padding table-responsive">
        <table class="table card-table">



          <thead>

         
            <tr>
            <th>ID</th>
              <th>EMPRESA</th>
              <th>TIPO</th>
              <th>ESTADO</th>
              <th>ACCIÓN</th>
            </tr>
          </thead>

         
          <tbody>


           
            @foreach($ofertasc as $oferta)
              

            <tr>
          
           <td> {{$oferta->id}}</td> 
           
          
                         
           <td> {{$oferta->razon_social}}</td>
           
           <td> {{$oferta->tipo}}</td> 
       
           

           <td> @if($oferta->Estado=="En espera") 
   <span class="label label-warning">{{$oferta->Estado}}</span>

  

   
   @else
<span class="label label-primary">{{$oferta->Estado}}</span>
   @endif</td>


    <td> <a href="{{route('ofertas.edit' , $oferta->id )}}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a> <a href="{{route('ofertas.pdf' , $ofertapc->id )}}" target="_blank" class="btn btn-success btn-xs" ><span class="fa fa-eye"> </a> <a href="{{route('ofertas.pdf.download' , $oferta->id )}}" target="_blank" class="btn btn-primary btn-xs" ><span class="fa fa-arrow-down"> </a></td>

      </tr>
      
   @endforeach

   
          </tbody>

          
        </table>
        
      </div>
    </div>






  </div>


  


@endsection


