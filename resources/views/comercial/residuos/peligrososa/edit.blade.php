@extends('admin.template.main')

@section('titulo')
Residuos

@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body app-heading">
          
          <div class="app-title">
            <div class="title"><span class="highlight">{{$peligrososa->id}}</span></div>
            <div class="description">Editar residuo Peligroso Aprovechable</div>

          </div>
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="card card-tab card-mini">
        <div class="card-header">
          <ul class="nav nav-tabs">
            <li role="tab1" class="active">
              <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">General</a>
            </li>  
          </ul>
        </div>
        <div class="card-body tab-content no-padding">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
            {!! Form::open(['route' => ['peligrososa.update', $peligrososa], 'method' => 'PUT']) !!}
               

                 <div class="col-xs-2">
                   {!! Form::label('Nombre','Nombre')!!}
                   {!!Form::text('nombre',$peligrososa->nombre, ['class' => 'form-control ', 'placeholder' => 'DV' , 'required']) !!}
                  </div>

                  <div class="col-xs-2">
                   {!! Form::label('precio','Precio por Kg')!!}
                   {!!Form::number('precio',$peligrososa->precio, ['class' => 'form-control ', 'placeholder' => 'Precio']) !!}
                  </div>

                  <div class="col-xs-2">
                   {!! Form::label('preciou','Precio Unidad')!!}
                   {!!Form::number('preciou',$peligrososa->preciou, ['class' => 'form-control ', 'placeholder' => 'Precio U']) !!}
                  </div>

                  <div class="col-xs-2">
                   {!! Form::label('preciog','Precio Galones')!!}
                   {!!Form::number('preciog',$peligrososa->preciog, ['class' => 'form-control ', 'placeholder' => 'Precio G']) !!}
                  </div>

                  <div class="col-xs-2">
                   {!! Form::label('pretratamiento','Pretratamiento')!!}
                   {!!Form::text('pretratamiento',$peligrososa->pretratamiento, ['class' => 'form-control ', 'placeholder' => 'Pretratamiento' , 'required']) !!}
                  </div>
  

              


            </div>
            <div class="row">
              <div class="col-xs-2">
                {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                {!! Form::close() !!}
              </div>
            </div>
          </div>


         

          
      </div>
   
  
          
          
@endsection


   

   



