@extends('admin.template.main')

@section('titulo')
Residuos

@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body app-heading">
          
          <div class="app-title">
            <div class="title"><span class="highlight">{{$peligrosos->id}}</span></div>
            <div class="description">Editar residuo peligroso</div>

          </div>
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="card card-tab card-mini">
        <div class="card-header">
          <ul class="nav nav-tabs">
            <li role="tab1" class="active">
              <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">General</a>
            </li>  
          </ul>
        </div>
        <div class="card-body tab-content no-padding">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
            {!! Form::open(['route' => ['peligrosos.update', $peligrosos], 'method' => 'PUT']) !!}
               

                 <div class="col-sm-4">
                   {!! Form::label('Nombre','Nombre')!!}
                   {!!Form::text('nombre',$peligrosos->nombre, ['class' => 'form-control ', 'placeholder' => 'DV' , 'required']) !!}
                  </div>

                  <div class="col-sm-3">
                   {!! Form::label('precio','Precio')!!}
                   {!!Form::number('precio',$peligrosos->precio, ['class' => 'form-control ', 'placeholder' => 'Precio' , 'required']) !!}
                  </div>

                  <div class="col-sm-3">
                   {!! Form::label('pretratamiento','Pretratamiento')!!}
                   {!!Form::text('pretratamiento',$peligrosos->pretratamiento, ['class' => 'form-control ', 'placeholder' => 'Pretratamiento' , 'required']) !!}
                  </div>

                  <div class="col-sm-3">
                   {!! Form::label('dispositor','Dispositor')!!}
                   {!!Form::select('dispositor', $dispositor,null,['class' => 'form-control select2', 'id'=>'dispositor','required'])!!}
                  
                  </div>
    

              


            </div>
            <div class="row">
              <div class="col-xs-2">
                {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                {!! Form::close() !!}
              </div>
            </div>
          </div>


         

          
      </div>
   
  
          
          
@endsection

@section('js')





@endsection


   

   



