@extends('admin.template.main')

@section('content')

@section('titulo', 'Residuos')



<div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          Residuos Aprovechables
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
            <thead>
                <tr>
                  <th>Id Aprovechables</th>
                  <th>Nombre</th>
                  <th>Precio por kg</th>
                  <th>Precio por unidad</th>
                  <th>Fecha y hora</th>
                  <th>Acción</th>

                </tr>
            </thead>
              <tbody>
      
                 <tr> 
               @foreach($aprovechables as $aprovechable)
                  <td>{{$aprovechable->id}}</td>
                  <td>{{$aprovechable->nombre}}</td>
                  <td>{{$aprovechable->precio}}</td>
                  <td>{{$aprovechable->preciou}}</td>
                  <td>{{$aprovechable->created_at}}</td>
                  <td> <a href="{{ route('aprovechables.edit', $aprovechable->id ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a></td>

               


                 </tr>
              @endforeach
       
    </tbody>
</table>
        </div>
      </div>
    </div>


  











@endsection