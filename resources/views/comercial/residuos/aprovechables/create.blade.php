@extends('admin.template.main')

@section('content')

    <div class="col-lg-12">
      <div class="card card-tab">
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
              

                <div class="section">
                  <div class="section-title"><i class="icon fa fa-recycle" aria-hidden="true"></i> Crear Residuos Aprovechables
                  </div>
            {!! Form::open(['route' => 'aprovechables.store', 'method' => 'POST']) !!}
               
               
             <div class="col-xs-2">
                   {!! Form::label('Nombre','Nombre')!!}
                   {!!Form::text('nombre',null, ['class' => 'form-control ', 'placeholder' => 'Nombre' , 'required']) !!}
                  </div>

                  <div class="col-xs-2">
                   {!! Form::label('precio','Precio por kg')!!}
                   {!!Form::number('precio',null, ['class' => 'form-control ', 'placeholder' => 'Precio' , 'required']) !!}
                  </div>

                  <div class="col-xs-2">
                   {!! Form::label('precio','Precio por unidad')!!}
                   {!!Form::number('preciou',null, ['class' => 'form-control ', 'placeholder' => 'Precio' , 'required']) !!}
                  </div>


               <hr>

                    <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                               {!! Form::close() !!}

                          </div>
                      </div>
                  </div>
                    
                
              </div>
            </div>
          </div>
         </div>
        </div><br><br><br><br><br><br><br><br><br><br><br><br>
      </div>
  
          
@endsection
