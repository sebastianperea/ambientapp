@extends('admin.template.main')

@section('content')

@section('titulo', 'Dispositores')

<div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          Dispositores</br>

	
        </div>
        <a href="{{ URL::previous() }}" class="btn btn-primary btn-xs"> <span class="fa fa-arrow-circle-left "> Volver</span></a>
          <div class="card-body no-padding">

            <table class="datatable table table-striped primary" cellspacing="0" width="100%">

              <thead>

                  <tr>
                      <th>ID</th>
                      <th>Nombre</th>
                      <th>Identificacion</th>
                      <th>DV</th>
                      <th>Categoria</th>
                      <th>Creación</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($dispositores as $dispositor)
              	 <tr> 
                  	 <td>{{$dispositor->id}}</td>
                  	 <td>{{$dispositor->nombre}}</td>
                  	 <td>{{$dispositor->identificacion}}</td>
                  	 <td>
                  	 {{$dispositor->dv}}</td>
                  	 <td>
                  	 @foreach(json_decode($dispositor->categoria, true) as $d)
                                <div>{{ $d }}</div>
                                @endforeach</td>
                   
                	 <td>{{$dispositor->created_at}}</td>
                	 
              	 </tr>
    	         @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>

 
<!--MIERDA-->

  






@endsection