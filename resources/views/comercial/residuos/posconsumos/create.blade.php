@extends('admin.template.main')

@section('content')

    <div class="col-lg-12">
      <div class="card card-tab">
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
              

                <div class="section">
                  <div class="section-title"><i class="icon fa fa-user" aria-hidden="true"></i> Crear Posconsumos
                  </div>
            {!! Form::open(['route' => 'posconsumos.store', 'method' => 'POST']) !!}
               
               
             <div class="col-xs-2">
                   {!! Form::label('Nombre','Nombre')!!}
                   {!!Form::text('nombre',null, ['class' => 'form-control ', 'placeholder' => 'Nombre' , 'required']) !!}
                  </div>

                  <div class="col-xs-2">
                   {!! Form::label('precio','Precio')!!}
                   {!!Form::number('precio',null, ['class' => 'form-control ', 'placeholder' => 'Precio' , 'required']) !!}
                  </div>

                  <div class="col-xs-2">
                   {!! Form::label('pretratamiento','Pretratamiento')!!}
                   {!!Form::text('pretratamiento',null, ['class' => 'form-control ', 'placeholder' => 'Pretratamiento' , 'required']) !!}
                  </div>


               <hr>

                    <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                               {!! Form::close() !!}

                          </div>
                      </div>
                  </div>
                    
                
              </div>
            </div>
          </div>
         </div>
        </div>
      </div>
  
          
@endsection
