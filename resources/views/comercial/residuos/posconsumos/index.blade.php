@extends('admin.template.main')

@section('content')

@section('titulo', 'Residuos')



<div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          Posconsumos
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
            <thead>
                <tr>
                  <th>Id posconsumos</th>
                  <th>Nombre</th>
                  <th>Precio</th>
                  <th>Pretratamiento</th>
                  <th>Fecha y hora</th>
                  <th>Acción</th>

                </tr>
            </thead>
              <tbody>
      
                 <tr> 
               @foreach($posconsumos as $posconsumo)
                  <td>{{$posconsumo->id}}</td>
                  <td>{{$posconsumo->nombre}}</td>
                  <td>{{$posconsumo->precio}}</td>
                  <td>{{$posconsumo->pretratamiento}}</td>
                  <td>{{$posconsumo->created_at}}</td>
                  <td> <a href="{{ route('posconsumos.edit', $posconsumo->id ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a></td>

               


                 </tr>
              @endforeach
       
    </tbody>
</table>
        </div>
      </div>
    </div>


  




<tbody>
  


   
</tbody>
</table>






@endsection