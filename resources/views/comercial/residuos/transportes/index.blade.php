@extends('admin.template.main')

@section('content')

@section('titulo', 'Residuos')



<div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          Posconsumos
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
            <thead>
                <tr>
                  <th>Id Transportes</th>
                  <th>Tipo</th>
                  <th>Precio por kg</th>
                  <th>Precio Global</th>
                  <th>Fecha y hora creación</th>
                  <th>Acción</th>

                </tr>
            </thead>
              <tbody>
      
                 <tr> 
               @foreach($transportes as $transporte)
                  <td>{{$transporte->id}}</td>
                  <td>{{$transporte->tipo}}</td>
                  <td>{{$transporte->preciokg}}</td>
                  <td>{{$transporte->preciog}}</td>
                  <td>{{$transporte->created_at}}</td>
                  <td> <a href="{{ route('transportes.edit', $transporte->id ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a></td>

                 </tr>
              @endforeach
       
    </tbody>
</table>
        </div>
      </div>
      <br><br><br><br><br><br><br><br><br>
    </div>









@endsection