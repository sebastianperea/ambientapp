@extends('admin.template.main')

@section('titulo')
Residuos

@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body app-heading">
          
          <div class="app-title">
            <div class="title"><span class="highlight">{{$transportes->id}}</span></div>
            <div class="description">Editar Transportes</div>

          </div>
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="card card-tab card-mini">
        <div class="card-header">
          <ul class="nav nav-tabs">
            <li role="tab1" class="active">
              <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">General</a>
            </li>  
          </ul>
        </div>
        <div class="card-body tab-content no-padding">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
            {!! Form::open(['route' => ['transportes.update', $transportes], 'method' => 'PUT']) !!}
               


      
                 <div class="col-xs-2">
                   {!! Form::label('tipo','Tipo')!!}
                   {!!Form::text('tipo',$transportes->tipo, ['class' => 'form-control ', 'placeholder' => 'Transportes tipo' , 'required']) !!}
                  </div>

                  <div class="col-xs-2">
                   {!! Form::label('preciokg','Precio por KG')!!}
                   {!!Form::number('preciokg',$transportes->preciokg, ['class' => 'form-control ', 'placeholder' => 'Precio por Kg']) !!}
                  </div>

                  <div class="col-xs-2">
                   {!! Form::label('preciog','Precio Global')!!}
                   {!!Form::number('preciog',$transportes->preciog, ['class' => 'form-control ', 'placeholder' => 'Precio Global']) !!}
                  </div>


            </div>
            <div class="row">
              <div class="col-xs-2">
                {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                {!! Form::close() !!}
              </div>
            </div>
          </div>


         

          
      </div>

    </div>
    <br><br><br><br><br><br><br><br>
   
          
          
@endsection


   

   



