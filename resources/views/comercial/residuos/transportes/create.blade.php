@extends('admin.template.main')

@section('content')

    <div class="col-lg-12">
      <div class="card card-tab">
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
              

                <div class="section">
                  <div class="section-title"><i class="icon fa fa-user" aria-hidden="true"></i> Crear transporte
                  </div>
            {!! Form::open(['route' => 'transportes.store', 'method' => 'POST']) !!}
               
               
             <div class="col-xs-2">
                   {!! Form::label('tipo','Tipo de transporte')!!}
                   {!!Form::text('tipo',null, ['class' => 'form-control ', 'placeholder' => 'tipo' , 'required']) !!}
                  </div>

                  <div class="col-xs-2">
                   {!! Form::label('preciokg','Precio por Kg')!!}
                   {!!Form::number('preciokg',null, ['class' => 'form-control ', 'placeholder' => 'Precio Por Kg','min' => 0]) !!}
                  </div>

                  <div class="col-xs-2">
                   {!! Form::label('preciog','Precio Global')!!}
                   {!!Form::number('preciog',null, ['class' => 'form-control ', 'placeholder' => 'Precio Global', 'min' => 0]) !!}
                  </div>

               <hr>

                    <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                               {!! Form::close() !!}

                          </div>
                      </div>
                  </div>
                    <br><br><br>
                
              </div>
            </div>

          </div>
         </div>
        </div>
        <br><br><br><br><br><br><br><br><br><br><br><br>
      </div>
  
          
@endsection
