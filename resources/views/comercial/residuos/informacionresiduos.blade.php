<!DOCTYPE html>
<html lang="es">
  <head>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=dashboard">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Información de residuos - Ambientapp v1.0</title>
<link href="{{asset('plugins/bootstrap/css/form1.css')}}" rel="stylesheet" media="screen">
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/bootstrap/css/imprimir.css')}}" media="print" />


<script language="javascript">


</script>


 
  </head>

  <body>

 
  <div class="general"> <!--Inicio div general -->
     <div class="cabecera"><!-- ini div header -->
     <IMG SRC=" {{asset('/plugins/images/logoeco.png')}}" class="logo">
     <h1 class="titulo">Información de residuos</h1>
     <div class="derecha"> <!--Inicio div derecha -->
     <p>Codigo:    GC-FO- 001 </p>
     <p>Fecha de Actualización: 25/11/2016 </p>
     <p>Versión: 02</p>
     <div style=" margin-left: 950px;"> Fecha: {{$fecha}}</div > 
     </div><!-- Fin div derecha -->
     </div><!-- Fin div header -->
@foreach($clientes as $cliente)
    <div class="caja1"> <!--Inicio div caja1 -->

      @include('flash::message')
      @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
          
    <li>{{ $error }}</li></div>
            @endforeach
        </ul>
    @endif
   {!! Form::open(['route' => ['inforesiduos.update', $cliente->identificacion],'files' => true, 'enctype'=>'multipart/form-data','method' => 'PUT' ]) !!}
   
<h4 class="titulo2">INFORMACIÓN DEL GENERADOR</h4><br>

<div>

    <p>Razón Social: {!!Form::text('Razón Social',$cliente->razon_social , ['class' => 'form-control ', 'class'=>'cuadro1','placeholder' => 'Razón Social' , 'required','readonly']) !!}
   Numero de identificación: 
    {!!Form::number('identificacion',$cliente->identificacion , ['class' => ' ', 'placeholder' => 'Identificación' , 'required','readonly']) !!} - {!!Form::number('dv',$cliente->dv, ['class' => '' ,'style'=> 'width:30px;', 'placeholder' => 'Digito de verificación' ,'min'=>'0', 'required','readonly']) !!} </p>   


<div class="seccion2"><!-- inicio seccion2-->
    <h4 class="seccion2">CONTACTO TÉCNICO DEL GENERADOR</h4>
        <p class="seccion2">Nombres: {!!Form::text('nombres',$cliente->nombres , ['class' => 'form-control ', 'class'=>'cuadro1 ','placeholder' => 'Razón Social' , 'required']) !!}</p>

        <p class="seccion2">Apellidos: {!!Form::text('apellidos',$cliente->apellidos , ['class' => 'form-control ', 'class'=>'cuadro1','placeholder' => 'Razón Social' , 'required']) !!}

        <p class="seccion2">E-mail: {!!Form::email('email',$cliente->email , ['class' => 'form-control ', 'class'=>'cuadro1','placeholder' => 'E-mail' , 'required']) !!}</p> 

        <p class="seccion2">Cargo: {!!Form::text('cargo',$cliente->cargo , ['class' => 'form-control ', 'class'=>'cuadro1','placeholder' => 'Cargo' , 'required']) !!}</p>

        <p class="seccion2">Télefono Fijo: {!!Form::number('telefono_contacto',$cliente->telefono_contacto , ['class' => 'form-control ','min'=>'0', 'class'=>'cuadro1','placeholder' => 'Cargo' , 'required']) !!} Ext: {!!Form::number('telefono_contacto',$cliente->ext, ['class' => 'form-control ', 'class'=>'cuadro1','placeholder' => 'Cargo', 'class'=>'exttecnico' ,'min'=>'0', 'required']) !!}  </p>

        <p class="seccion2">Télefono Movil: {!!Form::number('telefonocelular',$cliente->telefonocelular, ['class' => 'form-control ', 'min'=>'0','class'=>'cuadro1','placeholder' => 'Cargo', 'class'=>'intelefonomoviltecnico' , 'required']) !!}</p> 


        </div><!-- Fin sección 2 -->


        
  @endforeach

  <div class="seccion3"><!-- inicio seccion3-->


    <h4 class="seccion3">Información Recolección </h4>


        <table class="tabla" id="tabla3">
          <thead>
            <th></th>
          </thead>
          <tbody>
            <tr><p class="seccion3 c">Nombre Sede: <input type="text" name="nombre[]" class="inseccion3" style="width:430px;" required="required"> <p class="seccion3" ><p class="seccion3">Dirección sede: <input type="textarea" style="width: 425px; " name="direccion[]" class="" style="width:380px;" required="required"></p><p class="seccion3">Departamento:  <input type="text" style="width: 140px;" name="departamento[]" class="" style="width:300px;" required="required"> Ciudad:<input type="text" style="width: 190px;" name="ciudad[]" class="ciudad"  required="required"></p>  </tr>
            <tr></tr>
          </tbody>
        </table>

        </div><!-- Fin sección 3 --><br><br><br><br><br><br>

        <hr>
        
       
<h4 class="titulo2" style="margin-top: 40px;">INFORMACIÓN DE RESIDUOS</h4><br>
        
<div class="content">

<table class="table table-condensed" >
    <thead>
      <tr>
        <th class="success"><strong>Nombre residuo</strong></th>
        <th class="success">Tipo</th>
        <th class="success">Detalle los residuos<br> o quimicos</th>
        <th class="success">¿Estado?</th>
        <th class="success">Proceso que <br>genero el residuo</th>
        <th class="success">¿Se encuentra contaminado? S/N <br>¿Con que residuo?</th>
        <th class="success">Empaque</th>
        <th class="success">Cantidad<br>en kg</th>
        <th class="success">Archivos</th>
      </tr>
    </thead>
    <tbody>


@foreach($peligroso as $pelig)

  @foreach($pelig as $peligro)

      @if(count($peligro)>0)
      <tr>
        <td style="display: none;"><input type="number" name="idresiduo[]" style="display: none;" value="{{$peligro->id}}"></td>
        <td><textarea cols="40" rows="6" style="width: 170px;" name="nombrer[]" readonly="readonly">{{$peligro->nombre}}</textarea></td>
        <td><input type="text" name="tipo[]" value="Peligroso"></td>
        <td><textarea cols="40" rows="5" style="width: 100px;" name="detalle[]"  ></textarea></td>
        <td>{!!Form::select('estado []',['liquido'=>'líquido','solido'=>'sólido'],null,['class' => ' selecttipo ', 'required' ])!!}</td>
        <td><textarea cols="40" rows="5" style="width: 100px;" name="proceso[]"  ></textarea></td>
        <td><input type="text" name="contaminacion[]" style="width: 90px;" required="required"></td>
        <td><input type="text" style="width: 90px;" name="empaque[]"></td>
        <td><input type="number" min="0" style="width: 80px;" name="cantidad[]" required="required"></td>
        <td><input type="file" class="archivosfront" style="width: 200px;" name="peligroso[]"  multiple></td>
      </tr>
      @endif

    @endforeach
@endforeach

      
      @if(count($peligrososa)>0)

      @foreach($peligrososa as $peliga)

        @foreach($peliga as $peligroa)

      <tr>
        <td style="display: none;"><input type="number" name="idresiduo[]" style="display: none;" value="{{$peligroa->id}}"></td>
        <td><textarea cols="40" rows="5" style="width: 100px;" name="nombrer[]" readonly="readonly">{{$peligroa->nombre}}</textarea></td>
        <td> <input type="text" name="tipo[]" readonly="readonly" value="Peligrosos Aprovechables"></td></td>
        <td><textarea cols="40" rows="5" style="width: 140px;" name="detalle[]"  ></textarea></td>
        <td>{!!Form::select('estado []',['liquido'=>'líquido','solido'=>'sólido'],null,['class' => ' selecttipo ', 'required' ])!!}</td>
        <td><textarea cols="40" rows="5" style="width: 140px;" name="proceso[]"  ></textarea></td>
        <td><input type="text"  name="contaminacion[]" style="width: 100px;" required="required"></td>
        <td><input type="text" style="width: 100px;" name="empaque[]" required="required"></td>
        <td><input type="number" min="0" style="width: 50px;" name="cantidad[]" required="required"></td>
        <td><input type="file" class="archivosfront" style="width: 200px;" name="peligrososa[]" multiple></td>
      </tr>
      
      @endforeach
        @endforeach
      @endif

      @if(count($especiales)>0)
     
      @foreach($especiales as $espec)

        @foreach($espec as $especial)


      <tr>
        <td style="display: none;"><input type="number" name="idresiduo[]" style="display: none;" value="{{$especial->id}}"></td>
        <td><textarea cols="40" rows="5" style="width: 100px;" name="especialnombre[]" readonly="readonly">{{$especial->nombre}}</textarea></td>
        <td>  <input type="text" name="tipo[]" readonly="readonly" value="Especiales"></td>
        <td><textarea cols="40" rows="5" style="width: 140px;" name="detalleespeciales[]"  ></textarea></td>
        <td>{!!Form::select('estado []',['liquido'=>'líquido','solido'=>'sólido'],null,['class' => ' selecttipo ', 'required' ])!!}</td>
        <td><textarea cols="40" rows="5" style="width: 140px;" name="proceso[]"  ></textarea></td>
        <td><input type="text" style="width: 100px;" name="contaminacion[]"></td>
        <td><input type="text" style="width: 100px;" name="empaque[]" required="required"></td>
        <td><input type="number" min="0" style="width: 50px;" name="cantidad[]" required="required"></td>
        <td><input type="file" class="archivosfront" style="width: 200px;" name="especiales[]" multiple></td>
      </tr>
        @endforeach
      @endforeach


      @endif

    </tbody>
  </table>
</div>


        <div class="contenedorleyenda"><!--seccion leyenda-->
  

  
</div><!--Fin Contenedor leyenda-->


    <div class="leyinformativa" display="block"><!--Contenedor leyenda informativa-->

    <p>Para continuar con el proceso por favor antes de dar click en enviar,imprima este formulario, luego de eso de click en enviar ,firme el formulario, escaneelo y subalo en el 3 paso: "Subir Archivos" con firma y huella.</p> 
    </div><br>
    <div>
    <input type="button" name="imprimir" id="sub" value="Imprimir" onclick="window.print();" class="imprimirboton" style>
    <input type="submit" id="env" value="Enviar"  class="imprimirboton" disabled>
    </div>  

    <br><br><br><br><br><br><br>

    @foreach($clientes as $cliente)

    <div class="comp" style=""><p>Yo <strong>{{$cliente->nombres}} {{$cliente->apellidos}} </strong>identificado con cédula de ciudadanía N° <strong>{{$cliente->ide}}</strong> como representante autorizado de la compañía <strong>{{$cliente->razon_social}} </strong> identificada con NIT: <strong>{{$cliente->identificacion}}</strong> certifico bajo la gravedad de juramento, que toda la información aquí suministrada (incluyendo los anexos) es correcta, está completa y corresponde exactamente al residuo a disponer. Asumo en nombre de la compañía, cualquier inconsistencia en los datos reportados y la responsabilidad del contenido químico no declarado. Certifico que estoy familiarizado con la tecnología del pre tratamiento y funcionamiento del proceso de disposición final seleccionado y que tengo personalmente o por asignación de un profesional calificado, examinado y controlado el sistema de almacenamiento temporal de los residuos generados en las instalaciones de la compañía que represento. Por lo tanto, la recolección de los residuos peligrosos o especiales generados en la planta se ha mantenido y se ha operado apropiadamente para obedecer el Decreto 1079 / 15  articulo 2.2.1.7.8.1 Ministerio de Transporte y las Guías Ambientales así como el decreto 4741/05 del Ministerio del Medio Ambiente. Me comprometo a adjuntar la información necesaria que el personal de ECO INDUSTRIA SAS ESP pueda requerir para este objeto. Soy consciente de que hay multas significativas por remitir una certificación falsa, que se extiende la responsabilidad del generador por los perjuicios que ello pudiere ocasionar y que existe incluso la posibilidad de cierre y encarcelamiento. Adicionalmente, autorizo al equipo Profesional de ECO INDUSTRIA SAS ESP para agregar la información suplementaria al archivo de la aprobación del residuo en cuestión y a tomar muestras de cualquier cargue que considere para efectos de comprobación y confirmación de la información aquí descrita, sin que ello comprometa a ECO INDUSTRIA SAS ESP a reportar los resultados obtenidos en laboratorio. Cualquier cambio en el proceso de generación del residuo en cuestión, será reportado necesariamente con anticipación a ECO INDUSTRIA SAS ESP. NOTA: El proceso de disposicion, aprovechamiento y tratamiento de los residuos sera acordada entre el generador y ECO INDUSTRIA S.A.S ESP, no osbtante el dispositor final puede no aprobar algun residuo para un pretratamiento especifico y darle viabilidad a otro nuevo que no ha sido declarado, para lo cual se le informa al cliente el cambio suscripto y se actualizara esta declaracion la cual tendra que ser aprobada nuevamente por el cliente. </p></div>@endforeach<br><br>
<div class="firmas"><!-- Inicio contenedor Firmas -->
<div class="firma"><!--Inicio Contenedor firma-->
<hr style="margin-top: 110px;" align="center" size="1" width="360" color="black" />
<p style="text-align: center;">FIRMA Y SELLO<br></p>
<P style="margin-left: 60px;">CEDULA:</P>
    <div class="huella"><!-- Contenedor Huella-->

    </div><!--Fin Contenedor Huella-->

    <p class="huella">Huella</p>
 </div><!--Fin Contenedor Firma-->

 </div><!-- Fin contenedor firmas -->
 
    

    
  </div> <!-- Fin div general -->


    
    {!! Form::close() !!}

    
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script type="text/javascript">

      $(document).ready(function(){
        var clone = $("#tabla3 tbody tr:eq(0)").clone();
    $("#agregar").click(function(){ 
        $("#tabla3 tbody tr:eq(0)").clone().insertAfter("#tabla3 tbody tr");
    });
});
    </script>

    <script>
      $(document).ready(function(){

        $(".creditopn").click(function(evento){
              
            var valor = $(this).val();

            if(valor == 'si'){
              $(".creditopn2").show();
                
            }
            else{

                $(".creditopn2").hide();
            }
              });
          });
    </script>
    </script>

    <script>
      $(document).ready(function(){

        $(".creditopj").click(function(evento){
              
            var valor = $(this).val();

            if(valor == 'si'){
              $(".creditopj2").show();
                
            }
            else{

                $(".creditopj2").hide();
            }
              });
          });

     
    </script>

  <!--   disabled print button -->

    <script type="text/javascript">
      
      $(document).ready(function(){
        $( "#sub" ).click(function() {
            
            $('#env').attr('disabled',false);

           });
      });
    </script>
    
  </body>
  
</html>