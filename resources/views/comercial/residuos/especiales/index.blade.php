@extends('admin.template.main')

@section('content')

@section('titulo', 'Residuos')



<div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          Residuos especiales
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
            <thead>
                <tr>
                  <th>Id Especiales</th>
                  <th>Nombre</th>
                  <th>Precio</th>
                  <th>Pretratamiento</th>
                  <th>Fecha y hora</th>
                  <th>Acción</th>

                </tr>
            </thead>
              <tbody>
      
                 <tr> 
               @foreach($especiales as $especial)
                  <td>{{$especial->id}}</td>
                  <td>{{$especial->nombre}}</td>
                  <td>{{$especial->precio}}</td>
                  <td>{{$especial->pretratamiento}}</td>
                  <td>{{$especial->created_at}}</td>
                  <td> <a href="{{ route('especiales.edit', $especial->id ) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a></td>

               


                 </tr>
              @endforeach
       
    </tbody>
</table>
        </div>
      </div>
    </div>


  




<tbody>
  


   
</tbody>
</table>






@endsection