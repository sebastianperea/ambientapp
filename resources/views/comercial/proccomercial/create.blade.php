@extends('admin.template.main')

@section('content')

    <div class="col-lg-12">
      <div class="card card-tab">
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
              

                <div class="section">
                  <div class="section-title"><i class="icon fa fa-user" aria-hidden="true"></i> Crear Proceso comercial
                  </div>
            {!! Form::open(['route' => 'proccomercial.store', 'method' => 'POST']) !!}

            <input type="number" name="id_user" value="{{ Auth::id()}}" style="display: none; readonly">

              
            <div class="col-sm-4">
             <p> Tipo de Cliente: <input type="radio" name="tipo_de_cliente" id="tipodecliente" class="tipodecliente" value="ACTUAL" required="required"> Actual
              <input type="radio" name="tipo_de_cliente" class="tipodecliente" id="tipodecliente" value='NUEVO' required="required"> Nuevo
            </div></p>

          <div class="row">
                 

                    <div id="target" style="display: none;">
                    </div>
                    
                    <div id="ejdecuenta" >
                    <input type="number" name="ejdecuenta" value="{{ $clientes->ejdecuenta}}" style="display: none;"> </div>
   
            <div class="col-sm-2">
                    {!! Form::label('tipo_de_visita','Tipo de Visita')!!}
                    {!!Form::select('tipo_de_visita', ['Seleccione','Acercamiento comercial' => 'Acercamiento comercial','Proceso de creación'=> 'Proceso de creación', 'Seguimiento' =>'Seguimiento', 'Requiere nuevo seguimiento' => 'Requiere nuevo seguimiento', 'PQR' => 'PQR'],null,['class' => 'form-control tipovisita', 'id'=>'tipovisita','required'])!!}
              </div>
              <div class="col-sm-2">
                    {!! Form::label('canald','Canal')!!}
                    {!!Form::select('canald', ['Seleccione',' PRESENCIAL' => 'Presencial','EMAIL ENVIADO' => 'Email enviado','EMAIL RECIBIDO' => 'Email Recibido', 'LLAMADA ENTRANTE'=> 'Llamada entrante', 'LLAMADA SALIENTE' => 'Llamada saliente','PAG WEB' =>'Pag web', 'EMAIL' => 'Email'],null,['class' => 'form-control canal', 'id'=>'canald','required'])!!}
                </div>

                <div class="col-sm-2">
                  {!! Form::label('canal','¿Como nos conoció?')!!}
                  {!!Form::select('canal', ['Seleccione','Visita' => 'Visita','Llamada'=> 'Llamada', 'Pag_web' =>'Pag Web', 'Email' => 'Email', 'referido' => 'Referido'],null,['class' => 'form-control canal', 'id'=>'canal','required'])!!}
                </div>
        </div><br>

                <div class="col-sm-3">
                   {!! Form::label('identificacion','Identificación')!!}
                    {!!Form::number('identificacion',null, ['class' => 'form-control identificacion', 'id'=> 'identificacion', 'placeholder' => 'Identificación', 'required']) !!}
                </div>

                <div class="col-sm-1">
                    {!! Form::label('Dv','Dv')!!}
                    {!!Form::number('dv',null, ['class' => 'form-control dv', 'id'=>'dv','placeholder' => 'DV']) !!}
                </div>

                <div class="col-sm-3">
                  {!! Form::label('tipo_de_identificacion','Tipo de identificación')!!}
                  {!!Form::select('tipo_de_identificacion', ['Seleccione','NIT' => 'NIT','CC'=> 'CC', 'PAS' =>'PAS', 'CE' => 'CE'],null,['class' => 'form-control tipoid', 'id'=>'tipoid'])!!}
                </div>

                <div class="col-sm-4">
                   {!! Form::label('razon social','Razon Social')!!}
                   {!!Form::text('razon_social',null, ['class' => 'form-control razonsocial', 'id' => 'razonsocial' ,'placeholder' => 'Razón social' , 'required'])!!}
                </div>

                <div class="col-sm-3">
                   {!! Form::label('departamento','Departamento')!!}
                   {!!Form::text('departamento',null, ['class' => 'form-control departamento','id'=> 'departamento', 'placeholder' => 'Departamento' , 'required'])!!}
                </div>

                <div class="col-sm-3">
                   {!! Form::label('ciudad','Ciudad')!!}
                   {!!Form::text('ciudad',null, ['class' => 'form-control ciudad', 'id'=>'ciudad','placeholder' => 'Ciudad' , 'required'])!!}
                </div>

                <div class="col-sm-6">
                   {!! Form::label('direccion','Dirección')!!}
                   {!!Form::text('direccion',null, ['class' => 'form-control direccion', 'id'=>'direccion','placeholder' => 'Dirección' , 'required'])!!}
                </div>

                <div class="col-sm-3">
                   {!! Form::label('Telefono contácto','Télefono contacto')!!}
                   {!!Form::number('telefono_contacto',null, [  'class' => 'form-control telefono', 'placeholder' => 'Télefono' ,'id'=>'telefono_contacto'])!!}
                </div>

                <div class="col-sm-2">
                   {!! Form::label('extensión','Extensión')!!}
                   {!!Form::number('ext',null, ['class' => 'form-control extension', 'id'=>'extension','placeholder' => 'EXT'])!!}
                </div>

                <br><br><br><br><br><br>

                <div class="col-sm-4">
                   {!! Form::label('cargo','Cargo')!!}
                    {!!Form::text('cargo',null, ['class' => 'form-control cargo', 'id'=>'cargo','placeholder' => 'cargo']) !!}
                </div>

                <div class="col-sm-4">
                   {!! Form::label('nombre','Nombre')!!}
                   {!!Form::text('nombres',null, ['class' => 'form-control nombre', 'id'=>'nombre', 'placeholder' => 'Nombre Contácto'])!!}
                </div>

                <div class="col-sm-4">
                   {!! Form::label('Apellidos','Apellidos')!!}
                   {!!Form::text('apellidos',null, ['class' => 'form-control apellidos','id'=>'apellidos' ,'placeholder' => 'Apellidos' ])!!}
                </div>


                <div class="col-sm-4">
                   {!! Form::label('email','E-mail')!!}
                   {!!Form::email('email',null, ['class' => 'form-control email', 'id'=>'email','placeholder' => 'E- Mail'])!!}
                </div>

              

                <div class="col-sm-12">
                   {!! Form::label('procesos','Procesos que realiza')!!}
                   {!!Form::textarea('procesos',null, ['class' => 'form-control procesos','style'=>'height:60px;', 'id'=>'procesos', 'placeholder' => 'Procesos'])!!}
                </div>

                <div class="col-sm-12">
                   {!! Form::label('observaciones','Observaciones')!!}
                   {!!Form::textarea('observaciones',null, ['class' => 'form-control observacion','style'=>'height:60px;', 'id'=>'observacion','rows'=>'10','placeholder' => 'Observaciones', 'required'])!!}
                </div>

                <div class="col-sm-4">
                   {!! Form::label('fecha_nuevo_contacto','Fecha Nuevo Contacto')!!}
                   {!!Form::date('fecha_nuevo_contacto',null, ['class' => 'form-control fecha_nuevocontacto','id'=>'fecha_nuevo_contacto'])!!}
                </div>


               <hr>

                    <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               {!!Form::submit('Guardar', ['class' => 'btn btn-primary guardar', 'id'=>'guardar'])!!}
                               {!! Form::close() !!}

                          </div>
                      </div>
                  </div>
                    
                
              </div>
            </div>
          </div>
         </div>
        </div>
      </div>
    </div>



                  

          
          
@endsection

@section('js')



 <!-- Jquery -->


<script type="text/javascript">
    

$('#identificacion').autocomplete({

  source: ' {!!URL::route('autocompletec')!!}',
  minlenght: 1,
  autoFocus: true,
  select: function(e,ui){

    
    
    $('#identificacion').val(ui.item.value);
    $('#tipoid').val(ui.item.tipoid);
    $('#dv').val(ui.item.dv);
    $('#razonsocial').val(ui.item.razonsocial);
    $('#departamento').val(ui.item.departamento);
    $('#ciudad').val(ui.item.ciudad);
    $('#direccion').val(ui.item.direccion);
    $('#ide').val(ui.item.ide);
    $('#cargo').val(ui.item.cargo);
    $('#procesos').val(ui.item.procesos);
    $('#nombre').val(ui.item.nombre);
    $('#apellidos').val(ui.item.apellidos);
    $('#telefonoc').val(ui.item.telefonocelular);
    $('#telefono_contacto').val(ui.item.telefono_contacto);
    $('#extension').val(ui.item.extension);
    $('#email').val(ui.item.email);
    $('#ejdecuenta').val(ui.item.ejdecuenta);
    $('#canal').val(ui.item.canal);
    

    

  }


}); 


</script>



   <script type="text/javascript">
jQuery(function($){
  
  $('.select-transporte').chosen({


    no_results_text: "Su busqueda no arroja resultados",
    
    width: '90%',

  });
});

jQuery(function($){
  
  $('.select-residuos').chosen({


    no_results_text: "Su busqueda no arroja resultados",
    
    width: '80%',

  });
});
</script>


<!-- Peligrosos -->
<script>

$(document).ready(function(){

$(document).on('keydown', '.searchname', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocomplete')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#id_' + index).val(ui.item.id).clone(true);
    $('#peligrososprecio_' + index).val(ui.item.value2);
   


  }
    });

});

 
 // Add more
 $('#agregar2').click(function(){
  
 


  // Get last id 
  var lastname_id = $('.fila2 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila2'><td><input id='searchname_"+index+"' class=' form-control searchname' required name='nombre_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='id form-control' readonly id='id_"+index+"' name='id_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidad_[]' ></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='peligrososprecio_[]' class= 'form-control peligrososprecio' id= 'peligrososprecio_"+index+"'></div></td> <td class='eliminar2 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla2 tbody').append(html);
 
 });

$(document).on("click",".eliminar2",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

</script>


<!-- Aprovechables -->
  <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamea', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletea')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#ida_' + index).val(ui.item.id).clone();
    $('#aprovechablesprecio_' + index).val(ui.item.value2);
    $('#preciou_' + index).val(ui.item.value3);
   


  }
    });

});

 
 // Add more
 $('#agregar3').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila3 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila3'><td><input id='searchnamea_"+index+"' class=' form-control searchnamea' required name='nombrea_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='id form-control' readonly id='ida_"+index+"' name='ida_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidada_[]' ></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='aprovechablesprecio_[]' class= 'form-control aprovechablesprecio' id= 'aprovechablesprecio_"+index+"'></div></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='preciou_[]' class= 'form-control preciou' id= 'preciou_"+index+"'></div></td> <td class='eliminar3 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla3 tbody').append(html);
 
 });

$(document).on("click",".eliminar3",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>


    <!-- Posconsumos -->
 <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamepo', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletepo')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idpo_' + index).val(ui.item.id).clone(true);
    $('#posconsumosprecio_' + index).val(ui.item.value2);
   


  }
    });

});

 
 // Add more
 $('#agregar4').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila4 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila4'><td><input id='searchnamepo_"+index+"' class=' form-control searchnamepo' required name='nombrepo_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='idpo form-control' readonly id='idpo_"+index+"' name='idpo_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidadpo_[]' ></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='posconsumosprecio_[]' class= 'form-control posconsumosprecio' id= 'posconsumosprecio_"+index+"'></div></td> <td class='eliminar4 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla4 tbody').append(html);
 
 });

$(document).on("click",".eliminar4",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>


<!-- Peligrosos Aprovechables -->
 <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamepa', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletepa')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idpa_' + index).val(ui.item.id).clone(true);
    $('#peligrososaprecio_' + index).val(ui.item.value2);
    $('#peligrososapreciou_' + index).val(ui.item.value3);
    $('#peligrososapreciog_' + index).val(ui.item.value4);
   


  }
    });

});

 
 // Add more
 $('#agregar5').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila5 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila5'><td><input id='searchnamepa_"+index+"' class=' form-control searchnamepa' required name='nombrepa_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='idpa form-control' readonly id='idpa_"+index+"' name='idpa_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidadpa_[]' ></td> <td><div class='input-group'> <input type='number' name='peligrososaprecio_[]' class= 'form-control peligrososaprecio' id= 'peligrososaprecio_"+index+"'></div></td> <td><div class='input-group'> <input type='number' name='peligrososapreciou_[]' class= 'form-control peligrososapreciou' id= 'peligrososapreciou_"+index+"'></div></td><td><div class='input-group'> <input type='number' name='peligrososapreciog_[]' class= 'form-control peligrososapreciog' id= 'peligrososapreciog_"+index+"'></div></td> <td class='eliminar5 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla5 tbody').append(html);
 
 });

$(document).on("click",".eliminar5",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>
 


<script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamee', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletee')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#ide_' + index).val(ui.item.id).clone(true);
    $('#especialesprecio_' + index).val(ui.item.value2);
   
  }
    });

});

 
 // Add more
 $('#agregar6').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila6 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila6'><td><input id='searchnamee_"+index+"' class=' form-control searchnamee' required name='nombree_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='id form-control' readonly id='ide_"+index+"' name='ide_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidade_[]' ></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='especialesprecio_[]' class= 'form-control especialesprecio' id= 'especialesprecio_"+index+"'></div></td> <td class='eliminar6 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla6 tbody').append(html);
 
 });

$(document).on("click",".eliminar6",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>


  <!-- Transporte  -->

<script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamet', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletet')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idt_' + index).val(ui.item.id);
    $('#transportespreciokg_' + index).val(ui.item.value2);
    $('#transportespreciog_' + index).val(ui.item.value3);

  }
    });

});

 
 // Add more
 $('#agregar7').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila7 input[type=text]:nth-child(1)').last().attr('id');


  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila7'><td><input id='searchnamet_"+index+"' class=' form-control searchnamet' required name='tipo_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='idt form-control' readonly id='idt_"+index+"' name='idt_[]' ></td><td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='transportespreciokg_[]' class= 'form-control transportesprecio' id= 'transportespreciokg_"+index+"'></div></td><td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='transportespreciog_[]' class= 'form-control transportesprecio' id= 'transportespreciog_"+index+"'></div></td> <td class='eliminar6 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla7 tbody').append(html);
 
 });

$(document).on("click",".eliminar7",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

</script>

<!-- Inicio PYS -->


<script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamepys', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];


    $( '#' + id).autocomplete({  

     source: ' {!!URL::route('autocompletepys')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){


    $('#idpys_' + index).val(ui.item.id).clone(true);
    $('#pysprecio_' + index).val(ui.item.value2);
   
  }
    });

});

 
 // Add more
 $('#agregar8').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila8 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila6'><td><input id='searchnamee_"+index+"' class=' form-control searchnamepys' required name='nombrepys_[]'  type='text'  placeholder='Ingrese el nombre del PYS'></td><td><input type='number' class='id form-control' readonly id='idpys_"+index+"' name='idpys_[]' ></td> <td><div class='input-group'><span class='input-group-addon'>$ </span> <input type='number' name='pysprecio_[]' class= 'form-control pysprecio' id= 'pysprecio_"+index+"'></div></td> <td class='eliminar8 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla8 tbody').append(html);
 
 });

$(document).on("click",".eliminar8",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>

  <script type="text/javascript">

    
    
    var target = document.getElementById('target');
var watchId;


function appendLocation(location) {
  var newLocation = document.getElementById('target');
  newLocation.innerHTML ='<input type="text" name="lugar" value="+'+location.coords.latitude + ', ' + location.coords.longitude+ '">';
  target.append(newLocation);
}

if ('geolocation' in navigator) {
  window.addEventListener('load', function () {
    navigator.geolocation.getCurrentPosition(function (location) {
      appendLocation(location);
    });
    
  });
} else {
  target.innerText = 'Geolocation API not supported.';
}
  
  </script>



@endsection