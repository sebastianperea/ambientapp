@extends('admin.template.clientes.main')

@section('content')

@section('titulo', 'Ofertas Comerciales')


@foreach($ofertas as $oferta)

@endforeach
<div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          <br>
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Codigo oferta</th>
            <th>Identificación Cliente</th>
            <th>Estado</th>
            <th>Fecha y hora<br>creación</th>
            <th>Fecha y hora<br>envío</th>
            <th>Acción</th>

        </tr>
    </thead>
    <tbody>


      @foreach($ofertas as $oferta)

     
	 <tr> 
 
	 <td>{{$oferta->id}}</td>
	 <td>{{$oferta->identificacion}}</td>

	 <td> @if($oferta->Estado=="En espera")	
	 <span class="label label-warning">{{$oferta->Estado}}</span>
	 @elseif($oferta->Estado=="Aprobado")
  <span class="label label-success">{{$oferta->Estado}}</span>

  @else
  <span class="label label-danger">{{$oferta->Estado}}</span>
	 @endif</td>


   
	 <td>{{$oferta->created_at}}</td>
   <td>{{$oferta->fechaenv}}</td>

	 
	 <td> <a href="{{route('ofertas.clientes.aprove' ,['id' => $oferta->id, 'identificacion' => $oferta->identificacion] )}}" class="btn btn-success btn-xs" ><span class="fa fa-check"> </a>  <a href="{{route('ofertas.clientes.pdf' ,['id' => $oferta->id, 'identificacion' => $oferta->identificacion] )}}" target="_blank" class="btn btn-warning btn-xs" ><span class="fa fa-eye"> </a> <a href="{{route('ofertas.clientes.pdf.download' ,['id' => $oferta->id, 'identificacion' => $oferta->identificacion])}}" target="" class="btn btn-primary btn-xs" ><span class="fa fa-arrow-down"> </a></td>



	 </tr>
	



	 @endforeach


    </tbody>
</table>
        </div>
      </div>
    </div>
 


  






@endsection