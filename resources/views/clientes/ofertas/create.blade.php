@extends('admin.template.clientes.main')

@section('content')

@section('titulo', 'Solicitud de oferta comercial')


    <div class="col-lg-12">
      <div class="card card-tab">
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
              

                <div class="section">
                  <div class="section-title"><i class="icon fa fa-user" aria-hidden="true"></i> Crear Ofertas Comerciales
                  </div>
            {!! Form::open(['route' => ['ofertas.clientes.store', $identificacion], 'method' => 'POST']) !!}
               
               
            <div class="row">

              
                <div class="col-xs-3">
                   {!! Form::label('identificacion','Identificación')!!}
                    {!!Form::number('identificacion',$identificacion, ['class' => 'form-control identificacion', 'id'=> 'identificacion', 'placeholder' => 'Identificación' , 'required', 'readonly']) !!}
                </div><br>

                

                
               

        <!-- Inicio Peligrosos -->


             <div class="col-xs-12  "><div>
                  <p style="text-align: center;"><br><strong>Por favor elimine los residuos que estan por defecto si no se acopla a sus residuos generados.</strong></p><br>
                </div><h3 style="text-align: center;">Peligrosos</h3><hr> </div>

                        
                <table class="table table-striped" id="tabla">
                    <thead>
                     
                      <th>Nombre de residuo</th>  
                      <th>Cantidad en kg</th>

                    </thead>

                    <tbody>
                        
                    <tr class="fila">
                        

                     <td> {!!Form::select('nombrep []',$peligrosos,null,['min'=>'0', 'id'=>'oculto', 'class' => 'form-control select-residuos .col-md-4'])!!}</td>

                     <td> {!!Form::number('cantidadp []',null,[ 'class' => 'form-control col-md-4'])!!}</td>
                        

                         
                       <td class="eliminar btn btn-warning btn-xs">Eliminar</td>
                         
                         
                        
                    </tr>

                    </tbody>
                </table>

                <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               <input type="button" id="agregar" value="Agregar fila" class="btn btn-success btn-xs"/>
                          </div>
                      </div>
                  </div>

                
                    

<!-- Aprovechables Inicio-->
  

                        <hr>


                        <div class="col-xs-12  "><h3 style="text-align: center;">Residuos Aprovechables</h3><hr> </div>
                        
                <table class="table table-striped" id="tablaap">
                    <thead>
                     
                      <th>Nombre de residuo</th>
                      <th>Cantidad en kg</th>
                      
                    </thead>

                    <tbody>


                    <tr  class="filaap" >
                        
                     <td> {!!Form::select('nombrea []',$aprovechables,null,[ 'id'=>'oculto', 'class' => 'form-control select-residuosap .col-md-4'])!!}</td>

                     <td> {!!Form::number('cantidada []',null,[ 'class' => 'form-control .col-md-4'])!!}</td>
                        
                         
                         <td class="eliminarap btn btn-warning btn-xs">Eliminar</td>

                    </tr>


                    </tbody>
                </table>

                <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               <input type="button" id="agregarap" value="Agregar fila" class="btn btn-success btn-xs"/>
                          </div>
                      </div>
                  </div>
                    





          
    <!--Aprovechables fin-->



    <!-- Posconsumos Inicio-->


                        <hr>


                        <div class="col-xs-12  "><h3 style="text-align: center;">Posconsumos</h3><hr> </div>
                        
                <table class="table table-striped" id="tablapc">

                    <thead>
                     
                      <th>Nombre de residuo</th>
                      <th>Cantidad en kg</th>

                    </thead>

                    <tbody>

                    <tr  class="filapc" >
                        
                     <td> {!!Form::select('nombrepo []',$posconsumos,null,[ 'id'=>'oculto', 'class' => 'form-control select-residuospc .col-md-4'])!!}</td>

                     <td> {!!Form::number('cantidadpo []',null,[ 'class' => 'form-control .col-md-4'])!!}</td>
                        
                         <td class="eliminarpc btn btn-warning btn-xs">Eliminar</td>

                    </tr>
                    </tbody>
                </table>
                <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                                <input type="button" id="agregarpc" value="Agregar fila" class="btn btn-success btn-xs"/>
                          </div>
                      </div>
                  </div>
               
                    

    <!--Posconsumos fin-->



    <!-- Peligrosos aprovechables Inicio-->


                        <hr>


                       <div class="col-xs-12  "><h3 style="text-align: center;">Residuos Peligrosos Aprovechables</h3><hr> </div>
                        
                <table class="table table-striped" id="tablapa">
                    <thead>
                     
                      <th>Nombre de residuo</th>
                      <th>Cantidad estimada en kg</th>

                    </thead>

                    <tbody>

                    <tr  class="filapa">
                        
                     <td> {!!Form::select('nombrepa []',$peligrososa,null,[ 'id'=>'oculto', 'class' => 'form-control select-residuospa .col-md-4'])!!}</td>

                     <td> {!!Form::number('cantidadpa []',null,[ 'class' => 'form-control .col-md-4'])!!}</td>

                         
                         
                         <td class="eliminarpa btn btn-warning btn-xs">Eliminar</td>

                    </tr>

                    </tbody>
                </table>

               

                <div class="row">
                   <div class="col-lg-12">
                      <div class="col-xs-2">
                        <input type="button" id="agregar" value="Agregar fila" class="btn btn-success btn-xs"/>
                      </div><br> 
                    </div>
                  </div>
                    

          
    <!--Peligrosos aprovechables fin-->



    <!-- Residuos Especiales-->


                        <hr>


                       <div class="col-xs-12  "><h3 style="text-align: center;">Residuos Especiales</h3><hr> </div>
                <table class="table table-striped" id="tablae">
                    <thead>
                     
                      <th>Nombre de residuo</th>
                      <th>Cantidad estimada en kg</th>
                      



                    </thead>

                    <tbody>
                  
                    <tr  class="filae" >
                        
                     <td> {!!Form::select('nombree []',$especiales,null,[ 'id'=>'oculto', 'class' => 'form-control select-residuose .col-md-4'])!!}</td>

                     <td> {!!Form::number('cantidade []',null,[ 'class' => 'form-control .col-md-4'])!!}</td>
 
                         <td class="eliminare btn btn-warning btn-xs">Eliminar</td>

                    </tr>
                    </tbody>
                </table>

                <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                                <input type="button" id="agregare" value="Agregar fila" class="btn btn-success btn-xs"/>
                          </div>
                      </div>
                  </div>

                
                    
    <!--Residuos especiales fin-->



<!--Otros-->

 <hr>

                        <div class="row" style="margin-right: auto; margin-left: auto;">
                        <h2 style="text-align: center;">Otros</h2>
                        <p style="text-align:center;">Si el residuo que genera no se encuentra en ninguna de las categorias anterios por favor escriba nombre y cantidad acá</p><br>
                       <div class="col-md-5" style="margin-left: 15%; margin-right: auto;">

                        {!!Form::text('nombreotros',null, ['class' => 'form-control', 'placeholder' => 'Nombre Residuo' , 'id'=>'nombreotros']) !!}
                        </div>

                        <div class="col-md-2">
                        {!!Form::number('cantidadotros',null, ['class' => 'form-control', 'placeholder' => 'Cantidad en KG' , 'id'=>'cantidadotros']) !!}
                      </div>
                    
                
              </div>

              <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                               {!! Form::close() !!}

                          </div>
                      </div>
                  </div>
            </div>

          </div>

         </div>
          
        </div>

      </div>

    </div>
  </div>





                  

  <script type="text/javascript" src="{{asset('//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js')}}"></script>
<script src="{{asset('plugins/chosen/chosen.jquery.js')}}"></script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>



<script type="text/javascript" src="{{asset('plugins/bootstrap/assets/js/funciones.js')}}"></script> 
<script type="text/javascript" src="{{asset('plugins/bootstrap/assets/js/sweetalert.min.js')}}"></script>
<script src="{{asset('plugins/chosen/chosen.jquery.js')}}"></script>
<script>



  

 function passwordMatch() {
    if(nidentificacion.value !== nidentificacion2.value)
        nidentificacion2.setCustomValidity('Las contraseñas no coinciden.');
    else
        nidentificacion2.setCustomValidity('');
}

function passwordMatch() {
    if(email.value !== email2.value)
        email2.setCustomValidity('Las contraseñas no coinciden.');
    else
        email2.setCustomValidity('');
}




jQuery(function($){
  
  $('.select-residuos').chosen({


    no_results_text: "Su busqueda no arroja resultados",
    
    width: '80%',

  });
});
 

jQuery(function($){
  
  $('.select-residuosap').chosen({


    no_results_text: "Su busqueda no arroja resultados",
    
    width: '80%',

  });
});


jQuery(function($){
  
  $('.select-residuospc').chosen({


    no_results_text: "Su busqueda no arroja resultados",
    
    width: '80%',

  });
});

jQuery(function($){
  
  $('.select-residuospa').chosen({


    no_results_text: "Su busqueda no arroja resultados",
    
    width: '80%',

  });
});

jQuery(function($){
  
  $('.select-residuose').chosen({


    no_results_text: "Su busqueda no arroja resultados",
    
    width: '80%',

  });
});


jQuery(document).ready(function($){
    $('#make').change(function(){
      $.get("{{ url('solicitud/create')}}", 
        { option: $(this).val() }, 
        function(data) {
          var ciudad = $('#ciudad');
          ciudad.empty();
 
          $.each(data, function(index, element) {
                  ciudad.append("<option value='"+ element.id +"'>" + element.name + "</option>");
              });
        });
    });
  });






  




</script>

          
@endsection

@section('js')





@endsection


