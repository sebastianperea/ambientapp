@extends('admin.template.clientes.main')

@section('titulo')
Cliente

@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body app-heading">
          
          <div class="app-title">
            <div class="title"><span class="highlight">{{$clientes->identificacion}}</span></div>
            <div class="description">Cliente</div>

          </div>
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="card card-tab card-mini">
        <div class="card-header">
          <ul class="nav nav-tabs">
            <li role="tab1" class="active">
              <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">General</a>
            </li>
            <li role="tab2">
              <a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Contácto</a>
            </li>

            <li role="tab3">
              <a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Usuario</a>
            </li>
            
          </ul>
        </div>
        <div class="card-body tab-content no-padding">
          <div role="tabpanel" class="tab-pane active" id="tab1">

            <div class="row">
            {!! Form::open(['route' => ['clientes.update', $clientes], 'method' => 'PUT']) !!}
               


                     
                  <div class="form-group">
                    <div class="col-xs-3">
                      {!! Form::label('identificacion','Identificación')!!}
                  
                      {!!Form::number('identificacion',$clientes->identificacion, ['class' => 'form-control ', 'placeholder' => 'Identificación' , 'required','disabled']) !!}
                    </div>
                  </div>
                 <div class="col-xs-2">
                   {!! Form::label('DV','')!!}
                   {!!Form::number('dv',$clientes->dv, ['class' => 'form-control ', 'placeholder' => 'DV' , 'required']) !!}
                  </div>

                  <div class="col-xs-5">
                   {!! Form::label('Razon Social','')!!}
                   {!!Form::text('razon_social',$clientes->razon_social, ['class' => 'form-control', 'placeholder' => 'Razón social' , 'required'])!!}
                  </div>

                  <div class="col-xs-3">
                   {!! Form::label('departamento','')!!}
                   {!!Form::text('departamento',$clientes->departamento, ['class' => 'form-control', 'placeholder' => 'Departamento' , 'required'])!!}
                  </div>

                  <div class="col-xs-3">
                   {!! Form::label('Ciudad','')!!}
                   {!!Form::text('ciudad',$clientes->ciudad, ['class' => 'form-control', 'placeholder' => 'Ciudad' , 'required'])!!}
                  </div>

                  <div class="col-xs-6">
                   {!! Form::label('Direccion','')!!}
                   {!!Form::text('direccion',$clientes->direccion, ['class' => 'form-control', 'placeholder' => 'Dirección' , 'required'])!!}
                  </div>
                  

                  <div class="col-xs-6">
                   {!! Form::label('Telefono Contácto','')!!}
                   {!!Form::number('telefono_contacto',$clientes->telefono_contacto, ['class' => 'form-control', 'placeholder' => 'Telefono Contácto'])!!}
                  </div>
                  <div class="col-xs-2  ">
                   {!! Form::label('Extensión','')!!}
                   {!!Form::number('ext',$clientes->ext, ['class' => 'form-control', 'placeholder' => 'Extensión'])!!}
                  </div>
                 
      
                  <div class="col-xs-3">
                  <!-- {!! Form::label('Estado')!!}
                  {!!Form::select('Estado',['Estado','En espera'=>'En espera','Aprobado'=> 'Aprobado', 'Rechazado' =>'Rechazado'], null,['class' => 'form-control ', 'required'])!!} -->
                  </div>

              


            </div>
          </div>


          <div role="tabpanel" class="tab-pane" id="tab2">
             <div class="row">
                  <div class="col-xs-2">
                   {!! Form::label('nombre ','Nombres')!!}
                   {!!Form::text('nombres',$clientes->nombres, ['class' => 'form-control', 'required'])!!}
                  </div>

                  <div class="col-xs-2">
                   {!! Form::label('apellidos','Apellidos')!!}
                   {!!Form::text('apellidos',$clientes->apellidos, ['class' => 'form-control', 'placeholder' => 'Apellidos' , 'required'])!!}
                  </div>
                  
                
                  <div class="col-xs-2">
                   {!! Form::label('telefonocelular','Móvil')!!}
                   {!!Form::number('telefonocelular',$clientes->telefonocelular, [  'class' => 'form-control ', 'placeholder' => 'Télefono' , 'required'])!!}
                  </div>


                  <div class="col-xs-3">
                   {!! Form::label('email','E-Mail')!!}
                   {!!Form::email('email',$clientes->email, ['class' => 'form-control ', 'placeholder' => 'E - mail' , 'required'])!!}
                  </div>

                  <div class="col-xs-3">
                   {!! Form::label('Cargo','')!!}
                   {!!Form::text('cargo',$clientes->cargo, ['class' => 'form-control', 'placeholder' => 'Cargo' , 'required'])!!}
                  </div>


           </div>
            <div class="row">
              <div class="col-xs-2">
                {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                {!! Form::close() !!}
              </div>
            </div>
          </div>


          <div role="tabpanel" class="tab-pane" id="tab3">
             <div class="row">
                  <div class="col-xs-2">
                   {!! Form::label('nombre ','Nombres')!!}
                   {!!Form::text('nombres',$clientes->nombres, ['class' => 'form-control', 'required'])!!}
                  </div>

                  <div class="col-xs-2">
                   {!! Form::label('apellidos','Apellidos')!!}
                   {!!Form::text('apellidos',$clientes->apellidos, ['class' => 'form-control', 'placeholder' => 'Apellidos' , 'required'])!!}
                  </div>
                  
                
                  <div class="col-xs-2">
                   {!! Form::label('telefonocelular','Móvil')!!}
                   {!!Form::number('telefonocelular',$clientes->telefonocelular, [  'class' => 'form-control ', 'placeholder' => 'Télefono' , 'required'])!!}
                  </div>


                  <div class="col-xs-3">
                   {!! Form::label('email','E-Mail')!!}
                   {!!Form::email('email',$clientes->email, ['class' => 'form-control ', 'placeholder' => 'E - mail' , 'required'])!!}
                  </div>

                  <div class="col-xs-3">
                   {!! Form::label('Cargo','')!!}
                   {!!Form::text('cargo',$clientes->cargo, ['class' => 'form-control', 'placeholder' => 'Cargo' , 'required'])!!}
                  </div>


           </div>
            <div class="row">
              <div class="col-xs-2">
                {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                {!! Form::close() !!}
              </div>
            </div>
          </div>

          
      </div>
    </div>
  </div>
   
  
          
          
@endsection

@section('js')





@endsection


   

   



