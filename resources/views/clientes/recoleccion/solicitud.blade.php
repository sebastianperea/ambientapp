@extends('admin.template.clientes.main')

@section('titulo')
Solicitud de recolección

@endsection

@section('content')


 



<div class="row">
    <div class="col-sm-12 col-xs-12">
      <div class="card">
        <div class="card-header">
          Residuos habilitados recolección
        </div>
        <div class="card-body">
          <div class="section">
  <div class="section-body">
    <div>
      <p><strong>A continuación encontrara todos los residuos que están habilitados para realizar su recolección. Si tiene alguna inquietud por favor comuniquese con el area comercial al correo: comercial@ecoindustriasas.com o al PBX: 8966690 Ext: 105.</strong></p>
    </div>
    
  </div>
</div>




<div class="section">
  <div class="section-title">Peligrosos</div>
  <div class="section-body">

    <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          <br>
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Nombre Residuo</th>
            <th>Pretratamiento</th>
            <th>Precio por kg</th>
            <th>Codigo oferta</th>

 
        </tr>
    </thead>
    <tbody>


        
      @foreach($peligrosos as $peligroso)



      @if(\Carbon\Carbon::parse($peligroso->fechaven)->gte($hoy))


     
   <tr> 
    
   <td>{{$peligroso->nombre}}</td>
   <td>{{$peligroso->pretratamiento}}</td>
   <td>$ {{$peligroso->precio}}</td>  
   <td>{{$peligroso->oferta_id}}</td>
   <td>{{$peligroso->fechaven}}</td>
   

   



   </tr>
    @else

    {{dd('shit')}}
   @endif
   @endforeach


    </tbody>
</table>
        </div>
      </div>
    </div>


      <button type="button" class="btn btn-default btn-lg">Large</button>
      <button type="button" class="btn btn-default">Normal</button>
      <button type="button" class="btn btn-default btn-sm">Small</button>
      <button type="button" class="btn btn-default btn-xs">Extra Small</button>
  </div>
</div>
<div class="section">
  <div class="section-title">Social</div>
  <div class="section-body">
    <button type="button" class="btn btn-default btn-social __facebook">
      <div class="info">
        <i class="icon fa fa-facebook-official" aria-hidden="true"></i>
        <span class="title">Facebook</span>
      </div>
    </button>
    <button type="button" class="btn btn-default btn-sm btn-social __twitter">
      <div class="info">
        <i class="icon fa fa-twitter" aria-hidden="true"></i>
        <span class="title">Twitter</span>
      </div>
    </button>
  </div>
</div>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-xs-12">
      <div class="card">
        <div class="card-header">
          Alert
        </div>
        <div class="card-body">
          <div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="alert alert-success" role="alert">
            <strong>Well done!</strong> You successfully read <a href="#" class="alert-link">this important alert message.</a>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="alert alert-info" role="alert">
            <strong>Heads up!</strong> This alert needs your attention, but it's not super important.
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>Warning!</strong> Better check yourself, you're not looking too good.
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="alert alert-danger  alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>Oh snap!</strong> Change a few things up and try submitting again.
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 id="oh-snap!-you-got-an-error!">Oh snap! You got an error!<a class="anchorjs-link" href="#oh-snap!-you-got-an-error!"><span class="anchorjs-icon"></span></a></h4>
            <p>Change this and that and try again. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
            <p>
                <button type="button" class="btn btn-danger">Take this action</button>
                <button type="button" class="btn btn-link">Or do this</button>
            </p>
        </div>
    </div>
</div>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-xs-12">
      <div class="card">
        <div class="card-header">
          Progress Bar
        </div>
        <div class="card-body">
          <div>
    <div class="progress">
        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
            <span class="sr-only">40% Complete (success)</span>
        </div>
    </div>
    <div class="progress">
        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
            <span class="sr-only">20% Complete</span>
        </div>
    </div>
    <div class="progress" >
        <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
            <span class="sr-only">60% Complete (warning)</span>
        </div>
    </div>
    <div class="progress">
        <div class="progress-bar progress-bar-danger  progress-bar-striped active" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
            <span class="sr-only">80% Complete (danger)</span>
        </div>
    </div>
    <div class="progress">
        <div class="progress-bar progress-bar-success" style="width: 35%">
            <span class="sr-only">35% Complete (success)</span>
        </div>
        <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 20%">
            <span class="sr-only">20% Complete (warning)</span>
        </div>
        <div class="progress-bar progress-bar-danger" style="width: 10%">
            <span class="sr-only">10% Complete (danger)</span>
        </div>
    </div>
</div>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-xs-12">
      <div class="card">
        <div class="card-header">
          Pagination
        </div>
        <div class="card-body">
          <nav>
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li><a href="#">1</a></li>
    <li class="active"><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-xs-12">
      <div class="card">
        <div class="card-header">
          Label
        </div>
        <div class="card-body">
          <span class="label label-default">Default</span>
<span class="label label-primary">Primary</span>
<span class="label label-success">Success</span>
<span class="label label-info">Info</span>
<span class="label label-warning">Warning</span>
<span class="label label-danger">Danger</span>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-xs-12">
      <div class="card">
    <div class="card-header">
        <div class="card-title">
            <div class="title">Loader</div>
        </div>
    </div>
    <div class="card-body __loading">
        <div class="loader-container text-center">
            <div class="icon">
                <div class="sk-wave">
                    <div class="sk-rect sk-rect1"></div>
                    <div class="sk-rect sk-rect2"></div>
                    <div class="sk-rect sk-rect3"></div>
                    <div class="sk-rect sk-rect4"></div>
                    <div class="sk-rect sk-rect5"></div>
                  </div>
            </div>
            <div class="title">Loading</div>
        </div>
        <div class="text-indent">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla</div>
    </div>
</div>
    </div>
    <div class="col-sm-12 col-xs-12">
      <div class="card">
        <div class="card-header">
          List
        </div>
        <div class="card-body">
          <div class="row">
  <div class="col-sm-6">
    <div class="list-group">
      <a href="#" class="list-group-item">
        <span class="badge">14</span> Cras justo odio
      </a>
      <a href="#" class="list-group-item">
        <span class="badge">1</span> Morbi leo risus
      </a>
      <a href="#" class="list-group-item list-group-item-success">
        <span class="badge badge-success">4</span> Vestibulum at eros
      </a>
      <a href="#" class="list-group-item list-group-item-info">
        <span class="badge badge-info">5</span> Vestibulum at eros
      </a>
      <a href="#" class="list-group-item list-group-item-warning">
        <span class="badge badge-warning">2</span> Vestibulum at eros
      </a>
      <a href="#" class="list-group-item list-group-item-danger">
        Vestibulum at eros
      </a>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="list-group __timeline">
      <a href="#" class="list-group-item">
        <span class="badge badge-success">14</span> Cras justo odio
      </a>
      <a href="#" class="list-group-item">
        <span class="badge badge-success">1</span> Morbi leo risus
      </a>
      <a href="#" class="list-group-item">
        <span class="badge badge-success">2</span> Vestibulum at eros
      </a>
    </div>
  </div>
</div>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-xs-12">
      <div class="card">
        <div class="card-header">
          Thumbnail
        </div>
        <div class="card-body">
          <div class="row">
  <div class="col-md-3 col-sm-6">
    <div class="thumbnail">
      <img src="http://placehold.it/400x300" class="img-responsive">
      <div class="caption">
        <h3 class="title">Thumbnail label<a class="anchorjs-link" href="#thumbnail-label"><span class="anchorjs-icon"></span></a></h3>
        <p class="description">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
        <div><a href="#" class="btn btn-success btn-xs" role="button">Button</a> <a href="#" class="btn btn-light btn-default btn-xs" role="button">Button</a></div>
      </div>
    </div>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="#" class="thumbnail">
      <img src="http://placehold.it/400x300" class="img-responsive">
    </a>
  </div>
</div>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-xs-12">
      <div class="card">
        <div class="card-header">
          Media
        </div>
        <div class="card-body">
          <div class="media">
  <div class="media-left">
    <a href="#">
      <img src="http://placehold.it/60x60" />
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">Media heading</h4>
    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate.</p>
  </div>
</div>
<div class="media">
  <div class="media-left">
    <a href="#">
      <img src="http://placehold.it/60x60" />
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">Media heading</h4>
    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.</p>

    <div class="media">
      <div class="media-left">
        <a href="#">
          <img src="http://placehold.it/60x60" />
        </a>
      </div>
      <div class="media-body">
        <h4 class="media-heading">Media heading</h4>
        <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </p>
      </div>
    </div>
  </div>
</div>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-xs-12">
      <div class="card">
        <div class="card-header">
          Social Posts
        </div>
        <div class="card-body">
          <div class="media social-post">
  <div class="media-left">
    <a href="#">
      <img src="../assets/images/profile.png" />
    </a>
  </div>
  <div class="media-body">
    <div class="media-heading">
      <h4 class="title">Scott White</h4>
      <h5 class="timeing">20 mins ago</h5>
    </div>
    <div class="media-content">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate.</div>
    <div class="media-action">
      <button class="btn btn-link"><i class="fa fa-thumbs-o-up"></i> 2 Like</button>
      <button class="btn btn-link"><i class="fa fa-comments-o"></i> 10 Comments</button>
    </div>
    <div class="media-comment">
      <input type="text" class="form-control" placeholder="comment...">
    </div>
  </div>
</div>
<div class="media social-post">
  <div class="media-left">
    <a href="#">
      <img src="../assets/images/profile.png" />
    </a>
  </div>
  <div class="media-body">
    <div class="media-heading">
      <h4 class="title">Scott White</h4>
      <h5 class="timeing">20 mins ago</h5>
    </div>
    <div class="media-content">
      <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.</p>
      <div class="attach">
        <a href="#" class="thumbnail">
          <img src="http://placehold.it/100x100" class="img-responsive">
        </a>
        <a href="#" class="thumbnail">
          <img src="http://placehold.it/100x100" class="img-responsive">
        </a>
      </div>
    </div>
    <div class="media-action">
      <button class="btn btn-link"><i class="fa fa-thumbs-o-up"></i> 2 Like</button>
      <button class="btn btn-link"><i class="fa fa-comments-o"></i> 10 Comments</button>
    </div>
    <div class="media-comment">
      <input type="text" class="form-control" placeholder="comment...">
    </div>
  </div>
</div>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-xs-12">
      <div class="card">
        <div class="card-header">
          Tab & Step
        </div>
        <div class="card-body">
          <div class="section">
            <div class="section-title">Tab</div>
            <div class="section-body">
              <div role="tabpanel">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me
        </div>
        <div role="tabpanel" class="tab-pane" id="profile">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes d</div>
        <div role="tabpanel" class="tab-pane" id="messages">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium </div>
        <div role="tabpanel" class="tab-pane" id="settings">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. </div>
    </div>
</div>
            </div>
          </div>
          <div class="section">
            <div class="section-title">Step</div>
            <div class="section-body">
              <div class="step">
    <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="step">
            <a href="#step1" id="step1-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">
                <div class="icon fa fa-shopping-cart"></div>
                <div class="heading">
                    <div class="title">Shipping</div>
                    <div class="description">Enter your address</div>
                </div>
            </a>
        </li>
        <li role="step" class="active">
            <a href="#step2" role="tab" id="step2-tab" data-toggle="tab" aria-controls="profile">
                <div class="icon fa fa-credit-card"></div>
                <div class="heading">
                    <div class="title">Payment</div>
                    <div class="description">Billing Information.</div>
                </div>
            </a>
        </li>
        <li role="step">
            <a href="#step3" role="tab" id="step3-tab" data-toggle="tab" aria-controls="profile">
                <div class="icon fa fa-check"></div>
                <div class="heading">
                    <div class="title">Confirm Orders</div>
                    <div class="description">Confirmation your purchases</div>
                </div>
            </a>
        </li>
        <li role="step">
            <a href="#step4" role="tab" id="step4-tab" data-toggle="tab" aria-controls="profile">
                <div class="icon fa fa-truck "></div>
                <div class="heading">
                    <div class="title">Purchase Successfully</div>
                    <div class="description">Wait for us shipping</div>
                </div>
            </a>
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane" id="step1">
            <b>Step1</b> : Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel
        </div>
        <div role="tabpanel" class="tab-pane active" id="step2">
            <b>Step2</b> : Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore
        </div>
        <div role="tabpanel" class="tab-pane" id="step3">
            <b>Step3</b> : Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum
        </div>
        <div role="tabpanel" class="tab-pane" id="step4">
            <b>Step4</b> : Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequa
        </div>
    </div>
</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-xs-12">
      <div class="card">
        <div class="card-header">
          Code
        </div>
        <div class="card-body no-padding">
          <pre><code class="html">&lt;div class=&quot;card&quot;&gt;
  &lt;div class=&quot;card-header&quot;&gt;
    &lt;div class=&quot;card-title&quot;&gt;Card&lt;/div&gt;
    &lt;ul class=&quot;card-action&quot;&gt;
      &lt;li class=&quot;dropdown&quot;&gt;
        &lt;a href=&quot;/&quot; class=&quot;dropdown-toggle&quot; data-toggle=&quot;dropdown&quot;&gt;
          &lt;i class=&quot;fa fa-cogs&quot; aria-hidden=&quot;true&quot;&gt;&lt;/i&gt;
        &lt;/a&gt;
        &lt;ul class=&quot;dropdown-menu&quot;&gt;
          &lt;li&gt;&lt;a href=&quot;#&quot;&gt;Action 1&lt;/a&gt;&lt;/li&gt;
          &lt;li&gt;&lt;a href=&quot;#&quot;&gt;Action 2&lt;/a&gt;&lt;/li&gt;
          &lt;li&gt;&lt;a href=&quot;#&quot;&gt;Action 3&lt;/a&gt;&lt;/li&gt;
        &lt;/ul&gt;
      &lt;/li&gt;
    &lt;/ul&gt;
  &lt;/div&gt;
  &lt;div class=&quot;card-body&quot;&gt;
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  &lt;/div&gt;
&lt;/div&gt;</code></pre>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-xs-12">
      <div class="card">
        <div class="card-header">
          Modal
        </div>
        <div class="card-body">
          <div class="row">
  <div class="col-sm-12">
    <div>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
      Demo modal
      </button>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-sm btn-success">Save changes</button>
          </div>
        </div>
      </div>
    </div>
    <div class="simplio-example __modal">
      <div class="modal in">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-sm btn-success">Save changes</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
        </div>
      </div>
    </div>
  </div>

@endsection