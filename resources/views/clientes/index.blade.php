 
@extends('admin.template.clientes.main')

@section('content')

@section('titulo')
Escritorio

@endsection



@section('content')

<div class="row">
  <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          <div class="card-title"><h4 style="color: #29c75f;">Hola, {{ Auth::user()->name }} Feliz día! </h4></div>
        </div>
      </div>
    </div>
   </div> 





<div class="row">
  <div class="col-sm-12 col-xs-12 col-md-12 ">
    <div class="card card-mini">
      <div class="card-header">
        <div class="card-title">Últimas ofertas comerciales</div>
        <ul class="card-action">
          <li>
            <a href="/">
              <i class="fa fa-refresh"></i>
            </a>
          </li>
        </ul>
      </div>
      <div class="card-body no-padding table-responsive">
        <table class="table card-table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Estado oferta</th>
              <th>Estado <br> Envío</th>
              <th>Fecha oferta</th>
              <th>Acción</th>


            </tr>
          </thead>
          <tbody>


             @foreach($ofertas as $oferta)
              
            <tr>
              <td>{{$oferta->id}}</td>
              

                

              <td> @if($oferta->Estado=="En espera")  
                 <span class="badge badge-warning badge-icon"><i class="fa fa-clock-o" aria-hidden="true"></i>{{$oferta->Estado}}</span>
                 @elseif($oferta->Estado=="Aprobado")
              <span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i>{{$oferta->Estado}}</span>

              @elseif($oferta->Estado=="Rechazado")
              <span class="badge badge-danger badge-icon"><i class="fa fa-times" aria-hidden="true"></i>{{$oferta->Estado}}</span>

                 @endif</td>

                 <td> @if($oferta->estadoenv=="En Proceso")  
                 <span class="badge badge-warning badge-icon"><i class="fa fa-clock-o" aria-hidden="true"></i>{{$oferta->estadoenv}}</span>
                 @elseif($oferta->estadoenv=="Enviado")
              <span class="badge badge-primary badge-icon"><i class="fa fa-check" aria-hidden="true"></i>{{$oferta->estadoenv}}</span>

              @elseif($oferta->Estado=="Rechazado")
              <span class="badge badge-danger badge-icon"><i class="fa fa-times" aria-hidden="true"></i>{{$oferta->Estado}}</span>

                 @endif</td>

                 <td>{{$oferta->created_at}}</td>

                 <td> <a href="{{route('ofertas.clientes.pdf' ,['id' => $oferta->id, 'identificacion' => $oferta->identificacion] )}}" target="_blank" class="btn btn-success btn-xs" ><span class="fa fa-eye"> </a> <a href="{{route('ofertas.clientes.pdf.download' ,['id' => $oferta->id, 'identificacion' => $oferta->identificacion])}}" target="" class="btn btn-primary btn-xs" ><span class="fa fa-arrow-down"> </a></td>
            </tr>


            
          </tbody>
           
          @endforeach
        </table>
      </div>
    </div>
  </div>


  
</div>






  

@endsection


