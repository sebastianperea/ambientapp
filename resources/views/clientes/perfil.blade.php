
@extends('admin.template.clientes.main')

@section('content')

@section('titulo', 'Perfil')
<div class="row">

   @if ( count( $errors ) > 0 )
            <div class="alert alert-warning" role="alert">
               @foreach ($errors->all() as $error)
                  <div>{{ $error }}</div>
              @endforeach
            </div>
        @endif </br>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body app-heading">
          <!-- <div class="icon">
            <i class="fa fa-bell" aria-hidden:"true"></i>
          </div> -->
          <div class="app-title">
            <div class="title"><span class="highlight">{{$clientes->razon_social}}</span></div>
            <div class="description">{{$clientes->identificacion}}-{{$clientes->dv}} <br>{{$clientes->estado}}</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card card-tab">
        <div class="card-header">
          <ul class="nav nav-tabs">
            <li role="tab1" class="active">
              <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Perfil</a>
            </li>
            <li role="tab5">
              <a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">Sedes</a>
            </li>
            <li role="tab6">
              <a href="#tab6" aria-controls="tab6" role="tab" data-toggle="tab">Usuario</a>
            </li>
          </ul>
        </div>
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">
              <div class=" col-sm-12">
                <div class="section">
                  <div class="section-title"><i class="icon fa fa-user" aria-hidden="true"></i> Datos generales</div>
                    <div class="section-body __indent">
                       {!! Form::open(['route' => ['clientes.update.perfil', $clientes], 'method' => 'PUT']) !!}
                      <div class="row">
                        <div class="col-xs-3">
                            {!! Form::label('Departamento','')  !!}
                            {!!Form::text('departamento',$clientes->departamento, ['class' => 'form-control', 'placeholder' => 'Departamento' , 'required'])!!}
                        </div>
                        <div class="col-xs-3">
                            {!! Form::label('Ciudad','')  !!}
                            {!!Form::text('ciudad',$clientes->ciudad, ['class' => 'form-control', 'placeholder' => 'Departamento' , 'required'])!!}
                        </div>
                        <div class="col-xs-3">
                            {!! Form::label('Dirección','')  !!}
                            {!!Form::text('direccion',$clientes->direccion, ['class' => 'form-control', 'placeholder' => 'Direccion' , 'required'])!!}
                        </div>
                      </div>
                    </div>
                </div>
                <div class="section">
                  <div class="section-title"><i class="icon fa fa-book" aria-hidden="true"></i> Contácto</div>
                    <div class="section-body __indent">
                      <div class="row">
                        <div class="col-xs-3">
                            {!! Form::label('Identificación','')  !!}
                            {!!Form::text('ide',$clientes->ide, ['class' => 'form-control', 'placeholder' => 'Identificación' , 'required'])!!}
                        </div>
                        <div class="col-xs-3">
                            {!! Form::label('Nombres','')  !!}
                            {!!Form::text('nombre',$clientes->nombres, ['class' => 'form-control', 'placeholder' => 'Nombres' , 'required'])!!}
                        </div>
                        <div class="col-xs-3">
                            {!! Form::label('Apellidos','')  !!}
                            {!!Form::text('apellidos',$clientes->apellidos, ['class' => 'form-control', 'placeholder' => 'Apellidos' , 'required'])!!}
                        </div>
                        <div class="col-xs-3">
                            {!! Form::label('Teléfono Celular','')  !!}
                            {!!Form::number('telefonocelular',$clientes->telefonocelular, ['class' => 'form-control', 'placeholder' => 'Télefono Celular' , 'required'])!!} 
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-3">
                            {!! Form::label('E-Mail','')  !!}
                            {!!Form::email('email',$clientes->email, ['class' => 'form-control', 'placeholder' => 'E-Mail' , 'required'])!!}
                        </div>
                      </div>
                  </div>
                </div>
                <div class="section">
                  <div class="section-title"><i class="icon fa fa-money" aria-hidden="true"></i> Cartera</div>
                    <div class="section-body __indent">
                      <div class="row">
                          <div class="col-sm-3">
                              {!! Form::label('Nombres Y Apellidos Encargado','')  !!}
                              {!!Form::text('nombrecartera',$clientes->nombrecartera, ['class' => 'form-control', 'placeholder' => 'Nombre Encargado' , 'required'])!!}
                          </div>
                          <div class="col-sm-3">
                              {!! Form::label('Teléfono fijo','')  !!}
                              {!!Form::text('telefonofijocartera',$clientes->telefonofijocartera, ['class' => 'form-control', 'placeholder' => 'Teléfono Fijo' , 'required'])!!}
                          </div>
                          <div class="col-sm-1">
                              {!! Form::label('Ext','')  !!}
                              {!!Form::text('ext',$clientes->extcartera, ['class' => 'form-control', 'placeholder' => 'Extensión' , 'required'])!!}
                          </div>
                          <div class="col-sm-3">
                              {!! Form::label('Teléfono móvil','')  !!}
                              {!!Form::text('nombres',$clientes->telefonomovilcartera, ['class' => 'form-control', 'placeholder' => 'Teléfono móvil cartera' , 'required'])!!}
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-3">
                            {!! Form::label('Email','')  !!}
                            {!!Form::email('emailcartera',$clientes->emailcartera, ['class' => 'form-control', 'placeholder' => 'Email Cartera' , 'required'])!!}
                        </div>
                        <div class="col-sm-4">
                            {!! Form::label('Dirección Radicación','')  !!}
                            {!!Form::text('direccionradicacion',$clientes->direccionradicacion, ['class' => 'form-control', 'placeholder' => 'Dirección Radicación' , 'required'])!!}
                        </div>
                      </div>
                    </div>
                </div>
                <div class="section">
                  <div class="section-title"><i class="icon fa fa-money" aria-hidden="true"></i> Información Bancaria</div>
                    <div class="section-body __indent">
                      <div class="row">
                        <div class="col-sm-3">
                            {!! Form::label('Nombre Banco','')  !!}
                            {!!Form::text('nombrebanco',$clientes->nombrebanco, ['class' => 'form-control', 'placeholder' => 'Nombre Banco' , 'required'])!!}
                        </div>

                        <div class="col-sm-3">
                            {!! Form::label('Numero de cuenta','')  !!}
                            {!!Form::number('numerodecuenta',$clientes->numerodecuenta, ['min'=>'0','class' => 'form-control', 'placeholder' => 'Numero de cuenta' , 'required'])!!}
                        </div>

                        <div class="col-sm-3">
                            {!! Form::label('Tipo de cuenta','')  !!}<br>
                            {!!Form::text('tipodecuenta',$clientes->tipodecuenta, ['class' => 'form-control', 'placeholder' => 'Tipo de cuenta' , 'required'])!!}
                        </div>
                      </div>
                    </div>
                </div>
                <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                               {!! Form::close() !!}
                          </div>
                      </div>
                  </div>
              </div>   
          </div>
      </div>
          <div role="tabpanel" class="tab-pane" id="tab5">
            <div class="card">
              <div class="card-body">
                  {!! Form::open(['route' => ['cliente.sedes.save', $clientes->identificacion], 'method' => 'PUT']) !!}
                      <div class="col-xs-12"><h3 style="text-align: center;">Sedes Recolección</h3><hr> </div>
                    <table class="table table-striped" id="tabla">
                      <thead>
                          <th>Id Cliente</th>
                          <th>Nombre</th>
                          <th>Dirección</th>
                          <th>Departamento</th>
                          <th>Ciudad</th>
                      </thead>
                          <tbody>
                            @foreach($sedes as $sede)
                                <tr class="fila">
                                    <td> {!!Form::number('identificacion []',$sede->identificacion,['id'=>'searchname_1','class' => 'form-control searchname ', 'required', 'disabled']) !!}
                                    </td>
                                    <td>{!!Form::text('nombre [] ', $sede->nombre, ['id'=>'id_1','class' => 'form-control id ', 'required', 'disabled']) !!}
                                    </td>
                                    <td>
                                      {!!Form::text('direccion [] ', $sede->direccion, ['class' => 'form-control ', 'id'=>'direccion_1','required', 'disabled']) !!}
                                    </td>
                                    <td>
                                      {!!Form::text('departamento [] ', $sede->departamento, ['class' => 'form-control ', 'id'=>'departamento_1','required', 'disabled']) !!}
                                    </td>
                                    <td>
                                      {!!Form::text('ciudad [] ', $sede->ciudad, ['class' => 'form-control ', 'id'=>'cantidad_1','required', 'disabled']) !!}
                                    </td>

                                    <td><a href="{{route('comercial.sedes.borrar' , $sede->id)}}" class="btn btn-danger btn-xs "> Eliminar </a>
                                    </td>     
                              </tr>
                             @endforeach
                                <tr class="fila">
                                    <td> {!!Form::number('identificacion []',$clientes->identificacion,['id'=>'searchname_1','class' => 'form-control searchname ','readonly']) !!}
                                    </td>
                                    <td>{!!Form::text('nombre [] ', null, ['id'=>'nombre','class' => 'form-control id ', 'required']) !!}
                                    </td>
                                    <td>
                                      {!!Form::text('direccion [] ', null, ['class' => 'form-control ', 'id'=>'direccion_1','required']) !!}
                                    </td>
                                    <td>
                                      {!!Form::text('departamento [] ', null, ['class' => 'form-control ', 'id'=>'departamento','required']) !!}
                                    </td>
                                    <td>
                                      {!!Form::text('ciudad [] ', null, ['class' => 'form-control ', 'id'=>'cantidad_1','required']) !!}
                                    </td>
                                    <td class="eliminar btn btn-warning btn-xs">Eliminar Fila</td>   
                              </tr>
                          </tbody>
                    </table>
                  <td><input type="button" id="agregar" value="Agregar fila" class="btn btn-success btn-xs"/>
                  </td><br>

                  <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                               {!! Form::close() !!}

                          </div>
                      </div>
                  </div>
           </div>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane active" id="tab6">
          <div class="row">
            <div class=" col-sm-12">
              <div class="section">
                @foreach($usuario as $usuarios)
                  <div class="section-title"><i class="icon fa fa-user" aria-hidden="true"></i> Datos de usuario</div>
                    <div class="section-body __indent">
                        {!! Form::open(['route' => ['cliente.usuario.update', $clientes], 'method' => 'PUT']) !!}
                        <div class="row">
                          <div class="col-xs-3">
                            {!! Form::label('Nombre','')  !!}
                            {!!Form::text('name',$usuarios->name, ['class' => 'form-control', 'placeholder' => 'Departamento' , 'required'])!!}
                          </div>
                            <div class="col-xs-3">
                            {!! Form::label('email','')  !!}
                            {!!Form::text('email',null, ['class' => 'form-control', 'placeholder' => 'E-mail'])!!}
                            </div>

                            <div class="col-xs-3">
                                {!! Form::label('Contraseña Nueva','')  !!}
                                {!!Form::text('password',null, ['class' => 'form-control', 'placeholder' => 'Contraseña' ])!!}
                              </div>
                              
                              <div class="col-xs-3">
                              {!! Form::label('Repita su contraseña','')  !!}
                              {!!Form::text('password',null, ['class' => 'form-control', 'placeholder' => 'Confirmación Contraseña'])!!}
                              </div>
                      </div>

                  </div>
                     <div class="row">
                          <div class="col-lg-12">
                            <div class="col-xs-2">
                                 {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                                 {!! Form::close() !!}
                            </div>
                          </div>
                      </div>
                </div>
                @endforeach
                  
              </div>
            </div>
          </div>  
        </div>
      </div>
    </div>
  </div>



@section('js')
    <script type="text/javascript">
    

  $(document).ready(function(){



 
 // Add more
 $('#agregar').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila'><td><input id='identificacion' class=' form-control' required name='identificacion_[]' type='text' value= '{{$clientes->identificacion}}' readonly></td><td><input type='text' class='form-control' id='nombre' name='nombre_[]'></td><td><input type='text' class='form-control' id='direccion' name='direccion_[]'></td> <td><input type='text' class='form-control' id='departamento' name='departamento_[]'></td> <td><input type='text' class='form-control' id='ciudad' name='ciudad_[]'></td><td class='eliminar btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla tbody').append(html);
 
 });

$(document).on("click",".eliminar",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>
  
@endsection

@endsection


