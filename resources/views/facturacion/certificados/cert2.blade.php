<!DOCTYPE>
<html>
<head>

	<link rel="stylesheet" type="text/css" href="/home/ecoindustriasas/public_html/ambientapp/plugins/bootstrap/assets/css/bootstrap.min.css">
  <style>

@font-face { 
    font-family:Quicksand; src: url('{{ asset('/home/ecoindustriasas/public_html/ambientapp/plugins/bootstrap/assets/fonts/Quicksand-Regular.otf') }}');


 </style>


<style type="text/css">


  
  header{
position: fixed;

  font-family: 'Quicksand', sans-serif;

  width: 100%;

 
}


body{
font-family: 'Quicksand', sans-serif;
margin-left: 70px;



}

.logo{

  float: left;
  width: 100mm;
  height: auto;
  position: fixed;
  margin-left: 20px;


}
.pagina{
  page-break-before:always;

}


.titulo1{
text-align: left;
font-size: 30pt;
color: #604137;


}

.imgcurva{



}

.container1{

width: 600px;
height: auto;
margin-top: 30px;
margin-left: 20px;



}

.container2{

width:300px;
height: auto;
margin-left: 600px;


}

.container3{

float:left;
width:300px;
height: auto;
margin-top: 20px;




}


.container4{

width:95%;
height: auto;
margin-top: 30px;
margin-left: 20px;
font-size: 12pt;




}


.noferta{
width:95%;
margin-top: 30px;
margin-left: 20px;



}

.parrafo{
  font-size: 17pt;
  text-align: justify;
    text-justify: inter-word;
}



.portafolio1{

float: right;
  width: 830px;
  height: auto;
  margin-top: 50px;
  margin-right: 40px;
  

}

.container5{
margin-left: 20px;
  
}

.oferta{
float:right;

}

th {
    
    background-color: #95D935;
    color: white;

}
table {
    border-collapse: collapse;
    width: 90%;
    height: auto;
margin: auto;
z-index: 5mm;
margin-top: 20px;

}

table, td, th {
   border: 1px solid #ddd;
    text-align: left;
    border-radius: 5px;
}

.container5 p {
  margin-top: 30px;
  font-size: 12pt;
}


th, td {
 text-align: left;
    padding: 8px;
    font-size: 14pt;
}

}
tr:nth-child(even){background-color: #f2f2f2}
tr { page-break-inside: avoid !important; }

.cabecera{
  text-align: center;
  font-size: 18pt;
}


</style>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

</head>
<body>
  <br><br><br><br><br><br>

	     <p class="cabecera"><strong>Eco Industria S.A.S E.S.P identificada con NIT: 900.140.609-1<br>CERTIFICA QUE:</strong></p><br><br><br>

       <p style="text-align: center; font-size: 14pt;"><strong>{{ $clientes->razon_social }}</strong> con número de identificación: {{ $clientes->identificacion }}, ha realizado entrega de residuos peligrosos relacionados a continuación: </p><br>

       

          

	<table>
                <thead>
                  <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Pretratamiento</th>
                    <th scope="col">Cantidad en KG</th>        
                  </tr>
                </thead>
                      <tbody>
                        <?php 
                              foreach ($peligrosos as $peligroso) {   
                           ?>
                        <tr>
                          <td>{{($peligroso->nombre)}}</td>
                          <td>{{($peligroso->pretratamiento)}}</td>
                          <td>{{($peligroso->cantidad)}}</td>
                          
                        </tr>
                        <?php } ?>
                      </tbody>
                </table><br>

                <p style="font-size: 13pt; text-align: justify;">Transportados por <strong>Eco Industria SAS ESP</strong> para posteriormente ser llevados a la empresa <strong>Tecnologías Ambientales De Colombia Sas Esp Tecniamsa</strong>- Nit 805.001.538-5, para realizarles una correcta disposición final.</p>

                <p style="text-decoration: underline; font-size: 14pt;">Se anexa certificado de disposición final</p><br><br>
                <p style="font-size: 13pt;">En constancia de lo anterior se expide en Mosquera, Cundinamarca, el día {{ \Carbon\Carbon::parse($remision->fechaserv)->addDays(5)->format('d-m-y')}}</p><br>

                <p style="font-size: 13pt;">Cordialmente,</p><br><br>

                <p style="font-size: 14pt;"><strong>Carlos Alberto Garzón Díaz</strong></p>
                <p style="font-size: 13pt;"><strong>Gerente Administrativo</strong></p>
</body>
</html>