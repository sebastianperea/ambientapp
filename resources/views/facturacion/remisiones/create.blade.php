@extends('admin.template.facturacion.main')

@section('content')

@section('titulo', 'Crear Remisión')



    <div class="col-lg-12">
      <div class="card card-tab">
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">

                <div class="section">
                  <div class="section-title"><i class="icon fa fa-user" aria-hidden="true"></i> Crear Remisión
                  </div>
            {!! Form::open(['route' => 'remisiones.store', 'method' => 'POST']) !!}
               
               
            <div class="row">

              <div class="col-sm-2">
                   {!! Form::label('#','#')!!}
                   {!!Form::number('numero',null, ['class' => 'form-control numero', 'id' => 'numero' ,'placeholder' => '#' , 'required', 'min'=>1])!!}
                </div>

              <div class="col-xs-4">
                   {!! Form::label('razon social','Razon Social')!!}
                   {!!Form::text('razon_social',null, ['class' => 'form-control razonsocial', 'id' => 'razonsocial' ,'placeholder' => 'Razón social' , 'required'])!!}
                </div>

              <div class="col-xs-3">
                   {!! Form::label('identificacion','Identificación')!!}
                    {!!Form::number('identificacion',null, ['class' => 'form-control identificacion', 'id'=> 'identificacion', 'placeholder' => 'Identificación' , 'required', 'readonly']) !!}
                </div>
                <div class="col-xs-1">
                    {!! Form::label('Dv','Dv')!!}
                    {!!Form::number('dv',null, ['class' => 'form-control dv', 'id'=>'dv','placeholder' => 'DV' , 'required', 'readonly']) !!}
                </div>

 
                

                <div class="col-xs-3">
                   {!! Form::label('departamento','Departamento')!!}
                   {!!Form::text('departamento',null, ['class' => 'form-control departamento','id'=> 'departamento', 'placeholder' => 'Departamento' , 'required', 'readonly'])!!}
                </div>

                <div class="col-xs-3">
                   {!! Form::label('ciudad','Ciudad')!!}
                   {!!Form::text('ciudad',null, ['class' => 'form-control ciudad', 'id'=>'ciudad','placeholder' => 'Ciudad' , 'required', 'readonly'])!!}
                </div>

                <div class="col-xs-5">
                   {!! Form::label('direccion','Dirección')!!}
                   {!!Form::text('direccion',null, ['class' => 'form-control direccion', 'id'=>'direccion','placeholder' => 'Dirección' , 'required', 'readonly'])!!}
                </div>


                  <div class="col-xs-2">
                  {!! Form::label('ejdecuenta','Ej')!!}
                  {!!Form::number('ejdecuenta',null,['class' => 'form-control ejdecuenta', 'id'=>'ejdecuenta','required', 'readonly'])!!}
                </div>

                <div class="col-sm-4">
                   {!! Form::label('Fecha de servicio','')!!}
                   {!!Form::date('fechaserv',null, ['class' => 'form-control ', 'placeholder' => 'Fecha de servicio' , 'required']) !!}
                  </div>
                

              </div>

              
<!-- Inicio Peligrosos -->
             <div class="col-xs-12  "><h3 style="text-align: center;">Peligrosos</h3><hr> </div>
          
                <table class="table table-striped" id="tabla2">
                    <thead>
                        <th>Nombre</th>
                        <th>Codigo Residuos peligrosos</th>
                        <th>Cantidad en kg</th>
                    </thead>
                        <tbody>
                             <tr class="fila2">
                                <td> 
                                  {!!Form::text('nombre []',null,['id'=>'searchname_1','class' => 'form-control searchname ','placeholder' =>'Ingrese aquí el nombre' ,'size' => '80']) !!}
                                </td>
                                <td>
                                  {!!Form::number('id [] ', null, ['id'=>'id_1','class' => 'form-control id ', 'readonly']) !!}
                                </td>
                               <td>
                                <div class="input-group">
                                    {!!Form::number('cantidad [] ', null, ['class' => 'form-control ', 'id'=>'cantidad_1']) !!}
                                 </div>
                              </td>

                              <td class="eliminar2 btn btn-warning btn-xs">Eliminar Fila</td>
                            </tr>
                      </tbody>
                </table>
                 
                 <div class="row">
                   <div class="col-lg-12">
                      <div class="col-xs-2">
                        <input type="button" id="agregar2" value="Agregar fila" class="btn btn-success btn-xs"/>
                      </div><br> 
                    </div>
                  </div>

                  <!--    Fin peligrosos -->

          <!-- Inicio Aprovechables -->

                  <div class="col-xs-12"><h3 style="text-align: center;">Aprovechables</h3><hr> </div>
              <table class="table table-striped" id="tabla3">
                <thead>
 
                    <th>Nombre</th>
                    <th>Codigo Residuo</th>
                    <th>Cantidad en kg</th>
                    <th>Cantidad de Unidades</th>

                </thead>

                    <tbody>
                       
                         
                         <tr class="fila3">

                              <td> {!!Form::text('nombrea []',null,['id'=>'searchnamea_1','class' => 'form-control searchnamea ','placeholder' =>'Ingrese aquí el nombre', 'size' => '80']) !!}</td>

                              <td>{!!Form::number('ida [] ', null, ['id'=>'ida_1','class' => 'form-control ida ', 'readonly']) !!}</td>

                           <td>

                            <div class="input-group">
                                {!!Form::number('cantidada [] ', null, ['class' => 'form-control ', 'id'=>'cantidad_1']) !!}
                             </div>
                          </td>

                          <td>

                            <div class="input-group">
                                {!!Form::number('cantidadu [] ', null, ['class' => 'form-control ', 'id'=>'cantidad_2']) !!}
                             </div>
                          </td>


                          

                          
                          <td class="eliminar3 btn btn-warning btn-xs">Eliminar Fila</td>
                          
                        </tr>

                  </tbody>
                </table>

                      <div class="row">
                   <div class="col-lg-12">
                      <div class="col-xs-2">
                        <input type="button" id="agregar3" value="Agregar fila" class="btn btn-success btn-xs"/>
                      </div><br> 
                    </div>
                  </div>
 
  
                    <!--    Fin Aprovechables -->


                   <!-- Inicio Posconsumos -->
                    <div class="col-xs-12"><h3 style="text-align: center;"> Posconsumos </h3><hr> </div>
              <table class="table table-striped" id="tabla4">
                <thead>
 
                    <th>Nombre</th>
                    <th>Codigo Residuo</th>
                    <th>Cantidad en kg</th>

                </thead>

                    <tbody>
                       
                         
                         <tr class="fila4">

                              <td> {!!Form::text('nombrepo []',null,['id'=>'searchnamepo_1','class' => 'form-control searchnamepo ','placeholder' =>'Ingrese aquí el nombre', 'size' => '80']) !!}</td>

                              <td>{!!Form::number('idpo [] ', null, ['id'=>'idpo_1','class' => 'form-control idpo ', 'readonly']) !!}</td>

                           <td>

                            <div class="input-group">
                                {!!Form::number('cantidadpo [] ', null, ['class' => 'form-control ', 'id'=>'cantidad_1']) !!}
                             </div>
                          </td>


                          
                          <td class="eliminar4 btn btn-warning btn-xs">Eliminar Fila</td>
                          
                        </tr>
                         
                            

                      </tbody>
                    </table>

                    <div class="row">
                         <div class="col-lg-12">
                            <div class="col-xs-2">
                              <input type="button" id="agregar4" value="Agregar fila" class="btn btn-success btn-xs"/>
                            </div><br> 
                          </div>
                        </div>

                        <!-- Inicio peligrososa -->

                        <div class="col-xs-12"><h3 style="text-align: center;"> Peligrosos Aprovechables </h3><hr> </div>
              <table class="table table-striped" id="tabla5">
                <thead>
 
                    <th>Nombre</th>
                    <th>Codigo Residuo</th>
                    <th>Cantidad en kg</th>
                    <th>Cantidad en galones</th>
                    <th>Cantidad en unidades</th>

                </thead>

                    <tbody>
                       
                         
                         <tr class="fila5">

                              <td> {!!Form::text('nombrepa []',null,['id'=>'searchnamepa_1','class' => 'form-control searchnamepa ','placeholder' =>'Ingrese aquí el nombre']) !!}</td>

                              <td>{!!Form::number('idpa [] ', null, ['id'=>'idpa_1','class' => 'form-control idpa ', 'readonly']) !!}</td>

                           <td>

                            <div class="input-group">
                                {!!Form::number('cantidadpa [] ', null, ['class' => 'form-control ', 'id'=>'cantidad_1']) !!}
                             </div>
                          </td>

                          <td>

                            <div class="input-group">
                                {!!Form::number('cantidadpau [] ', null, ['class' => 'form-control ', 'id'=>'cantidad_1']) !!}
                             </div>
                          </td>

                          <td>

                            <div class="input-group">
                                {!!Form::number('cantidadpag [] ', null, ['class' => 'form-control ', 'id'=>'cantidad_1']) !!}
                             </div>
                          </td>

                          
                          <td class="eliminar5 btn btn-warning btn-xs">Eliminar Fila</td>
                          
                        </tr>

                      </tbody>
                    </table>

                     <div class="row">
                         <div class="col-lg-12">
                            <div class="col-xs-2">
                              <input type="button" id="agregar5" value="Agregar fila" class="btn btn-success btn-xs"/>
                            </div><br> 
                          </div>
                        </div>


    <!-- Inicio Especiales -->
                        <div class="col-xs-12"><h3 style="text-align: center;"> Especiales </h3><hr> </div>
              <table class="table table-striped" id="tabla6">
                <thead>
 
                    <th>Nombre</th>
                    <th>Codigo Residuo</th>
                    <th>Cantidad en kg</th>

                </thead>

                    <tbody>
                       
                         
                         <tr class="fila6">

                              <td> {!!Form::text('nombree []',null,['id'=>'searchnamee_1','class' => 'form-control searchnamee ','placeholder' =>'Ingrese aquí el nombre']) !!}</td>

                              <td>{!!Form::number('ide [] ', null, ['id'=>'ide_1','class' => 'form-control ide ', 'readonly']) !!}</td>

                           <td>

                            <div class="input-group">
                                {!!Form::number('cantidade [] ', null, ['class' => 'form-control ', 'id'=>'cantidad_1']) !!}
                             </div>
                          </td>

                          

                          
                          <td class="eliminar5 btn btn-warning btn-xs">Eliminar Fila</td>
                          
                        </tr>

                     
                   </tbody>
                 </table>

                 <div class="row">
                         <div class="col-lg-12">
                            <div class="col-xs-2">
                              <input type="button" id="agregar6" value="Agregar fila" class="btn btn-success btn-xs"/>
                            </div><br> 
                          </div>
                        </div>  

            
            <!-- Inicio Transporte -->
                         <div class="col-xs-12"><h3 style="text-align: center;"> Transporte </h3><hr> </div>
              <table class="table table-striped" id="tabla7">
                <thead>
 
                    <th>Tipo</th>
                    <th>Codigo</th>
                    <th>Cantidad</th>

                </thead>

                    <tbody>
                       
                         
                         <tr class="fila7">

                              <td> {!!Form::text('tipo []',null,['id'=>'searchnamet_1','class' => 'form-control searchnamet ','placeholder' =>'Ingrese aquí el nombre']) !!}</td>

                              <td>{!!Form::number('idt [] ', null, ['id'=>'idt_1','class' => 'form-control ide ', 'readonly']) !!}</td>

                              <td>{!!Form::number('cantidadt [] ', null, ['id'=>'cantidadt_1','class' => 'form-control ide ']) !!}</td>


                          
                          <td class="eliminar7 btn btn-warning btn-xs">Eliminar Fila</td>
                          
                        </tr>
                      </tbody>
                    </table>

                        <div class="row">
                         <div class="col-lg-12">
                            <div class="col-xs-2">
                              <input type="button" id="agregar7" value="Agregar fila" class="btn btn-success btn-xs"/>
                            </div><br> 
                          </div>
                        </div>  

          <!-- Inicio PYS -->

            <div class="col-xs-12"><h3 style="text-align: center;"> Productos y servicios </h3><hr> </div>
              <table class="table table-striped" id="tabla8">
                <thead>
                    <th>Nombre</th>
                    <th>Codigo</th>
                    <th>Cantidad</th>
                </thead>

                    <tbody>
                         <tr class="fila8">

                              <td> {!!Form::text('nombrepys []',null,['id'=>'searchnamepys_1','class' => 'form-control searchnamepys ','placeholder' =>'Ingrese aquí el nombre']) !!}</td>

                              <td>{!!Form::number('idpys [] ', null, ['id'=>'idpys_1','class' => 'form-control idpys ', 'readonly']) !!}</td>

                              <td>{!!Form::number('cantidadpys [] ', null, ['id'=>'cantidadpys_1','class' => 'form-control cantidadpys ']) !!}</td>

                          <td class="eliminar8 btn btn-warning btn-xs">Eliminar Fila</td>
                          
                        </tr>

                      </tbody>
                    </table>

                    <div class="row">
                         <div class="col-lg-12">
                            <div class="col-xs-2">
                              <input type="button" id="agregar8" value="Agregar fila" class="btn btn-success btn-xs"/>
                            </div><br> 
                          </div>
                        </div>
               			

               			<!-- Fin tabla -->

               			<div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               {!!Form::submit('Guardar', ['class' => 'btn btn-primary'])!!}
                               {!! Form::close() !!}

                          </div>
                      </div>
                  </div>
            </div>
          </div>
         </div>
        </div>
      </div>
    </div>


                  

          
          
@endsection

@section('js')



 <!-- Jquery -->


<script type="text/javascript">
    

$('#razonsocial').autocomplete({

  source: ' {!!URL::route('facturacion.autocompleter')!!}',
  minlenght: 1,
  autoFocus: true,
  select: function(e,ui){



    $('#identificacion').val(ui.item.identificacion);
    $('#dv').val(ui.item.dv);
    $('#razonsocial').val(ui.item.value);
    $('#departamento').val(ui.item.departamento);
    $('#ciudad').val(ui.item.ciudad);
    $('#direccion').val(ui.item.direccion);
    $('#ejdecuenta').val(ui.item.ejdecuenta);
  
  }


}); 


</script>

<!-- Peligrosos -->
<script>

$(document).ready(function(){

$(document).on('keydown', '.searchname', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocomplete')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#id_' + index).val(ui.item.id).clone(true);



  }
    });

});

 
 // Add more
 $('#agregar2').click(function(){
  
 


  // Get last id 
  var lastname_id = $('.fila2 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila2'><td><input id='searchname_"+index+"' class=' form-control searchname' required name='nombre_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='id form-control' readonly id='id_"+index+"' name='id_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidad_[]' ></td>  </div></td> <td class='eliminar2 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla2 tbody').append(html);
 
 });

$(document).on("click",".eliminar2",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

</script>


<!-- Aprovechables -->
  <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamea', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletea')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#ida_' + index).val(ui.item.id).clone();
   


  }
    });

});

 
 // Add more
 $('#agregar3').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila3 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila3'><td><input id='searchnamea_"+index+"' class=' form-control searchnamea' required name='nombrea_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='id form-control' readonly id='ida_"+index+"' name='ida_[]' ></td><td><input type='number' class='cantidada form-control' id='cantidada_"+index+"' name='cantidada_[]' ></td> <td><input type='number' class='cantidad form-control' id='cantidadu_"+index+"' name='cantidadu_[]' ></td>  <td class='eliminar3 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla3 tbody').append(html);
 
 });

$(document).on("click",".eliminar3",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>


    <!-- Posconsumos -->
 <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamepo', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletepo')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idpo_' + index).val(ui.item.id).clone(true);
    $('#posconsumosprecio_' + index).val(ui.item.value2);
   


  }
    });

});

 
 // Add more
 $('#agregar4').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila4 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila4'><td><input id='searchnamepo_"+index+"' class=' form-control searchnamepo' required name='nombrepo_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='idpo form-control' readonly id='idpo_"+index+"' name='idpo_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidadpo_[]' ></td>  <td class='eliminar4 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla4 tbody').append(html);
 
 });

$(document).on("click",".eliminar4",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>


<!-- Peligrosos Aprovechables -->
 <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamepa', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletepa')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idpa_' + index).val(ui.item.id).clone(true);

   


  }
    });

});

 
 // Add more
 $('#agregar5').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila5 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila5'><td><input id='searchnamepa_"+index+"' class=' form-control searchnamepa' required name='nombrepa_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='idpa form-control' readonly id='idpa_"+index+"' name='idpa_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidadpa_[]' ></td> <td><input type='number' class='cantidad form-control' id='cantidadpau_"+index+"' name='cantidadpau_[]' ></td>  <td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidadpag_[]' ></td> <td class='eliminar5 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla5 tbody').append(html);
 
 });

$(document).on("click",".eliminar5",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>
 


<script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamee', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletee')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#ide_' + index).val(ui.item.id).clone(true);
    $('#especialesprecio_' + index).val(ui.item.value2);
   


  }
    });

});

 
 // Add more
 $('#agregar6').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila6 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila6'><td><input id='searchnamee_"+index+"' class=' form-control searchnamee' required name='nombree_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='id form-control' readonly id='ide_"+index+"' name='ide_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidade_[]' ></td>  <td class='eliminar6 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla6 tbody').append(html);
 
 });

$(document).on("click",".eliminar6",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>


  <!-- Transporte  -->

<script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamet', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletet')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idt_' + index).val(ui.item.id);
    

  }
    });

});

 
 // Add more
 $('#agregar7').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila7 input[type=text]:nth-child(1)').last().attr('id');


  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila7'><td><input id='searchnamet_"+index+"' class=' form-control searchnamet' required name='tipo_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='idt form-control' readonly id='idt_"+index+"' name='idt_[]' ></td><td><input type='number' class='cantidadt form-control' id='cantidadt_"+index+"' name='cantidadt_[]' ></td> <td class='eliminar6 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla7 tbody').append(html);
 
 });

$(document).on("click",".eliminar7",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

</script>

<!-- Inicio PYS -->


<script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamepys', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];


    $( '#' + id).autocomplete({  

     source: ' {!!URL::route('autocompletepys')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){


    $('#idpys_' + index).val(ui.item.id);
   
  }
    });

});

 
 // Add more
 $('#agregar8').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila8 input[type=text]:nth-child(1)').last().attr('id');


  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;




  // Create row with input elements
  var html = "<tr class='fila8'><td><input id='searchnamepys_"+index+"' class=' form-control searchnamepys' required name='nombrepys_[]'  type='text'  placeholder='Ingrese el nombre del PYS'></td><td><input type='number' class='id form-control' readonly id='idpys_"+index+"' name='idpys_[]' ></td><td><input type='number' class='cantidadpys form-control'  id='cantidadpys_"+index+"' name='cantidadpys_[]' ></td>  <td class='eliminar8 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla8 tbody').append(html);
 
 });

 // <tr class='fila6'><td><input id='searchnamepys_"+index+"' class=' form-control searchnamepys' required name='nombrepys_[]'  type='text'  placeholder='Ingrese el nombre del PYS'></td><td><input type='number' class='id form-control' readonly id='idpys_"+index+"' name='idpys_[]' ></td><td><input type='number' class='cantidadpys form-control'  id='cantidadpys_"+index+"' name='cantidadpys_[]' ></td>  <td class='eliminar8 btn btn-warning btn-xs'>Eliminar fila</td></tr>

$(document).on("click",".eliminar8",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>





@endsection
