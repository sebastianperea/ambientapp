@extends('admin.template.facturacion.main')

@section('content')

@section('titulo', 'Ver remisión')


  @include('facturacion.remisiones.modal')

    <div class="col-lg-12">
      <div class="card card-tab">
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row">


                <div class="section">
                  <div class="section-title"><i class="icon fa fa-user" aria-hidden="true"></i> Ver remisión
                  </div>

                  


               
               
            <div class="row">

              <div class="col-sm-2">
                   {!! Form::label('#','#')!!}
                   <p><strong>{{ $remision->numero }}</strong></p>
                </div>

                <div class="col-xs-3">
                   {!! Form::label('Razón social','Razón social')!!}
                    <p>{{ $cliente->razon_social }}</p>
                </div>

              <div class="col-xs-3">
                   {!! Form::label('identificacion','Identificación')!!}
                    <p>{{ $remision->identificacion }}</p>
                </div>

                <div class="col-sm-4">
                   {!! Form::label('Fecha de servicio','')!!}
                   {{ $remision->fechaserv }}
                  </div>

                  <div class="col-sm-4">
                   <a href="#" target="_blank" class="btn btn-success btn-xs" ><span class="fa fa-eye"> </a>
            <a href="#" target="#" class="btn btn-primary btn-xs" ><span class="fa fa-arrow-down"> </a>

              <a href="{{route('cert.pdf' , $remision->id)}}" target="" class="btn btn-primary btn-xs" ><span class="fa fa-file-pdf-o"> </a>
            
                  </div>
                

              </div>

              <hr>

              
              <div>

                @if(count($peligrosos)>0)
              <h3>Peligrosos</h3>

              <a href="{{route('facturacion.certpeligrosospdf.pdf',['id'=>$remision->id, 'identificacion' => $cliente->identificacion])}}" target="" class="btn btn-primary btn-xs" ><span class="fa fa-file-pdf-o"> </a>
              <button
                     type="button"
                     class="btn btn-warning btn-xs"
                      data-toggle="modal"
                      data-id1="{{ $remision->id }}"
                      data-target="#myModal">
                  <span class="fa fa-cloud-upload">
                </button>
              </div>

              @include('facturacion.remisiones.modal')


                
              <div class="card-body no-padding">
            <table class=" table table-striped primary" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Nombre</th>
                      <th>Cantidad</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($peligrosos as $peligroso)
                 <tr> 
                     <td>{{$peligroso->id}}</td>
                     <td>{{$peligroso->nombre}}</td>
                     <td>{{$peligroso->cantidad}}</td>
                     
                 </tr>
               @endforeach
              </tbody>
            </table>
          </div>
          </div><br>

          @endif
          <hr>

          @if(count($aprovechables)>0)
          <h3>Aprovechables</h3>

              <div class="card-body no-padding">
            <table class=" table table-striped primary" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Nombre</th>
                      <th>Cantidad en kg</th>
                      <th>Cantidad en unidades</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($aprovechables as $aprovechable)
                 <tr> 
                     <td>{{$aprovechable->id}}</td>
                     <td>{{$aprovechable->nombre}}</td>
                     <td>{{$aprovechable->cantidada}}</td>
                     <td>{{$aprovechable->cantidadu}}</td>
                     
                 </tr>
               @endforeach
              </tbody>
            </table>
          </div><br>
          <hr>
          @endif

          @if(count($posconsumos)>0)

          <h3>Posconsumos</h3>

              <div class="card-body no-padding">
            <table class=" table table-striped primary" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Nombre</th>
                      <th>Cantidad</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($posconsumos as $posconsumo)
                 <tr> 
                     <td>{{$posconsumo->id}}</td>
                     <td>{{$posconsumo->nombre}}</td>
                     <td>{{$posconsumo->cantidad}}</td>
                     
                 </tr>
               @endforeach
              </tbody>
            </table>
          </div><br>

          <hr>

          @endif
          @if(count($peligrososa)>0)
          <h3>Peligrososa</h3>

              <div class="card-body no-padding">
            <table class=" table table-striped primary" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Nombre</th>
                      <th>Cantidad en kg</th>
                      <th>Cantidad en galones</th>
                      <th>Cantidad por unidad</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($peligrososa as $peligrosoa)
                 <tr> 
                     <td>{{$peligrosoa->id}}</td>
                     <td>{{$peligrosoa->nombre}}</td>
                     <td>{{$peligrosoa->cantidad}}</td>
                     <td>{{$peligrosoa->cantidadpag}}</td>
                     <td>{{$peligrosoa->cantidadpau}}</td>
                     
                 </tr>
               @endforeach
              </tbody>
            </table>
          </div><br>
          <hr>
          @endif

          @if(count($especiales)>0)
          <h3>Especiales</h3>

              <div class="card-body no-padding">
            <table class=" table table-striped primary" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Nombre</th>
                      <th>Cantidad en kg</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($especiales as $especial)
                 <tr> 
                     <td>{{$especial->id}}</td>
                     <td>{{$especial->nombre}}</td>
                     <td>{{$peligrosoa->cantidad}}</td>
                     
                 </tr>
               @endforeach
              </tbody>
            </table>
          </div><br>
          <hr>
          @endif

          @if(count($transporte)>0)

          <h3>Transporte</h3>

              <div class="card-body no-padding">
            <table class=" table table-striped primary" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Nombre</th>
                      <th>Cantidad en kg</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($transporte as $trans)
                 <tr> 
                     <td>{{$trans->id}}</td>
                     <td>{{$trans->nombre}}</td>
                     <td>{{$trans->cantidad}}</td>
                     
                 </tr>
               @endforeach
              </tbody>
            </table>
          </div><br>
          <hr>

          @endif

          @if(count($pys)>0)
          <h3>Productos y servicios</h3>

              <div class="card-body no-padding">
            <table class=" table table-striped primary" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Nombre</th>
                      <th>Cantidad</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($pys as $p)
                 <tr> 
                     <td>{{$p->id}}</td>
                     <td>{{$p->nombre}}</td>
                     <td>{{$p->cantidad}}</td>
                     
                 </tr>
               @endforeach
              </tbody>
            </table>
          </div>
            @endif



   

                    
            </div>
          </div>
         </div>
        </div>
      </div>
    </div>


                  

          
          
@endsection

@section('js')



 <!-- Jquery -->


<script type="text/javascript">
    

$('#razonsocial').autocomplete({

  source: ' {!!URL::route('facturacion.autocompleter')!!}',
  minlenght: 1,
  autoFocus: true,
  select: function(e,ui){



    $('#identificacion').val(ui.item.identificacion);
    $('#dv').val(ui.item.dv);
    $('#razonsocial').val(ui.item.value);
    $('#departamento').val(ui.item.departamento);
    $('#ciudad').val(ui.item.ciudad);
    $('#direccion').val(ui.item.direccion);
    $('#ejdecuenta').val(ui.item.ejdecuenta);
  
  }


}); 


</script>

<!-- Peligrosos -->
<script>

$(document).ready(function(){

$(document).on('keydown', '.searchname', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocomplete')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#id_' + index).val(ui.item.id).clone(true);



  }
    });

});

 
 // Add more
 $('#agregar2').click(function(){
  
 


  // Get last id 
  var lastname_id = $('.fila2 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila2'><td><input id='searchname_"+index+"' class=' form-control searchname' required name='nombre_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='id form-control' readonly id='id_"+index+"' name='id_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidad_[]' ></td>  </div></td> <td class='eliminar2 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla2 tbody').append(html);
 
 });

$(document).on("click",".eliminar2",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

</script>


<!-- Aprovechables -->
  <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamea', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletea')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#ida_' + index).val(ui.item.id).clone();
   


  }
    });

});

 
 // Add more
 $('#agregar3').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila3 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila3'><td><input id='searchnamea_"+index+"' class=' form-control searchnamea' required name='nombrea_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='id form-control' readonly id='ida_"+index+"' name='ida_[]' ></td><td><input type='number' class='cantidada form-control' id='cantidada_"+index+"' name='cantidada_[]' ></td> <td><input type='number' class='cantidad form-control' id='cantidadu_"+index+"' name='cantidadu_[]' ></td>  <td class='eliminar3 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla3 tbody').append(html);
 
 });

$(document).on("click",".eliminar3",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>


    <!-- Posconsumos -->
 <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamepo', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletepo')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idpo_' + index).val(ui.item.id).clone(true);
    $('#posconsumosprecio_' + index).val(ui.item.value2);
   


  }
    });

});

 
 // Add more
 $('#agregar4').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila4 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila4'><td><input id='searchnamepo_"+index+"' class=' form-control searchnamepo' required name='nombrepo_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='idpo form-control' readonly id='idpo_"+index+"' name='idpo_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidadpo_[]' ></td>  <td class='eliminar4 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla4 tbody').append(html);
 
 });

$(document).on("click",".eliminar4",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>


<!-- Peligrosos Aprovechables -->
 <script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamepa', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletepa')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idpa_' + index).val(ui.item.id).clone(true);

   


  }
    });

});

 
 // Add more
 $('#agregar5').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila5 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila5'><td><input id='searchnamepa_"+index+"' class=' form-control searchnamepa' required name='nombrepa_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='idpa form-control' readonly id='idpa_"+index+"' name='idpa_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidadpa_[]' ></td> <td><input type='number' class='cantidad form-control' id='cantidadpau_"+index+"' name='cantidadpau_[]' ></td>  <td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidadpag_[]' ></td> <td class='eliminar5 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla5 tbody').append(html);
 
 });

$(document).on("click",".eliminar5",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>
 


<script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamee', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletee')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#ide_' + index).val(ui.item.id).clone(true);
    $('#especialesprecio_' + index).val(ui.item.value2);
   


  }
    });

});

 
 // Add more
 $('#agregar6').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila6 input[type=text]:nth-child(1)').last().attr('id');
  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila6'><td><input id='searchnamee_"+index+"' class=' form-control searchnamee' required name='nombree_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='id form-control' readonly id='ide_"+index+"' name='ide_[]' ></td><td><input type='number' class='cantidad form-control' id='cantidad_"+index+"' name='cantidade_[]' ></td>  <td class='eliminar6 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla6 tbody').append(html);
 
 });

$(document).on("click",".eliminar6",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>


  <!-- Transporte  -->

<script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamet', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];



    $( '#' + id).autocomplete({  

                  

     source: ' {!!URL::route('autocompletet')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){



    $('#idt_' + index).val(ui.item.id);
    

  }
    });

});

 
 // Add more
 $('#agregar7').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila7 input[type=text]:nth-child(1)').last().attr('id');


  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;



  // Create row with input elements
  var html = "<tr class='fila7'><td><input id='searchnamet_"+index+"' class=' form-control searchnamet' required name='tipo_[]'  type='text'  placeholder='Ingrese el nombre del residuo'></td><td><input type='number' class='idt form-control' readonly id='idt_"+index+"' name='idt_[]' ></td><td><input type='number' class='cantidadt form-control' id='cantidadt_"+index+"' name='cantidadt_[]' ></td> <td class='eliminar6 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla7 tbody').append(html);
 
 });

$(document).on("click",".eliminar7",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

</script>

<!-- Inicio PYS -->


<script type="text/javascript">
    

  $(document).ready(function(){

$(document).on('keydown', ' .searchnamepys', function() {

  var id = this.id;
  var splitid = id.split('_');
  var index = splitid[1];


    $( '#' + id).autocomplete({  

     source: ' {!!URL::route('autocompletepys')!!}',
  minlenght: 1,
  autoFocus: true,

  select: function(e,ui){


    $('#idpys_' + index).val(ui.item.id);
   
  }
    });

});

 
 // Add more
 $('#agregar8').click(function(){
  
 

  // Get last id 
  var lastname_id = $('.fila8 input[type=text]:nth-child(1)').last().attr('id');


  var split_id = lastname_id.split('_');

  // New index
  var index = Number(split_id[1]) + 1;




  // Create row with input elements
  var html = "<tr class='fila8'><td><input id='searchnamepys_"+index+"' class=' form-control searchnamepys' required name='nombrepys_[]'  type='text'  placeholder='Ingrese el nombre del PYS'></td><td><input type='number' class='id form-control' readonly id='idpys_"+index+"' name='idpys_[]' ></td><td><input type='number' class='cantidadpys form-control'  id='cantidadpys_"+index+"' name='cantidadpys_[]' ></td>  <td class='eliminar8 btn btn-warning btn-xs'>Eliminar fila</td></tr> ";

  // Append data
  $('#tabla8 tbody').append(html);
 
 });

 // <tr class='fila6'><td><input id='searchnamepys_"+index+"' class=' form-control searchnamepys' required name='nombrepys_[]'  type='text'  placeholder='Ingrese el nombre del PYS'></td><td><input type='number' class='id form-control' readonly id='idpys_"+index+"' name='idpys_[]' ></td><td><input type='number' class='cantidadpys form-control'  id='cantidadpys_"+index+"' name='cantidadpys_[]' ></td>  <td class='eliminar8 btn btn-warning btn-xs'>Eliminar fila</td></tr>

$(document).on("click",".eliminar8",function(){
    var parent = $(this).parents().get(0);
    $(parent).remove();

    });
});

  </script>

<script>
$(function() {
    $('#myModal').on("show.bs.modal", function (e) {
         $("#id1").html($(e.relatedTarget).data('id1'));
        

    });
});
</script>



@endsection
