

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  {!! Form::open(['route' => ['facturacion.certpeligrosos.up', $remision->id],'files' => true, 'enctype'=>'multipart/form-data','method' => 'PUT' ]) !!}
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Subir certificados en peligrosos</h4>
      </div>
      <div class="modal-body">

            
                
                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Buscar</span>
                        <input type="file" class="form-control" name="certificados[]" multiple /> <!-- rename it -->
                    </div>
                </span>
            
        <div class="row">
                      <div class="col-lg-12">
                          <div class="col-xs-2">
                               {!!Form::submit('Guardar', ['class' => 'btn btn-primary guardar', 'id'=>'guardar'])!!}
                               {!! Form::close() !!}

                          </div>
                      </div>
                  </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>