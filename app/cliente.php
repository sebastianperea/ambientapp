<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Notifications\NotifyOfertas;
use Illuminate\Notifications\Notifiable;

class cliente extends Model
{

   use Notifiable;
   
   protected $table="clientes";
   protected $primaryKey = 'identificacion';
   protected $fillable= ['identificacion','tipo_de_identificacion','dv','razon_social','departamento','ciudad', 'direccion','tipo_de_ide','ide','nombres','apellidos','cargo','telefonocelular','email','emailop','telefono_contacto','ext', 'cargo','nombrecartera','telefonofijocartera','telefonomovilcartera','extcartera','emailcartera','nombreempresauno','telefonoempresauno','nombrempresados','telefonoempresados','nombrebanco','numerodecuenta','tipodecuenta','terminos','politicadeprivacidad','procesos','tipo','estado','tipodepersona','credito','canal','ejdecuenta'];

   public function oferta()
   {

return $this ->belongsTo('App\oferta');
   }


// public function cliente()
//    {

// return $this ->belongsToMany('App\cliente');
//    }

   public function sedes()
   {

      return $this->hasMany('App\sedes','identificacion');

   }

   public function scopeSearch($query, $identificacion)
   {

			return $query->where('identificacion','LIKE',"%$identificacion%");
   }   
     
      
      public function cbancariapj()
   {

      return $this->hasMany('App\cbancariapj','cliente_id');

   }

    public function cerlpj()
   {

      return $this->hasMany('App\cerlpj','id');

   }

    public function cmpn()
   {

      return $this->hasMany('App\cmpn','cliente_id');

   }

   public function drentapj()
   {

      return $this->hasMany('App\drentapj','cliente_id');

   }

   public function drentapn()
   {

      return $this->hasMany('App\drentapn','cliente_id');

   }

   public function estadosfinancieros()
   {

      return $this->hasMany('App\estadosfinancieros','cliente_id');

   }

   public function fotocopiacpj()
   {

      return $this->hasMany('App\fotocopiacpj','cliente_id');

   }

   public function fotocopiacpn()
   {

      return $this->hasMany('App\fotocopiacpn','cliente_id');

   }

   public function rutpj()
   {

      return $this->hasMany('App\rutpj','cliente_id');

   }

   public function rutpn()
   {

      return $this->hasMany('App\rutpn','cliente_id');

   }


   public function cbancariapn()
   {

      return $this->hasMany('App\cbancariapn','cliente_id');

   }

   
}
