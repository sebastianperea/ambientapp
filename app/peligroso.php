<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class peligroso extends Model
{
   protected $table="peligrosos";
   protected $fillable= ['id','nombre','pretratamiento','dispositor','precio']; //

   public function oferta()
   {

return $this->belongsToMany('App\oferta')->withPivot('cantidad','precio');
   }

   public function remision()
   {

	return $this->belongsToMany('App\remision')->withPivot('cantidad');
   }
}

