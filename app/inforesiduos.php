<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class inforesiduos extends Model
{
	 protected $primaryKey = 'id';
     protected $table="inforesiduos"; 
    protected $fillable= ['idresiduo','nombreresiduo','tipo','detalle','estado','proceso','contaminacion','inforesiduos','habilitado_rec','identificacion','declaracionta'];


    public function residuos()
   {

      return $this->hasMany('App\residuos','inforesiduos');

   }


}
