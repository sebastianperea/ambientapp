<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class residuos extends Model
{
	 protected $table="residuos";
   protected $fillable= ['id','identificacion']; 

     public function filesresiduo()
   {

return $this->belongsToMany('App\filesresiduo');

   } 

    public function filespeligrosos()
   {

return $this->belongsToMany('App\filespeligrosos')->withTimestamps();

   } 

   public function filespeligrososa()
   {

return $this->belongsToMany('App\filespeligrososa')->withTimestamps();

   }   


   public function filese()
   {

return $this->belongsToMany('App\filese')->withTimestamps();

   }   
}
