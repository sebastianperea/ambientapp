<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ofertapc extends Model
{

	protected $table="ofertaspc";
   protected $fillable= ['identificacion','transporte','tipo_vehiculo','desde','hasta','tipo']; 

  public function peligroso()
   {

return $this->belongsToMany('App\peligroso')->withPivot('cantidad','observacion');

   }  //
}
