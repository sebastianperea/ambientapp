<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class filese extends Model
{
    protected $table="filese"; 
    protected $fillable= ['nombre'];


   public function inforesiduos()
   {

		return $this->belongsToMany('App\inforesiduos');
   }


   public function residuos()
   {

		return $this->belongsToMany('App\residuos')->withTimestamps();
   }
}