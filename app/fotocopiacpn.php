<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fotocopiacpn extends Model
{
   protected $table="fotocopiacpn";
   protected $fillable= ['nombre','cliente_id'];

   public function cliente()
   {

return $this->belongsTo('App\cliente','cliente_id');

   }
}
