<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sedes extends Model
{
    protected $table="sedes"; 
    protected $fillable= ['id','identificacion','nombre','direccion', 'departamento','ciudad', 'responsable','cargo','email','emailop', 'telefono'];


    public function cliente()
   {

		return $this->belongsTo('App\cliente','identificacion');

   }

   public function oferta()
   {

return $this->belongsToMany('App\oferta');
   }
}
