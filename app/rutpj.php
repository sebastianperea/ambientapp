<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rutpj extends Model
{
   protected $table="rutpj";
   protected $fillable= ['nombre', 'cliente_id'];

   public function cliente()
   {

return $this->belongsTo('App\cliente','cliente_id');

   }
}
