<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ingresos_lumina extends Model
{
     	
     protected $table="ingresos_lumina"; 
    protected $fillable= ['prefijo','prefijo2','remision','fecha_recepcion','generador','tipo_de_tecnologia','gestor','canal','fecha_aprovechamiento'];

    public function tipos_tecnologia()
   {

	return $this->belongsToMany('App\tipos_tecnologia')->withPivot('pesorkg','pesodfkg');

   } 
}
