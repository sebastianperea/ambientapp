<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fcreacion extends Model
{
    protected $table="cc";
   protected $fillable= ['id', 'nombre', 'identificacion'];

   public function cliente()
   {

		return $this->belongsTo('App\cliente','identificacion');

   }
}
