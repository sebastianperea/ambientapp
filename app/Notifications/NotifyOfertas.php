<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\oferta;
use App\cliente;

class NotifyOfertas extends Notification
{
    use Queueable;
    public $oferta;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($oferta)
    {
       $this->oferta = $oferta;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {
        return [
        'oferta' => $this->oferta,
        ];
    }
    // public function toBroadcast($notifiable)
    // {
    //     return [
    //     'oferta' => $this->oferta,
    //     ];
    // }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
