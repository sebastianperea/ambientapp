<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class Welcome extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

public $pdf;
public $ofertas;
public $clientes;
public $parameter2;
public $useremail;


    public function __construct($pdf, $ofertas, $clientes, $parameter2, $useremail)
    {
      $this->pdf = $pdf;
      $this->ofertas = $ofertas;
      $this->clientes = $clientes; 
      $this->parameter2 = $parameter2;
      $this->useremail = $useremail; 
    
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
     

        return $this->view('comercial.oferta.email.create')
        ->with([
                        'ofertas' => $this->ofertas,
                        'clientes' => $this->clientes,
                        'parameter2' => $this->parameter2,
                        'useremail' => $this->useremail,
                    ])

        ->from($this->useremail,'Ecoindustria S.A.S E.S.P')
        ->subject('Oferta comercial Ecoindustria S.A.S E.S.P')
        ->attachData($this->pdf, 'OF'.$this->ofertas->id.'.pdf', [
                        'mime' => 'application/pdf',
                    ]);
    }
}
