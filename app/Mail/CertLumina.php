<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CertLumina extends Mailable 
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $pdf;

    public function __construct($pdf)
    {
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('posconsumos.email.view')

        ->from('comercial@ecoindustriasas.com','Ecoindustria S.A.S E.S.P')
        ->subject('Certificado Ecoindustria S.A.S E.S.P')->attachData($this->pdf, 'OF.pdf', [
                        'mime' => 'application/pdf',
                    ]);
    }
}
