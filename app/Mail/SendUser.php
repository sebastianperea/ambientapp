<?php

namespace App\Mail;

use App\User;
use App\cliente;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $user;
    public $cliente;
    public $password;

    public function __construct($user,$cliente,$password)
    {
       $this->user = $user;
       $this->cliente = $cliente;
       $this->password = $password;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->view('comercial.clientes.email.createandsend')
        ->with([
                        'user' => $this->user,
                        'cliente' => $this->cliente,
                        'password' => $this->password,
                    ])

        ->from('ambientapp@ecoindustriasas.com','Ambientapp - Ecoindustria S.A.S E.S.P')
        ->subject('Credenciales de ingreso ambientapp');
    }
}
