<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class especiales extends Model
{
   
   protected $table="especiales";
   protected $fillable= ['id','nombre','pretratamiento','precio']; //

   public function oferta()
   {

	return $this->belongsToMany('App\oferta')->withPivot('cantidad','precio');
    //
	}

	public function remision()
   {

	return $this->belongsToMany('App\remision')->withPivot('cantidade');
    //
	} //
}
