<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fresiduos extends Model
{
   protected $table="info_residuos";
   protected $fillable= ['id', 'nombre', 'identificacion'];

   public function cliente()
   {

		return $this->belongsTo('App\cliente','identificacion');

   }

   
}
