<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cerlpj extends Model
{
    protected $table="cerlpj";
   protected $fillable= ['nombre', 'cliente_id'];

   public function cliente()
   {

return $this->belongsTo('App\cliente','cliente_id');

   }
}
