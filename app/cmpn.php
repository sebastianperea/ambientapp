<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cmpn extends Model
{
   protected $table="cmpn";
   protected $fillable= ['nombre','cliente_id'];

   public function cliente()
   {

return $this->belongsTo('App\cliente','cliente_id');

   }
}
