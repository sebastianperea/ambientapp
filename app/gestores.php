<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gestores extends Model
{
    protected $table="gestores";
   protected $fillable= ['id','razonsocial','direccion','telefono','identificacion','dv','contacto_gestor', 'correo','correo2op', 'ciudad', 'departamento', 'centro_de_acopio']; //
}
