<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class remision extends Model
{
    protected $table="remision";
   protected $fillable= ['id','identificacion', 'numero','fechaserv'];

   public function peligroso()
   {

	return $this->belongsToMany('App\peligroso')->withPivot('cantidad');

	}

	public function aprovechables()
   {

	return $this->belongsToMany('App\aprovechables')->withPivot('cantidada', 'cantidadu');

	}

	public function posconsumos()
   {

	return $this->belongsToMany('App\posconsumos')->withPivot('cantidadpo');

	}

	public function peligrososa()
   {

	return $this->belongsToMany('App\peligrososa')->withPivot('cantidad', 'cantidadpau', 'cantidadpag');

	}

	public function especiales()
   {

	return $this->belongsToMany('App\especiales')->withPivot('cantidade');

	}

	public function transporte()
   {

	return $this->belongsToMany('App\transporte')->withPivot('cantidadt');

	}

	public function pys()
   {

	return $this->belongsToMany('App\pys')->withPivot('cantidad');

	}

}
