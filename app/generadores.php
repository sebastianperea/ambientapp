<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class generadores extends Model
{
    protected $table="generadores";
   protected $fillable= ['id','razon_social','direccion','telefono','telefono2','responsable_envio','correoelectronico', 'correo2','ciudad', 'departamento', 'identificacion', 'dv']; //
}
