<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productoyservicio extends Model
{
   
protected $table="productosyservicios";
   protected $fillable= ['Nombre','Precio','Numerodeclaracion','Tratamiento','Hoja_de_seguridad','Estado'];
    //
   public function oferta(){

   	return this > belongToMany('App/oferta');
   }

public function clientes(){

   	return this > belongToMany('App/cliente');
   }

   public function remision(){

   	return $this->belongsToMany('App/remision');
   }
   
}
