<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface; 
use Cviebrock\EloquentSluggable\SluggableInterface;

class articulo extends Model implements SluggableInterface
{
   use SluggableTrait;
   protected $sluggable = [  
   'build_from' => 'titulo',
   "save_to" => 'slug',
   ];
   protected $table="articulos"; 
   protected $fillable= ['titulo','contenido', 'user_id','category_id']; 

   public function categoria()
   {

return $this >belongsTo('App\categoria');
   }

   public function user(){

   	return $this >belongsTo('App\User');
   }

   public function images(){

	return $this >hasMany('App\images');	
   }
   
   public function tags(){

   	return $this >belongsToMany('App\tag');
   }
    //
}
