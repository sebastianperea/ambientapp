<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dispositores extends Model
{
   protected $table ="dispositores";
  protected $fillable= ['nombre','identificacion','dv','categoria'];

}
