<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombres'=> 'required|max:255|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'apellidos' => 'required|max:255',
            'email' => 'required|max:255|email',
            'cargo' => 'required|max:255|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'telefonocelular' => 'required|numeric',
            'nombrecartera' => 'required',
            'telefonofijocartera' => 'required|numeric',
            'telefonomovilcartera' => 'required|numeric',
            'emailcartera' => 'required|email',
            'nombreempresauno' => 'required|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'telefonoempresauno' => 'required|numeric',
            'nombreempresados' => 'required|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'telefonoempresados' => 'required|numeric',
            'nombrebanco' => 'required|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'numerodecuenta' => 'required|numeric',
            'tipodecuenta' => 'required',
            'fotocopiapn' => 'sometimes|required|mimes:jpeg,bmp,png,pdf',
            'rutpn' => 'sometimes|required|mimes:jpeg,bmp,png,pdf',
            'certificadodeexistencia' => 'sometimes|required|mimes:jpeg,bmp,png,pdf',
            'certificadodeexistencia' => 'sometimes|required|mimes:jpeg,bmp,png,pdf',
            'certificacionbancaria' => 'sometimes|required|mimes:jpeg,bmp,png,pdf',
            'declaracionpn' => 'sometimes|mimes:jpeg,bmp,png,pdf',
            'fotorepl' => 'sometimes|required|mimes:jpeg,bmp,png,pdf',
            'rutpj' => 'sometimes|required|mimes:jpeg,bmp,png,pdf',
            'certificadoexistencia' => 'sometimes|required|mimes:jpeg,bmp,png,pdf',
            'certificacionbancariapj' => 'sometimes|required|mimes:jpeg,bmp,png,pdf',
            'declaracionpj' => 'sometimes|mimes:jpeg,bmp,png,pdf',
            'estadospj' => 'sometimes|mimes:jpeg,bmp,png,pdf',
            
        ];
    }
}
