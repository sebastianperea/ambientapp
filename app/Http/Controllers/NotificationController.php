<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notification;
use App\cliente;

class NotificationController extends Controller
{
    public function get(){
        
        $clientes = cliente::all(); 

        foreach ($clientes as $cliente) {
        	$notification = $cliente->unreadNotifications;
        }
    	
		return $notification;
    }


    public function read(Request $request){

    	$clientes = cliente::all(); 

        foreach ($clientes as $cliente) {
        	$notification = $cliente->unreadNotifications()->find($request->id)->markAsRead();
        	return 'succes';
        }

    }
}
