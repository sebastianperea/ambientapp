<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\cliente;
use Laracasts\Flash\Flash;
use App\Http\Requests\UsersRequest;
use App\Http\Requests\UserseditRequest;
use Illuminate\Support\Facades\Hash;
use App\Mail\SendUser as SendUser;
use Illuminate\Support\Facades\Mail;

class UsersController extends Controller
{
  
  public function index (){

  	$users = User::orderBy('id' , 'ASC')->get();
  	return view ('gerenciaadmin.users.index')->with('users', $users);
  }

  public function create(){

  	return view('gerenciaadmin.users.create');

  }


  public function edit($id){

    $users = User::find($id);

    return view('gerenciaadmin.users.edit')
    ->with('users',$users);

  }

  public function editcomercial($identificacion){

    $users = User::where('identificacion',$identificacion)->first();
    return view('comercial.clientes.user.edit')
    ->with('users',$users);

  }

  public function update(UserseditRequest $request, $id){

    $users = User::find($id);
    $users->name = $request->name;
    $users->email = $request->email;
    $users->password = bcrypt($request->password);
    $users->type = $request->type;
    $users->save();

    Flash::success("Se ha Actualizado el usuario ". $users->name . " de forma existosa");
    return back();

  }

  public function updatecomercial(UserseditRequest $request, $id){

    $users = User::find($id);
    $users->name = $request->name;
    $users->email = $request->email;
    $users->password = bcrypt($request->password);
    $users->save();

    Flash::success("Se ha actualizado el usuario ". $users->name . " de forma existosa");
    return back();

  }


  public function store(UsersRequest $request){

  	$user= new User($request->all());
  	$user->password=bcrypt($request->password);
  	$user->save();
  	Flash::success("Se ha Registrado". $user->name . "De forma existosa");
  	return back();

  }
    //


  public function destroy($idusuario){

  	$user = user::find($idusuario);
  	
  	$user->delete();
  	Flash::warning('El usuario ' .$user->name. ' a sido borrado de forma existosa');
  	return back();

  }

  public function crearyenviar($identificacion){

    
    $userif = user::where('identificacion',$identificacion)->first();


    $cliente = cliente::find($identificacion);
    
    
    if ($userif) {
      dd('El usuario ya existe');
    }

    else{
      $password = (str_random(6));
      $user = new User;
      $user->name =str_finish($cliente->nombres ,' '.$cliente->apellidos); 
      $user->email =$cliente->email;
      $user->password = Hash::make($password);
      $user->identificacion = $cliente->identificacion;
      $user->type = 'cliente';
      $user->save();

       Mail::to($cliente->email)
        ->send(new SendUser($user,$cliente,$password));
    }

    Flash::success('El usuario ha sido creado de forma existosa');
    return back();

  }


}
