<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pys;
use Laracasts\Flash\Flash;

class PysController extends Controller
{
    public function index (){

		$pys= pys::orderBy('id','ASC')->get();
                
		    return view ('comercial.pys.index')
		    ->with('pys',$pys);
  } 

  public function create(){

    return view('comercial.pys.create');

  }

  public function store(Request $request){

           $pys= new pys($request->all());
           $pys->save();
            Flash::success('El producto o servicio ah sido creado con exito');
            return back();


             }


 
   public function edit($id){

    $pys= pys::find($id);
  
    return view('comercial.pys.edit')
    ->with('pys',$pys);


    }  


    public function update(Request $request, $id){
          
        $pys= pys::find($id);
        
         $pys->nombre = $request->nombre;
         $pys->precio = $request->precio;
       	 $pys->save();
        Flash::success('El Producto o Servicio ah sido actualizado con exito');
        return back ();

      

    } 
}
