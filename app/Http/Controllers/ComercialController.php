<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\oferta;
use App\ofertapc;
use Laracasts\Flash\Flash;
use Torann\Currency\CurrencyServiceProvider;
use Auth;
use Carbon\Carbon;
use App\visitas;


class ComercialController extends Controller
{
   

public function index (){

    

$ofertasc= DB::table('ofertas')
                
                ->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')
                ->join('users', 'ofertas.user_id', '=', 'users.id')
                ->select('ofertas.*', 'ofertas.id','clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','clientes.email','clientes.tipo', 'users.name')->orderBy('created_at','DSC')->simplePaginate(5);


$cc= DB::table('cc')
                
                ->join('clientes', 'clientes.identificacion', '=', 'cc.identificacion')
                ->select('cc.*', 'cc.id','clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','email','clientes.tipo')->orderBy('created_at','DSC')->simplePaginate(5);                
                

              


    $ofertas= DB::table('ofertas')->count();
    $clientes = DB::table('clientes')->where('Estado', '=', 'Activo')->count();


     $idu = Auth::id();
                $now = Carbon::now();
                $date = $now->format('Y-m-d');
                $hoy = $now->toFormattedDateString();

                
                $seguimientos =   DB::table('visitas')
                
                ->join('clientes', 'visitas.identificacion', '=', 'clientes.identificacion')
                ->select('visitas.*', 'visitas.id','visitas.observaciones','clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','clientes.email','clientes.tipo', 'clientes.telefono_contacto', 'clientes.procesos', 'clientes.identificacion', 'visitas.ESTADO')->where('id_user', '=', $idu)->where('fecha_nuevo_contacto', '=', $date)->where('visitas.ESTADO','=', 'VENCIDO')->get();
                $seguimientosvenc =   DB::table('visitas')
                
                ->join('clientes', 'visitas.identificacion', '=', 'clientes.identificacion')
                ->select('visitas.*', 'visitas.id','visitas.observaciones','clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','clientes.email','clientes.tipo', 'clientes.telefono_contacto', 'clientes.procesos', 'clientes.identificacion', 'visitas.ESTADO')->where('id_user', '=', $idu)->where('fecha_nuevo_contacto', '<', $date)->where('visitas.ESTADO','=', 'VENCIDO')->get();


    return view ('comercial.index')
    ->with('ofertas',$ofertas)
    ->with('clientes',$clientes)
    ->with('cc',$cc)
    ->with('ofertasc',$ofertasc)
    ->with('seguimientos', $seguimientos)
    ->with('hoy', $hoy)
    ->with('seguimientosvenc',$seguimientosvenc);


  }


   public function edit($id){

   	$ofertaspcclientes = DB::table('ofertaspc')
                
                ->join('posiblesclientes', 'ofertaspc.identificacion', '=', 'posiblesclientes.identificacion')
               
                ->select('ofertaspc.*', 'posiblesclientes.razon_social','posiblesclientes.dv','posiblesclientes.departamento','posiblesclientes.ciudad','posiblesclientes.direccion','posiblesclientes.nombres','posiblesclientes.apellidos','posiblesclientes.telefonocelular','email')
                ->where('id', '=', $id)
                ->orderBy('id','ASC')->get();

                $ofertaspcclientes2 = DB::table('ofertapc_peligroso')
                
                ->join('peligrosos', 'peligrosos.id', '=', 'ofertapc_peligroso.peligroso_id')
                ->join('ofertaspc','ofertaspc.id','=','ofertapc_peligroso.ofertapc_id')
                ->select('ofertapc_peligroso.*', 'peligrosos.nombre','peligrosos.precio', 'ofertaspc.transporte', 'ofertaspc.tipo_vehiculo', 'ofertaspc.desde','ofertaspc.hasta','ofertaspc.tipo','ofertaspc.precio_transporte')
                ->where('ofertapc_id', '=', $id)->orderBy('id','ASC')->get();



  // dd($ofertaspcclientes2);




   

    $ofertaspc=ofertapc::find($id);
  
    return view('comercial.ofertapc.edit')
    ->with('ofertaspc',$ofertaspc)
    ->with('ofertaspcclientes',$ofertaspcclientes)
    ->with('ofertaspcclientes2',$ofertaspcclientes2);

    }




    public function update(Request $request, $id){


      $ofertaspcclientes = DB::table('ofertaspc')
                
                ->join('posiblesclientes', 'ofertaspc.identificacion', '=', 'posiblesclientes.identificacion')
                ->select('ofertaspc.*', 'posiblesclientes.razon_social','posiblesclientes.dv','posiblesclientes.departamento','posiblesclientes.ciudad','posiblesclientes.direccion','posiblesclientes.nombres','posiblesclientes.apellidos','posiblesclientes.telefonocelular','email')
                ->where('id','=',$id)
                ->update(['dv'=>$request->dv , 'razon_social' =>$request->razon_social , 'departamento' =>$request->departamento, 'ciudad' =>$request->ciudad, 'direccion' =>$request->direccion, 'nombres' =>$request->nombres, 'apellidos' =>$request->apellidos, 'telefonocelular' =>$request->telefonocelular, 'email' =>$request->email]);
                

             $ofertaspcclientes2 = DB::table('ofertapc_peligroso')
                
                
                ->join('peligrosos', 'peligrosos.id', '=', 'ofertapc_peligroso.peligroso_id')
                ->join('ofertaspc','ofertaspc.id','=','ofertapc_peligroso.ofertapc_id')
                ->select('ofertapc_peligroso.*', 'peligrosos.nombre','peligrosos.precio', 'ofertaspc.transporte', 'ofertaspc.tipo_vehiculo', 'ofertaspc.desde','ofertaspc.hasta','ofertaspc.tipo','ofertaspc.precio')->where('ofertapc_id', '=', $id)
                ->update(['precio' => $request->peligrososprecio, 'precio_transporte'=>$request->precio_transporte, 'Estado_C'=>$request->Estado_C]);
              
              $prueba= ofertapc::orderBy('id','ASC')->paginate(15);

                dd($request->Estado_C);

if ($prueba->Estado_C == 'Aprobado') {

  



}

else{


    
  }
   

        $ofertapc = ofertapc::find($id);
        return redirect()->route('comercial.index');

  }




        public function storeseg(Request $request){

           
              $idu = Auth::id();
                $visitas= new visitas($request->all());
                $visitas->id_user = Auth::id();
                $visitas->ESTADO = 'VENCIDO';
                $visitas->save();

                $visitasant= visitas::find($request->idv);
                $visitasant->ESTADO = 'HECHO';
                $visitasant->save();
         
              return back ();
                                     
                }
   
}
