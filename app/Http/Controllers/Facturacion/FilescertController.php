<?php

namespace App\Http\Controllers\Facturacion;

use App\filescert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

class FilescertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {

        $certificados = $request->file('certificados');
        
        $path = Storage_path() . '/files/certificados/';

        foreach ($certificados as $certificado) {
             $namefpn = 'Ecoindustria_'.time().rand(1,99999).'.'.$certificado->getClientOriginalExtension();
             $certificado->move($path, $namefpn);
            filescert::create(['nombre' => $namefpn, 'categoria' => 'Peligrosos', 'remision_id'=> $id]);
        }
        
        Flash::success(" Se han guardado los archivos de forma existosa");
                   return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\filescert  $filescert
     * @return \Illuminate\Http\Response
     */
    public function show(filescert $filescert)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\filescert  $filescert
     * @return \Illuminate\Http\Response
     */
    public function edit(filescert $filescert)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\filescert  $filescert
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, filescert $filescert)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\filescert  $filescert
     * @return \Illuminate\Http\Response
     */
    public function destroy(filescert $filescert)
    {
        //
    }
}
