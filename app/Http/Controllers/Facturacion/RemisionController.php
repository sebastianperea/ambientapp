<?php

namespace App\Http\Controllers\Facturacion;

use Illuminate\Http\Request;
use App\peligroso;
use App\remision;
use App\posconsumos;
use App\pys;
use App\cliente;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Carbon\Carbon;


class RemisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function certpdf()
    {
        $header =view('pdf.header');
        $footer =view('pdf.footer');
         $pdf = PDF::loadView('facturacion.certificados.cert1')->setPaper('letter')->setOrientation('portrait')->setOption('dpi', 300);

         $pdf->setOption('header-html', $header)->setOption('margin-top',28);
        $pdf->setOption('footer-html', $footer)->setOption('margin-bottom',30)->setOption('margin-left',0);

         return $pdf->stream('OF', array("Attachment" => false));
    }

    public function certpeligrosospdf($id, $identificacion)
    {
      
      $peligrosos = DB::table('peligroso_remision')
                      ->join('peligrosos', 'peligrosos.id', '=', 'peligroso_remision.peligroso_id')
                      ->join('remision', 'remision.id', '=', 'peligroso_remision.remision_id')
                      ->select('peligroso_remision.*', 'peligrosos.nombre', 'peligrosos.id','peligrosos.pretratamiento', 'remision.numero', 'remision.fechaserv')->where('peligroso_remision.remision_id','=', $id)
                      ->get();



      ;
        $clientes = cliente::find($identificacion);
        $remision = remision::find($id);



        $header =view('pdf.header');
        $footer =view('pdf.footer');
         $pdf = PDF::loadView('facturacion.certificados.cert2',compact('peligrosos', $peligrosos, 'clientes', $clientes, 'remision', $remision))->setPaper('letter')->setOrientation('portrait')->setOption('dpi', 300);

         $pdf->setOption('header-html', $header)->setOption('margin-top',28);
        $pdf->setOption('footer-html', $footer)->setOption('margin-bottom',30)->setOption('margin-left',0);

         return $pdf->stream('CF', array("Attachment" => false));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('facturacion.remisiones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      

                    $remision= new remision($request->all()); 
                        $remision->save();

                          if (!empty($request->nombre_)) {

                            
                                $extra = array_map(function($cantidad){
                               return ['cantidad' => $cantidad];
                                }, $request->cantidad_);

                              $data = array_combine($request->id_, $extra);   

                              $remision->peligroso()->sync($data);

                          }

                          if (!empty($request->nombrea_)) {
                        
                             $extra = array_map(function($cantidada, $cantidadu){
                                   return ['cantidada' => $cantidada, 'cantidadu' => $cantidadu ];
                                    }, $request->cantidada_,$request->cantidadu_ );

                                    $data = array_combine($request->ida_, $extra);   

                                    $remision->aprovechables()->sync($data);
                          }

                           if (!empty($request->nombrepo_)) {

                            
                            
                                  $extra = array_map(function( $cantidad){
                                   return ['cantidad' => $cantidad ];
                                    }, $request->cantidadpo_);

                                    $data = array_combine($request->idpo_, $extra);   

                                    $remision->posconsumos()->sync($data);
                          }

                          if (!empty($request->nombrepa_)) {
                                    
                                  
                                     $extra = array_map(function($cantidad, $cantidadpau, $cantidadpag){
                                   return ['cantidad' => $cantidad, 'cantidadpau' => $cantidadpau, 'cantidadpag' => $cantidadpag ];
                                    }, $request->cantidadpa_, $request->cantidadpau_ , $request->cantidadpag_ );

                                    $data = array_combine($request->idpa_, $extra);   

                                    $remision->peligrososa()->sync($data);
                          }

                          if (!empty($request->nombree_)) {
                            
                                            $extra = array_map(function( $cantidad){
                                   return ['cantidad' => $cantidad];
                                    } , $request->cantidade_);

                                    $data = array_combine($request->ide_, $extra);   


                                    $remision->especiales()->sync($data);

                          }

                          if (!empty($request->tipo_)) {
                            
                                            $extra = array_map(function( $cantidadt){
                                   return ['cantidadt' => $cantidadt];
                                    } , $request->cantidadt_);

                                    $data = array_combine($request->idt_, $extra);   

                                    $remision->transporte()->sync($data);

                          }

                          if (!empty($request->nombrepys_)) {
                            
                                    $extra = array_map(function($cantidad){
                               return ['cantidad' => $cantidad];
                                }, $request->cantidadpys_);

                              $data = array_combine($request->idpys_, $extra);   

                              $remision->pys()->sync($data);

                          }
                          
                          Flash::success('La remisión '.$remision->id.'-'. $remision->numero. ' ha sido creada con exito');


                    $cliente = cliente::find($remision->identificacion);

                    $peligrosos = DB::table('peligroso_remision')
                      ->join('peligrosos', 'peligrosos.id', '=', 'peligroso_remision.peligroso_id')
                      ->join('remision', 'remision.id', '=', 'peligroso_remision.remision_id')
                      ->select('peligroso_remision.*', 'peligrosos.nombre', 'peligrosos.id', 'remision.numero')->where('peligroso_remision.remision_id','=', $remision->id)
                      ->get();  

                      $aprovechables = DB::table('aprovechables_remision')
                      ->join('aprovechables', 'aprovechables.id', '=', 'aprovechables_remision.aprovechables_id')
                      ->join('remision', 'remision.id', '=', 'aprovechables_remision.remision_id')
                      ->select('aprovechables_remision.*', 'aprovechables.nombre', 'aprovechables.id', 'remision.numero')->where('aprovechables_remision.remision_id','=', $remision->id)
                      ->get(); 

                      $posconsumos = DB::table('posconsumos_remision')
                      ->join('posconsumos', 'posconsumos.id', '=', 'posconsumos_remision.posconsumos_id')
                      ->join('remision', 'remision.id', '=', 'posconsumos_remision.remision_id')
                      ->select('posconsumos_remision.*', 'posconsumos.nombre', 'posconsumos.id', 'remision.numero')->where('posconsumos_remision.remision_id','=', $remision->id)
                      ->get();  

                      $peligrososa = DB::table('peligrososa_remision')
                      ->join('peligrososa', 'peligrososa.id', '=', 'peligrososa_remision.peligrososa_id')
                      ->join('remision', 'remision.id', '=', 'peligrososa_remision.remision_id')
                      ->select('peligrososa_remision.*', 'peligrososa.nombre', 'peligrososa.id', 'remision.numero')->where('peligrososa_remision.remision_id','=', $remision->id)
                      ->get(); 

                      $especiales = DB::table('especiales_remision')
                      ->join('especiales', 'especiales.id', '=', 'especiales_remision.especiales_id')
                      ->join('remision', 'remision.id', '=', 'especiales_remision.remision_id')
                      ->select('especiales_remision.*', 'especiales.nombre', 'especiales.id', 'remision.numero')->where('especiales_remision.remision_id','=', $remision->id)
                      ->get(); 

                      $transporte = DB::table('remision_transporte')
                      ->join('transporte', 'transporte.id', '=', 'remision_transporte.transporte_id')
                      ->join('remision', 'remision.id', '=', 'remision_transporte.remision_id')
                      ->select('remision_transporte.*', 'transporte.tipo', 'transporte.id', 'remision.numero')->where('remision_transporte.remision_id','=', $remision->id)
                      ->get();

                        $pys = DB::table('pys_remision')
                      ->join('pys', 'pys.id', '=', 'pys_remision.pys_id')
                      ->join('remision', 'remision.id', '=', 'pys_remision.remision_id')
                      ->select('pys_remision.*', 'pys.nombre', 'pys.id', 'remision.numero')->where('pys_remision.remision_id','=', $remision->id)
                      ->get();

            
        return view('facturacion.remisiones.view')
        ->with('remision', $remision)
        ->with('cliente', $cliente)
        ->with('peligrosos', $peligrosos)
        ->with('aprovechables', $aprovechables)
        ->with('posconsumos', $posconsumos)
        ->with('peligrososa', $peligrososa)
        ->with('especiales', $especiales)
        ->with('transporte', $transporte)
        ->with('pys', $pys)
        ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function autocompleteser(Request $request)
    {
        $term=$request->term;
                          $data = peligroso::where('nombre','LIKE','%'.$term.'%')
                          
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['id'=>$v->id,'value'=>$v->nombre];
                          }
                            return response()->json($results);
    }

    public function autocompleter(Request $request){
               
                          $term=$request->term;
                          $data = cliente::where('razon_social','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['value'=>$v->razon_social,'identificacion' => $v->identificacion, 'tipoid' => $v->tipo_de_identificacion,'dv'=>$v->dv , 'departamento'=>$v->departamento, 'ciudad' => $v->ciudad, 'direccion' => $v->direccion,'tipo_de_ide' => $v->tipo_de_ide, 'ide' => $v->ide,'cargo' => $v->cargo ,'procesos' => $v->procesos,'tipodepersona' => $v->tipodepersona,'nombre' =>$v->nombres, 'apellidos'=> $v->apellidos, 'telefonocelular'=> $v->telefonocelular, 'email'=> $v->email, 'telefono_contacto' => $v->telefono_contacto, 'ext' => $v->ext, 'ejdecuenta' => $v->ejdecuenta, 'canal' => $v->canal];
                          }
                            return response()->json($results);
                        }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function show()
    {
        return view('facturacion.remisiones.view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
