<?php

namespace App\Http\Controllers;
use App\visitas;
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GeradminReportesController extends Controller
{
    public function index (Request $request){

    	$idu = Auth::id();
                $now = Carbon::now();
                $date = $now->format('Y-m-d');
                $hoy = $now->toFormattedDateString();

    	$seguimientosvenc = visitas::orderby('id', 'ASC')->where('ESTADO', '=', 'VENCIDO')->where('fecha_nuevo_contacto', '<', $date )->where('fecha_nuevo_contacto', '>', '0000-00-00' )->count();

    	$seguimientoshc = visitas::orderby('id', 'ASC')->where('ESTADO', '=', 'HECHO')->count();
    	$seguimientost = visitas::orderby('id', 'ASC')->count();

    	
     
  return view('gerenciaadmin.reportes.index')
  ->with('seguimientosvenc', $seguimientosvenc)
  ->with('seguimientoshc', $seguimientoshc)
  ->with('seguimientost', $seguimientost);
	}


	public function show(Request $request){

    	$procesos =  DB::table('visitas')
                
                ->join('clientes', 'visitas.identificacion', '=', 'clientes.identificacion')
                ->join('users', 'visitas.id_user', '=', 'users.id')
                ->select('visitas.*', 'visitas.id','clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','clientes.email','clientes.tipo', 'users.name','clientes.telefono_contacto')->orderBy('created_at','DSC')->get();

                $seguimientos = DB::table('visitas')
                
                ->join('clientes', 'clientes.identificacion', '=', 'visitas.identificacion')
                ->join('users', 'users.id','=','visitas.id_user')

                ->select('visitas.*', 'visitas.observaciones','users.name', 'clientes.razon_social', 'clientes.procesos','clientes.email', 'clientes.telefono_contacto','clientes.procesos')

                ->orderBy('id','ASC')->simplePaginate(5);


                

                

  return view('gerenciaadmin.reportes.show')
  ->with('procesos', $procesos)
  ->with('seguimientos', $seguimientos)
                  ;
	}

	
}
