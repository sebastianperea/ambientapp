<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\especiales;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;

class Especialescontroller extends Controller
{
  public function index (){



		$especiales= especiales::orderBy('id','ASC')->get();
                

  	
		    return view ('comercial.residuos.especiales.index')
		    ->with('especiales',$especiales);
   

  }



public function create(){

    return view('comercial.residuos.especiales.create');

  }



  public function store(Request $request){
   
           $especiales= new especiales($request->all());
          
           $especiales->save();


            Flash::success('El residuo Peligroso Aprovechable ah sido creado con exito');
            return back();

             }


 
    

             
   public function edit($id){

    $especiales= especiales::find($id);
  
    return view('comercial.residuos.especiales.edit')
    ->with('especiales',$especiales);

      
      

    }  


    public function update(Request $request, $id){
          
        $especiales= especiales::find($id);
        
         $especiales->nombre = $request->nombre;
         $especiales->precio = $request->precio;
         $especiales->pretratamiento = $request->pretratamiento;
        $especiales->save();
        Flash::success('El residuo Especial ah sido actualizado con exito');
        return back ();

      

    } //
}
