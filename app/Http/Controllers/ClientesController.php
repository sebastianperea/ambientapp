<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\cliente;
use App\oferta;
use App\fotocopiacpn;
use App\peligrosos;
use App\rutpj;
use App\rutpn;
use App\cbancariapn;
use App\cmpn;
use App\cerlpj;
use App\user;
use App\cbancariapj;
use App\drentapj;
use App\fcreacion;
use App\fresiduos;
use App\drentapn;
use App\estadosfinancieros;
use App\sedes;
use App\visitas;
use App\inforesiduos;
use App\fotocopiacpj;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


use Laracasts\Flash\Flash;

class ClientesController extends Controller
{
 
public function index (Request $request){

      // $ofertasc= DB::table('oferta_peligroso')
                
      //           ->join('ofertas', 'ofertas.id', '=', 'oferta_peligroso.oferta_id')
      //           ->join('peligrosos', 'oferta_peligroso.peligroso_id','=','peligrosos.id')
      //           ->select('oferta_peligroso.*', 'ofertas.id')->orderBy('created_at','DSC')->get();

      //           dd($ofertasc);
        
        $clientes= cliente::orderby('identificacion', 'ASC')->get();
  	  	return view ('comercial.clientes.index')->with('clientes', $clientes);

  }

  public function creacion(Request $request, $identificacion){
  

    $clientes= cliente::find($identificacion);
    $fotocopiacpj = fotocopiacpj::where('cliente_id','=', $identificacion)->first();
    $fotocopiacpn = fotocopiacpn::where('cliente_id','=', $identificacion)->first();
    $certificadodeexistencia = cmpn::where('cliente_id','=', $identificacion)->first();
    $rutpj = rutpj::where('cliente_id','=', $identificacion)->first();
    $rutpn = rutpn::where('cliente_id','=', $identificacion)->first();
    $cerlpj = cerlpj::where('cliente_id','=', $identificacion)->first();
    $cbancariapj = cbancariapj::where('cliente_id','=', $identificacion)->first();
    $declaracionpn = drentapn::where('cliente_id','=', $identificacion)->first();
   
    $certificacionbancaria = cbancariapn::where('cliente_id','=', $identificacion)->first();
    $drentapj = drentapj::where('cliente_id','=', $identificacion)->first();
    $estadosfinancieros = estadosfinancieros::where('cliente_id','=', $identificacion)->first();
    $info_residuos = fresiduos::where('identificacion','=', $identificacion)->first();
    $cc = fcreacion::where('identificacion','=', $identificacion)->first();
        $fecha = Carbon::now()->format('d-m-Y');

    $ofi  = oferta::with('peligroso','peligrososa','especiales')->where('identificacion','=',$identificacion)->get();
 
 
    $peligroso = array_pluck($ofi, 'peligroso');
    $peligrososa = array_pluck($ofi, 'peligrososa');
   $especiales = array_pluck($ofi,'especiales');
 
    return view('comercial.clientes.creacion')
    ->with('clientes', $clientes)
    ->with('peligroso', $peligroso)
    ->with('peligrososa', $peligrososa)
    ->with('especiales', $especiales)
    ->with('fotocopiacpj', $fotocopiacpj)
    ->with('rutpj', $rutpj)
    ->with('cerlpj', $cerlpj)
    ->with('cbancariapj', $cbancariapj)
    ->with('drentapj', $drentapj)
    ->with('estadosfinancieros', $estadosfinancieros)
    ->with('cc', $cc)
    ->with('info_residuos', $info_residuos)
    ->with('fotocopiacpn', $fotocopiacpn)
    ->with('rutpn', $rutpn)
    ->with('certificadodeexistencia', $certificadodeexistencia)
    ->with('certificacionbancaria', $certificacionbancaria)
    ->with('declaracionpn', $declaracionpn);
                                     

  }

  public function create(Request $request){
  

    $clientes=cliente::search($request->identificacion)->orderBy('identificacion' , 'ASC')->get();
 
    return view('admin.clientes.clientes')->with('clientes',$clientes);
                                     

  }

      public function edit($identificacion){

        $clientes= cliente::find($identificacion);
        return view ('comercial.clientes.edit')->with('clientes', $clientes);
    }


     public function update(Request $request, $identificacion){
          
        $clientes= cliente::find($identificacion);
         $clientes->dv = $request->dv;
         $clientes->razon_social = $request->razon_social;
         $clientes->departamento = $request->departamento;
         $clientes->ciudad = $request->ciudad;
         $clientes->direccion = $request->direccion;
         $clientes->telefono_contacto = $request->telefono_contacto;
         $clientes->ext = $request->ext;
         $clientes->nombres = $request->nombres;
         $clientes->apellidos = $request->apellidos;
         $clientes->telefonocelular = $request->telefonocelular;
         $clientes->email = $request->email;
         $clientes->cargo = $request->cargo;
         $clientes->ide = $request->ide;
        $clientes->save();

        Flash::success("Se ha editado el cliente ". $clientes->razon_social. " de forma existosa");
        return back ();

      

    }


  public function destroy($identificacion){

  	// $clientes = cliente::find($identificacion);

  	// $clientes->delete();
  	// Flash::warning('El Cliente' .$cliente->identificacion. ' ah sido borrado de forma existosa');
  	// return redirect()->route('admin.clientes.clientes');

  }

  public function store(Request $request){

    dd($request->all());

   $clientes= new cliente($request->all());
    
    $clientes->save();

  	

Flash::success("Se ha Registrado el cliente ". $clientes->razon_social. " De forma existosa");
    return redirect()->route('comercial.clientes.index');
    
  	
  }

   public function borrarsede(Request $request, $sedes){

              $sede = sedes::find($sedes);
              $sede->delete();         

            Flash::success("Se ha Borrado la sede ". $sedes. " De forma existosa");
                return back();
    
  }


  public function save(Request $request){

        $sedes= new sedes($request->all());
    
        $sedes->save();

      //   $num_elements = 0;
      //           $sqlData = []; 
      //           $input= $request->all();
      //                 while($num_elements < count($input['nombre_'])){
      //                   $sqlData[] = array(
      //                       'identificacion' => $identificacion,
      //                        'nombre'         => $input['nombre_'][$num_elements],
      //                       'direccion'          => $input['direccion_'][$num_elements],
      //                       'departamento'     => $input['departamento_'][$num_elements],
      //                       'ciudad'      => $input['ciudad_'][$num_elements],
      //                       'created_at'    => Carbon::now(), // only if your table has this column
      //                       'updated_at'    => Carbon::now(), // only if your table has this column
      //                   );
      //                 $num_elements++;
      //            }

      // sedes::insert($sqlData);      

            Flash::success("Se ha agregado la sede de forma existosa");
                return back();
    
    
  }


  public function editsede($id,$identificacion){
      
      $sede = sedes::find($id);

      return view('comercial.clientes.sedes.edit')->with('sede', $sede);
    
  }

  public function updatesede(Request $request, $id, $identificacion){
      

      
      $sede = sedes::find($id);
      $sede->identificacion = $identificacion;
      $sede->nombre = $request->Nombre;
      $sede->direccion = $request->direccion;
      $sede->departamento = $request->Departamento;
      $sede->ciudad = $request->Ciudad;
      $sede->responsable = $request->responsable;
      $sede->email = $request->Email;
      $sede->emailop = $request->Emailop;
      $sede->telefono = $request->telefono;
      $sede->cargo = $request->cargo;

      $sede->save();

       Flash::success("Se ha editado la sede ". $sede->id. " De forma existosa");

      return redirect()->route('clientes.show', $identificacion);
      
  }




   public function show(Request $request, $identificacion){

              $userifexist = user::where('identificacion',$identificacion)->first();

              
              $user = user::where('identificacion',$identificacion)->first();
              
              $now = Carbon::now();
                $date = $now->format('Y-m-d');
                $hoy = $now->toFormattedDateString();

                $clientes = cliente::find($identificacion);

                  $ofertas = DB::table('ofertas')
                
                ->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')
               
                ->select('ofertas.*','ofertas.id','ofertas.estado','ofertas.created_at')
                ->where('ofertas.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $cbancariapj = DB::table('cbancariapj')
                
                ->join('clientes', 'cbancariapj.cliente_id', '=', 'clientes.identificacion')
               
                ->select('cbancariapj.*','cbancariapj.id','cbancariapj.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $cerlpj = DB::table('cerlpj')
                
                ->join('clientes', 'cerlpj.cliente_id', '=', 'clientes.identificacion')
               
                ->select('cerlpj.*','cerlpj.id','cerlpj.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();


                $fotocopiacpj = DB::table('fotocopiacpj')
                
                ->join('clientes', 'fotocopiacpj.cliente_id', '=', 'clientes.identificacion')
               
                ->select('fotocopiacpj.*','fotocopiacpj.id','fotocopiacpj.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $rutpj = DB::table('rutpj')
                
                ->join('clientes', 'rutpj.cliente_id', '=', 'clientes.identificacion')
               
                ->select('rutpj.*','rutpj.id','rutpj.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $drentapj = DB::table('drentapj')
                
                ->join('clientes', 'drentapj.cliente_id', '=', 'clientes.identificacion')
               
                ->select('drentapj.*','drentapj.id','drentapj.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $estadosfinancieros = DB::table('estadosfinancieros')
                
                ->join('clientes', 'estadosfinancieros.cliente_id', '=', 'clientes.identificacion')
               
                ->select('estadosfinancieros.*','estadosfinancieros.id','estadosfinancieros.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $cbancariapn = DB::table('cbancariapn')
                
                ->join('clientes', 'cbancariapn.cliente_id', '=', 'clientes.identificacion')
               
                ->select('cbancariapn.*','cbancariapn.id','cbancariapn.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();


                $cmpn = DB::table('cmpn')
                
                ->join('clientes', 'cmpn.cliente_id', '=', 'clientes.identificacion')
               
                ->select('cmpn.*','cmpn.id','cmpn.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $drentapn = DB::table('drentapn')
                
                ->join('clientes', 'drentapn.cliente_id', '=', 'clientes.identificacion')
               
                ->select('drentapn.*','drentapn.id','drentapn.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

               $fotocopiacpn = DB::table('fotocopiacpn')
                
                ->join('clientes', 'fotocopiacpn.cliente_id', '=', 'clientes.identificacion')
               
                ->select('fotocopiacpn.*','fotocopiacpn.id','fotocopiacpn.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $rutpn = DB::table('rutpn')
                
                ->join('clientes', 'rutpn.cliente_id', '=', 'clientes.identificacion')
               
                ->select('rutpn.*','rutpn.id','rutpn.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                 $cc = DB::table('cc')
                
                ->join('clientes', 'cc.identificacion', '=', 'clientes.identificacion')
               
                ->select('cc.*','cc.id','cc.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();
                
                $inforesiduos = DB::table('info_residuos')
                
                ->join('clientes', 'info_residuos.identificacion', '=', 'clientes.identificacion')
               
                ->select('info_residuos.*','info_residuos.id','info_residuos.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

               $filespeligrosos= DB::table('filespeligrosos_residuos')
                
                ->join('filespeligrosos', 'filespeligrosos.id', '=', 'filespeligrosos_residuos.filespeligrosos_id')
                ->join('residuos', 'residuos.id', '=', 'filespeligrosos_residuos.residuos_id')
                
                ->select('filespeligrosos_residuos.*', 'filespeligrosos.nombre')

                ->where('residuos.identificacion', '=', $identificacion)->orderBy('id','ASC')->get();

                $filespeligrososa= DB::table('filespeligrososa_residuos')
                
                ->join('filespeligrososa', 'filespeligrososa.id', '=', 'filespeligrososa_residuos.filespeligrososa_id')
                ->join('residuos', 'residuos.id', '=', 'filespeligrososa_residuos.residuos_id')
                
                ->select('filespeligrososa_residuos.*', 'filespeligrososa.nombre')

                ->where('residuos.identificacion', '=', $identificacion)->orderBy('id','ASC')->get();

                $especiales3= DB::table('filese_residuos')
                
                ->join('filese', 'filese.id', '=', 'filese_residuos.filese_id')
                ->join('residuos', 'residuos.id', '=', 'filese_residuos.residuos_id')
                
                ->select('filese_residuos.*', 'filese.nombre')

                ->where('residuos.identificacion', '=', $identificacion)->orderBy('id','ASC')->get();

                $sedes= DB::table('sedes')
                
                ->join('clientes', 'clientes.identificacion', '=', 'sedes.identificacion')
                
                ->select('sedes.*')

                ->where('sedes.identificacion', '=', $identificacion)->orderBy('id','ASC')->get();

                $seguimientos = DB::table('visitas')
                
                ->join('clientes', 'clientes.identificacion', '=', 'visitas.identificacion')
                ->join('users', 'users.id','=','visitas.id_user')

                ->select('visitas.*', 'visitas.observaciones','users.name', 'clientes.razon_social', 'clientes.procesos','clientes.email', 'clientes.telefono_contacto','clientes.procesos')

                ->where('visitas.identificacion', '=', $identificacion)->orderBy('id','ASC')->simplePaginate(5);


                $seguimientoscountp = DB::table('visitas')->where('visitas.identificacion', '=', $identificacion)->where('canald','=','PRESENCIAL')->count();

                $seguimientoscountpw = DB::table('visitas')->where('visitas.identificacion', '=', $identificacion)->where('canald','=','PAG WEB')->count();
                $seguimientocountee = DB::table('visitas')->where('visitas.identificacion', '=', $identificacion)->where('canald','=','EMAIL ENVIADO')->count();
                $seguimientocounter = DB::table('visitas')->where('visitas.identificacion', '=', $identificacion)->where('canald','=','EMAIL RECIBIDO')->count();
                $seguimientocountle = DB::table('visitas')->where('visitas.identificacion', '=', $identificacion)->where('canald','=','LLAMADA ENTRANTE')->count();
                $seguimientocountls = DB::table('visitas')->where('visitas.identificacion', '=', $identificacion)->where('canald','=','LLAMADA SALIENTE')->count();
                $seguimientocountsecop = DB::table('visitas')->where('visitas.identificacion', '=', $identificacion)->where('canald','=','SECOP')->count();
                $seguimientocountvenc = DB::table('visitas')->where('visitas.identificacion', '=', $identificacion)->where('ESTADO','=','VENCIDO')->where('fecha_nuevo_contacto', '<', $date)->count();

                


                

               
                  return view ('comercial.clientes.view')
                  ->with('clientes',$clientes)
                  ->with('ofertas', $ofertas)
                  ->with('cbancariapj', $cbancariapj)
                  ->with('cerlpj', $cerlpj)
                  ->with('fotocopiacpj', $fotocopiacpj)
                  ->with('rutpj', $rutpj)
                  ->with('drentapj', $drentapj)
                  ->with('estadosfinancieros', $estadosfinancieros)
                  ->with('cbancariapn', $cbancariapn)
                  ->with('cmpn', $cmpn)
                  ->with('drentapn', $drentapn)
                  ->with('fotocopiacpn', $fotocopiacpn)
                  ->with('cc',$cc)
                  ->with('inforesiduos',$inforesiduos)
                  ->with('filespeligrososa', $filespeligrososa)
                  ->with('filespeligrosos', $filespeligrosos)
                  ->with('especiales3', $especiales3)
                  ->with('sedes',$sedes)
                  ->with('rutpn', $rutpn)
                  ->with('seguimientos', $seguimientos)
                  ->with('seguimientoscountp', $seguimientoscountp)
                  ->with('seguimientoscountpw', $seguimientoscountpw)
                  ->with('seguimientocountee',$seguimientocountee )
                  ->with('seguimientocounter',$seguimientocounter )
                  ->with('seguimientocountle',$seguimientocountle )
                  ->with('seguimientocountls',$seguimientocountls )
                  ->with('seguimientocountsecop',$seguimientocountsecop )
                  ->with('seguimientocountvenc',$seguimientocountvenc )
                  ->with('user',$user)
                  ->with('userifexist', $userifexist);

          // Flash::success("Se ha Registrado el cliente ". $clientes->razon_social. " De forma existosa");
          //     return redirect()->route('comercial.clientes.index');
              
              
            }


           
        public function up(Request $request, $identificacion){
          
            // dd($request->all());
            
          $cliente = cliente::find($identificacion);
          $cliente->tipo = $request->tipo;
          $cliente->nombres = $request->nombres;
          $cliente->apellidos = $request->apellidos;
          $cliente->email = $request->email;
          $cliente->cargo = $request->cargo;
          $cliente->direccion = $request->direccion;
          $cliente->telefonocelular = $request->telefonocelular;
          $cliente->nombrecartera = $request->nombrecartera;
          $cliente->telefonofijocartera =$request->telefonofijocartera;
          $cliente->extcartera =$request->extcartera;
          $cliente->telefonomovilcartera =$request->telefonomovilcartera;
          $cliente->emailcartera =$request->emailcartera;
          $cliente->direccionradicacion =$request->direccionradicacion;
          $cliente->nombreempresauno =$request->nombreempresauno;
          $cliente->telefonoempresauno =$request->telefonoempresauno;
          $cliente->nombreempresados =$request->nombreempresados;
          $cliente->telefonoempresados = $request->telefonoempresados;
          $cliente->nombrebanco = $request->nombrebanco;
          $cliente->numerodecuenta = $request->numerodecuenta;
          $cliente->tipodecuenta = $request->tipodecuenta;
          $cliente->tipodepersona= $request->tipodepersona;
          $cliente->cargo= $request->cargo;
          $cliente->estado = 'Activo';
          $cliente->tipo= 'Cliente';

          
          $path = Storage_path() . '/files/comercial/';


            if($request->tipodepersona == 'natural'){
               $cliente->credito= $request->creditopn;
             }

              elseif ($request->tipodepersona == 'juridica') {
               $cliente->credito= $request->creditopj;
              }



              if ($request->tipodepersona == 'natural') {

                
                if (!empty($request->file('fotocopiacpn'))) {
                  
                   $fotocopiacpn = $request->file('fotocopiacpn');

                    $namefpn = 'Ecoindustria_'.time().rand(1,99999).'.'.$fotocopiacpn->getClientOriginalExtension();
                    $path = Storage_path() . '/files/comercial/';

                    $fotocopiacpn->move($path, $namefpn);
                    $fotopn = new fotocopiacpn();
                    $fotopn->nombre = $namefpn;
                    $fotopn->cliente()->associate($cliente);
                    $fotopn->save();
              }

              if (!empty($request->file('rutpn'))) {   

                        $rutpn = $request->file('rutpn');
                        $namerpn = 'Ecoindustria_'.time().rand(1,99999).'.'.$rutpn->getClientOriginalExtension();
                        $rutpn->move($path, $namerpn);
                        $rtpn = new rutpn();
                        $rtpn->nombre = $namerpn;
                        $rtpn->cliente()->associate($cliente);
                        $rtpn->save();
                    }

               if (!empty($request->file('certificacionbancaria'))){

                    $certificacionbancaria = $request->file('certificacionbancaria');
                    $namecbnpn = 'Ecoindustria_'.time().rand(1,99999).'.'.$certificacionbancaria->getClientOriginalExtension();
                    $certificacionbancaria->move($path, $namecbnpn);
                    $cbancariapn = new cbancariapn();
                    $cbancariapn->nombre = $namecbnpn;
                    $cbancariapn->cliente()->associate($cliente);
                    $cbancariapn->save();
                  }
              

              if (!empty($request->file('certificadodeexistencia'))) {


                $certificadodeexistencia = $request->file('certificadodeexistencia');
                    $namecdepn = 'Ecoindustria_'.time().rand(1,99999).'.'.$certificadodeexistencia->getClientOriginalExtension();
                    $certificadodeexistencia->move($path, $namecdepn);
                    $cex = new cmpn();
                    $cex->nombre = $namecdepn;
                    $cex->cliente()->associate($cliente);
                    $cex->save();
                   }

              if ($request->creditopn == 'si') {
                 if (!empty($request->file('declaracionpn'))) {
                 $declaracionpn = $request->file('declaracionpn');
                    $namedpn = 'Ecoindustria_'.time().rand(1,99999).'.'.$declaracionpn->getClientOriginalExtension();
                    $declaracionpn->move($path, $namedpn);
                    $drentapn= new drentapn();
                    $drentapn->nombre = $namedpn;
                    $drentapn->cliente()->associate($cliente);
                    $drentapn->save();
                    

             }

         }
        }

        elseif ($request->tipodepersona == 'juridica') {

              
               if (!empty($request->file('fotorepl'))) {

                   $fotorepl = $request->file('fotorepl');
                    $namefpj = 'Ecoindustria_'.time().rand(1,99999).'.'.$fotorepl->getClientOriginalExtension();
                    $path = Storage_path() . '/files/comercial/';
                    $fotorepl->move($path, $namefpj);
                    $fotocopiacpj= new fotocopiacpj();
                    $fotocopiacpj->nombre = $namefpj;
                    $fotocopiacpj->cliente()->associate($cliente);
                    $fotocopiacpj->save();
                   }
               
                   if (!empty($request->file('rutpj'))) {

                    $rutpj = $request->file('rutpj');
                    $namerpj = 'Ecoindustria_'.time().rand(1,99999).'.'.$rutpj->getClientOriginalExtension();
                    $rutpj->move($path, $namerpj);
                    $pjrut= new rutpj();
                    $pjrut->nombre = $namerpj;
                    $pjrut->cliente()->associate($cliente);
                    $pjrut->save();

                  }
                        
                   if (!empty($request->file('certificadoexistencia'))) {
                         
                    $certificadoexistencia = $request->file('certificadoexistencia');
                    $namecdepj = 'Ecoindustria_'.time().rand(1,99999).'.'.$certificadoexistencia->getClientOriginalExtension();
                    $certificadoexistencia->move($path, $namecdepj);
                    $cerlpj= new cerlpj();
                    $cerlpj->nombre = $namecdepj;
                    $cerlpj->cliente()->associate($cliente);
                    $cerlpj->save();
                    }

                    if (!empty($request->file('certificacionbancariapj'))) {

                    $certificacionbancariapj = $request->file('certificacionbancariapj');
                    $namecbnpj = 'Ecoindustria_'.time().rand(1,99999).'.'.$certificacionbancariapj->getClientOriginalExtension();
                    $certificacionbancariapj->move($path, $namecbnpj);
                    $cbancariapj= new cbancariapj();
                    $cbancariapj->nombre = $namecbnpj;
                    $cbancariapj->cliente()->associate($cliente);
                    $cbancariapj->save();

                   }


                  
                   if ($request->creditopj == 'si') {


                    if (!empty($request->file('declaracionpj'))) {

                 $declaracionpj = $request->file('declaracionpj');
                    $namedpj = 'Ecoindustria_'.time().rand(1,99999).'.'.$declaracionpj->getClientOriginalExtension();
                    $declaracionpj->move($path, $namedpj);
                    $drentapj= new drentapj();
                    $drentapj->nombre = $namedpj;
                    $drentapj->cliente()->associate($cliente);
                    $drentapj->save();

                    }

                     if (!empty($request->file('estadospj'))) {

                    $estadospj = $request->file('estadospj');
                    $nameestadospj = 'Ecoindustria_'.time().rand(1,99999).'.'.$estadospj->getClientOriginalExtension();
                    $estadospj->move($path, $nameestadospj);
                    $estadopj= new estadosfinancieros();
                    $estadopj->nombre = $nameestadospj;
                    $estadopj->cliente()->associate($cliente);
                    $estadopj->save();
                    
                  }
                  
                }
                  
              }


              if (!empty($request->file('creaciondeclientes')) && !empty($request->file('inforesiduos'))>0 ) {
        
       
    if (!empty($request->file('creaciondeclientes'))) {
      $fcreacion = $request->file('creaciondeclientes');

                    $namefcreacion = 'Ecoindustria_'.time().rand(1,99999).'.'.$fcreacion->getClientOriginalExtension();
                    $path = Storage_path() . '/files/comercial/';

                    $fcreacion->move($path, $namefcreacion);
                    $fcreacion = new fcreacion();
                    $fcreacion->nombre = $namefcreacion;
                    $fcreacion->cliente()->associate($cliente);
                    $fcreacion->save();
    }


    if (!empty($request->file('inforesiduos'))) {
      $fresiduos = $request->file('inforesiduos');

                    $namefresiduos= 'Ecoindustria_'.time().rand(1,99999).'.'.$fresiduos->getClientOriginalExtension();
                    $path = Storage_path() . '/files/comercial/';

                    $fresiduos->move($path, $namefresiduos);
                    $fresiduos= new fresiduos();
                    $fresiduos->nombre = $namefresiduos;
                    $fresiduos->cliente()->associate($cliente);
                    $fresiduos->save();
    }
  Flash::success(" Se han guardado los archivos de forma existosa");
                   return back();
  } 
   
  

     if (!empty($request->file('creaciondeclientes'))>0 || !empty($request->file('inforesiduos'))>0 ) {

              if (!empty($request->file('creaciondeclientes'))>0) {
                $fcreacion = $request->file('creaciondeclientes');

                              $namefcreacion = 'Ecoindustria_'.time().rand(1,99999).'.'.$fcreacion->getClientOriginalExtension();
                              $path = Storage_path() . '/files/comercial/';

                              $fcreacion->move($path, $namefcreacion);
                              $fcreacion = new fcreacion();
                              $fcreacion->nombre = $namefcreacion;
                              $fcreacion->cliente()->associate($cliente);
                              $fcreacion->save();
              }


              if (!empty($request->file('inforesiduos'))>0) {
                $fresiduos = $request->file('inforesiduos');

                              $namefresiduos= 'Ecoindustria_'.time().rand(1,99999).'.'.$fresiduos->getClientOriginalExtension();
                              $path = Storage_path() . '/files/comercial/';

                              $fresiduos->move($path, $namefresiduos);
                              $fresiduos= new fresiduos();
                              $fresiduos->nombre = $namefresiduos;
                              $fresiduos->cliente()->associate($cliente);
                              $fresiduos->save();
              }
            }

  
          $cliente->save();   

          Flash::success("Se ha guardado de forma exitosa");
          return back(); 
        }
    //
}
