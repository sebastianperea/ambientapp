<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\oferta;
use App\ofertapc;
use Laracasts\Flash\Flash;


class GeradminController extends Controller
{
    public function index (){

    

$ofertasc= DB::table('ofertas')
                
                ->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')
                ->select('ofertas.*', 'ofertas.id','clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','email','clientes.tipo')->orderBy('created_at','DSC')->simplePaginate(10);


$cc= DB::table('cc')
                
                ->join('clientes', 'clientes.identificacion', '=', 'cc.identificacion')
                ->select('cc.*', 'cc.id','clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','email','clientes.tipo')->orderBy('created_at','DSC')->simplePaginate(10);                
                

              


    $ofertas= DB::table('ofertas')->count();
    $clientes = DB::table('clientes')->count();


    
    return view ('gerenciaadmin.index')
    ->with('ofertas',$ofertas)
    ->with('clientes',$clientes)
    ->with('cc',$cc)
    ->with('ofertasc',$ofertasc);


  }
}
