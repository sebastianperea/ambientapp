<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\generadores;  

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\oferta;
use App\transporte;
use App\peligroso;
use App\aprovechables;
use App\posconsumos;
use App\peligrososa;
use App\especiales;
use App\otros;
use App\cliente;

use App\pys;
use App\User;
use App\sedes;
use App\Notifications\NotifyOfertas;
use Laracasts\Flash\Flash;
use UxWeb\SweetAlert\SweetAlert;
use Auth;

class Ofertascontroller extends Controller
{


   public function index (){

  	$ofertasc= DB::table('ofertas')
                
                ->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')
                ->join('users', 'ofertas.user_id', '=', 'users.id')
                ->select('ofertas.*', 'ofertas.id','clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','clientes.email','clientes.tipo', 'users.name')->orderBy('created_at','DSC')->get();
    return view ('comercial.oferta.index')->with('ofertasc',$ofertasc);

  }


      public function create(Request $request){



          $idu = Auth::id();
          $clientes = cliente::orderby('identificacion', 'ASC')->get();
          $peligrosos= peligroso::orderby('id', 'ASC')->get();
          $aprovechables= aprovechables::orderby('id','ASC')->get();
          $posconsumos= posconsumos::orderby('id','ASC')->get();
          $peligrososa= peligrososa::orderby('id', 'ASC')->get();
          $especiales= especiales::orderby('id','ASC')->get();
          $transporte = transporte::orderBy('id' , 'ASC')->pluck('tipo','id');
          $peligroso2 = peligroso::orderBy('id' , 'ASC')->pluck('nombre','id');
          $pys = pys::orderby('id','ASC')->get();
          $ofertas=oferta::orderBy('id','ASC')->get();
          $sedes=sedes::orderBy('id','ASC')->get();

          return view('comercial.oferta.create')
          ->with('ofertas',$ofertas)
          ->with('clientes',$clientes)
          ->with('peligrosos',$peligrosos)
          ->with('aprovechables', $aprovechables)
          ->with('posconsumos', $posconsumos)
          ->with('peligrososa', $peligrososa)
          ->with('especiales', $especiales)
          ->with('pys', $pys)
          ->with('peligroso2', $peligroso2)
          ->with('idu', $idu)
          ->with('sedes', $sedes);

    }
        


       public function edit($id){

    $clientes = DB::table('ofertas')
                
                ->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')

                ->join('sedes', 'ofertas.sedes_id', '=','sedes.id')
               
                ->select('ofertas.*','sedes.*' ,'clientes.razon_social','clientes.dv','clientes.ide', 'ofertas.Estado', 'sedes.nombre', 'sedes.emailop')
                ->where('ofertas.id', '=', $id)->get();

                
 
                $peligrosos= DB::table('oferta_peligroso')
                
                ->join('peligrosos', 'peligrosos.id', '=', 'oferta_peligroso.peligroso_id')
                
                ->select('oferta_peligroso.*', 'peligrosos.nombre','peligrosos.precio', 'oferta_peligroso.peligroso_id','oferta_peligroso.cantidad', 'oferta_peligroso.precio' )
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $aprovechables= DB::table('aprovechables_oferta')
                
                ->join('aprovechables', 'aprovechables.id', '=', 'aprovechables_oferta.aprovechables_id')
                
                ->select('aprovechables_oferta.*', 'aprovechables.nombre','aprovechables.precio', 'aprovechables_oferta.aprovechables_id','aprovechables_oferta.cantidad', 'aprovechables_oferta.precio')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $posconsumos= DB::table('oferta_posconsumos')
                
                ->join('posconsumos', 'posconsumos.id', '=', 'oferta_posconsumos.posconsumos_id')
                
                ->select('oferta_posconsumos.*', 'posconsumos.nombre','posconsumos.precio', 'oferta_posconsumos.precio','oferta_posconsumos.posconsumos_id','oferta_posconsumos.cantidad')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $peligrososa= DB::table('oferta_peligrososa')
                
                ->join('peligrososa', 'peligrososa.id', '=', 'oferta_peligrososa.peligrososa_id')
                
                ->select('oferta_peligrososa.*', 'peligrososa.nombre', 'oferta_peligrososa.peligrososa_id','oferta_peligrososa.cantidad', 'oferta_peligrososa.precio', 'oferta_peligrososa.preciou', 'oferta_peligrososa.preciog')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $especiales= DB::table('especiales_oferta')
                
                ->join('especiales', 'especiales.id', '=', 'especiales_oferta.especiales_id')
                
                ->select('especiales_oferta.*', 'especiales.nombre','especiales.precio', 'especiales_oferta.especiales_id','especiales_oferta.cantidad', 'especiales_oferta.precio')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();

                $transportes= DB::table('oferta_transporte')
                
                ->join('transporte', 'transporte.id', '=', 'oferta_transporte.transporte_id')
                
                ->select('oferta_transporte.*', 'transporte.tipo', 'transporte.preciokg','transporte.preciog','oferta_transporte.preciokg','oferta_transporte.preciog', 'oferta_transporte.transporte_id')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();

                  $otros = DB::table('otros')
                
                ->join('ofertas', 'ofertas.id', '=', 'otros.oferta_id')
                
                ->select('otros.*', 'otros.nombreotros', 'otros.cantidadotros')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();

         
                $peligroso2 = peligroso::orderBy('id' , 'ASC')->pluck('nombre','id');

                $pys= DB::table('oferta_pys')
                
                ->join('pys', 'pys.id', '=', 'oferta_pys.pys_id')
                
                ->select('oferta_pys.*', 'pys.nombre', 'oferta_pys.pys_id'  )
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();

               $ofertas=oferta::find($id);

    return view('comercial.oferta.edit')
    ->with('ofertas',$ofertas)
    ->with('clientes',$clientes)
    ->with('peligrosos',$peligrosos)
    ->with('aprovechables', $aprovechables)
    ->with('posconsumos', $posconsumos)
    ->with('peligrososa', $peligrososa)
    ->with('especiales', $especiales)
    ->with('otros', $otros)
    ->with('transportes',$transportes)
    // ->with('estado', $estado)
    ->with('pys', $pys)
    ->with('peligroso2', $peligroso2);

    }




  
    public function update(Request $request, $id){

      $clientes = DB::table('ofertas')
                
                ->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')
                ->select('ofertas.*', 'clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','email', 'clientes.emailop', 'clientes.ide')
                ->where('id','=',$id)
                ->update(['dv'=>$request->dv , 'razon_social' =>$request->razon_social , 'departamento' =>$request->departamento, 'ciudad' =>$request->ciudad, 'direccion' =>$request->direccion, 'nombres' =>$request->nombres, 'apellidos' =>$request->apellidos, 'telefonocelular' =>$request->telefonocelular, 'email' =>$request->email, 'emailop' =>$request->emailop, 'ide'=> $request->ide]);

                $ofertas= oferta::find($id)->update(['fechaven'=> $request->fechaven, 'Estado'=>$request->Estado, 'observacion' => $request->observacion]);


        Flash::success('La oferta ' .$id. ' ha sido editada de forma existosa');          
        return back();


      

  }


     public function borraro(Request $request, $id){
                                  


                            $otros = otros::find($id);
                            $otros->delete();

                          return back();

                         } 


           public function peligrosos(Request $request, $id){

                

         
                  $oferta = oferta::find($id);
                       
                        $extra = array_map(function($precio, $cantidad){
                         return ['cantidad' => $cantidad, 'precio' => $precio ];
                          }, $request->peligrososprecio_ , $request->cantidad_);

                        $data = array_combine($request->id_, $extra);   

                        $oferta->peligroso()->sync($data);

                        return back();

              }
                     



                      public function borrarp(Request $request, $idoferta , $peligroso){

                        

                            $oferta = oferta::find($idoferta);
                            

                            $oferta->peligroso()->detach($peligroso);


                         return back();
                         }    
                  


             public function aprovechables(Request $request, $id){
                

                  $oferta = oferta::find($id);
                       
                        $extra = array_map(function($cantidad,$precio,$preciou){
                         return ['cantidad' => $cantidad, 'precio' => $precio, 'preciou'=> $preciou ];
                          }, $request->cantidada_, $request->aprovechablesprecio_ ,  $request->preciou_);

                        $data = array_combine($request->id_ , $extra);  



                        $oferta->aprovechables()->sync($data);

                        return back();

              }




                      public function borrara(Request $request, $idoferta , $aprovechable){


                       $oferta = oferta::find($idoferta);
                            
                        $oferta->aprovechables()->detach($aprovechable);

                        return back();
                         }    



              public function posconsumos(Request $request, $id){

                  $oferta = oferta::find($id);
                       
                        $extra = array_map(function($precio, $cantidad){
                         return ['cantidad' => $cantidad, 'precio' => $precio ];
                          }, $request->posconsumosprecio_ , $request->cantidad_);

                        $data = array_combine($request->id_, $extra);   

                        $oferta->posconsumos()->sync($data);

                        return back();

              }




                      public function borrarpo(Request $request, $idoferta , $posconsumo){


                       $oferta = oferta::find($idoferta);
                            
                        $oferta->posconsumos()->detach($posconsumo);

                        return back();
                         }  





              public function peligrososa(Request $request, $id){
                
                  
                  $oferta = oferta::find($id);
                       
                        $extra = array_map(function($precio, $cantidad, $preciou, $preciog){
                         return ['cantidad' => $cantidad, 'precio' => $precio, 'preciou' => $preciou, 'preciog' => $preciog,  ];
                          }, $request->peligrososaprecio_ , $request->cantidadpa_, $request->peligrososapreciou_, $request->peligrososapreciog_);

                        $data = array_combine($request->idpa_, $extra);   

                        $oferta->peligrososa()->sync($data);

                        return back();

              }


                      public function borrarpa(Request $request, $idoferta , $peligrososa){


                       $oferta = oferta::find($idoferta);
                            
                        $oferta->peligrososa()->detach($peligrososa);

                        return back();
                         }  


                  public function especiales(Request $request, $id){

                     $oferta = oferta::find($id);
                       
                        $extra = array_map(function($precio, $cantidad){
                         return ['cantidad' => $cantidad, 'precio' => $precio];
                          }, $request->especialesprecio_ , $request->cantidad_);
                        


                        $data = array_combine($request->id_, $extra);   

                        $oferta->especiales()->sync($data);

                        return back();

              }

                     



                      public function borrare(Request $request, $idoferta , $especiales){
                                  
                            $oferta = oferta::find($idoferta);
                            $oferta->especiales()->detach($especiales);
                         return back();

                         }   
                  





              public function transporte(Request $request, $id){

             
                $oferta = oferta::find($id);

                            $extra = array_map(function($preciokg, $preciog){
                             return ['preciokg' => $preciokg, 'preciog' => $preciog];
                              }, $request->transportespreciokg_, $request->transportespreciog_);

                            $data = array_combine($request->id_, $extra);

                            $oferta->transporte()->sync($data);

                            return back();


                  }

               
                       public function borrart(Request $request, $idoferta , $transporte){

                            $oferta = oferta::find($idoferta);
                            $oferta->transporte()->detach($transporte);
                         return back();

                         }   


             public function pys(Request $request, $id){
                
                
                  $oferta = oferta::find($id);
                       
                        $extra = array_map(function($precio){
                         return ['precio' => $precio ];
                          }, $request->pysprecio_);

                        $data = array_combine($request->id_, $extra);   

                        $oferta->pys()->sync($data);

                        return back();

              }
                     



                      public function borrarpys(Request $request, $idoferta , $pys){
                            $oferta = oferta::find($idoferta);
                            $oferta->pys()->detach($pys);
                         return back();
                         }    
                  


                       public function autocomplete(Request $request){

                        
               
                          $term=$request->term;
                          $data = peligroso::where('nombre','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['id'=>$v->id,'value'=>$v->nombre,'value2'=>$v->precio];
                          }
                            return response()->json($results);
                        }


                        public function autocompletea(Request $request){
               
                          $term=$request->term;
                          $data = aprovechables::where('nombre','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['id'=>$v->id,'value'=>$v->nombre,'value2'=>$v->precio, 'value3' =>$v->preciou];
                          }
                            return response()->json($results);
                        }


                         public function autocompletepo(Request $request){
               
                          $term=$request->term;
                          $data = posconsumos::where('nombre','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['id'=>$v->id,'value'=>$v->nombre,'value2'=>$v->precio];
                          }
        
                            return response()->json($results);
                        }



                        public function autocompletepa(Request $request){
               
                          $term=$request->term;
                          $data = peligrososa::where('nombre','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['id'=>$v->id,'value'=>$v->nombre,'value2'=>$v->precio ,'value3'=>$v->preciou ,'value4'=>$v->preciog];
                          }
                            return response()->json($results);
                        }


                        public function autocompletee(Request $request){
               
                          $term=$request->term;
                          $data = especiales::where('nombre','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['id'=>$v->id,'value'=>$v->nombre,'value2'=>$v->precio];
                          }
                            return response()->json($results);
                        }

                        


                        public function autocompletec(Request $request){
               
                          $term=$request->term;
                          $sedes = sedes::where('identificacion','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();




                           
                          foreach ($sedes as $sede => $s) {
                          
                            $resultados = $s->id;
                          }

                          $data = cliente::where('identificacion','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['value'=>$v->identificacion,'tipoid' => $v->tipo_de_identificacion,'dv'=>$v->dv,'razonsocial'=>$v->razon_social , 'departamento'=>$v->departamento, 'ciudad' => $v->ciudad, 'direccion' => $v->direccion,'tipo_de_ide' => $v->tipo_de_ide, 'ide' => $v->ide,'cargo' => $v->cargo ,'procesos' => $v->procesos,'tipodepersona' => $v->tipodepersona,'nombre' =>$v->nombres, 'apellidos'=> $v->apellidos, 'telefonocelular'=> $v->telefonocelular, 'email'=> $v->email, 'telefono_contacto' => $v->telefono_contacto, 'ext' => $v->ext, 'ejdecuenta' => $v->ejdecuenta, 'canal' => $v->canal, 'sedes'=> $resultados];


                          }
                            return response()->json($results);
                        }




                        public function autocompletet(Request $request){


               
                          $term=$request->term;
                          $data = transporte::where('tipo','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['id'=>$v->id,'value'=>$v->tipo,'value2'=>$v->preciokg ,'value3' => $v->preciog];
                          }
                            return response()->json($results);
                        }

                       public function autocompletepys(Request $request){


               
                          $term=$request->term;
                          $data = pys::where('nombre','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['id'=>$v->id,'value'=>$v->nombre,'value2'=>$v->precio];
                          }
                            return response()->json($results);
                        } 






  public function destroy($id){

  	// $oferta = oferta::find($id);

  	// $oferta->delete();
  	// Flash::warning('La oferta ' .$oferta->id. ' a sido borrada de forma existosa');
  	// return redirect()->route('comercial.ofertas.index');

  }



  public function store(Request $request){

        

           $iduser = Auth::id();

                  

                if (is_null($request->nombre_ || $request->nombrea_ || $request->nombrepo_ || $request->nombrepa_ || $request->nombree_ || $request->tipo_) || empty($request->nombre_ || $request->nombrea_ || $request->nombrepo_ || $request->nombrepa_ || $request->nombree_ || $request->tipo_ || $request->nombrepys_) ) 
                       {  
                            
                        Flash::error("No tienes servicios para la oferta ");
                        return redirect()->back()->withInput();

                          }

                          else{

                            if ($request->ejdecuenta == '0') {

                              $clientes= cliente::updateOrCreate(['identificacion' => $request->identificacion],
                                ['tipodepersona' => $request->tipodepersona, 'razon_social' => $request->razon_social, 'tipo_de_identificacion' => $request->tipo_de_identificacion, 'dv'=> $request->dv , 'departamento' => $request->departamento, 'ciudad'=>$request->ciudad, 'direccion' => $request->direccion,'ide'=>$request->ide, 'cargo'=>$request->cargo, 'nombres'=>$request->nombres, 'apellidos' =>$request->apellidos, 'telefonocelular' =>$request->telefonocelular, 'email' =>$request->email,'emailop' =>$request->emailop, 'telefono_contacto' => $request->telefono_contacto, 'ext' =>$request->ext, 'procesos'=>$request->procesos, 'ejdecuenta' => $iduser,'canal' => $request->canal]);


                              
                            }

                            else{
                                $clientes= cliente::updateOrCreate(['identificacion' => $request->identificacion],
                                ['tipodepersona' => $request->tipodepersona, 'razon_social' => $request->razon_social, 'tipo_de_identificacion' => $request->tipo_de_identificacion, 'dv'=> $request->dv , 'departamento' => $request->departamento, 'ciudad'=>$request->ciudad, 'direccion' => $request->direccion,'ide'=>$request->ide, 'cargo'=>$request->cargo, 'nombres'=>$request->nombres, 'apellidos' =>$request->apellidos, 'telefonocelular' =>$request->telefonocelular, 'email' =>$request->email,'emailop' =>$request->emailop, 'telefono_contacto' => $request->telefono_contacto, 'ext' =>$request->ext, 'procesos'=>$request->procesos,'canal' => $request->canal]);

                            }

                             if ($request->sede == '0') {
                                $sede = new sedes($request->all());
                                $sede->nombre = "Principal";
                                $sede->responsable = $request->nombres.' '. $request->apellidos;

                                $sede->telefono = $request->telefono_contacto;
                                $sede->save();

                                $ofertas= new oferta($request->all());
                                  $ofertas->sedes_id = $sede->id;
                                  $ofertas->save();
                              }

                              else{

                                $ofertas= new oferta($request->all());
                                  $sedes = $request->sede;
                                  $ofertas->sedes_id = $sedes;
                                  $ofertas->save();
                              }
                          

                          if (!empty($request->nombre_)) {

                                $extra = array_map(function($precio, $cantidad){
                               return ['cantidad' => $cantidad, 'precio' => $precio ];
                                }, $request->peligrososprecio_ , $request->cantidad_);

                              $data = array_combine($request->id_, $extra);   

                              $ofertas->peligroso()->sync($data);

                          }

                          if (!empty($request->nombrea_)) {
                       
                             $extra = array_map(function($cantidad, $precio, $preciou){
                                   return ['cantidad' => $cantidad, 'precio' => $precio, 'preciou' =>$preciou ];
                                    }, $request->cantidada_,$request->aprovechablesprecio_ , $request->preciou_);

                                    $data = array_combine($request->ida_, $extra);   

                                    $ofertas->aprovechables()->sync($data);
                          }



                           if (!empty($request->nombrepo_)) {

                            
                            
                                  $extra = array_map(function($precio, $cantidad){
                                   return ['cantidad' => $cantidad, 'precio' => $precio ];
                                    }, $request->posconsumosprecio_ , $request->cantidadpo_);

                                    $data = array_combine($request->idpo_, $extra);   

                                    $ofertas->posconsumos()->sync($data);


                          }

                          if (!empty($request->nombrepa_)) {
                                    
                                  
                                     $extra = array_map(function($precio, $cantidad, $preciou, $preciog){
                                   return ['cantidad' => $cantidad, 'precio' => $precio, 'preciou' => $preciou, 'preciog' => $preciog ];
                                    }, $request->peligrososaprecio_ , $request->cantidadpa_, $request->peligrososapreciou_ , $request->peligrososapreciog_ );

                                    $data = array_combine($request->idpa_, $extra);   

                                    $ofertas->peligrososa()->sync($data);
                          }

                          if (!empty($request->nombree_)) {
                            
                                            $extra = array_map(function($precio, $cantidad){
                                   return ['cantidad' => $cantidad, 'precio' => $precio ];
                                    }, $request->especialesprecio_ , $request->cantidade_);

                                    $data = array_combine($request->ide_, $extra);   

                                    $ofertas->especiales()->sync($data);

                          }

                          if (!empty($request->tipo_)) {
                            
                            $extra = array_map(function($preciokg, $preciog){
                             return ['preciokg' => $preciokg, 'preciog' => $preciog];
                              }, $request->transportespreciokg_, $request->transportespreciog_);

                            $data = array_combine($request->idt_, $extra);

                            $ofertas->transporte()->sync($data);

                            return back();

                          }

                          if (!empty($request->nombrepys_)) {
                            
                                    $extra = array_map(function($precio){
                               return ['precio' => $precio];
                                }, $request->pysprecio_);

                              $data = array_combine($request->idpys_, $extra);   

                              $ofertas->pys()->sync($data);

                          }
                          }

                           

                      
 
                         
$user = Auth::User();

                       
                          
        $oferta = oferta::find($ofertas->id);


     $clientes->notify(new NotifyOfertas($oferta));

Flash::success("Se ha Registrado la oferta ". $ofertas->id . " De forma existosa");
     return redirect()->route('ofertas.edit', $ofertas->id);
    
  	
  }


   

}
