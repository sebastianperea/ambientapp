<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\cliente;
use App\sedes;
use App\fcreacion;
use App\fresiduos;
use App\oferta;
use Illuminate\Support\Facades\Crypt;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Http\Requests\UsuarioRequest;
use App\Http\Requests;
use App\Http\Requests\UsersRequest;


class ClienteController extends Controller
{
    public function index (Request $request, $identificacion){

        $users = User::where('identificacion', '=', $identificacion);
        $clientes= cliente::find($identificacion);
       
    $oferta = oferta::where('identificacion', '=', $identificacion)->get();

    $ofertas = $oferta->where('estadoenv', '=', 'Enviado')->take(15);



  return view('clientes.index')
    ->with('users',$users)
    ->with('ofertas', $ofertas)
    ->with('clientes', $clientes);

  }

  public function edit (Request $request, $identificacion){

	$users = User::where('identificacion', '=', $identificacion)->first();
	$clientes= cliente::find($identificacion);



	return view('clientes.edit')
    ->with('users',$users)
    ->with('clientes', $clientes);

  }	


  public function upload (Request $request, $identificacion){

  	

  	$data = Crypt::decrypt($identificacion);

	$clientes= cliente::find($data);

	return view('publicocliente.upload')
    ->with('clientes', $clientes);

  }	

  public function up (Request $request, $identificacion){

  	
  	
	$cliente= cliente::find($identificacion);

         if (count($request->file('creaciondeclientes'))>0) {
      $fcreacion = $request->file('creaciondeclientes');

                    $namefcreacion = 'Ecoindustria_'.time().rand(1,99999).'.'.$fcreacion->getClientOriginalExtension();
                    $path = Storage_path() . '/files/comercial/';

                    $fcreacion->move($path, $namefcreacion);
                    $fcreacion = new fcreacion();
                    $fcreacion->nombre = $namefcreacion;
                    $fcreacion->cliente()->associate($cliente);
                    $fcreacion->save();
    }

    if (count($request->file('inforesiduos'))>0) {
      $fresiduos = $request->file('inforesiduos');

                    $namefresiduos= 'Ecoindustria_'.time().rand(1,99999).'.'.$fresiduos->getClientOriginalExtension();
                    $path = Storage_path() . '/files/comercial/';

                    $fresiduos->move($path, $namefresiduos);
                    $fresiduos= new fresiduos();
                    $fresiduos->nombre = $namefresiduos;
                    $fresiduos->cliente()->associate($cliente);
                    $fresiduos->save();
    }

    $ofertas = oferta::where('identificacion', '=', $identificacion)->first();
    $ofertas->Estado = 'Aprobado';
    $ofertas->save();

    $random = str_random(5);
       return redirect()->route('cliente.up.success',$random);

  }	

  public function success($random){

       Flash::success("Se han subido los archivos de forma exitosa");
    return view('publicocliente.succupload');

  }

    public function save(Request $request, $identificacion){


      $num_elements = 0;
                $sqlData = []; 
                $input= $request->all();
                      while($num_elements < count($input['nombre_'])){
                        $sqlData[] = array(
                            'identificacion' => $identificacion,
                             'nombre'         => $input['nombre_'][$num_elements],
                            'direccion'          => $input['direccion_'][$num_elements],
                            'departamento'     => $input['departamento_'][$num_elements],
                            'ciudad'      => $input['ciudad_'][$num_elements],
                            'created_at'    => Carbon::now(), // only if your table has this column
                            'updated_at'    => Carbon::now(), // only if your table has this column
                        );
                      $num_elements++;
                 }

      sedes::insert($sqlData);      

            Flash::success("Se ha agregado la sede de forma existosa");
                return back();

       

  }


  public function perfil($identificacion){

                $usuario = User::where('identificacion', '=', $identificacion)->get();

                $clientes = cliente::find($identificacion);

                $ofertas = DB::table('ofertas')
                
                ->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')
               
                ->select('ofertas.*','ofertas.id','ofertas.estado','ofertas.created_at')
                ->where('ofertas.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $cbancariapj = DB::table('cbancariapj')
                
                ->join('clientes', 'cbancariapj.cliente_id', '=', 'clientes.identificacion')
               
                ->select('cbancariapj.*','cbancariapj.id','cbancariapj.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $cerlpj = DB::table('cerlpj')
                
                ->join('clientes', 'cerlpj.cliente_id', '=', 'clientes.identificacion')
               
                ->select('cerlpj.*','cerlpj.id','cerlpj.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();


                $fotocopiacpj = DB::table('fotocopiacpj')
                
                ->join('clientes', 'fotocopiacpj.cliente_id', '=', 'clientes.identificacion')
               
                ->select('fotocopiacpj.*','fotocopiacpj.id','fotocopiacpj.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $rutpj = DB::table('rutpj')
                
                ->join('clientes', 'rutpj.cliente_id', '=', 'clientes.identificacion')
               
                ->select('rutpj.*','rutpj.id','rutpj.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $drentapj = DB::table('drentapj')
                
                ->join('clientes', 'drentapj.cliente_id', '=', 'clientes.identificacion')
               
                ->select('drentapj.*','drentapj.id','drentapj.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $estadosfinancieros = DB::table('estadosfinancieros')
                
                ->join('clientes', 'estadosfinancieros.cliente_id', '=', 'clientes.identificacion')
               
                ->select('estadosfinancieros.*','estadosfinancieros.id','estadosfinancieros.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $cbancariapn = DB::table('cbancariapn')
                
                ->join('clientes', 'cbancariapn.cliente_id', '=', 'clientes.identificacion')
               
                ->select('cbancariapn.*','cbancariapn.id','cbancariapn.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();


                $cmpn = DB::table('cmpn')
                
                ->join('clientes', 'cmpn.cliente_id', '=', 'clientes.identificacion')
               
                ->select('cmpn.*','cmpn.id','cmpn.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $drentapn = DB::table('drentapn')
                
                ->join('clientes', 'drentapn.cliente_id', '=', 'clientes.identificacion')
               
                ->select('drentapn.*','drentapn.id','drentapn.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

               $fotocopiacpn = DB::table('fotocopiacpn')
                
                ->join('clientes', 'fotocopiacpn.cliente_id', '=', 'clientes.identificacion')
               
                ->select('fotocopiacpn.*','fotocopiacpn.id','fotocopiacpn.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $rutpn = DB::table('rutpn')
                
                ->join('clientes', 'rutpn.cliente_id', '=', 'clientes.identificacion')
               
                ->select('rutpn.*','rutpn.id','rutpn.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                 $cc = DB::table('cc')
                
                ->join('clientes', 'cc.identificacion', '=', 'clientes.identificacion')
               
                ->select('cc.*','cc.id','cc.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();
                
                $inforesiduos = DB::table('info_residuos')
                
                ->join('clientes', 'info_residuos.identificacion', '=', 'clientes.identificacion')
               
                ->select('info_residuos.*','info_residuos.id','info_residuos.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

               $filespeligrosos= DB::table('filespeligrosos_residuos')
                
                ->join('filespeligrosos', 'filespeligrosos.id', '=', 'filespeligrosos_residuos.filespeligrosos_id')
                ->join('residuos', 'residuos.id', '=', 'filespeligrosos_residuos.residuos_id')
                
                ->select('filespeligrosos_residuos.*', 'filespeligrosos.nombre')

                ->where('residuos.identificacion', '=', $identificacion)->orderBy('id','ASC')->get();

                $filespeligrososa= DB::table('filespeligrososa_residuos')
                
                ->join('filespeligrososa', 'filespeligrososa.id', '=', 'filespeligrososa_residuos.filespeligrososa_id')
                ->join('residuos', 'residuos.id', '=', 'filespeligrososa_residuos.residuos_id')
                
                ->select('filespeligrososa_residuos.*', 'filespeligrososa.nombre')

                ->where('residuos.identificacion', '=', $identificacion)->orderBy('id','ASC')->get();

                $especiales3= DB::table('filese_residuos')
                
                ->join('filese', 'filese.id', '=', 'filese_residuos.filese_id')
                ->join('residuos', 'residuos.id', '=', 'filese_residuos.residuos_id')
                
                ->select('filese_residuos.*', 'filese.nombre')

                ->where('residuos.identificacion', '=', $identificacion)->orderBy('id','ASC')->get();

                $sedes= DB::table('sedes')
                
                ->join('clientes', 'clientes.identificacion', '=', 'sedes.identificacion')
                
                ->select('sedes.*', 'sedes.nombre', 'sedes.direccion', 'sedes.departamento', 'sedes.ciudad')

                ->where('sedes.identificacion', '=', $identificacion)->orderBy('id','ASC')->get();


                

               
                  return view ('clientes.perfil')
                  ->with('clientes',$clientes)
                  ->with('ofertas', $ofertas)
                  ->with('cbancariapj', $cbancariapj)
                  ->with('cerlpj', $cerlpj)
                  ->with('fotocopiacpj', $fotocopiacpj)
                  ->with('rutpj', $rutpj)
                  ->with('drentapj', $drentapj)
                  ->with('estadosfinancieros', $estadosfinancieros)
                  ->with('cbancariapn', $cbancariapn)
                  ->with('cmpn', $cmpn)
                  ->with('drentapn', $drentapn)
                  ->with('fotocopiacpn', $fotocopiacpn)
                  ->with('cc',$cc)
                  ->with('inforesiduos',$inforesiduos)
                  ->with('filespeligrososa', $filespeligrososa)
                  ->with('filespeligrosos', $filespeligrosos)
                  ->with('especiales3', $especiales3)
                  ->with('sedes',$sedes)
                  ->with('usuario', $usuario)
                  ->with('rutpn', $rutpn);    

  }


  public function update(Request $request, $identificacion){

    
    $cliente = cliente::find($identificacion);
    $cliente->tipo = $request->tipo;
    $cliente->nombres = $request->nombre;
    $cliente->apellidos = $request->apellidos;
    $cliente->email = $request->email;
    $cliente->cargo = $request->cargo;
    $cliente->telefonocelular = $request->telefonocelular;
    $cliente->nombrecartera = $request->nombrecartera;
    $cliente->telefonofijocartera =$request->telefonofijocartera;
    $cliente->extcartera =$request->extcartera;
    $cliente->telefonomovilcartera =$request->telefonomovilcartera;
    $cliente->emailcartera =$request->emailcartera;
    $cliente->direccionradicacion =$request->direccionradicacion;
    $cliente->nombrebanco = $request->nombrebanco;
    $cliente->numerodecuenta = $request->numerodecuenta;
    $cliente->tipodecuenta = $request->tipodecuenta;
    $cliente->save();

    Flash::success("Se ha actualizado el perfil de forma exitosa");

        return back ();

      

    }

    public function usuario(UsuarioRequest $request, $identificacion){

      

    $usuario = User::where('identificacion', '=', $identificacion)->first();

      $usuario->name = $request->name;

      if(!empty($request->email)){


        $usuario->email = $request->email;
      }
      
      if (!empty($request->password)) {
        $usuario->password = bcrypt($request->password);
      }

      $usuario->save();
    

    Flash::success("Se ha actualizado el perfil de forma exitosa");

        return back ();

    }


public function solicitudrecoleccion(Request $request, $identificacion){

     $hoy = Carbon::now();

      // $hoy = $date->format('Y-m-d');


     $clientes = cliente::find($identificacion);
     $peligrosos = DB::table('oferta_peligroso')

                ->join('ofertas', 'ofertas.id', '=', 'oferta_peligroso.oferta_id')
                ->join('peligrosos', 'peligrosos.id', '=', 'oferta_peligroso.peligroso_id')

                ->select('oferta_peligroso.*', 'peligrosos.nombre','peligrosos.pretratamiento', 'oferta_peligroso.peligroso_id','oferta_peligroso.cantidad', 'oferta_peligroso.precio', 'ofertas.identificacion', 'ofertas.fechaven')
                ->where('ofertas.identificacion', '=', $identificacion)->orderBy('id','ASC')->get();


      $peligrososa = DB::table('oferta_peligrososa')

                ->join('ofertas', 'ofertas.id', '=', 'oferta_peligrososa.oferta_id')
                ->join('peligrososa', 'peligrososa.id', '=', 'oferta_peligrososa.peligrososa_id')

                ->select('oferta_peligrososa.*', 'peligrososa.nombre','peligrososa.precio', 'oferta_peligrososa.peligrososa_id','oferta_peligrososa.cantidad', 'oferta_peligrososa.precio', 'ofertas.identificacion', 'ofertas.fechaven')
                ->where('ofertas.identificacion', '=', $identificacion)->orderBy('id','ASC')->get(); 

      $especiales = DB::table('especiales_oferta')

                ->join('ofertas', 'ofertas.id', '=', 'especiales_oferta.oferta_id')
                ->join('especiales', 'especiales.id', '=', 'especiales_oferta.especiales_id')

                ->select('especiales_oferta.*', 'especiales.nombre','especiales.precio', 'especiales_oferta.especiales_id','especiales_oferta.cantidad', 'especiales_oferta.precio', 'ofertas.identificacion', 'ofertas.fechaven')
                ->where('ofertas.identificacion', '=', $identificacion)->orderBy('id','ASC')->get();



     return view ('clientes.recoleccion.solicitud')
     ->with('hoy', $hoy)
     ->with('peligrosos', $peligrosos)
     ->with('peligrososa', $peligrososa)
     ->with('especiales', $especiales);

      

    }

}
