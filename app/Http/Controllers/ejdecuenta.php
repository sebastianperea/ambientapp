<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Carbon\Carbon;

class ejdecuenta extends Controller
{
   public function index ($iduser){
   		$iduser = Auth::id();
   		

			$ofertas= DB::table('ofertas')
			                
			                ->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')
			                ->select('ofertas.*', 'ofertas.id','clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','clientes.email','clientes.tipo')->orderBy('created_at','DSC')->where('clientes.ejdecuenta', '=', $iduser)->simplePaginate(15);




			$cc= DB::table('ofertas')

							->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')
			                ->join('users', 'ofertas.user_id', '=', 'users.id')
			                ->join('cc', 'cc.identificacion', '=', 'clientes.identificacion')
			                ->select('ofertas.*', 'ofertas.id', 'cc.id','clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','clientes.email','clientes.tipo', 'users.name')->orderBy('created_at','DSC')->where('user_id', '=', $iduser)->simplePaginate(15);



			    $ofertasc= DB::table('ofertas')->where('user_id', '=', $iduser)->count();
			    $cc2 = DB::table('clientes')->join('ofertas', 'ofertas.identificacion', '=', 'clientes.identificacion')
			                ->join('users', 'ofertas.user_id', '=', 'users.id')->where('clientes.ejdecuenta', '=', $iduser)->where('tipo', '=', 'Cliente')->count();

			    

			    $idu = Auth::id();
			    $now = Carbon::now();
			    $date = $now->format('Y-m-d');
			    $hoy = $now->toFormattedDateString();

			    
			    $seguimientos =   DB::table('visitas')
                
                ->join('clientes', 'visitas.identificacion', '=', 'clientes.identificacion')
                ->select('visitas.*', 'visitas.id','visitas.observaciones','clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','clientes.email','clientes.tipo', 'clientes.telefono_contacto', 'clientes.procesos', 'clientes.identificacion')->where('id_user', '=', $idu)->where('fecha_nuevo_contacto', '=', $date)->orderBy('created_at','DSC')->get();

                $seguimientosvenc =   DB::table('visitas')
                
                ->join('clientes', 'visitas.identificacion', '=', 'clientes.identificacion')
                ->select('visitas.*', 'visitas.id','visitas.observaciones','clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','clientes.email','clientes.tipo', 'clientes.telefono_contacto', 'clientes.procesos', 'clientes.identificacion', 'visitas.ESTADO')->where('id_user', '=', $idu)->where('fecha_nuevo_contacto', '<', $date)->where('visitas.ESTADO','=', 'VENCIDO')->get();


			    return view ('ejdecuenta.index')
			    ->with('iduser',$iduser)
			    ->with('ofertas',$ofertas)
			    ->with('cc2',$cc2)
			    ->with('cc',$cc)
			    ->with('ofertasc',$ofertasc)
			    ->with('seguimientos', $seguimientos)
			    ->with('hoy', $hoy)
			    ->with('seguimientosvenc', $seguimientosvenc);


  }
}
