<?php

namespace App\Http\Controllers;
use App\peligroso;
use App\dispositores;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class Peligrososcontroller extends Controller
{
  

  public function index (){



		$peligrosos= peligroso::orderBy('id','ASC')->get();
                

  	
		    return view ('comercial.residuos.peligrosos.index')
		    ->with('peligrosos',$peligrosos);
   

  }



public function create(){

    return view('comercial.residuos.peligrosos.create');

  }





  public function store(Request $request){
   


           $peligrosos= new peligroso($request->all());
          
           $peligrosos->save();


            Flash::success('El residuo peligroso ha sido creado con exito');
            return back();

           

             }

   public function edit($id){

    $peligrosos= peligroso::find($id);
    
    $dispositor = dispositores::orderBy('id','ASC')->pluck('nombre','id');

    


    return view('comercial.residuos.peligrosos.edit')
    ->with('peligrosos',$peligrosos)->with('dispositor', $dispositor);

    }  


    public function update(Request $request, $id){

        $peligrosos= peligroso::orderBy('id','ASC')->get();  
        $peligroso= peligroso::find($id);
         $peligroso->nombre = $request->nombre;
         $peligroso->precio = $request->precio;
         $peligroso->pretratamiento = $request->pretratamiento;
        $peligroso->save();

        Flash::success('El residuo peligroso ah sido actualizado con exito');
        return redirect()->route('peligrosos.index')
        ->with('peligrosos',$peligrosos);

      

    }

}
