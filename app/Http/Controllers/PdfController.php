<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use App\User;
use App\oferta;
use App\cliente;
use App\peligroso;
use App\aprovechables;
use App\transporte;
use App\posiblecliente;
use App\ofertapc;
use App\Mail\Welcome as Welcome;
use App\Notifications\RepliedToThread;
use App\productoyservicio;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
    
 
use Illuminate\Support\Facades\Hash;


class PdfController extends Controller
{
  
 public function ofertapcpdf($id)


    {

        $user = Auth::user()->name;
        $useremail = Auth::user()->email;
        

        $clientes = DB::table('ofertas')
                
                ->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')

                ->join('sedes', 'ofertas.sedes_id', '=','sedes.id')
               
                ->select('ofertas.*','sedes.*' ,'clientes.razon_social','clientes.dv','clientes.ide','clientes.tipo', 'ofertas.Estado', 'sedes.nombre', 'sedes.emailop')
                ->where('ofertas.id', '=', $id)->first();


                $peligrosos= DB::table('oferta_peligroso')
                
                ->join('peligrosos', 'peligrosos.id', '=', 'oferta_peligroso.peligroso_id')
                
                ->select('oferta_peligroso.*', 'peligrosos.nombre','peligrosos.precio', 'peligrosos.pretratamiento','oferta_peligroso.peligroso_id','oferta_peligroso.cantidad', 'oferta_peligroso.precio' )
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $aprovechables= DB::table('aprovechables_oferta')
                
                ->join('aprovechables', 'aprovechables.id', '=', 'aprovechables_oferta.aprovechables_id')
                
                ->select('aprovechables_oferta.*', 'aprovechables.nombre','aprovechables.precio', 'aprovechables_oferta.aprovechables_id','aprovechables_oferta.cantidad', 'aprovechables_oferta.precio', 'aprovechables_oferta.preciou')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $posconsumos= DB::table('oferta_posconsumos')
                
                ->join('posconsumos', 'posconsumos.id', '=', 'oferta_posconsumos.posconsumos_id')
                
                ->select('oferta_posconsumos.*', 'posconsumos.nombre','posconsumos.pretratamiento','posconsumos.precio', 'oferta_posconsumos.precio','oferta_posconsumos.posconsumos_id','oferta_posconsumos.cantidad')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $peligrososa= DB::table('oferta_peligrososa')
                
                ->join('peligrososa', 'peligrososa.id', '=', 'oferta_peligrososa.peligrososa_id')
                
                ->select('oferta_peligrososa.*', 'peligrososa.nombre', 'peligrososa.pretratamiento', 'oferta_peligrososa.peligrososa_id','oferta_peligrososa.cantidad', 'oferta_peligrososa.precio', 'oferta_peligrososa.preciou', 'oferta_peligrososa.preciog')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $especiales= DB::table('especiales_oferta')
                
                ->join('especiales', 'especiales.id', '=', 'especiales_oferta.especiales_id')
                
                ->select('especiales_oferta.*', 'especiales.nombre','especiales.pretratamiento','especiales.precio', 'especiales_oferta.especiales_id','especiales_oferta.cantidad', 'especiales_oferta.precio')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();

                $transportes= DB::table('oferta_transporte')
                
                ->join('transporte', 'transporte.id', '=', 'oferta_transporte.transporte_id')
                
                ->select('oferta_transporte.*', 'transporte.tipo','oferta_transporte.preciokg','oferta_transporte.preciog', 'oferta_transporte.transporte_id')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();
                
                $pys= DB::table('oferta_pys')
                
                ->join('pys', 'pys.id', '=', 'oferta_pys.pys_id')
                
                ->select('oferta_pys.*', 'pys.nombre','oferta_pys.precio', 'oferta_pys.pys_id')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();





    $ofertas=oferta::find($id);
     
   
    $header =view('pdf.header');
    $footer =view('pdf.footer');
    $date = Carbon::now()->toFormattedDateString();
        
        $pdf = PDF::loadView('pdf.ofertas',compact('ofertas',$ofertas, 'clientes',$clientes,'peligrosos',$peligrosos, 'aprovechables', $aprovechables, 'posconsumos', $posconsumos, 'peligrososa',$peligrososa, 'especiales', $especiales, 'transportes', $transportes, 'pys', $pys,'date', 'user', 'useremail'))->setPaper('letter')->setOrientation('portrait')->setOption('dpi', 300);

        $pdf->setOption('header-html', $header)->setOption('margin-top',28);
        $pdf->setOption('footer-html', $footer)->setOption('margin-bottom',30)->setOption('margin-left',0);
          

        

        return $pdf->stream('OF'.$ofertas->id, array("Attachment" => false));

            
        


 }


 public function ofertapcpdfdownload($id) 
    {   

        $user = Auth::user()->name;
         $useremail = Auth::user()->email;
        $clientes = DB::table('ofertas')
                
                ->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')

                ->join('sedes', 'ofertas.sedes_id', '=','sedes.id')
               
                ->select('ofertas.*','sedes.*' ,'clientes.razon_social','clientes.dv','clientes.ide','clientes.tipo', 'ofertas.Estado', 'sedes.nombre', 'sedes.emailop')
                ->where('ofertas.id', '=', $id)->first();


                $peligrosos= DB::table('oferta_peligroso')
                
                ->join('peligrosos', 'peligrosos.id', '=', 'oferta_peligroso.peligroso_id')
                
                ->select('oferta_peligroso.*', 'peligrosos.nombre','peligrosos.precio', 'peligrosos.pretratamiento','oferta_peligroso.peligroso_id','oferta_peligroso.cantidad', 'oferta_peligroso.precio' )
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $aprovechables= DB::table('aprovechables_oferta')
                
                ->join('aprovechables', 'aprovechables.id', '=', 'aprovechables_oferta.aprovechables_id')
                
                ->select('aprovechables_oferta.*', 'aprovechables.nombre','aprovechables.precio', 'aprovechables_oferta.aprovechables_id','aprovechables_oferta.cantidad', 'aprovechables_oferta.precio')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $posconsumos= DB::table('oferta_posconsumos')
                
                ->join('posconsumos', 'posconsumos.id', '=', 'oferta_posconsumos.posconsumos_id')
                
                ->select('oferta_posconsumos.*', 'posconsumos.nombre','posconsumos.pretratamiento','posconsumos.precio', 'oferta_posconsumos.precio','oferta_posconsumos.posconsumos_id','oferta_posconsumos.cantidad')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $peligrososa= DB::table('oferta_peligrososa')
                
                ->join('peligrososa', 'peligrososa.id', '=', 'oferta_peligrososa.peligrososa_id')
                
                ->select('oferta_peligrososa.*', 'peligrososa.nombre', 'peligrososa.pretratamiento', 'oferta_peligrososa.peligrososa_id','oferta_peligrososa.cantidad', 'oferta_peligrososa.precio', 'oferta_peligrososa.preciou', 'oferta_peligrososa.preciog')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $especiales= DB::table('especiales_oferta')
                
                ->join('especiales', 'especiales.id', '=', 'especiales_oferta.especiales_id')
                
                ->select('especiales_oferta.*', 'especiales.nombre','especiales.pretratamiento','especiales.precio', 'especiales_oferta.especiales_id','especiales_oferta.cantidad', 'especiales_oferta.precio')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();

                $transportes= DB::table('oferta_transporte')
                
                ->join('transporte', 'transporte.id', '=', 'oferta_transporte.transporte_id')
                
                ->select('oferta_transporte.*', 'transporte.tipo','transporte.preciokg','transporte.preciog','oferta_transporte.preciokg','oferta_transporte.preciog', 'oferta_transporte.transporte_id')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();

                $pys= DB::table('oferta_pys')
                
                ->join('pys', 'pys.id', '=', 'oferta_pys.pys_id')
                
                ->select('oferta_pys.*', 'pys.nombre','oferta_pys.precio', 'oferta_pys.pys_id')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();





    $ofertas=oferta::find($id);
  
   
    
        
        $header =view('pdf.header');
    $footer =view('pdf.footer');
 
        
        $date = Carbon::now()->toFormattedDateString();
        
        $pdf = PDF::loadView('pdf.ofertas',compact('ofertas',$ofertas, 'clientes',$clientes,'peligrosos',$peligrosos, 'aprovechables', $aprovechables, 'posconsumos', $posconsumos, 'peligrososa',$peligrososa, 'especiales', $especiales, 'transportes', $transportes, 'pys', $pys,'date', 'user',  'useremail'))->setPaper('letter')->setOrientation('portrait')->setOption('dpi', 300);

        $pdf->setOption('header-html', $header)->setOption('margin-top',28);
        $pdf->setOption('footer-html', $footer)->setOption('margin-bottom',30)->setOption('margin-left',0);
        
        
  return $pdf->download('OF'.$ofertas->id.' - '.$clientes->razon_social.'.pdf');


 }

 public function mail($id) 
    {

        $user = Auth::user()->name;
        $useremail = Auth::user()->email;
        $clientes = DB::table('ofertas')
                
                ->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')

                ->join('sedes', 'ofertas.sedes_id', '=','sedes.id')
               
                ->select('ofertas.*','sedes.*' ,'clientes.razon_social','clientes.dv','clientes.ide','clientes.tipo', 'ofertas.Estado', 'sedes.nombre', 'sedes.emailop')
                ->where('ofertas.id', '=', $id)->first();
                    

                $peligrosos= DB::table('oferta_peligroso')
                
                ->join('peligrosos', 'peligrosos.id', '=', 'oferta_peligroso.peligroso_id')
                
                ->select('oferta_peligroso.*', 'peligrosos.nombre','peligrosos.precio', 'peligrosos.pretratamiento','oferta_peligroso.peligroso_id','oferta_peligroso.cantidad', 'oferta_peligroso.precio' )
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $aprovechables= DB::table('aprovechables_oferta')
                
                ->join('aprovechables', 'aprovechables.id', '=', 'aprovechables_oferta.aprovechables_id')
                
                ->select('aprovechables_oferta.*', 'aprovechables.nombre','aprovechables.precio', 'aprovechables_oferta.aprovechables_id','aprovechables_oferta.cantidad', 'aprovechables_oferta.precio')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $posconsumos= DB::table('oferta_posconsumos')
                
                ->join('posconsumos', 'posconsumos.id', '=', 'oferta_posconsumos.posconsumos_id')
                
                ->select('oferta_posconsumos.*', 'posconsumos.nombre','posconsumos.pretratamiento','posconsumos.precio', 'oferta_posconsumos.precio','oferta_posconsumos.posconsumos_id','oferta_posconsumos.cantidad')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $peligrososa= DB::table('oferta_peligrososa')
                
                ->join('peligrososa', 'peligrososa.id', '=', 'oferta_peligrososa.peligrososa_id')
                
                ->select('oferta_peligrososa.*', 'peligrososa.nombre', 'peligrososa.pretratamiento', 'oferta_peligrososa.peligrososa_id','oferta_peligrososa.cantidad', 'oferta_peligrososa.precio', 'oferta_peligrososa.preciou', 'oferta_peligrososa.preciog')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();


                $especiales= DB::table('especiales_oferta')
                
                ->join('especiales', 'especiales.id', '=', 'especiales_oferta.especiales_id')
                
                ->select('especiales_oferta.*', 'especiales.nombre','especiales.pretratamiento','especiales.precio', 'especiales_oferta.especiales_id','especiales_oferta.cantidad', 'especiales_oferta.precio')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();

                $transportes= DB::table('oferta_transporte')
                
                ->join('transporte', 'transporte.id', '=', 'oferta_transporte.transporte_id')
                
                ->select('oferta_transporte.*', 'transporte.tipo','transporte.preciokg','transporte.preciog','oferta_transporte.preciokg','oferta_transporte.preciog', 'oferta_transporte.transporte_id')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();

                $pys= DB::table('oferta_pys')
                
                ->join('pys', 'pys.id', '=', 'oferta_pys.pys_id')
                
                ->select('oferta_pys.*', 'pys.nombre','oferta_pys.precio', 'oferta_pys.pys_id')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();

          $ofertas=oferta::find($id);
     
        $header =view('pdf.header');
    $footer =view('pdf.footer');
 
        
        $parameter =[
            'identificacion' =>$clientes->identificacion,
        ];
    $parameter2= Crypt::encrypt($parameter);
    
   

    

         $date = Carbon::now()->toFormattedDateString();
         $date2 = Carbon::now();

         // if ($clientes->tipo === 'Posible') {
         // $user = new User();
         // $user->name = $clientes->nombres;
         // $user->email = $clientes->email;
         // $pass = str_random (8);
         // $user->password = bcrypt($pass);
         // $user->identificacion = $clientes->identificacion;
         // $user->save();
             
         // }

         // else{

         //    $user= null;
         //    $pass = null;
         // }

       
        $pdf = PDF::loadView('pdf.ofertas',compact('ofertas',$ofertas, 'clientes',$clientes,'peligrosos',$peligrosos, 'aprovechables', $aprovechables, 'posconsumos', $posconsumos, 'peligrososa',$peligrososa, 'especiales', $especiales, 'transportes', $transportes, 'pys', $pys,'date', 'user', 'useremail'))->setPaper('letter')->setOrientation('portrait')->setOption('dpi', 300);


        $pdf->setOption('header-html', $header)->setOption('margin-top',28);
        $pdf->setOption('footer-html', $footer)->setOption('margin-bottom',30)->setOption('margin-left',0);

        $pdf2= $pdf->download();


        

        
        if (empty($clientes->emailop)) {

             Mail::to($clientes->email)
        ->send(new Welcome($pdf2,$ofertas,$clientes,$parameter2,$useremail));
             
        }

        else{
           Mail::to($clientes->email)->cc($clientes->emailop)
        ->send(new Welcome($pdf2,$ofertas,$clientes,$parameter2,$useremail));     
            
        }
      

        $ofertas->estadoenv = "Enviado";
        $ofertas->fechaenv = $date2;
        $ofertas->save();


        Flash::success("Se ha enviado la oferta ". $ofertas->id . " De forma existosa");

        return back();
 }


 }



