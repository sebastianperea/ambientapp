<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\oferta;
use App\cliente;
use App\peligroso;
use App\aprovechables;
use App\transporte;
use App\posiblecliente;
use App\ofertapc;
use App\Notifications\RepliedToThread;


use App\productoyservicio;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;

class Ofertaspccontroller extends Controller
{
   

public function index (){



$ofertaspc= DB::table('ofertaspc')
                
                ->join('posiblesclientes', 'ofertaspc.identificacion', '=', 'posiblesclientes.identificacion')
                ->select('ofertaspc.*', 'posiblesclientes.razon_social','posiblesclientes.dv','posiblesclientes.departamento','posiblesclientes.ciudad','posiblesclientes.direccion','posiblesclientes.nombres','posiblesclientes.apellidos','posiblesclientes.telefonocelular','email')->orderBy('id','ASC')->paginate(15);
                

                
$ofertas=oferta::orderBy('id' , 'ASC')->paginate(15);




   
    
    

  	$posiblesclientes = DB::table('posiblesclientes')->count();
  	$clientes = DB::table('clientes')->count();
  	
    return view ('comercial.ofertapc.index')
    ->with('posiblesclientes',$posiblesclientes)
    ->with('clientes',$clientes)
    ->with('ofertaspc',$ofertaspc)
    ->with('ofertas',$ofertas);
   


  }



public function create(){



$peligrosos = peligroso::orderBy('id' , 'ASC')->pluck('nombre','id');
$aprovechables = aprovechables::orderBy('id','ASC')->paginate(15);
$transporte = transporte::orderBy('id','ASC')->paginate(15);

 

    return view('comercial.ofertapc.create')
    ->with('peligrosos',$peligrosos)
    ->with('aprovechables',$aprovechables)
    ->with('transporte',$transporte);


  
 


  }



  public function store(Request $request){
   
// dd($request->all());


 // $v = \Validator::make($request->all(), [
            
 //            'nombres' => 'required',
 //            'apellidos' => 'required',
 //            'telefonocelular' => 'required',
 //            'email'    => 'required|email',
 //            'identificacion'    => 'required|integer|unique:posiblesclientes',
 //            'direccion'    => 'required',

 //            'email' => 'required|email',




 //        ]);


 //    if ($v->fails())
 //        {


 //            return redirect()->back()->withInput()->withErrors($v->errors());
 //        }
        

 //        else{
     

           $solicitud= new posiblecliente($request->all());
          
           $solicitud->save();



            $ofertapc= new ofertapc($request->all());

            $ofertapc->save();

                   
           $ofertapc->peligroso()->sync($request->nombre2_);
           
   

auth()->user()->notify(new RepliedToThread());
           
        
           
           
           // App\peligroso::find(1)->ofertapc()->save($peligroso, ['cantidad' => $request->cantidad_]);

           // $ofertapc= ofertapc::find($ofertapc->id);

         

                
           


              // $ofertapc->peligroso()->attach($request->nombre2_ , array('cantidad'=> ));

            Flash::success('La oferta ha sido creada con exito');
            return redirect()->route('ofertasposiblesclientes.create');

           

          



             }




             
   public function edit($id){

    $ofertaspcclientes = DB::table('ofertaspc')
                
                ->join('posiblesclientes', 'ofertaspc.identificacion', '=', 'posiblesclientes.identificacion')
               
                ->select('ofertaspc.*', 'posiblesclientes.razon_social','posiblesclientes.dv','posiblesclientes.departamento','posiblesclientes.ciudad','posiblesclientes.direccion','posiblesclientes.nombres','posiblesclientes.apellidos','posiblesclientes.telefonocelular','email')
                ->where('id', '=', $id)
                ->orderBy('id','ASC')->paginate(15);

                $ofertaspcclientes2 = DB::table('ofertapc_peligroso')
                
                ->join('peligrosos', 'peligrosos.id', '=', 'ofertapc_peligroso.peligroso_id')
                ->join('ofertaspc','ofertaspc.id','=','ofertapc_peligroso.ofertapc_id')
                ->select('ofertapc_peligroso.*', 'peligrosos.nombre','peligrosos.precio', 'ofertaspc.transporte', 'ofertaspc.tipo_vehiculo', 'ofertaspc.desde','ofertaspc.hasta','ofertaspc.tipo','ofertaspc.precio_transporte')
                ->where('ofertapc_id', '=', $id)->orderBy('id','ASC')->paginate(15);



  // dd($ofertaspcclientes2);




   

    $ofertaspc=ofertapc::find($id);
  
    return view('comercial.ofertapc.edit')
    ->with('ofertaspc',$ofertaspc)
    ->with('ofertaspcclientes',$ofertaspcclientes)
    ->with('ofertaspcclientes2',$ofertaspcclientes2);

      
      

    }

    //
}
