<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\oferta;
use App\transporte;
use App\peligroso;
use App\aprovechables;
use App\posconsumos;
use App\peligrososa;
use App\especiales;
use App\otros;
use App\cliente;
use App\pys;
use App\User;
use App\Notifications\NotifyOfertas;
use Laracasts\Flash\Flash;
use Auth;
class Ofertasgcontroller extends Controller
{


   public function index (){

  	$ofertasc= DB::table('ofertas')
                
                ->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')
                ->select('ofertas.*', 'ofertas.id','clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','email','clientes.tipo')->orderBy('created_at','DSC')->get();
    return view ('gerenciaadmin.ofertas.index')->with('ofertasc',$ofertasc);


  }


      public function create(Request $request){

          $clientes = cliente::orderby('identificacion', 'ASC')->paginate(15);
          $peligrosos= peligroso::orderby('id', 'ASC')->paginate(15);
          $aprovechables= aprovechables::orderby('id','ASC')->paginate(15);
          $posconsumos= posconsumos::orderby('id','ASC')->paginate(15);
          $peligrososa= peligrososa::orderby('id', 'ASC')->paginate(15);
          $especiales= especiales::orderby('id','ASC')->paginate(15);
          $transporte = transporte::orderBy('id' , 'ASC')->pluck('tipo','id');
          $peligroso2 = peligroso::orderBy('id' , 'ASC')->pluck('nombre','id');
          $pys = pys::orderby('id','ASC')->get();
          $ofertas=oferta::orderBy('id','ASC')->paginate(15);

          return view('gerenciaadmin.ofertas.create', compact('transporte3'))
          ->with('ofertas',$ofertas)
          ->with('clientes',$clientes)
          ->with('peligrosos',$peligrosos)
          ->with('aprovechables', $aprovechables)
          ->with('posconsumos', $posconsumos)
          ->with('peligrososa', $peligrososa)
          ->with('especiales', $especiales)
          ->with('pys', $pys)
          ->with('peligroso2', $peligroso2);

    }
        


       public function edit($id){

    $clientes = DB::table('ofertas')
                
                ->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')
               
                ->select('ofertas.*', 'clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','email','ofertas.Estado')
                ->where('id', '=', $id)
                ->orderBy('id','ASC')->get();


                $peligrosos= DB::table('oferta_peligroso')
                
                ->join('peligrosos', 'peligrosos.id', '=', 'oferta_peligroso.peligroso_id')
                
                ->select('oferta_peligroso.*', 'peligrosos.nombre','peligrosos.precio', 'oferta_peligroso.peligroso_id','oferta_peligroso.cantidad', 'oferta_peligroso.precio' )
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->paginate(15);


                $aprovechables= DB::table('aprovechables_oferta')
                
                ->join('aprovechables', 'aprovechables.id', '=', 'aprovechables_oferta.aprovechables_id')
                
                ->select('aprovechables_oferta.*', 'aprovechables.nombre','aprovechables.precio', 'aprovechables_oferta.aprovechables_id','aprovechables_oferta.cantidad', 'aprovechables_oferta.precio')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->paginate(15);


                $posconsumos= DB::table('oferta_posconsumos')
                
                ->join('posconsumos', 'posconsumos.id', '=', 'oferta_posconsumos.posconsumos_id')
                
                ->select('oferta_posconsumos.*', 'posconsumos.nombre','posconsumos.precio', 'oferta_posconsumos.precio','oferta_posconsumos.posconsumos_id','oferta_posconsumos.cantidad')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->paginate(15);


                $peligrososa= DB::table('oferta_peligrososa')
                
                ->join('peligrososa', 'peligrososa.id', '=', 'oferta_peligrososa.peligrososa_id')
                
                ->select('oferta_peligrososa.*', 'peligrososa.nombre','peligrososa.precio', 'oferta_peligrososa.peligrososa_id','oferta_peligrososa.cantidad')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->paginate(15);


                $especiales= DB::table('especiales_oferta')
                
                ->join('especiales', 'especiales.id', '=', 'especiales_oferta.especiales_id')
                
                ->select('especiales_oferta.*', 'especiales.nombre','especiales.precio', 'especiales_oferta.especiales_id','especiales_oferta.cantidad', 'especiales_oferta.precio')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->paginate(15);

                 $transportes= DB::table('oferta_transporte')
                
                ->join('transporte', 'transporte.id', '=', 'oferta_transporte.transporte_id')
                
                ->select('oferta_transporte.*', 'transporte.tipo', 'transporte.preciokg','transporte.preciog','oferta_transporte.preciokg','oferta_transporte.preciog', 'oferta_transporte.transporte_id')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();

                  $otros = DB::table('otros')
                
                ->join('ofertas', 'ofertas.id', '=', 'otros.oferta_id')
                
                ->select('otros.*', 'otros.nombreotros', 'otros.cantidadotros')
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->paginate(15);

                  
                  


                $peligroso2 = peligroso::orderBy('id' , 'ASC')->pluck('nombre','id');

                $pys= DB::table('oferta_pys')
                
                ->join('pys', 'pys.id', '=', 'oferta_pys.pys_id')
                
                ->select('oferta_pys.*', 'pys.nombre', 'oferta_pys.pys_id'  )
                ->where('oferta_id', '=', $id)->orderBy('id','ASC')->get();

               $ofertas=oferta::find($id);


  
    return view('gerenciaadmin.ofertas.edit', compact('transporte3'))
    ->with('ofertas',$ofertas)
    ->with('clientes',$clientes)
    ->with('peligrosos',$peligrosos)
    ->with('aprovechables', $aprovechables)
    ->with('posconsumos', $posconsumos)
    ->with('peligrososa', $peligrososa)
    ->with('especiales', $especiales)
    ->with('otros', $otros)
    ->with('transportes',$transportes)
    // ->with('estado', $estado)
    ->with('pys', $pys)
    ->with('peligroso2', $peligroso2);


    }




  
    public function update(Request $request, $id){

  

      $clientes = DB::table('ofertas')
                
                ->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')
                ->select('ofertas.*', 'clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','email')
                ->where('id','=',$id)
                ->update(['dv'=>$request->dv , 'razon_social' =>$request->razon_social , 'departamento' =>$request->departamento, 'ciudad' =>$request->ciudad, 'direccion' =>$request->direccion, 'nombres' =>$request->nombres, 'apellidos' =>$request->apellidos, 'telefonocelular' =>$request->telefonocelular, 'email' =>$request->email]);

                $ofertas= oferta::find($id)->update(['Estado'=>$request->Estado]);


               


        return redirect()->route('ofertasg.index');

      

  }


     public function borraro(Request $request, $id){
                                  


                            $otros = otros::find($id);
                            $otros->delete();

                          return back();

                         } 


           public function peligrosos(Request $request, $id){
                

         
                  $oferta = oferta::find($id);
                       
                        $extra = array_map(function($precio, $cantidad){
                         return ['cantidad' => $cantidad, 'precio' => $precio ];
                          }, $request->peligrososprecio_ , $request->cantidad_);

                        $data = array_combine($request->id_, $extra);   

                        $oferta->peligroso()->sync($data);

                        return back();

              }
                     



                      public function borrarp(Request $request, $idoferta , $peligroso){

                        

                            $oferta = oferta::find($idoferta);
                            

                            $oferta->peligroso()->detach($peligroso);


                         return back();
                         }    
                  


             public function aprovechables(Request $request, $id){
                

         
                  $oferta = oferta::find($id);
                       
                        $extra = array_map(function($precio, $cantidad){
                         return ['cantidad' => $cantidad, 'precio' => $precio ];
                          }, $request->aprovechablesprecio_ , $request->cantidad_);

                        $data = array_combine($request->id_, $extra);   

                        $oferta->aprovechables()->sync($data);

                        return back();

              }




                      public function borrara(Request $request, $idoferta , $aprovechable){


                       $oferta = oferta::find($idoferta);
                            
                        $oferta->aprovechables()->detach($aprovechable);

                        return back();
                         }    



              public function posconsumos(Request $request, $id){
                

         
                  $oferta = oferta::find($id);
                       
                        $extra = array_map(function($precio, $cantidad){
                         return ['cantidad' => $cantidad, 'precio' => $precio ];
                          }, $request->posconsumosprecio_ , $request->cantidad_);

                        $data = array_combine($request->id_, $extra);   

                        $oferta->posconsumos()->sync($data);

                        return back();

              }




                      public function borrarpo(Request $request, $idoferta , $posconsumo){


                       $oferta = oferta::find($idoferta);
                            
                        $oferta->posconsumos()->detach($posconsumo);

                        return back();
                         }  





              public function peligrososa(Request $request, $id){
                

         
                  $oferta = oferta::find($id);
                       
                        $extra = array_map(function($precio, $cantidad){
                         return ['cantidad' => $cantidad, 'precio' => $precio ];
                          }, $request->peligrososaprecio_ , $request->cantidad_);

                        $data = array_combine($request->id_, $extra);   

                        $oferta->peligrososa()->sync($data);

                        return back();

              }




                      public function borrarpa(Request $request, $idoferta , $peligrososa){


                       $oferta = oferta::find($idoferta);
                            
                        $oferta->peligrososa()->detach($peligrososa);

                        return back();
                         }  

                      

                  public function especiales(Request $request, $id){
                

         
                     $oferta = oferta::find($id);
                       
                        $extra = array_map(function($precio, $cantidad){
                         return ['cantidad' => $cantidad, 'precio' => $precio];
                          }, $request->especialesprecio_ , $request->cantidad_);
                        


                        $data = array_combine($request->id_, $extra);   

                        $oferta->especiales()->sync($data);

                        return back();

              }

                     



                      public function borrare(Request $request, $idoferta , $especiales){
                                  
                            $oferta = oferta::find($idoferta);
                            $oferta->especiales()->detach($especiales);
                         return back();

                         }   
                  





              public function transporte(Request $request, $id){

             
                $oferta = oferta::find($id);

                            $extra = array_map(function($preciokg, $preciog){
                             return ['preciokg' => $preciokg, 'preciog' => $preciog];
                              }, $request->transportespreciokg_, $request->transportespreciog_);

                            $data = array_combine($request->id_, $extra);

                            $oferta->transporte()->sync($data);

                            return back();


                  }

               
                       public function borrart(Request $request, $idoferta , $transporte){

                            $oferta = oferta::find($idoferta);
                            $oferta->transporte()->detach($transporte);
                         return back();

                         }   


             public function pys(Request $request, $id){

                
                  $oferta = oferta::find($id);
                       
                        $extra = array_map(function($precio){
                         return ['precio' => $precio ];
                          }, $request->pysprecio_);

                        $data = array_combine($request->id_, $extra);   

                        $oferta->pys()->sync($data);

                        return back();

              }
                     



                      public function borrarpys(Request $request, $idoferta , $pys){
                            $oferta = oferta::find($idoferta);
                            $oferta->pys()->detach($pys);
                         return back();
                         }    
                  


                       public function autocomplete(Request $request){
               
                          $term=$request->term;
                          $data = peligroso::where('nombre','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['id'=>$v->id,'value'=>$v->nombre,'value2'=>$v->precio];
                          }
                            return response()->json($results);
                        }


                        public function autocompletea(Request $request){
               
                          $term=$request->term;
                          $data = aprovechables::where('nombre','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['id'=>$v->id,'value'=>$v->nombre,'value2'=>$v->precio];
                          }
                            return response()->json($results);
                        }


                         public function autocompletepo(Request $request){
               
                          $term=$request->term;
                          $data = posconsumos::where('nombre','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['id'=>$v->id,'value'=>$v->nombre,'value2'=>$v->precio];
                          }
                            return response()->json($results);
                        }



                        public function autocompletepa(Request $request){
               
                          $term=$request->term;
                          $data = peligrososa::where('nombre','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['id'=>$v->id,'value'=>$v->nombre,'value2'=>$v->precio];
                          }
                            return response()->json($results);
                        }


                        public function autocompletee(Request $request){
               
                          $term=$request->term;
                          $data = especiales::where('nombre','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['id'=>$v->id,'value'=>$v->nombre,'value2'=>$v->precio];
                          }
                            return response()->json($results);
                        }


                        public function autocompletec(Request $request){
               
                          $term=$request->term;
                          $data = cliente::where('identificacion','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                           $results[]=['value'=>$v->identificacion,'tipoid' => $v->tipo_de_identificacion,'dv'=>$v->dv,'razonsocial'=>$v->razon_social , 'departamento'=>$v->departamento, 'ciudad' => $v->ciudad, 'direccion' => $v->direccion,'tipo_de_ide' => $v->tipo_de_ide, 'ide' => $v->ide,'cargo' => $v->cargo ,'procesos' => $v->procesos,'tipodepersona' => $v->tipodepersona,'nombre' =>$v->nombres, 'apellidos'=> $v->apellidos, 'telefonocelular'=> $v->telefonocelular, 'email'=> $v->email, 'telefono_contacto' => $v->telefono_contacto, 'ext' => $v->ext];
                          }
                            return response()->json($results);
                        }


                        public function autocompletet(Request $request){


               
                          $term=$request->term;
                          $data = transporte::where('tipo','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['id'=>$v->id,'value'=>$v->tipo,'value2'=>$v->preciokg ,'value3' => $v->preciog];
                          }
                            return response()->json($results);
                        }

                       public function autocompletepys(Request $request){


               
                          $term=$request->term;
                          $data = pys::where('nombre','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['id'=>$v->id,'value'=>$v->nombre,'value2'=>$v->precio];
                          }
                            return response()->json($results);
                        } 


            public function destroy($id){

            	$oferta = oferta::find($id);
            	$oferta = oferta::find($id);
              $oferta->peligroso()->detach();
              $oferta->aprovechables()->detach();
              $oferta->posconsumos()->detach();
              $oferta->peligrososa()->detach();
              $oferta->especiales()->detach();
              $oferta->transporte()->detach();
              $oferta->pys()->detach();
            	$oferta->otros()->delete();
              $oferta->delete();
            	Flash::warning('La oferta ' .$oferta->id. ' a sido borrada de forma existosa');
            	return redirect()->route('ofertasg.index');

            }



  public function store(Request $request){

                    
                     $clientes= cliente::updateOrCreate(['identificacion' => $request->identificacion],
                      ['tipodepersona' => $request->tipodepersona, 'razon_social' => $request->razon_social, 'tipo_de_identificacion' => $request->tipo_de_identificacion, 'dv'=> $request->dv , 'departamento' => $request->departamento, 'ciudad'=>$request->ciudad, 'direccion' => $request->direccion,'ide'=>$request->ide, 'cargo'=>$request->cargo, 'nombres'=>$request->nombres, 'apellidos' =>$request->apellidos, 'telefonocelular' =>$request->telefonocelular, 'email' =>$request->email, 'telefono_contacto' => $request->telefono_contacto, 'ext' =>$request->ext, 'procesos'=>$request->procesos]);

                       if (is_null($request->nombre_ || $request->nombrea_ || $request->nombrepo_ || $request->nombrepa_ || $request->nombree_ || $request->tipo_) || empty($request->nombre_ || $request->nombrea_ || $request->nombrepo_ || $request->nombrepa_ || $request->nombree_ || $request->tipo_ || $request->nombrepys_) ) 
                       {  
                            
                          $ofertas= new oferta($request->all());
                          $ofertas->save();
                          }

                          $ofertas= new oferta($request->all());
                                $ofertas->save();

                          if (!empty($request->nombre_)) {

                           


                                $extra = array_map(function($precio, $cantidad){
                               return ['cantidad' => $cantidad, 'precio' => $precio ];
                                }, $request->peligrososprecio_ , $request->cantidad_);

                              $data = array_combine($request->id_, $extra);   

                              $ofertas->peligroso()->sync($data);

                          }

                          if (!empty($request->nombrea_)) {
                       
                             $extra = array_map(function($precio, $cantidad){
                                   return ['cantidad' => $cantidad, 'precio' => $precio ];
                                    }, $request->aprovechablesprecio_ , $request->cantidada_);

                                    $data = array_combine($request->ida_, $extra);   

                                    $ofertas->aprovechables()->sync($data);
                          }



                           if (!empty($request->nombrepo_)) {
                            
                                  $extra = array_map(function($precio, $cantidad){
                                   return ['cantidad' => $cantidad, 'precio' => $precio ];
                                    }, $request->posconsumosprecio_ , $request->cantidadpo_);

                                    $data = array_combine($request->idpo_, $extra);   

                                    $ofertas->posconsumos()->sync($data);
                          }

                          if (!empty($request->nombrepa_)) {
                            
                                  
                                     $extra = array_map(function($precio, $cantidad){
                                   return ['cantidad' => $cantidad, 'precio' => $precio ];
                                    }, $request->peligrososaprecio_ , $request->cantidadpa_);

                                    $data = array_combine($request->idpa_, $extra);   

                                    $ofertas->peligrososa()->sync($data);
                          }

                          if (!empty($request->nombree_)) {
                            
                                            $extra = array_map(function($precio, $cantidad){
                                   return ['cantidad' => $cantidad, 'precio' => $precio ];
                                    }, $request->especialesprecio_ , $request->cantidade_);

                                    $data = array_combine($request->ide_, $extra);   

                                    $ofertas->especiales()->sync($data);

                          }

                         if (!empty($request->tipo_)) {
                            
                            $extra = array_map(function($preciokg, $preciog){
                             return ['preciokg' => $preciokg, 'preciog' => $preciog];
                              }, $request->transportespreciog_, $request->transportespreciokg_ );

                            $data = array_combine($request->idt_, $extra);

                            $ofertas->transporte()->sync($data);

                            return back();

                          }

                          if (!empty($request->nombrepys_)) {
                            
                                    $extra = array_map(function($precio){
                               return ['precio' => $precio];
                                }, $request->pysprecio_);

                              $data = array_combine($request->idpys_, $extra);   

                              $ofertas->pys()->sync($data);

                          }
 
                         
$user = Auth::User();

                       
                          
        $oferta = oferta::find($ofertas->id);


     $clientes->notify(new NotifyOfertas($oferta));
                
              
  	 

Flash::success("Se ha Registrado la oferta ". $ofertas->id . " De forma existosa");
    return redirect()->route('ofertasg.index');
    
  	
  }


   

}
