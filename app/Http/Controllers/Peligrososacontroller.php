<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\peligrososa;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;

class Peligrososacontroller extends Controller
{
  

  public function index (){



		$peligrososa= peligrososa::orderBy('id','ASC')->paginate(15);
                

  	
		    return view ('comercial.residuos.peligrososa.index')
		    ->with('peligrososa',$peligrososa);
   

  }



public function create(){

    return view('comercial.residuos.peligrososa.create');

  }



  public function store(Request $request){

    
   
           $peligrososa= new peligrososa($request->all());
          
           $peligrososa->save();


            Flash::success('El residuo Peligroso Aprovechable ah sido creado con exito');
            return back();

             }


 
    

             
   public function edit($id){

    $peligrososa= peligrososa::find($id);
  
    return view('comercial.residuos.peligrososa.edit')
    ->with('peligrososa',$peligrososa);

      
      

    }  


    public function update(Request $request, $id){
          
        $peligrososa= peligrososa::find($id);
        
         $peligrososa->nombre = $request->nombre;
         $peligrososa->precio = $request->precio;
         $peligrososa->preciou = $request->preciou;
         $peligrososa->preciog = $request->preciog;
         $peligrososa->pretratamiento = $request->pretratamiento;
        $peligrososa->save();
        Flash::success('El residuo Peligroso Aprovechable ah sido actualizado con exito');
        return back ();

      

    }
  
}
