<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\cliente;
use App\visitas;
use App\cbancariapj;
use App\oferta;
use App\sedes;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;
use Auth;


class VisitasEjdecuentaController extends Controller
{

  public function index(Request $request){

    

    $idu = Auth::id();

    $procesos =  DB::table('visitas')
                
                ->join('clientes', 'visitas.identificacion', '=', 'clientes.identificacion')
                ->select('visitas.*', 'visitas.id','clientes.razon_social','clientes.dv','clientes.departamento','clientes.ciudad','clientes.direccion','clientes.nombres','clientes.apellidos','clientes.telefonocelular','clientes.email','clientes.tipo')->where('id_user', '=', $idu)->orderBy('created_at','DSC')->get();

                
    return view ('ejdecuenta.visitas.index')->with('procesos',$procesos);
                                     
  }

    public function create(Request $request){

    $clientes = cliente::orderby('identificacion', 'ASC')->first();

    return view('ejdecuenta.visitas.create') 
    ->with('clientes', $clientes);
                                     

  }
  public function store(Request $request){
    $idu = Auth::id();


           $visitas= new visitas($request->all());
           $visitas->id_user = Auth::id();
           $visitas->save();

            

               if ($request->ejdecuenta == '0') {

                    $clientes= cliente::updateOrCreate(['identificacion' => $request->identificacion],
                      ['razon_social' => $request->razon_social, 'tipo_de_identificacion' => $request->tipo_de_identificacion, 'dv'=> $request->dv , 'departamento' => $request->departamento, 'ciudad'=>$request->ciudad, 'direccion' => $request->direccion, 'cargo'=>$request->cargo, 'nombres'=>$request->nombres, 'apellidos' =>$request->apellidos, 'email' =>$request->email, 'telefono_contacto' => $request->telefono_contacto, 'ext' =>$request->ext, 'procesos'=>$request->procesos, 'ejdecuenta' => $idu,'canal' => $request->canal]);
                    
                  }

                  else{
                      $clientes= cliente::updateOrCreate(['identificacion' => $request->identificacion],
                      ['razon_social' => $request->razon_social, 'tipo_de_identificacion' => $request->tipo_de_identificacion, 'dv'=> $request->dv , 'departamento' => $request->departamento, 'ciudad'=>$request->ciudad, 'direccion' => $request->direccion,'cargo'=>$request->cargo, 'nombres'=>$request->nombres, 'apellidos' =>$request->apellidos, 'email' =>$request->email, 'telefono_contacto' => $request->telefono_contacto, 'ext' =>$request->ext, 'procesos'=>$request->procesos,'canal' => $request->canal]);
                  }

            Flash::success('La visita '.$visitas->id .' ha sido creada con exito');
            return back();
           }

           public function view(Request $request, $idvisita){

            $auth= Auth::id();

            $ejauth = visitas::find($idvisita);

          if($ejauth->id_user == $auth ){
            $visitas = DB::table('visitas')
                ->join('clientes', 'clientes.identificacion', '=', 'visitas.identificacion')
                ->select('visitas.*', 'clientes.razon_social')
                ->where('id', '=', $idvisita)->first();

              return view('ejdecuenta.visitas.view')
              ->with('idvisita', $idvisita)
              ->with('visitas', $visitas);

                  }

                  else{

                    dd('No tienes permisos para acceder a este proceso comercial');
                    
                    }
                  }


    

             
          }
