<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\cliente;
use App\cbancariapj;
use App\oferta;
use App\sedes;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Laracasts\Flash\Flash;

class ClientesgController extends Controller
{
 
public function index (Request $request){

        $clientes= cliente::orderby('identificacion', 'ASC')->get();
  	  	return view ('gerenciaadmin.clientes.index')->with('clientes', $clientes);
  }


      public function edit($identificacion){

        $clientes= cliente::find($identificacion);

      return view ('gerenciaadmin.clientes.edit')->with('clientes', $clientes);

    }


     public function update(Request $request, $identificacion){
          
        $clientes= cliente::find($identificacion);
         $clientes->dv = $request->dv;
         $clientes->razon_social = $request->razon_social;
         $clientes->departamento = $request->departamento;
         $clientes->ciudad = $request->ciudad;
         $clientes->direccion = $request->direccion;
         $clientes->telefono_contacto = $request->telefono_contacto;
         $clientes->ext = $request->ext;
         $clientes->nombres = $request->nombres;
         $clientes->apellidos = $request->apellidos;
         $clientes->telefonocelular = $request->telefonocelular;
         $clientes->email = $request->email;
         $clientes->cargo = $request->cargo;
        $clientes->save();

        return back ();

      

    }




  public function destroy($identificacion){

  	$clientes = cliente::find($identificacion);

  	$clientes->delete();
  	Flash::warning('El Cliente' .$cliente->identificacion. ' ah sido borrado de forma existosa');
  	return redirect()->route('admin.clientes.clientes');

  }

  public function store(Request $request){



   $clientes= new cliente($request->all());
    
    $clientes->save();

  	

Flash::success("Se ha Registrado el cliente ". $clientes->razon_social. " De forma existosa");
    return redirect()->route('comercial.clientes.index');
    
  	
  }

   public function borrarsede(Request $request, $sedes){

              $sede = sedes::find($sedes);
              $sede->delete();         

            Flash::success("Se ha Borrado la sede ". $sedes. " De forma existosa");
                return back();
    
    
  }


  public function save(Request $request, $identificacion){

        $num_elements = 0;
                $sqlData = []; 
                $input= $request->all();
                      while($num_elements < count($input['nombre_'])){
                        $sqlData[] = array(
                            'identificacion' => $identificacion,
                             'nombre'         => $input['nombre_'][$num_elements],
                            'direccion'          => $input['direccion_'][$num_elements],
                            'departamento'     => $input['departamento_'][$num_elements],
                            'ciudad'      => $input['ciudad_'][$num_elements],
                            'created_at'    => Carbon::now(), // only if your table has this column
                            'updated_at'    => Carbon::now(), // only if your table has this column
                        );
                      $num_elements++;
                 }

      sedes::insert($sqlData);      

            Flash::success("Se ha agregado la sede de forma existosa");
                return back();
    
    
  }


   public function show(Request $request, $identificacion){

                $clientes = cliente::find($identificacion);

                  $ofertas = DB::table('ofertas')
                
                ->join('clientes', 'ofertas.identificacion', '=', 'clientes.identificacion')
               
                ->select('ofertas.*','ofertas.id','ofertas.estado','ofertas.created_at')
                ->where('ofertas.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $cbancariapj = DB::table('cbancariapj')
                
                ->join('clientes', 'cbancariapj.cliente_id', '=', 'clientes.identificacion')
               
                ->select('cbancariapj.*','cbancariapj.id','cbancariapj.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $cerlpj = DB::table('cerlpj')
                
                ->join('clientes', 'cerlpj.cliente_id', '=', 'clientes.identificacion')
               
                ->select('cerlpj.*','cerlpj.id','cerlpj.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();


                $fotocopiacpj = DB::table('fotocopiacpj')
                
                ->join('clientes', 'fotocopiacpj.cliente_id', '=', 'clientes.identificacion')
               
                ->select('fotocopiacpj.*','fotocopiacpj.id','fotocopiacpj.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $rutpj = DB::table('rutpj')
                
                ->join('clientes', 'rutpj.cliente_id', '=', 'clientes.identificacion')
               
                ->select('rutpj.*','rutpj.id','rutpj.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $drentapj = DB::table('drentapj')
                
                ->join('clientes', 'drentapj.cliente_id', '=', 'clientes.identificacion')
               
                ->select('drentapj.*','drentapj.id','drentapj.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $estadosfinancieros = DB::table('estadosfinancieros')
                
                ->join('clientes', 'estadosfinancieros.cliente_id', '=', 'clientes.identificacion')
               
                ->select('estadosfinancieros.*','estadosfinancieros.id','estadosfinancieros.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $cbancariapn = DB::table('cbancariapn')
                
                ->join('clientes', 'cbancariapn.cliente_id', '=', 'clientes.identificacion')
               
                ->select('cbancariapn.*','cbancariapn.id','cbancariapn.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();


                $cmpn = DB::table('cmpn')
                
                ->join('clientes', 'cmpn.cliente_id', '=', 'clientes.identificacion')
               
                ->select('cmpn.*','cmpn.id','cmpn.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $drentapn = DB::table('drentapn')
                
                ->join('clientes', 'drentapn.cliente_id', '=', 'clientes.identificacion')
               
                ->select('drentapn.*','drentapn.id','drentapn.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

               $fotocopiacpn = DB::table('fotocopiacpn')
                
                ->join('clientes', 'fotocopiacpn.cliente_id', '=', 'clientes.identificacion')
               
                ->select('fotocopiacpn.*','fotocopiacpn.id','fotocopiacpn.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

                $rutpn = DB::table('rutpn')
                
                ->join('clientes', 'rutpn.cliente_id', '=', 'clientes.identificacion')
               
                ->select('rutpn.*','rutpn.id','rutpn.nombre')
                ->where('clientes.identificacion', '=', $identificacion)
                ->orderBy('id','ASC')->get();

               $filespeligrosos= DB::table('filespeligrosos_residuos')
                
                ->join('filespeligrosos', 'filespeligrosos.id', '=', 'filespeligrosos_residuos.filespeligrosos_id')
                ->join('residuos', 'residuos.id', '=', 'filespeligrosos_residuos.residuos_id')
                
                ->select('filespeligrosos_residuos.*', 'filespeligrosos.nombre')

                ->where('residuos.identificacion', '=', $identificacion)->orderBy('id','ASC')->get();

                $filespeligrososa= DB::table('filespeligrososa_residuos')
                
                ->join('filespeligrososa', 'filespeligrososa.id', '=', 'filespeligrososa_residuos.filespeligrososa_id')
                ->join('residuos', 'residuos.id', '=', 'filespeligrososa_residuos.residuos_id')
                
                ->select('filespeligrososa_residuos.*', 'filespeligrososa.nombre')

                ->where('residuos.identificacion', '=', $identificacion)->orderBy('id','ASC')->get();

                $especiales3= DB::table('filese_residuos')
                
                ->join('filese', 'filese.id', '=', 'filese_residuos.filese_id')
                ->join('residuos', 'residuos.id', '=', 'filese_residuos.residuos_id')
                
                ->select('filese_residuos.*', 'filese.nombre')

                ->where('residuos.identificacion', '=', $identificacion)->orderBy('id','ASC')->get();

                $sedes= DB::table('sedes')
                
                ->join('clientes', 'clientes.identificacion', '=', 'sedes.identificacion')
                
                ->select('sedes.*', 'sedes.nombre', 'sedes.direccion', 'sedes.departamento', 'sedes.ciudad')

                ->where('sedes.identificacion', '=', $identificacion)->orderBy('id','ASC')->get();


                $tab = oferta::with('peligroso','aprovechables','posconsumos','peligrososa','especiales')->where('identificacion','=',$identificacion)->first();
     

              if (count($tab->peligroso)>0) {
                  foreach ($tab->peligroso as $peligro) {
                  $peligroso = $peligro->get();

                  }
                } 

                else{

                  $peligroso = null;
                } 
                
                       if (count($tab->aprovechable)>0) {
                         foreach ($tab->aprovechables as $aprovechable) {
                          $aprovechable = $aprovechable->get();
                        }
                       }
                       else{
                        $aprovechable = null;
                       }
                        

                       if (count($tab->posconsumos)>0) {
                         foreach ($tab->posconsumos as $pos) {
                          $posconsumo = $pos->get();

                        }
                       }

                       else{
                        $posconsumo = null;
                       }
                         

                      if (count($tab->peligrososa)>0) {
                        foreach ($tab->peligrososa as $pa) {
                          $peligrosoa = $pa->get();

                        }
                      }

                      else{
                        $peligrosoa = null;
                      }

                      if (count($tab->especiales)>0) {
                        foreach ($tab->especiales as $e) {
                          $especiales2 = $e->get();
                        }
                      }

                      else{

                        $especiales2 = null;
                      }
    
                        

               
                  return view ('gerenciaadmin.clientes.view')
                  ->with('clientes',$clientes)
                  ->with('ofertas', $ofertas)
                  ->with('cbancariapj', $cbancariapj)
                  ->with('cerlpj', $cerlpj)
                  ->with('fotocopiacpj', $fotocopiacpj)
                  ->with('rutpj', $rutpj)
                  ->with('drentapj', $drentapj)
                  ->with('estadosfinancieros', $estadosfinancieros)
                  ->with('cbancariapn', $cbancariapn)
                  ->with('cmpn', $cmpn)
                  ->with('drentapn', $drentapn)
                  ->with('fotocopiacpn', $fotocopiacpn)
                  ->with('aprovechable', $aprovechable)
                  ->with('posconsumo', $posconsumo)
                   ->with('peligrosoa', $peligrosoa)
                   ->with('especiales2', $especiales2)
                   ->with('peligroso', $peligroso)
                   ->with('filespeligrososa', $filespeligrososa)
                   ->with('filespeligrosos', $filespeligrosos)
                   ->with('especiales3', $especiales3)
                  ->with('sedes',$sedes)
                  ->with('rutpn', $rutpn);
              


          // Flash::success("Se ha Registrado el cliente ". $clientes->razon_social. " De forma existosa");
          //     return redirect()->route('comercial.clientes.index');
              
              
            }


           

    //
}

