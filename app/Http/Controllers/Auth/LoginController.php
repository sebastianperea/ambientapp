<?php

namespace App\Http\Controllers\Auth;
use Datetime;
use Laracasts\Flash\Flash;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

     
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

     public function redirectPath()
    {



        $identificacion = auth()->user()->identificacion;
        if(\Auth::check()){


        if (auth()->user()->type == 'cliente' && auth()->user()->lastlogin !== '0000-00-00 00:00:00') {
             auth()->user()->lastlogin = new DateTime();
            auth()->user()->save();

           return route('cliente', $identificacion);
        }

        elseif(auth()->user()->type == 'cliente'){

             auth()->user()->lastlogin = new DateTime();
        auth()->user()->save();

            Flash::warning('Por favor cambie la contraseña en la etiqueta usuarios contraseña. Muchas gracias por confiar en nosotros');    
            return route('cliente.edit', $identificacion);

        }

        if (auth()->user()->type == 'comercial') {
            return '/comercial';
             auth()->user()->lastlogin = new DateTime();
            auth()->user()->save();
        }

        if (auth()->user()->type == 'gerenciaadmin') {
            return '/geradmin';
             auth()->user()->lastlogin = new DateTime();
            auth()->user()->save();
        }

        $iduser = auth()->user()->id;
        if (auth()->user()->type == 'ejdecuenta') {
            auth()->user()->lastlogin = new DateTime();
            auth()->user()->save();
           return route('ejdecuenta', $iduser);
        }

        $iduser = auth()->user()->id;
        if (auth()->user()->type == 'facturacion') {
            auth()->user()->lastlogin = new DateTime();
            auth()->user()->save();
            return '/facturacion';    
        }

        $iduser = auth()->user()->id;
        if (auth()->user()->type == 'posconsumos') {
            auth()->user()->lastlogin = new DateTime();
            auth()->user()->save();
            return '/pos';    
        }
   }

   

}

}


