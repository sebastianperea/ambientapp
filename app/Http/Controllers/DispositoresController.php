<?php

namespace App\Http\Controllers;
use App\dispositores;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;


class DispositoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

         $dispositores= dispositores::orderBy('id','ASC')->get();
        return view('comercial.residuos.dispositores.index')->with('dispositores', $dispositores);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        $dispositor= new dispositores($request->all());
        $dispositor->categoria= json_encode($request->categoria);
         
        $dispositor->save();

         Flash::success("Se ha Registrado el dispositor ". $dispositor->nombre." de forma existosa");
        return back ();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    
    //  $dispositor = dispositores::find($id);

    // $dispositor->delete();
    // Flash::warning('El dispositor ' .$dispositor->id. ' ha sido borrada de forma existosa');
    // return back ();
    }
}
