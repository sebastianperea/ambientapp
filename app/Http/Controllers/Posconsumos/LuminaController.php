<?php

namespace App\Http\Controllers\Posconsumos;
use App\Http\Controllers\Controller;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\generadores;
use App\gestores;
use App\canales;
use App\tipos_tecnologia;
use App\ingresos_lumina;
use Carbon\Carbon;
use Laracasts\Flash\Flash;
use App\Mail\CertLumina as CertLumina;
use Illuminate\Support\Facades\Mail;



class LuminaController extends Controller
{
    public function index()
    {

      $certificados= DB::table('ingresos_lumina_tipos_tecnologia')
                
                ->join('ingresos_lumina', 'ingresos_lumina_tipos_tecnologia.ingresos_lumina_id', '=', 'ingresos_lumina.id')
                ->select('ingresos_lumina_tipos_tecnologia.*', DB::raw('SUM(pesodfkg) as pesodfkg'),'ingresos_lumina.remision','ingresos_lumina.fecha_recepcion','ingresos_lumina.estado_ingreso', 'ingresos_lumina.estado')->groupBy('ingresos_lumina_id')->orderBy('created_at','DSC')->get();



      return view('posconsumos.lumina.index')->with('certificados',$certificados);
        
    }

    public function edit($id){

      $datos= DB::table('ingresos_lumina')
        ->join('generadores', 'ingresos_lumina.generador', '=', 'generadores.id')
        ->join('gestores', 'ingresos_lumina.gestor', 'gestores.id')
        ->select('ingresos_lumina.*', 'generadores.razon_social', 'generadores.identificacion', 'generadores.dv', 'generadores.departamento', 'generadores.ciudad', 'generadores.direccion', 'generadores.telefono', 'generadores.telefono2', 'generadores.responsable_envio', 'generadores.correo_electronico','generadores.correo2')
        ->where('ingresos_lumina.id', $id)->first();

        
      $canales = canales::orderby('id', 'ASC')->pluck('nombre','id');
        $gestores = gestores::orderby('id', 'ASC')->pluck('razonsocial','id');
        $generadores =  generadores::orderby('id', 'ASC')->pluck('razon_social','id');
        $tecnologia = tipos_tecnologia::orderby('id', 'ASC')->get();

       $pesos= DB::table('ingresos_lumina_tipos_tecnologia')
                
                ->join('tipos_de_tecnologia', 'tipos_de_tecnologia.id', '=', 'ingresos_lumina_tipos_tecnologia.tipos_tecnologia_id')
                
                ->select('ingresos_lumina_tipos_tecnologia.*', 'tipos_de_tecnologia.nombre','tipos_de_tecnologia.clasificacion_cretib', 'tipos_de_tecnologia.clasificacion_dec1076', 'tipos_de_tecnologia.estado')
                ->where('ingresos_lumina_id', '=', $id)->orderBy('id','ASC')->get();

                


      return view('posconsumos.lumina.edit')
      ->with('canales', $canales)
      ->with('gestores', $gestores)
      ->with('generadores', $generadores)
      ->with('tecnologia', $tecnologia)
      ->with('datos', $datos)
      ->with('pesos', $pesos);
    }

    public function update(Request $request)
    {


      $fecha= Carbon::now()->format('d-m-Y');
      
        DB::table('ingresos_lumina')->whereIn('id', $request->id)->update(array('estado_ingreso' => 'APROVECHADO', 'fecha_aprovechamiento' => $fecha));


    $gestores = DB::table('ingresos_lumina')
                
                ->join('gestores', 'gestores.id', '=', 'ingresos_lumina.gestor')
                
                ->select('ingresos_lumina.*', 'gestores.razonsocial', 'gestores.identificacion', 'gestores.dv', 'gestores.direccion', 'gestores.ciudad')
                ->whereIn('ingresos_lumina.id', $request->id)->orderBy('id','ASC')->get();


    $conexion = DB::table('ingresos_lumina')
                
                ->join('generadores', 'generadores.id', '=', 'ingresos_lumina.generador')
                ->join('gestores', 'gestores.id', '=', 'ingresos_lumina.gestor')
                ->select('ingresos_lumina.*', 'generadores.razon_social', 'generadores.identificacion as nit', 'generadores.dv as digv', 'generadores.direccion as direc', 'generadores.ciudad as city', 'generadores.correo_electronico', 'gestores.razonsocial', 'gestores.identificacion', 'gestores.dv', 'gestores.direccion', 'gestores.ciudad')
                ->whereIn('ingresos_lumina.id', $request->id)->get();


      foreach ($conexion as $cone) {



        $header =view('pdf.header');
    $footer =view('pdf.footer');
    $date = Carbon::now()->toFormattedDateString();

    $tipostecnologia = DB::table('ingresos_lumina_tipos_tecnologia')
                
                ->join('tipos_de_tecnologia', 'tipos_de_tecnologia.id', '=', 'ingresos_lumina_tipos_tecnologia.tipos_tecnologia_id')
                
                ->select('ingresos_lumina_tipos_tecnologia.*', 'tipos_de_tecnologia.nombre', 'tipos_de_tecnologia.clasificacion_cretib', 'tipos_de_tecnologia.clasificacion_dec1076', 'tipos_de_tecnologia.estado')
                ->where('ingresos_lumina_id', $cone->id)->orderBy('id','ASC')->get();
        
        $pdf = PDF::loadView('posconsumos.lumina.pdf.env',compact('cone', $cone, 'tipostecnologia', $tipostecnologia))->setPaper('letter')->setOrientation('portrait')->setOption('dpi', 300);
          $pdf->setOption('header-html', $header)->setOption('margin-top',28);
        $pdf->setOption('footer-html', $footer)->setOption('margin-bottom',30)->setOption('margin-left',0);

    
        $pdf2= $pdf->download();
        
         Mail::to($cone->correo_electronico)
        ->send(new CertLumina($pdf2));
      }
               

     Flash::success(" Se han guardado y enviado los certificados de forma exitosa ");

         return back();

        
    }


    public function store(Request $request)
    { 


      setlocale(LC_ALL, 'es_ES');
        $fecha = Carbon::now();
        $fecha->format("F"); // Inglés.
        $mes = $fecha->formatLocalized('%B');// mes en idioma español

        $month = str_limit($mes, 3, '-');

        if ($request->idgen == '') {

          // $lastValue = DB::table('generadores')->orderBy('id', 'desc')->first();

           $generador = new generadores ;

           $generador->razon_social = $request->razon_social;
           $generador->identificacion = $request->identificacion;
           $generador->dv = $request->dv;
            $generador->departamento = $request->departamento;
            $generador->ciudad = $request->ciudad;
            $generador->direccion = $request->direccion;

            $generador->direccion = $request->direccion;
            $generador->telefono = $request->telefono;

            $generador->telefono2 = $request->telefono2;

            $generador->responsable_envio = $request->responsable_envio;

            $generador->responsable_envio = $request->responsable_envio;

            $generador->correo_electronico = $request->correoelectronico;

            $generador->correo2 = $request->correo2;

            $generador ->save();  

            $ingresos_lumina = new ingresos_lumina($request->all());

          $ingresos_lumina->prefijo = 'PLEC';
          $ingresos_lumina->prefijo2 =strtoupper($month);
          $ingresos_lumina->fecha_recepcion = $request->fecha_recepcion;
          $ingresos_lumina ->generador = $generador->id;
          $ingresos_lumina ->gestor = $request->gestor;

          $ingresos_lumina->save();

        }

        else{

        $generador= generadores::updateOrCreate(['id' => $request->idgen],



                                ['razon_social' => $request->razon_social, 'identificacion' => $request->identificacion, 'dv'=> $request->dv , 'departamento' => $request->departamento, 'ciudad'=>$request->ciudad, 'direccion' => $request->direccion, 'telefono'=>$request->telefono,'telefono2'=>$request->telefono2,'responsable_envio'=>$request->responsable_envio, 'correo_electronico' =>$request->correoelectronico, 'correo2' =>$request->correo2]);


                    $ingresos_lumina = new ingresos_lumina($request->all());

                    $ingresos_lumina->prefijo = 'PLEC';
                    $ingresos_lumina->prefijo2 =strtoupper($month);
                    $ingresos_lumina->fecha_recepcion = $request->fecha_recepcion;
                    $ingresos_lumina ->generador = $request->idgen;
                    $ingresos_lumina ->gestor = $request->gestor;

                    $ingresos_lumina->save();

                }
        



          $ingresos = $ingresos_lumina->id;
                       
                        $extra = array_map(function($pesorkg, $pesodfkg){
                         return ['pesorkg' => $pesorkg, 'pesodfkg' => $pesodfkg  ];
                          }, $request->pesorkg , $request->pesodfkg);

                        $data = array_combine($request->id, $extra);   

                        $ingresos_lumina->tipos_tecnologia()->sync($data);

                         Flash::success(" Se han guardado los datos de forma exitosa ");

                         return back();

                            }
    


public function certpdf($id)
    {
        
    $fecha= Carbon::now()->format('d-m-Y');

    $certificados = ingresos_lumina::find($id);

    $gestores = DB::table('ingresos_lumina')
                
                ->join('gestores', 'gestores.id', '=', 'ingresos_lumina.gestor')
                
                ->select('ingresos_lumina.*', 'gestores.razonsocial', 'gestores.identificacion', 'gestores.dv', 'gestores.direccion', 'gestores.ciudad')
                ->where('ingresos_lumina.id', '=', $id)->orderBy('id','ASC')->first();

    $conexion = DB::table('ingresos_lumina')
                
                ->join('generadores', 'generadores.id', '=', 'ingresos_lumina.generador')
                
                ->select('ingresos_lumina.*', 'generadores.razon_social', 'generadores.identificacion', 'generadores.dv', 'generadores.direccion', 'generadores.ciudad')
                ->where('ingresos_lumina.id', '=', $id)->orderBy('id','ASC')->first();


     $tipostecnologia = DB::table('ingresos_lumina_tipos_tecnologia')
                
                ->join('tipos_de_tecnologia', 'tipos_de_tecnologia.id', '=', 'ingresos_lumina_tipos_tecnologia.tipos_tecnologia_id')
                
                ->select('ingresos_lumina_tipos_tecnologia.*', 'tipos_de_tecnologia.nombre', 'tipos_de_tecnologia.clasificacion_cretib', 'tipos_de_tecnologia.clasificacion_dec1076', 'tipos_de_tecnologia.estado')
                ->where('ingresos_lumina_id', '=', $id)->orderBy('id','ASC')->get();




     $header =view('pdf.header');
    $footer =view('pdf.footer');
    $date = Carbon::now()->toFormattedDateString();
        
        $pdf = PDF::loadView('posconsumos.lumina.pdf.certificados',compact('certificados', $certificados, 'tipostecnologia', $tipostecnologia, 'conexion', $conexion, 'gestores', $gestores,'fecha'))->setPaper('letter')->setOrientation('portrait')->setOption('dpi', 300);
          $pdf->setOption('header-html', $header)->setOption('margin-top',28);
        $pdf->setOption('footer-html', $footer)->setOption('margin-bottom',30)->setOption('margin-left',0);


        return $pdf->stream($certificados->prefijo. $certificados->prefijo2. $certificados->id , array("Attachment" => false));

        
    }


    public function certalmcpdf($id)
    {

      
      $fecha= Carbon::now()->format('d-m-Y');

    $certificados = ingresos_lumina::find($id);

    $gestores = DB::table('ingresos_lumina')
                
                ->join('gestores', 'gestores.id', '=', 'ingresos_lumina.gestor')
                
                ->select('ingresos_lumina.*', 'gestores.razonsocial', 'gestores.identificacion', 'gestores.dv', 'gestores.direccion', 'gestores.ciudad')
                ->where('ingresos_lumina.id', '=', $id)->orderBy('id','ASC')->first();

    $conexion = DB::table('ingresos_lumina')
                
                ->join('generadores', 'generadores.id', '=', 'ingresos_lumina.generador')
                
                ->select('ingresos_lumina.*', 'generadores.razon_social', 'generadores.identificacion', 'generadores.dv', 'generadores.direccion', 'generadores.ciudad')
                ->where('ingresos_lumina.id', '=', $id)->orderBy('id','ASC')->first();


     $tipostecnologia = DB::table('ingresos_lumina_tipos_tecnologia')
                
                ->join('tipos_de_tecnologia', 'tipos_de_tecnologia.id', '=', 'ingresos_lumina_tipos_tecnologia.tipos_tecnologia_id')
                
                ->select('ingresos_lumina_tipos_tecnologia.*', 'tipos_de_tecnologia.nombre', 'tipos_de_tecnologia.clasificacion_cretib', 'tipos_de_tecnologia.clasificacion_dec1076', 'tipos_de_tecnologia.estado')
                ->where('ingresos_lumina_id', '=', $id)->orderBy('id','ASC')->get();


     $header =view('pdf.header');
    $footer =view('pdf.footer');
    $date = Carbon::now()->toFormattedDateString();
        
        $pdf = PDF::loadView('posconsumos.lumina.pdf.almacenamiento',compact('certificados', $certificados, 'tipostecnologia', $tipostecnologia, 'conexion', $conexion, 'gestores', $gestores,  'fecha'))->setPaper('letter')->setOrientation('portrait')->setOption('dpi', 300);
          $pdf->setOption('header-html', $header)->setOption('margin-top',28);
        $pdf->setOption('footer-html', $footer)->setOption('margin-bottom',30)->setOption('margin-left',0);

        
        return $pdf->stream($certificados->prefijo. $certificados->prefijo2. $certificados->id."-".$certificados->remision.$certificados->razonsocial .".pdf" , array("Attachment" => false));
        
    }

    public function certalmcpdfdown($id){

      $fecha= Carbon::now()->format('d-m-Y');

    $certificados = ingresos_lumina::find($id);

    $gestores = DB::table('ingresos_lumina')
                
                ->join('gestores', 'gestores.id', '=', 'ingresos_lumina.gestor')
                
                ->select('ingresos_lumina.*', 'gestores.razonsocial', 'gestores.identificacion', 'gestores.dv', 'gestores.direccion', 'gestores.ciudad')
                ->where('ingresos_lumina.id', '=', $id)->orderBy('id','ASC')->first();

    $conexion = DB::table('ingresos_lumina')
                
                ->join('generadores', 'generadores.id', '=', 'ingresos_lumina.generador')
                
                ->select('ingresos_lumina.*', 'generadores.razon_social', 'generadores.identificacion', 'generadores.dv', 'generadores.direccion', 'generadores.ciudad')
                ->where('ingresos_lumina.id', '=', $id)->orderBy('id','ASC')->first();


     $tipostecnologia = DB::table('ingresos_lumina_tipos_tecnologia')
                
                ->join('tipos_de_tecnologia', 'tipos_de_tecnologia.id', '=', 'ingresos_lumina_tipos_tecnologia.tipos_tecnologia_id')
                
                ->select('ingresos_lumina_tipos_tecnologia.*', 'tipos_de_tecnologia.nombre', 'tipos_de_tecnologia.clasificacion_cretib', 'tipos_de_tecnologia.clasificacion_dec1076', 'tipos_de_tecnologia.estado')
                ->where('ingresos_lumina_id', '=', $id)->orderBy('id','ASC')->get();




     $header =view('pdf.header');
    $footer =view('pdf.footer');
    $date = Carbon::now()->toFormattedDateString();
        
        $pdf = PDF::loadView('posconsumos.lumina.pdf.almacenamiento',compact('certificados', $certificados, 'tipostecnologia', $tipostecnologia, 'conexion', $conexion, 'gestores', $gestores, 'fecha'))->setPaper('letter')->setOrientation('portrait')->setOption('dpi', 300);
          $pdf->setOption('header-html', $header)->setOption('margin-top',28);
        $pdf->setOption('footer-html', $footer)->setOption('margin-bottom',30)->setOption('margin-left',0);



        return $pdf->download($certificados->prefijo. $certificados->prefijo2. $certificados->id."-".$certificados->remision.$certificados->razonsocial .".pdf");

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $canales = canales::orderby('id', 'ASC')->pluck('nombre','id');
    		$gestores = gestores::orderby('id', 'ASC')->pluck('razonsocial','id');
    		$generadores =  generadores::orderby('id', 'ASC')->pluck('razon_social','id');
        $tecnologia = tipos_tecnologia::orderby('id', 'ASC')->get();

            return view('posconsumos.lumina.create')
            ->with('generadores', $generadores)
            ->with('gestores', $gestores)
            ->with('tecnologia', $tecnologia)
            ->with('canales', $canales);
    }


public function autocompletegen(Request $request){
               
                           $term=$request->term;
                          $data = generadores::where('razon_social','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['value'=>$v->razon_social,'identificacion'=>$v->identificacion,'dv'=>$v->dv, 'direccion'=>$v->direccion, 'telefono'=>$v->telefono, 'responsableenvio'=>$v->responsable_envio,
                                'correoelectronico'=>$v->correo_electronico,'correo2'=>$v->correo2, 'departamento'=>$v->departamento, 'ciudad'=>$v->ciudad, 'idgen'=>$v->id, 'telefono2'=>$v->telefono2];
                          }
                            return response()->json($results);

                          }


  public function autocompleteges(Request $request){
               
                           $term=$request->term;
                          $data = gestores::where('razon_social','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['value'=>$v->razon_social,'identificacion'=>$v->identificacion,'dv'=>$v->dv, 'direccion'=>$v->direccion, 'telefono'=>$v->telefono,'contactogestor'=>$v->contacto_gestor, 
                                'correoelectronico'=>$v->correo, 'departamento'=>$v->departamento, 'ciudad'=>$v->ciudad, 'id'=>$v->id];
                          }
                            return response()->json($results);

                          }


      public function autocompletecanal(Request $request){
               
                           $term=$request->term;
                          $data = canales::where('nombre','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['value'=>$v->nombre];
                          }
                            return response()->json($results);

                          }
   

    }

