<?php

namespace App\Http\Controllers\Posconsumos;
use App\Http\Controllers\Controller;
use App\generadores;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Imports\GeneradoresImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\GeneradoresExport;


class GeneradoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $generadores = generadores::orderBy('id','ASC')->get();

        
        return view('posconsumos.generadores.index')
        ->with('generadores', $generadores);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('posconsumos.generadores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function import(Request $request) 
    {

        Excel::import(new GeneradoresImport,request()->file('file'));
           Flash::success(" Se han importado todos los datos de manera exitosa");
        return back();
    }

    public function export() 
    {
        return Excel::download(new GeneradoresExport, 'generadores.xlsx');
    }
}
