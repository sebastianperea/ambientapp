<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\posconsumos;
use Illuminate\Support\Facades\DB;


use Laracasts\Flash\Flash;

class Posconsumoscontroller extends Controller
{
  public function index (){

		$posconsumos= posconsumos::orderBy('id','ASC')->paginate(15);
                
		    return view ('comercial.residuos.posconsumos.index')
		    ->with('posconsumos',$posconsumos);
  }



public function create(){

    return view('comercial.residuos.posconsumos.create');

  }





  public function store(Request $request){
   


           $posconsumos= new posconsumos($request->all());
          
           $posconsumos->save();


            Flash::success('El posconsumo ah sido creado con exito');
            return back();


             }


 
    

             
   public function edit($id){

    $posconsumos= posconsumos::find($id);
  
    return view('comercial.residuos.posconsumos.edit')
    ->with('posconsumos',$posconsumos);

      
      

    }  


    public function update(Request $request, $id){
          
        $posconsumos= posconsumos::find($id);
        
         $posconsumos->nombre = $request->nombre;
         $posconsumos->precio = $request->precio;
         $posconsumos->pretratamiento = $request->pretratamiento;
       	 $posconsumos->save();
        Flash::success('El Posconsumo ah sido actualizado con exito');
        return back ();

      

    } 
}
