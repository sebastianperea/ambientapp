<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\cliente;
use App\fotocopiacpn;
use App\rutpn;
use App\cmpn;
use App\rutpj;
use App\cerlpj;
use App\drentapj;
use App\drentapn;
use App\estadosfinancieros;
use App\fotocopiacpj;
use App\cbancariapn;
use App\cbancariapj;
use App\fcreacion;
use App\fresiduos;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\UploadRequest;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;  


class CreacionController extends Controller
{
    
 public function index (Request $request){


    
 
  }


public function create(){

                                     
  }


 public function edit($identificacion){

      $data = Crypt::decrypt($identificacion);
        $clientes= cliente::find($data);
      $fecha = Carbon::now()->format('d-m-Y');

      return view ('publicocliente.creacion_de_clientes')
      ->with('clientes', $clientes)
      ->with('fecha', $fecha);


    }

public function getFile($filename){

  return response()->download(Storage_path('files/comercial/'.$filename),null,[],null);
}

    public function deleteFile($filename){
            
            $cbancariapj = cbancariapj::where('nombre', '=', $filename);
            $cbancariapj->delete();
            $cbancariapn = cbancariapn::where('nombre', '=', $filename);
            $cbancariapn->delete();
            $cerlpj = cerlpj::where('nombre', '=', $filename);
            $cerlpj->delete();
            $cmpn = cmpn::where('nombre', '=', $filename);
            $cmpn->delete();
            $drentapj = drentapj::where('nombre', '=', $filename);
            $drentapj->delete();
            $drentapn = drentapn::where('nombre', '=', $filename);
            $drentapn->delete();
            $estadosfinancieros = estadosfinancieros::where('nombre', '=', $filename);
            $estadosfinancieros->delete();
            $fotocopiacpj = fotocopiacpj::where('nombre', '=', $filename);
            $fotocopiacpj->delete();
            $fotocopiacpn = fotocopiacpn::where('nombre', '=', $filename);
            $fotocopiacpn->delete();
            $rutpj = rutpj::where('nombre', '=', $filename);
            $rutpj->delete();
            $rutpn = rutpn::where('nombre', '=', $filename);
            $rutpn->delete();
            $cc = fcreacion::where('nombre', '=', $filename);
            $cc->delete();
            $inforesiduos = fresiduos::where('nombre', '=', $filename);
            $inforesiduos->delete();

  
 
         \File::delete(Storage_path('files/comercial/'.$filename));
    

      Flash::success("Se ha borrado el archivo de forma existosa");
    return back();
     
     
    }



  public function destroy($id){

  	

  }

  public function success($random){

       Flash::success("Se ha guardado la información de forma exitosa");
    return view('publicocliente.success');

  }

  public function residuos($identificacion){

      $data = Crypt::decrypt($identificacion);
       dd($data);

  }

  public function storecomercial( Request $request, $identificacion){

$path = Storage_path() . '/files/comercial/';
    $cliente = cliente::find($identificacion);


    if($request->tipodepersona == 'natural'){
       $cliente->credito= $request->creditopn;
     }

      elseif ($request->tipodepersona == 'juridica') {
       $cliente->credito= $request->creditopj;
      }


      // Inicio if 1
     if ($request->tipodepersona == 'natural') {

         if (!empty($request->file('fotocopiacpn'))) {

                   $fotocopiacpn = $request->file('fotocopiacpn');

                    $namefpn = 'Ecoindustria_'.time().rand(1,99999).'.'.$fotocopiacpn->getClientOriginalExtension();
                    $path = Storage_path() . '/files/comercial/';

                    $fotocopiacpn->move($path, $namefpn);
                    $fotopn = new fotocopiacpn();
                    $fotopn->nombre = $namefpn;
                    $fotopn->cliente()->associate($cliente);
                    $fotopn->save();
              }      

                  if (!empty($request->file('rutpn'))) {   

                        $rutpn = $request->file('rutpn');
                        $namerpn = 'Ecoindustria_'.time().rand(1,99999).'.'.$rutpn->getClientOriginalExtension();
                        $rutpn->move($path, $namerpn);
                        $rtpn = new rutpn();
                        $rtpn->nombre = $namerpn;
                        $rtpn->cliente()->associate($cliente);
                        $rtpn->save();
                    }    

                    if (!empty($request->file('certificacionbancaria'))){

                    $certificacionbancaria = $request->file('certificacionbancaria');
                    $namecbnpn = 'Ecoindustria_'.time().rand(1,99999).'.'.$certificacionbancaria->getClientOriginalExtension();
                    $certificacionbancaria->move($path, $namecbnpn);
                    $cbancariapn = new cbancariapn();
                    $cbancariapn->nombre = $namecbnpn;
                    $cbancariapn->cliente()->associate($cliente);
                    $cbancariapn->save();
                  }
              

              if (!empty($request->file('certificadodeexistencia'))) {


                $certificadodeexistencia = $request->file('certificadodeexistencia');
                    $namecdepn = 'Ecoindustria_'.time().rand(1,99999).'.'.$certificadodeexistencia->getClientOriginalExtension();
                    $certificadodeexistencia->move($path, $namecdepn);
                    $cex = new cmpn();
                    $cex->nombre = $namecdepn;
                    $cex->cliente()->associate($cliente);
                    $cex->save();
                   }

              if ($request->creditopn == 'si') {
                 if (!empty($request->file('declaracionpn'))) {
                 $declaracionpn = $request->file('declaracionpn');
                    $namedpn = 'Ecoindustria_'.time().rand(1,99999).'.'.$declaracionpn->getClientOriginalExtension();
                    $declaracionpn->move($path, $namedpn);
                    $drentapn= new drentapn();
                    $drentapn->nombre = $namedpn;
                    $drentapn->cliente()->associate($cliente);
                    $drentapn->save();
                    

             }

         }
       
         Flash::success(" Se han guardado los archivos de forma existosa");

    return back();


     }

         elseif ($request->tipodepersona == 'juridica') {

              
               if (!empty($request->file('fotorepl'))) {

                   $fotorepl = $request->file('fotorepl');
                    $namefpj = 'Ecoindustria_'.time().rand(1,99999).'.'.$fotorepl->getClientOriginalExtension();
                    $path = Storage_path() . '/files/comercial/';
                    $fotorepl->move($path, $namefpj);
                    $fotocopiacpj= new fotocopiacpj();
                    $fotocopiacpj->nombre = $namefpj;
                    $fotocopiacpj->cliente()->associate($cliente);
                    $fotocopiacpj->save();
                   }
               
                   if (!empty($request->file('rutpj'))) {

                    $rutpj = $request->file('rutpj');
                    $namerpj = 'Ecoindustria_'.time().rand(1,99999).'.'.$rutpj->getClientOriginalExtension();
                    $rutpj->move($path, $namerpj);
                    $pjrut= new rutpj();
                    $pjrut->nombre = $namerpj;
                    $pjrut->cliente()->associate($cliente);
                    $pjrut->save();

                  }
                        
                   if (!empty($request->file('certificadoexistencia'))) {
                         
                    $certificadoexistencia = $request->file('certificadoexistencia');
                    $namecdepj = 'Ecoindustria_'.time().rand(1,99999).'.'.$certificadoexistencia->getClientOriginalExtension();
                    $certificadoexistencia->move($path, $namecdepj);
                    $cerlpj= new cerlpj();
                    $cerlpj->nombre = $namecdepj;
                    $cerlpj->cliente()->associate($cliente);
                    $cerlpj->save();
                    }

                    if (!empty($request->file('certificacionbancariapj'))) {

                    $certificacionbancariapj = $request->file('certificacionbancariapj');
                    $namecbnpj = 'Ecoindustria_'.time().rand(1,99999).'.'.$certificacionbancariapj->getClientOriginalExtension();
                    $certificacionbancariapj->move($path, $namecbnpj);
                    $cbancariapj= new cbancariapj();
                    $cbancariapj->nombre = $namecbnpj;
                    $cbancariapj->cliente()->associate($cliente);
                    $cbancariapj->save();

                   }


                  
                   if ($request->creditopj == 'si') {


                    if (!empty($request->file('declaracionpj'))) {

                 $declaracionpj = $request->file('declaracionpj');
                    $namedpj = 'Ecoindustria_'.time().rand(1,99999).'.'.$declaracionpj->getClientOriginalExtension();
                    $declaracionpj->move($path, $namedpj);
                    $drentapj= new drentapj();
                    $drentapj->nombre = $namedpj;
                    $drentapj->cliente()->associate($cliente);
                    $drentapj->save();

                    }

                     if (!empty($request->file('estadospj'))) {

                    $estadospj = $request->file('estadospj');
                    $nameestadospj = 'Ecoindustria_'.time().rand(1,99999).'.'.$estadospj->getClientOriginalExtension();
                    $estadospj->move($path, $nameestadospj);
                    $estadopj= new estadosfinancieros();
                    $estadopj->nombre = $nameestadospj;
                    $estadopj->cliente()->associate($cliente);
                    $estadopj->save();
                    
                  }
                  
                }
                  
              }
 

if (count($request->file('creaciondeclientes'))>0 && count($request->file('inforesiduos'))>0 ) {
        
       
    if (count($request->file('creaciondeclientes'))>0) {
      $fcreacion = $request->file('creaciondeclientes');

                    $namefcreacion = 'Ecoindustria_'.time().rand(1,99999).'.'.$fcreacion->getClientOriginalExtension();
                    $path = Storage_path() . '/files/comercial/';

                    $fcreacion->move($path, $namefcreacion);
                    $fcreacion = new fcreacion();
                    $fcreacion->nombre = $namefcreacion;
                    $fcreacion->cliente()->associate($cliente);
                    $fcreacion->save();
    }


    if (count($request->file('inforesiduos'))>0) {
      $fresiduos = $request->file('inforesiduos');

                    $namefresiduos= 'Ecoindustria_'.time().rand(1,99999).'.'.$fresiduos->getClientOriginalExtension();
                    $path = Storage_path() . '/files/comercial/';

                    $fresiduos->move($path, $namefresiduos);
                    $fresiduos= new fresiduos();
                    $fresiduos->nombre = $namefresiduos;
                    $fresiduos->cliente()->associate($cliente);
                    $fresiduos->save();
    }
  Flash::success(" Se han guardado los archivos de forma existosa");
                   return back();
  } 
   
  

     if (count($request->file('creaciondeclientes'))>0 || count($request->file('inforesiduos'))>0 ) {

              if (count($request->file('creaciondeclientes'))>0) {
                $fcreacion = $request->file('creaciondeclientes');

                              $namefcreacion = 'Ecoindustria_'.time().rand(1,99999).'.'.$fcreacion->getClientOriginalExtension();
                              $path = Storage_path() . '/files/comercial/';

                              $fcreacion->move($path, $namefcreacion);
                              $fcreacion = new fcreacion();
                              $fcreacion->nombre = $namefcreacion;
                              $fcreacion->cliente()->associate($cliente);
                              $fcreacion->save();
              }


              if (count($request->file('inforesiduos'))>0) {
                $fresiduos = $request->file('inforesiduos');

                              $namefresiduos= 'Ecoindustria_'.time().rand(1,99999).'.'.$fresiduos->getClientOriginalExtension();
                              $path = Storage_path() . '/files/comercial/';

                              $fresiduos->move($path, $namefresiduos);
                              $fresiduos= new fresiduos();
                              $fresiduos->nombre = $namefresiduos;
                              $fresiduos->cliente()->associate($cliente);
                              $fresiduos->save();
              }

                  Flash::success(" Se han guardado los archivos de forma existosa");

              return back();
            }

            else{

         Flash::success(" Se han guardado los archivos de forma existosa");
              return back();
    }   



          
  }

  public function update( UploadRequest $request, $identificacion){


  $cliente = cliente::find($identificacion);
  $cliente->tipo = $request->tipo;
  $cliente->nombres = $request->nombres;
  $cliente->apellidos = $request->apellidos;
  $cliente->email = $request->email;
  $cliente->cargo = $request->cargo;
  
  $cliente->telefonocelular = $request->telefonocelular;
  $cliente->nombrecartera = $request->nombrecartera;
  $cliente->telefonofijocartera =$request->telefonofijocartera;
  $cliente->extcartera =$request->extcartera;
  $cliente->telefonomovilcartera =$request->telefonomovilcartera;
  $cliente->emailcartera =$request->emailcartera;
  $cliente->direccionradicacion =$request->direccionradicacion;
  $cliente->nombreempresauno =$request->nombreempresauno;
  $cliente->telefonoempresauno =$request->telefonoempresauno;
  $cliente->nombreempresados =$request->nombreempresados;
  $cliente->telefonoempresados = $request->telefonoempresados;
  $cliente->nombrebanco = $request->nombrebanco;
  $cliente->numerodecuenta = $request->numerodecuenta;
  $cliente->tipodecuenta = $request->tipodecuenta;
  $cliente->tipodepersona= $request->tipodepersona;

  if($request->tipodepersona == 'natural'){
   $cliente->credito= $request->creditopn;
 }

  elseif ($request->tipodepersona == 'juridica') {
   $cliente->credito= $request->creditopj;
  }
  
  
  $cliente->politicadeprivacidad =$request->politicadeprivacidad;

  $cliente->save();


// Inicio if 1
     if ($request->tipodepersona == 'natural') {

     

         if (!empty($request->file('fotocopiacpn')) && !empty($request->file('rutpn'))  && !empty($request->file('certificacionbancaria'))) {

                   $fotocopiacpn = $request->file('fotocopiacpn');

                    $namefpn = 'Ecoindustria_'.time().rand(1,99999).'.'.$fotocopiacpn->getClientOriginalExtension();
                    $path = Storage_path() . '/files/comercial/';

                    $fotocopiacpn->move($path, $namefpn);
                    $fotopn = new fotocopiacpn();
                    $fotopn->nombre = $namefpn;
                    $fotopn->cliente()->associate($cliente);
                    $fotopn->save();


                    $rutpn = $request->file('rutpn');
                    $namerpn = 'Ecoindustria_'.time().rand(1,99999).'.'.$rutpn->getClientOriginalExtension();
                    $rutpn->move($path, $namerpn);
                    $rtpn = new rutpn();
                    $rtpn->nombre = $namerpn;
                    $rtpn->cliente()->associate($cliente);
                    $rtpn->save();

                    $certificacionbancaria = $request->file('certificacionbancaria');
                    $namecbnpn = 'Ecoindustria_'.time().rand(1,99999).'.'.$certificacionbancaria->getClientOriginalExtension();
                    $certificacionbancaria->move($path, $namecbnpn);
                    $cbancariapn = new cbancariapn();
                    $cbancariapn->nombre = $namecbnpn;
                    $cbancariapn->cliente()->associate($cliente);
                    $cbancariapn->save();

              }

              if (!empty($request->file('certificadodeexistencia'))) {


                $certificadodeexistencia = $request->file('certificadodeexistencia');
                    $namecdepn = 'Ecoindustria_'.time().rand(1,99999).'.'.$certificadodeexistencia->getClientOriginalExtension();
                    $certificadodeexistencia->move($path, $namecdepn);
                    $cex = new cmpn();
                    $cex->nombre = $namecdepn;
                    $cex->cliente()->associate($cliente);
                    $cex->save();
                   }

              if ($request->creditopn == 'si') {
                 if (!empty($request->file('declaracionpn'))) {
                 $declaracionpn = $request->file('declaracionpn');
                    $namedpn = 'Ecoindustria_'.time().rand(1,99999).'.'.$declaracionpn->getClientOriginalExtension();
                    $declaracionpn->move($path, $namedpn);
                    $drentapn= new drentapn();
                    $drentapn->nombre = $namedpn;
                    $drentapn->cliente()->associate($cliente);
                    $drentapn->save();
                    

             }

         }
       
      
  $random = str_random(5);


    return redirect()->route('creacion',$random);
     }

         elseif ($request->tipodepersona == 'juridica') {

              
               if (!empty($request->file('fotorepl')) && !empty($request->file('rutpj')) && !empty($request->file('certificadoexistencia')) && !empty($request->file('certificacionbancariapj'))) {

                   $fotorepl = $request->file('fotorepl');
                    $namefpj = 'Ecoindustria_'.time().rand(1,99999).'.'.$fotorepl->getClientOriginalExtension();
                    $path = Storage_path() . '/files/comercial/';
                    $fotorepl->move($path, $namefpj);
                    $fotocopiacpj= new fotocopiacpj();
                    $fotocopiacpj->nombre = $namefpj;
                    $fotocopiacpj->cliente()->associate($cliente);
                    $fotocopiacpj->save();

                    $rutpj = $request->file('rutpj');
                    $namerpj = 'Ecoindustria_'.time().rand(1,99999).'.'.$rutpj->getClientOriginalExtension();
                    $rutpj->move($path, $namerpj);
                    $pjrut= new rutpj();
                    $pjrut->nombre = $namerpj;
                    $pjrut->cliente()->associate($cliente);
                    $pjrut->save();

                    $certificadoexistencia = $request->file('certificadoexistencia');
                    $namecdepj = 'Ecoindustria_'.time().rand(1,99999).'.'.$certificadoexistencia->getClientOriginalExtension();
                    $certificadoexistencia->move($path, $namecdepj);
                    $cerlpj= new cerlpj();
                    $cerlpj->nombre = $namecdepj;
                    $cerlpj->cliente()->associate($cliente);
                    $cerlpj->save();

                    $certificacionbancariapj = $request->file('certificacionbancariapj');
                    $namecbnpj = 'Ecoindustria_'.time().rand(1,99999).'.'.$certificacionbancariapj->getClientOriginalExtension();
                    $certificacionbancariapj->move($path, $namecbnpj);
                    $cbancariapj= new cbancariapj();
                    $cbancariapj->nombre = $namecbnpj;
                    $cbancariapj->cliente()->associate($cliente);
                    $cbancariapj->save();

                  }

                   if ($request->creditopj == 'si') {


                    if (!empty($request->file('declaracionpj')) && !empty($request->file('estadospj'))) {

                 $declaracionpj = $request->file('declaracionpj');
                    $namedpj = 'Ecoindustria_'.time().rand(1,99999).'.'.$declaracionpj->getClientOriginalExtension();
                    $declaracionpj->move($path, $namedpj);
                    $drentapj= new drentapj();
                    $drentapj->nombre = $namedpj;
                    $drentapj->cliente()->associate($cliente);
                    $drentapj->save();

                    $estadospj = $request->file('estadospj');
                    $nameestadospj = 'Ecoindustria_'.time().rand(1,99999).'.'.$estadospj->getClientOriginalExtension();
                    $estadospj->move($path, $nameestadospj);
                    $estadopj= new estadosfinancieros();
                    $estadopj->nombre = $nameestadospj;
                    $estadopj->cliente()->associate($cliente);
                    $estadopj->save();
                    
                  }
                }

                $random = str_random(5);


     return redirect()->route('creacion',$random);
              }

          

}

}   



