<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Laracasts\Flash\Flash;
use Illuminate\Support\Collection as Collection;
use Carbon\Carbon;
use App\oferta;
use App\transporte;
use App\peligroso;
use App\aprovechables;
use App\posconsumos;
use App\peligrososa;
use App\especiales;
use App\otros;
use App\cliente;
use App\pys;
use App\sedes;
use App\residuos;
use App\inforesiduos;
use App\filespeligrosos;
use App\filespeligrososa;
use App\filese;
use App\dispositores;
class ResiduosController extends Controller
{
    
    public function index (){

      $peligrosos= peligroso::orderBy('id','ASC')->paginate(5);

      $dispositores= dispositores::orderBy('id','ASC')->paginate(5);


      $dispositoressel= dispositores::orderBy('id','ASC')->pluck('nombre', 'id');

      


      return view ('comercial.residuos.index')
      ->with('peligrosos', $peligrosos)
      ->with('dispositores', $dispositores)
      ->with('dispositoressel', $dispositoressel);
		
  } 

  public function create($id){

    

  }

  public function success($random){

       Flash::success("Se ha guardado la información de forma exitosa");
    return view('publicocliente.successinfo');

  }

  public function store(Request $request){

        


             }


 
   public function edit($identificacion){

$data = Crypt::decrypt($identificacion);
		$clientes = cliente::find($data);
    $fecha = Carbon::now()->format('d-m-Y');



    $ofi  = oferta::with('peligroso','peligrososa','especiales')->where('identificacion','=',$data)->get();
 
 
    $peligroso = array_pluck($ofi, 'peligroso');
    $peligrososa = array_pluck($ofi, 'peligrososa');
   $especiales = array_pluck($ofi,'especiales');



 
		return view('comercial.residuos.informacionresiduos')
		->with('clientes',$clientes)
    ->with('peligroso',$peligroso)
    ->with('fecha', $fecha)
    ->with('peligrososa', $peligrososa)
    ->with('especiales', $especiales);
      

    } 

    public function getFile($filename){

  return response()->download(Storage_path('files/comercial/'.$filename),null,[],null);
}

    public function deleteFile($filename){


            
            $filespeligrosos = filespeligrosos::where('nombre', '=', $filename)->first();
            $filespeligrososa = filespeligrososa::where('nombre', '=', $filename)->first();
            $filese = filese::where('nombre', '=', $filename)->first();
                  if (count($filespeligrosos)>0) {
                      $filespeligrosos->residuos()->detach();

                      $filespeligrosos->delete();
                      \File::delete(Storage_path('files/comercial/'.$filename));
                  Flash::success("Se ha borrado el archivo de forma existosa");

                }

                  elseif (count($filespeligrososa)>0) {
                      $filespeligrososa->residuos()->detach();

                      $filespeligrososa->delete();
                      \File::delete(Storage_path('files/comercial/'.$filename));
                  Flash::success("Se ha borrado el archivo de forma existosa");

                }

                elseif (count($filese)>0) {
                      $filese->residuos()->detach();

                      $filese->delete();
                      \File::delete(Storage_path('files/comercial/'.$filename));
                  Flash::success("Se ha borrado el archivo de forma existosa");

                }

                else {
                  Flash::warning(" No se ha borrado el archivo de forma existosa");

                }

            
 
         
    

      
    return back();
     
     
    } 


    public function update(Request $request, $identificacion){
                       
                  
  



      // Residuos
        

      $residuos = new residuos;
      
      $residuos->identificacion = $request->identificacion;
         $residuos->save();

      // Sedes
       $num_elements = 0;
        $sqlData = []; 
        $input= $request->all();
              while($num_elements < count($input['nombre'])){
                $sqlData[] = array(
                    'nombre'         => $input['nombre'][$num_elements],
                    'identificacion' => $identificacion,
                    'direccion'          => $input['direccion'][$num_elements],
                    'departamento'     => $input['departamento'][$num_elements],
                    'ciudad'      => $input['ciudad'][$num_elements],

                    'created_at'    => Carbon::now(), // only if your table has this column
                    'updated_at'    => Carbon::now(), // only if your table has this column
                );
              $num_elements++;
          }

      sedes::insert($sqlData);



      $num_elements2 = 0;
        $sqlData2 = []; 
        $input= $request->all();
              while($num_elements2 < count($input['nombrer'])){
                $sqlData2[] = array(

                    'idresiduo'         => $input['idresiduo'][$num_elements2],
                    'nombreresiduo'         => $input['nombrer'][$num_elements2],
                    // 'tipo'          => $input['tipo'][$num_elements2],
                    'detalle'     => $input['detalle'][$num_elements2],
                    'estado'      => $input['estado_'][$num_elements2],
                    'proceso'      => $input['proceso'][$num_elements2],
                    'contaminacion'      => $input['contaminacion'][$num_elements2],
                    'empaque'      => $input['empaque'][$num_elements2],
                    'cantidad'      => $input['cantidad'][$num_elements2],
                    'inforesiduos' => $residuos->id,
                    'identificacion' => $request->identificacion,
                    'tipo' => $input['tipo'][$num_elements2],
                    'created_at'    => Carbon::now(), // only if your table has this column
                    'updated_at'    => Carbon::now(), // only if your table has this column
                );
              $num_elements2++;
          }
         
     $inforesiduos= inforesiduos::insert($sqlData2);
     $lastIds = inforesiduos::orderBy('id', 'desc')->take(count($sqlData2))->get();




  



      if (!empty($request->file('peligroso'))) {

                     $files = $request->file();


                     $peligroso = $request->file('peligroso');

                     foreach ($peligroso as $peligro) {
                       $namefpn = 'Ecoindustria_'.time().rand(1,99999).'.'.$peligro->getClientOriginalExtension();
                       $path = Storage_path() . '/files/comercial/';
                       $peligro->move($path, $namefpn);
                       
                       $peligrosos = new filespeligrosos();
                       $peligrosos->nombre = $namefpn;
                       $peligrosos->save();
                        $peligrosos->residuos()->sync($residuos);
                      
                     }

        }


         if (!empty($request->file('peligrososa'))) {

           $peligrosoa = $request->file('peligrososa');



                     foreach ($peligrosoa as $peligroa) {
                       $namepa = 'Ecoindustria_'.time().rand(1,99999).'.'.$peligroa->getClientOriginalExtension();
                       $path = Storage_path() . '/files/comercial/';
                       $peligroa->move($path, $namepa);
                       
                       $peligrososa = new filespeligrososa();
                       $peligrososa->nombre = $namepa;
                       $peligrososa->save();
                        $peligrososa->residuos()->sync($residuos);
                      
                     }

                      

        }



          if (!empty($request->file('especiales'))) {

          $especiales = $request->file('especiales');



                     foreach ($especiales as $especial) {
                       $namee = 'Ecoindustria_'.time().rand(1,99999).'.'.$especial->getClientOriginalExtension();
                       $path = Storage_path() . '/files/comercial/';
                       $especial->move($path, $namee);
                       
                       $especial = new filese();
                       $especial->nombre = $namee;
                       $especial->save();
                        $especial->residuos()->sync($residuos);
                      
                     }
        
      }
      $random = str_random(5);


                      return redirect()->route('informacion',$random);
                      
    }

  } 

 
