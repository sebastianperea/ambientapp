<?php

namespace App\Http\Middleware;

use Closure;

class posconsumos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $usuario_actual = \Auth::user();
     

        if ($usuario_actual->type === 'posconsumos') {
          
                return $next($request); 
        }

        else{
         dd('No tienes permisos para acceder a este directorio');
        }
        return $next($request);
    }
}
