<?php

namespace App\Http\Middleware;

use Closure;

class clientes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $usuario_actual = \Auth::user();
        $id= $request->identificacion;

        if ($usuario_actual->type === 'cliente' &&  $usuario_actual->identificacion === $id) {
          
                return $next($request); 
        }

        else{
         dd('No tienes permisos para acceder a este directorio');
        }
    }
}
