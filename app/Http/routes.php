<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/', ,  function () {
    
    return view('auth.login', true);

});

Route::group(['prefix' => 'admin' ,'middleware' => 'auth'], function () {
    
   Route::resource('users','UsersController');
   Route::get('users/{id}/destroy', 
   	[ 'uses' => 'UsersController@destroy',
   	'as' => 'admin.users.destroy']);
});






Route::resource('comercial','ComercialController', ['except' => ['show']]);

 Route::resource('solicitud','SolicitudController');


   Route::group(['prefix' => 'comercial','middleware' => 'auth'], function () {
 $exitCode = Artisan::call('cache:clear');

    Route::resource('ofertasposiblesclientes','Ofertaspccontroller');
   Route::resource('creacion_de_clientes','CreacionController');
   
   Route::resource('ofertas','Ofertascontroller',['except' => ['show']]);
   Route::get('comercial/{id}/destroy', 
   	[ 'uses' => 'Ofertascontroller@destroy',
   	'as' => 'Comercial.ofertas.destroy']);
   Route::resource('clientes','ClientesController',['except' => ['show']]);

   


  



});

// Route::group(['prefix' => 'comercial' ,'middleware' => 'auth'], function () {
    
//    Route::resource('clientes','ClientesController');
//    Route::get('clientes/{identificacion}/destroy', 
//       [ 'uses' => 'ClientesController@destroy',
//       'as' => 'comercial.clientes.destroy']);
// });



    


Route::auth();

Route::get('/home', 'HomeController@index');

Route::filter('forcehttps', function()
{
  if(!Request::secure())
  {
    return Redirect::secure(Request::getRequestUri(),301);
  }
});