<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class oferta extends Model
{
  
  protected $table="ofertas";
   protected $fillable= ['identificacion','Estado','estadoenv','fechaenv','observacion','user_id','sedes_id','fechaven']; 
    //

public function otros()
   {

      return $this->hasMany('App\otros','oferta_id');

   }
 

   public function user()
   {

return $this-> hasMany('App\User');
   }

   public function cliente()
   {

return $this->hasMany('App\cliente');
   }

   public function productoyservicio(){

   	return $this->belongsToMany('App\productoyservicio');
   }

   public function peligroso()
   {

return $this->belongsToMany('App\peligroso')->withPivot('cantidad','precio');

   }  

   public function posconsumos()
   {

return $this->belongsToMany('App\posconsumos')->withPivot('cantidad','precio');

   } 


   public function especiales()
   {

return $this->belongsToMany('App\especiales')->withPivot('cantidad','precio');

   }

    public function aprovechables()
   {

return $this->belongsToMany('App\aprovechables')->withPivot('cantidad','precio');

   }


   public function peligrososa()
   {

return $this->belongsToMany('App\peligrososa')->withPivot('cantidad','precio');

   }


   public function transporte()
   {

return $this->belongsToMany('App\transporte');
   }

   public function pys()
   {

return $this->belongsToMany('App\pys');
   }

   public function sedes()
   {

return $this->belongsToMany('App\sedes');
   }
}

