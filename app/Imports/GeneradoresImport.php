<?php

namespace App\Imports;

use App\generadores;
use Maatwebsite\Excel\Concerns\ToModel;

class GeneradoresImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
        return new generadores([
           'razon_social'     => $row[0],
            'direccion'    => $row[1],
            'telefono'    => $row[2], 
            'telefono2'    => $row[3], 
            'responsable_envio'    => $row[4], 
            'correo_electronico'    => $row[5],
            'correo2' => $row[6],
            'ciudad' => $row[7],
            'departamento' => $row[8],
            'identificacion' => $row[9],
            'dv' => $row[10]
            
        ]);
    }
}
