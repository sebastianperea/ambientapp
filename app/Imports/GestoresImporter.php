<?php

namespace App\Imports;

use App\gestores;
use Maatwebsite\Excel\Concerns\ToModel;

class GestoresImporter implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        

        return new gestores([
           'razonsocial'     => $row[0],
            'direccion'    => $row[1],
            'telefono'    => $row[2], 
            'telefono2op'    => $row[3], 
            'identificacion'    => $row[4], 
            'dv'    => $row[5],
            'contacto_gestor' => $row[6],
            'correo' => $row[7],
            'correo2op' => $row[8],
            'ciudad' => $row[9],
            'departamento' => $row[10],
            'centro_de_acopio' => $row[11]
            
        ]);
    }
}
