<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class filescert extends Model
{
     protected $primaryKey = 'id';
     protected $table="filescerts"; 
    protected $fillable= ['nombre','categoria','remision_id'];
}
