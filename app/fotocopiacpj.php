<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fotocopiacpj extends Model
{
    protected $table="fotocopiacpj";
   protected $fillable= ['nombre','cliente_id'];

   public function cliente()
   {

return $this->belongsTo('App\cliente','cliente_id');

   }
}
