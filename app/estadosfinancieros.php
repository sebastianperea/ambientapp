<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estadosfinancieros extends Model
{
    protected $table="estadosfinancieros";
   protected $fillable= ['nombre', 'cliente_id'];

   public function cliente()
   {

return $this->belongsTo('App\cliente','cliente_id');

   }
}
