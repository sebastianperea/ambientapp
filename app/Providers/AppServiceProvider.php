<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

//     if ($this->app->environment() == 'production') {
//     URL::forceScheme('https');
// }
    public function boot()
    {
       \Carbon\Carbon::setlocale(config('app.locale'));
       
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {


       $this->app->bind('path.public', function() {
          return base_path().'/public';
        });
    }
}
