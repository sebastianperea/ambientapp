<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class GeneradoresExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return generadores::all();
    }
}
