<?php

namespace App\Exports;

use App\gestores;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;


class GestoresExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */


    public function collection()
    {


        return gestores::all();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Razón Social',
            'Dirección',
            'Télefono',
            'Telefono opcional',
            'Identificacion',
            'DV',
            'Contacto Gestor',
            'Correo',
            'Correo Opcional',
            'Ciudad',
            'Departamento',
            'Centro de acopio',
            'Fecha y hora de creación',
            'Fecha y hora de actualización',
        ];
    }
}
