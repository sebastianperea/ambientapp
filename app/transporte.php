<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transporte extends Model
{
    protected $table="transporte";
   protected $fillable= ['id','tipo','preciokg','preciog'];

   public function oferta()
   {
return $this-> hasMany('App\oferta');

   }

   public function remision()
   {
	return $this->belongsToMany('App\remision')->withPivot('cantidadt');
   }
}
