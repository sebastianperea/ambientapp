<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class visitas extends Model
{

	protected $table="visitas"; 
    protected $fillable= ['id', 'tipo_de_cliente','tipo_de_proceso_comercial','canald','lugar', 'identificacion','id_user','observaciones','fecha_nuevo_contacto', 'ESTADO'];

}
